package tests;

import java.util.Scanner;
import model.Editore;
import model.Luogo;

/**
 * @author Simone Tugnetti
 * Gestione di una Biblioteca
 */
public class TestEditore {

    public static void main(String[] args) {
	// Test Editore
		
	Scanner tastiera=new Scanner(System.in);
	Editore e1=new Editore();
	Luogo ubicazione=new Luogo();
	System.out.println("Inserisci la ragione sociale!");
	String ragioneS=tastiera.nextLine();
	e1.setRagioneS(ragioneS);
	System.out.println("Inserisci l'indirizzo di operativita'!");
	ubicazione.setIndirizzo(tastiera.nextLine());
	System.out.println("Inserisci il CAP!");
	try {
            ubicazione.setCap(tastiera.nextLine());
	}catch(Exception e) {
            System.out.println(e.getMessage());
	}
	System.out.println("Inserisci la citt� di operativita'!");
	ubicazione.setCitta(tastiera.nextLine());
	System.out.println("Inserisci la provincia!");
	ubicazione.setProvincia(tastiera.nextLine());
	e1.setUbicazione(ubicazione);
	System.out.println("Inserisci il numero di telefono!");
	String telefono=tastiera.nextLine();
	try {
            e1.setTelefono(telefono);
	}catch(Exception e) {
            System.out.println(e.getMessage());
	}
        System.out.println("Inserisci l'indirizzo del sito Web ufficiale!");
	String sitoWeb=tastiera.nextLine();
	e1.setSitoWeb(sitoWeb);
	System.out.println("Inserisci l'indirizzo e-mail ufficiale!");
	String mail=tastiera.nextLine();
	e1.setMail(mail);
		
	System.out.println("\n"+e1);
	System.out.println("\n"+e1.stampaDettaglio());
			
	Editore e2=new Editore(ragioneS,ubicazione,telefono,sitoWeb,mail);
	System.out.println("\n"+e2);
	System.out.println("\n"+e2.stampaDettaglio());
	tastiera.close();

    }

}
