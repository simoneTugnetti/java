package tests;

import java.util.Scanner;
import model.ISBN;

/**
 * @author Simone Tugnetti
 * Gestione di una Biblioteca
 */
public class TestISBN {

    public static void main(String[] args) {
        // Test ISBN
		
	ISBN isbn=new ISBN();
	Scanner tastiera=new Scanner(System.in);
		
	System.out.println("Inserisci il codice ISBN!");
	String codice=tastiera.nextLine();
	try {
            isbn.setCodice(codice);
	}catch(Exception e) {
            System.out.println(e.getMessage());
	}
	System.out.println(isbn);
		
	ISBN isbn1=new ISBN(codice);
	System.out.println(isbn1);
	tastiera.close();
    }

}
