package tests;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import model.Autore;
import model.Data;

/**
 * @author Simone Tugnetti
 */
public class TestAutore {

    public static void main(String[] args) throws Exception{
	// Test Autore
	
        InputStreamReader input=new InputStreamReader(System.in);
	BufferedReader tastiera=new BufferedReader(input);
	Autore a1=new Autore();
	Data dataMorte=null,dataNascita;
	String luogoMorte=null,nome,cognome,luogoNascita,risp;
	int giorno,mese,annon,annom;
	System.out.println("Inserisci il nome dell'autore!");
	nome=tastiera.readLine();
	a1.setNome(nome);
	System.out.println("Inserisci il cognome dell'autore!");
	cognome=tastiera.readLine();
	a1.setCognome(cognome);
	System.out.println("Inserisci la data di nascita (Invio dopo giorno-mese-anno)");
	giorno=Integer.parseInt(tastiera.readLine());
	mese=Integer.parseInt(tastiera.readLine());
	annon=Integer.parseInt(tastiera.readLine());
	dataNascita=new Data(giorno,mese,annon);
	a1.setDataNascita(dataNascita);
	System.out.println("Inserisci il luogo di nascita!");
	luogoNascita=tastiera.readLine();
	a1.setLuogoNascita(luogoNascita);
	System.out.println("E' ancora vivo? (si - no)");
	risp=tastiera.readLine();
	while(!risp.equals("si") && !risp.equals("no")) {
		System.out.println("Risposta non valida, riprova!");
		risp=tastiera.readLine();
	}
	if(risp.equals("no")) {
            System.out.println("Inserisci la data di morte (Invio dopo giorno-mese-anno)");
            giorno=Integer.parseInt(tastiera.readLine());
            mese=Integer.parseInt(tastiera.readLine());
            annom=Integer.parseInt(tastiera.readLine());
            while(annom<annon) {
                System.out.println("Anno errato, riprovare!");
                annom=Integer.parseInt(tastiera.readLine());
            }
            dataMorte=new Data(giorno,mese,annom);
            a1.setDataMorte(dataMorte);
            System.out.println("Inserisci il luogo di morte!");
            luogoMorte=tastiera.readLine();
            a1.setLuogoMorte(luogoMorte);
	}
		
	System.out.println("\n"+a1);
	System.out.println("\n"+a1.stampaDettaglio());
		
	Autore a2=new Autore(nome,cognome,dataNascita,dataMorte,luogoNascita,luogoMorte);
		
	System.out.println("\n"+a2);
	System.out.println("\n"+a2.stampaDettaglio());
	tastiera.close();
    }

}
