package tests;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import model.Autore;
import model.Data;
import model.Editore;
import model.ISBN;
import model.Libro;
import model.Luogo;

/**
 * @author Simone Tugnetti
 * Gestione di una Biblioteca
 */
public class TestLibro {

    public static void main(String[] args) throws Exception {
	// Test Libro
		
	InputStreamReader input=new InputStreamReader(System.in);
	BufferedReader tastiera=new BufferedReader(input);
	Libro l1=new Libro();
	Luogo ubicazione=new Luogo();
	Editore e1=new Editore();
	Autore[] a1;
	Data dataMorte=null;
	String luogoMorte=null;
	int giorno,mese,annon = 0,annom,annop;
	
	System.out.println("\nInserisci il titolo del libro!");
	String titolo=tastiera.readLine();
	l1.setTitolo(titolo);

	System.out.println("\nDati dell'autore!");
	System.out.println("Quanti sono gli autori?");
	a1=new Autore[Integer.parseInt(tastiera.readLine())];
	for(int i=0;i<a1.length;i++) {
            a1[i]=new Autore();
        }
	for(int i=0;i<a1.length;i++) {
            System.out.println("\nInserisci il nome dell'autore!");
            a1[i].setNome(tastiera.readLine());
            System.out.println("Inserisci il cognome dell'autore!");
            a1[i].setCognome(tastiera.readLine());
            System.out.println("Inserisci la data di nascita (Invio dopo giorno-mese-anno)");
            giorno=Integer.parseInt(tastiera.readLine());
            mese=Integer.parseInt(tastiera.readLine());
            annon=Integer.parseInt(tastiera.readLine());
            a1[i].setDataNascita(new Data(giorno,mese,annon));
            System.out.println("Inserisci il luogo di nascita!");
            a1[i].setLuogoNascita(tastiera.readLine());
            System.out.println("E' ancora vivo? (si - no)");
            String risp=tastiera.readLine();
            while(!risp.equals("si") && !risp.equals("no")) {
		System.out.println("Risposta non valida, riprova!");
		risp=tastiera.readLine();
            }
            if(risp.equals("no")) {
		System.out.println("Inserisci la data di morte (Invio dopo giorno-mese-anno)");
		giorno=Integer.parseInt(tastiera.readLine());
		mese=Integer.parseInt(tastiera.readLine());
                annom=Integer.parseInt(tastiera.readLine());
		while(annom<annon) {
                    System.out.println("Anno errato, riprovare!");
                    annom=Integer.parseInt(tastiera.readLine());
		}
		dataMorte=new Data(giorno,mese,annom);
		a1[i].setDataMorte(dataMorte);
		System.out.println("Inserisci il luogo di morte!");
		luogoMorte=tastiera.readLine();
		a1[i].setLuogoMorte(luogoMorte);
            }
	}
	l1.setAutore(a1);
		
        System.out.println("\nDati dell'editore!");
	System.out.println("Inserisci la ragione sociale!");
	e1.setRagioneS(tastiera.readLine());
	System.out.println("Inserisci l'indirizzo di operativita'!");
	ubicazione.setIndirizzo(tastiera.readLine());
	System.out.println("Inserisci il CAP!");
	ubicazione.setCap(tastiera.readLine());
	System.out.println("Inserisci la citta' di operativita'!");
	ubicazione.setCitta(tastiera.readLine());
	System.out.println("Inserisci la provincia!");
	ubicazione.setProvincia(tastiera.readLine());
	e1.setUbicazione(ubicazione);
	System.out.println("Inserisci il numero di telefono!");
	try {
		e1.setTelefono(tastiera.readLine());
	}catch(Exception e) {
		System.out.println(e.getMessage());
	}
	System.out.println("Inserisci l'indirizzo del sito Web ufficiale!");
	e1.setSitoWeb(tastiera.readLine());
	System.out.println("Inserisci l'indirizzo e-mail ufficiale!");
	e1.setMail(tastiera.readLine());
	l1.setEditore(e1);
		
	System.out.println("\nInserisci l'ISBN!");
	ISBN isbn=new ISBN(tastiera.readLine());
	l1.setIsbn(isbn);
		
	System.out.println("\nInserisci il numero di pagine!");
	int pagine=Integer.parseInt(tastiera.readLine());
	l1.setPagine(pagine);
		
	System.out.println("\nInserisci la data di pubblicazione (Invio dopo giorno-mese-anno)");
	giorno=Integer.parseInt(tastiera.readLine());
	mese=Integer.parseInt(tastiera.readLine());
	annop=Integer.parseInt(tastiera.readLine());
	while(annop<annon) {
            System.out.println("Anno errato, riprovare!");
            annop=Integer.parseInt(tastiera.readLine());
        }
	Data pubblico=new Data(giorno,mese,annop);
	l1.setDataPubblicazione(pubblico);
		
	System.out.println("\nInserisci la lingua di scrittura!");
	String lingua=tastiera.readLine();
	l1.setLingua(lingua);
		
	System.out.println(l1);
	System.out.println(l1.stampaDettaglio());
		
	Libro l2=new Libro(titolo,a1,e1,isbn,pagine,pubblico,lingua);
		
	System.out.println(l2);
	System.out.println(l2.stampaDettaglio());
		
    }

}
