package model;

/**
 * @author Simone Tugnetti
 */
public class Luogo extends Editore{
	
    private String indirizzo;
    private String cap;
    private String citta;
    private String provincia;
	
    public Luogo() {}

    public Luogo(String indirizzo, String cap, String citta, String provincia) {
	this.indirizzo = indirizzo;
	try {
            this.setCap(cap);
	}catch(Exception e) {
            cap=e.getMessage();
	}
	this.citta = citta;
	this.provincia = provincia;
    }

    public String getIndirizzo() {
	return indirizzo;
    }

    public void setIndirizzo(String indirizzo) {
	this.indirizzo = indirizzo;
    }

    public String getCap() {
	return cap;
    }

    public void setCap(String cap) throws Exception{
	if(cap.length()>5) {
            throw new Exception("Formato CAP non valido!");
	}else {
            this.cap = cap;
	}
    }

    public String getCitta() {
	return citta;
    }

    public void setCitta(String citta) {
	this.citta = citta;
    }

    public String getProvincia() {
	return provincia;
    }

    public void setProvincia(String provincia) {
	this.provincia = provincia;
    }

	
    @Override
    public String toString() {
	return "Ubicazione --> [" 
                + (indirizzo != null ? "indirizzo = " + indirizzo + ", " : "")
		+ (cap != null ? "cap = " + cap + ", " : "") 
                + (citta != null ? "citta = " + citta + ", " : "")
		+ (provincia != null ? "provincia = " + provincia : "")+"]";
    }
	
}
