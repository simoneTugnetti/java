package model;

/**
 * @author Simone Tugnetti
 */
public class ISBN{
	
    private String codice;

    public ISBN() {}

    public ISBN(String codice){
	try {
            this.setCodice(codice);
	}catch(Exception e) {
            this.codice=e.getMessage();
	}
    }

    public String getCodice() {
	return codice;
    }

    public void setCodice(String codice) throws Exception{
	if(codice.length()>13) {
            throw new Exception("Formato non valido!");
	}else {
            this.codice=codice;
	}
    }

    @Override
    public String toString() {
	return "ISBN --> [" 
                + (codice != null ? "codice=" + codice : "Non specificato")+"]";
    }

}
