package model;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * @author Simone Tugnetti
 */
public class Data {

    //Gestione di una data
    private int giorno;
    private int mese;
    private int anno;
    private GregorianCalendar cal=new GregorianCalendar();
	
    public Data() {}
	
    public Data(int giorno, int mese, int anno) {
	this.giorno = giorno;
	this.mese = mese;
	this.anno = anno;
    }

    public int getGiorno() {
	return giorno;
    }

    public void setGiorno(int giorno) {
	this.giorno = giorno;
    }

    public int getMese() {
	return mese;
    }

    public void setMese(int mese) {
	this.mese = mese;
    }

    public int getAnno() {
	return anno;
    }

    public void setAnno(int anno) {
	this.anno = anno;
    }
	
    private boolean mesiSpecifici() {
        return ((mese==4 || mese==6 || mese==9 || mese==11) && giorno>30) ||
                (mese==2 && giorno>28);
    }
	
    public String stampaLineare() {
	if((giorno>31 || giorno<1) || (mese<1 || mese>12) || 
                (anno>cal.get(Calendar.YEAR)) || mesiSpecifici()) {
            return "Non definita!";
	}else {
            return giorno+"/"+mese+"/"+anno;
	}
    }
	
    public String stampaLetterale() {
	if((giorno>31 || giorno<1) || (mese<1 || mese>12) || 
                (anno>cal.get(Calendar.YEAR)) || mesiSpecifici()) {
            return "Non definita!";
	}else {
            String[] mesi= {"Gennaio","Febbraio","Marzo","Aprile","Maggio",
                "Giugno","Luglio","Agosto","Settembre","Ottobre","Novembre",
                "Dicembre"};
            return giorno+"/"+mesi[mese-1]+"/"+anno;
	}
    }

    @Override
    public String toString() {
	return "Data --> [giorno = " + giorno 
			+ ", mese = " + mese 
			+ ", anno = " + anno 
			+ ", stampaLineare() = " + stampaLineare()
			+ ", stampaLetterale() = " + stampaLetterale()+"]";
    }
	
}
