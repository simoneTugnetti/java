package model;

/**
 * @author Simone Tugnetti
 */
public class Autore {

    private String nome;
    private String cognome;
    private Data dataNascita;
    private Data dataMorte;
    private String luogoNascita;
    private String luogoMorte;

    public Autore() {
    }

    public Autore(String nome, String cognome, Data dataNascita, Data dataMorte,
            String luogoNascita, String luogoMorte) {
        this.nome = nome;
        this.cognome = cognome;
        this.dataNascita = dataNascita;
        this.dataMorte = dataMorte;
        this.luogoNascita = luogoNascita;
        this.luogoMorte = luogoMorte;
    }

    public Data getDataNascita() {
        return dataNascita;
    }

    public void setDataNascita(Data dataNascita) {
        this.dataNascita = dataNascita;
    }

    public Data getDataMorte() {
        return dataMorte;
    }

    public void setDataMorte(Data dataMorte) {
        this.dataMorte = dataMorte;
    }

    public String getLuogoNascita() {
        return luogoNascita;
    }

    public void setLuogoNascita(String luogoNascita) {
        this.luogoNascita = luogoNascita;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String getLuogoMorte() {
        return luogoMorte;
    }

    public void setLuogoMorte(String luogoMorte) {
        this.luogoMorte = luogoMorte;
    }

    public boolean isVivente() {
        return (this.dataMorte == null || (this.dataMorte.getGiorno() <= 0 && 
                this.dataMorte.getMese() <= 0 && this.dataMorte.getAnno() <= 0));
    }

    public String capitalized() throws Exception {
        if (nome == null || cognome == null) {
            throw new Exception("Nome o cognome mancanti");
        } else {
            String nome = this.nome.substring(0, 1).toUpperCase();
            String cognome = this.cognome.substring(0, 1).toUpperCase() 
                    + this.cognome.substring(1).toLowerCase();
            return nome + ". " + cognome;
        }
    }

    public String stampaDettaglio() {
        String nome;
        try {
            nome = capitalized();
        } catch (Exception e) {
            nome = e.getMessage();
        }
        return "\nAutore"
                + "Nome = " + nome
                + (dataNascita != null ? "\nData di nascita = " + dataNascita.stampaLineare() : "")
                + (dataMorte != null ? "\nData di morte = " + dataMorte.stampaLineare() : "")
                + (luogoNascita != null ? "\nLuogo di nascita = " + luogoNascita : "Non definita!")
                + (luogoMorte != null ? "\nLuogo di Morte = " + luogoMorte : "Non definita!")
                + "\nE' ancora vivo? = " + (isVivente() == true ? "Si" : "No");
    }

    @Override
    public String toString() {
        return "Autore --> [" + (nome != null ? "nome=" + nome + ", " : "")
                + (cognome != null ? "cognome=" + cognome + ", " : "")
                + (dataNascita != null ? "dataNascita = " + dataNascita + ", " : "")
                + (dataMorte != null ? "dataMorte = " + dataMorte + ", " : "")
                + (luogoNascita != null ? "luogoNascita=" + luogoNascita + ", " : "")
                + (luogoMorte != null ? "luogoMorte=" + luogoMorte + ", " : "") 
                + "isVivente()=" + isVivente() + "]";
    }

}
