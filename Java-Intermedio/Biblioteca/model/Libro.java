package model;

/**
 * @author Simone Tugnetti
 */
public class Libro {
	
    private String titolo;
    private Autore[] autore;
    private Editore editore;
    private ISBN isbn;
    private int pagine;
    private Data dataPubblicazione;
    private String lingua;
	
    public Libro() {}

    public Libro(String titolo, Autore[] autore, Editore editore, ISBN isbn, 
            int pagine, Data dataPubblicazione, String lingua) {
	this.titolo = titolo;
	this.autore = autore;
	this.editore = editore;
	this.isbn = isbn;
	this.pagine = pagine;
	this.dataPubblicazione = dataPubblicazione;
	this.lingua = lingua;
    }

    public String getTitolo() {
	return titolo;
    }

    public void setTitolo(String titolo) {
	this.titolo = titolo;
    }

    public Autore[] getAutore() {
	return autore;
    }

    public void setAutore(Autore[] autore) {
	this.autore = autore;
    }

    public Editore getEditore() {
	return editore;
    }

    public void setEditore(Editore editore) {
    	this.editore = editore;
    }

    public ISBN getIsbn() {
	return isbn;
    }

    public void setIsbn(ISBN isbn) {
	this.isbn = isbn;
    }

    public int getPagine() {
	return pagine;
    }

    public void setPagine(int pagine) {
	this.pagine = pagine;
    }

    public Data getDataPubblicazione() {
	return dataPubblicazione;
    }

    public void setDataPubblicazione(Data dataPubblicazione) {
	this.dataPubblicazione = dataPubblicazione;
    }

    public String getLingua() {
	return lingua;
    }

    public void setLingua(String lingua) {
    	this.lingua = lingua;
    }
	
    public double Prezzo() {
	return 5.85+(this.getPagine()*0.05);
    }

    private String stampaAutori() {
        String msg="";
	for(int i=0; i<this.autore.length; i++) {
            msg=msg+ (autore[i] != null ? "\n" + autore[i].stampaDettaglio() : "");
        }
	return msg;
    }
	
    private String stampaAutoriToString() {
	String msg="";
	for(int i=0;i<this.autore.length;i++) {
            msg=msg+ (autore[i] != null ? "\n" + autore[i] : "");
        }
	return msg;
    }
	
	
    public String stampaDettaglio() {
	return "\nLibro" 
                    + (titolo != null ? "\nTitolo = " + titolo : "")
                    + stampaAutori()
                    + (editore != null ? "\n" + editore.stampaDettaglio() : "")
                    + (isbn != null ? "\n" + isbn : "")
                    + "\nNumero di Pagine = " + pagine
                    + (dataPubblicazione != null ? "\nData pubblicazione = " 
                            + dataPubblicazione.stampaLineare() : "")
                    + (lingua != null ? "\nLingua di scrittura = " + lingua : "")
                    + "\nPrezzo del Libro = " + Prezzo();
    }
	
    @Override
    public String toString() {
        return "Libro --> [" + (titolo != null ? "titolo = " + titolo + " - " : "")
                    + stampaAutoriToString() + " - "
                    + (editore != null ? editore + " - " : "") 
                    + (isbn != null ? isbn + " - " : "")
                    + "pagine = " + pagine + " - "
                    + (dataPubblicazione != null ? "dataPubblicazione = " 
                            + dataPubblicazione + " - " : "")
                    + (lingua != null ? "lingua = " + lingua + " - " : "") 
                    + "Prezzo() = " + Prezzo()+"]";
    }
	
}
