package model;

/**
 * @author Simone Tugnetti
 */
public class Editore{
	
    private String ragioneS;
    private Luogo ubicazione;
    private String telefono;
    private String sitoWeb;
    private String mail;
	
    public Editore() {}

    public Editore(String ragioneS, Luogo ubicazione, String telefono, 
            String sitoWeb, String mail){
		
        this.ragioneS = ragioneS;
	this.ubicazione = ubicazione;
	try {
            this.setTelefono(telefono);
	}catch(Exception e) {
            this.telefono=e.getMessage();
	}
	this.sitoWeb = sitoWeb;
	this.mail = mail;
    }

    public String getRagioneS() {
	return ragioneS;
    }

    public void setRagioneS(String ragioneS) {
	this.ragioneS = ragioneS;
    }

    public Luogo getUbicazione() {
	return ubicazione;
    }

    public void setUbicazione(Luogo ubicazione) {
	this.ubicazione = ubicazione;
    }

    public String getTelefono() {
	return telefono;
    }

    public void setTelefono(String telefono) throws Exception{
	if(telefono.length()>10) {
            throw new Exception("Formato numero non valido!");
	}else {
            this.telefono=telefono;
	}
    }

    public String getSitoWeb() {
	return sitoWeb;
    }

    public void setSitoWeb(String sitoWeb) {
	this.sitoWeb = sitoWeb;
    }

    public String getMail() {
	return mail;
    }

    public void setMail(String mail) {
	this.mail = mail;
    }

    public String stampaDettaglio() {
	return "\nEditore" 
                    + (ragioneS != null ? "\nRagione Sociale = " + ragioneS : "")
                    + (ubicazione != null ? "\n" + ubicazione : "")
                    + (telefono != null ? "\nTelefono = " + telefono : "")
                    + (sitoWeb != null ? "\nSito Web = " + sitoWeb : "")
                    + (mail != null ? "\nIndirizzo E-mail = " + mail : "");
    }
	
    @Override
    public String toString() {
	return "Editore --> [" 
                    + (ragioneS != null ? "ragioneS = " + ragioneS + ", " : "")
                    + (ubicazione != null ? ubicazione + ", " : "")
                    + (telefono != null ? "telefono = " + telefono + ", " : "")
                    + (sitoWeb != null ? "sitoWeb = " + sitoWeb + ", " : "") 
                    + (mail != null ? "mail = " + mail : "")+"]";
    }
	
}
