package test;

import view.Finestra;

/**
 * @author Simone Tugnetti
 */
public class GUIAuto {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Test GUIAuto
        
        Finestra f=new Finestra();
        f.setVisible(true);
    }
    
}
