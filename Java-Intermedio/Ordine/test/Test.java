package test;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Scanner;
import java.util.Vector;

import model.*;

/**
 * @author Simone Tugnetti
 * Gestionale di Ordini
 */
public class Test {

    @SuppressWarnings("UseOfObsoleteCollectionType")
    public static void main(String[] args) {
	int scelta=1;
	Scanner tastiera=new Scanner(System.in);
		
	GregorianCalendar data=new GregorianCalendar(2010,2,15,15,16,20);
	GregorianCalendar data2=new GregorianCalendar(2009,5,20,10,11,18);
	ArrayList<Venditore> vend=new ArrayList<>();
	ArrayList<Meccanico> mecc=new ArrayList<>();
	Vector<Prodotto> prod=new Vector<>();
	Vector<Prodotto> prod2=new Vector<>();
	Vector<Ordine> ord=new Vector<>();
		
	vend.add(new Venditore("Giorgio","Franceschini",1200,SettoreVendite.CAMION));
	vend.add(new Venditore("Federico","Vespucci",1100,SettoreVendite.AUTO));
        ord.add(new Ordine("32I98K",data,prod,vend.get(0)));
	ord.add(new Ordine("RE98IJ",data2,prod2,vend.get(1)));
	prod.add(new Prodotto("012STR4","Poggiatesta","E' bello",12.5));
	prod.add(new Prodotto("563A87B","Schienale","Non molto bello",16.7));
	prod2.add(new Prodotto("C786YU","Volante","Intrigante",78.6));
	prod2.add(new Prodotto("78B65J","Radio","Innovativo",20.4));
	mecc.add(new Meccanico("Francesco","Giuggioli",1000,TipoSettore.CARROZZERIA));
	mecc.add(new Meccanico("Giovanni","Bippuli",800,TipoSettore.MECCANICA));
	ResponsabileVenditori respVend=new ResponsabileVenditori(
                "Giancarlo","Migliorini",2000,SettoreVendite.MANAGER,vend);
	CapoOfficina capOff=new CapoOfficina("Gianfrancesco","Sebetti",1800,
                TipoSettore.SPECIALISTA,ord);
		
	while(scelta!=0) {
            System.out.println("Cosa desideri?");
            System.out.println("1 - La stampa dell'elenco dei venditori!");
            System.out.println("2 - La stampa dell'elenco dei meccanici!");
            System.out.println("3 - La stampa di un certo ordine!");
            System.out.println("4 - Stampa dei dati del responsabile venditori!");
            System.out.println("5 - Stampa dei dati del capo officina!");
            System.out.println("0 - Uscire!");
            scelta=tastiera.nextInt();
            
            switch(scelta) {
		case 1:
                    for(Venditore v:vend) {
			System.out.println(v);
                    }
                    System.out.println();
                    break;
		case 2:
                    for(Meccanico m:mecc) {
			System.out.println(m);
                    }
                    System.out.println();
                    break;
		case 3:
                    System.out.println(ord.get(0).scontrino());
                    System.out.println("\n"+ord.get(0)+"\n");
                    break;
		case 4:
                    System.out.println(respVend+"\n");
                    break;
		case 5:
                    System.out.println(capOff+"\n");
                    break;
                case 0:
                    System.out.println("Grazie per aver fatto visita alla nostra officina!");
                    break;
                default:
                    System.out.println("Scelta non valida, riprova!");
                    break;
            }
	}
        
	tastiera.close();

    }

}
