package model;

import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.Vector;

/**
 * @author Simone Tugnetti
 */
public final class Ordine {

    private String idOrdine;
    private GregorianCalendar data;
    private Vector<Prodotto> prodotti;
    private Venditore venditore;
    private SimpleDateFormat sdf;
	
    public Ordine() {
	data=new GregorianCalendar();
	prodotti=new Vector<Prodotto>();
	sdf=new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    }

    public Ordine(String idOrdine, GregorianCalendar data, 
            Vector<Prodotto> prodotti, Venditore venditore) {
	this.idOrdine = idOrdine;
	this.data = data;
	this.prodotti = prodotti;
	this.venditore = venditore;
	sdf=new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    }

    public String getIdOrdine() {
	return idOrdine;
    }

    public void setIdOrdine(String idOrdine) {
	this.idOrdine = idOrdine;
    }

    public GregorianCalendar getData() {
	return data;
    }

    public void setData(GregorianCalendar data) {
	this.data = data;
    }

    public Vector<Prodotto> getProdotti() {
	return prodotti;
    }

    public void setProdotti(Vector<Prodotto> prodotti) {
	this.prodotti = prodotti;
    }

    public Venditore getVenditore() {
	return venditore;
    }

    public void setVenditore(Venditore venditore) {
	this.venditore = venditore;
    }
	
    public int nProdotti() {
	return prodotti.size();
    }
	
    private int quantitaProdotto(String codice){
	int i=0;
	for(Prodotto p:prodotti) {
            if(p.getCodice()==codice) {
		i++;
            }
	}
	return i;
    }
	
    public double totale() {
	double tot=0;
	for(Prodotto p:prodotti) {
            tot+=p.getPrezzo();
	}
	return tot;
    }
	
    public String scontrino() {
	String msg="\nScontrino\n"
			+"IDOrdine: "+idOrdine+"\n"
			+"Data: "+sdf.format(data.getTime())+"\n"
			+"Venditore: "+venditore.getCognome()+"\n"
			+"Prodotti:"+"\n";
	for(Prodotto p:prodotti) {
		msg+="Codice: "+p.getCodice()+", "+"nome: "+p.getProdotto()+
                        ", "+"quantita: "+quantitaProdotto(p.getCodice())+", "+
                        "prezzo: "+p.getPrezzo()+".\n";
                
		msg+="Subtotale: "+(p.getPrezzo()*quantitaProdotto(p.getCodice()))+"\n";
	}
	msg+="Totale: "+totale();
	return msg;
    }

    @Override
    public String toString() {
	return "Ordine [" + (idOrdine != null ? "idOrdine=" + idOrdine + ", " : "")
                    + (data != null ? "data=" + sdf.format(data.getTime()) + ", " : "") 
                    + (prodotti != null ? "prodotti=" + prodotti + ", " : "")
                    + (venditore != null ? "venditore=" + venditore + ", " : "") 
                    + "totale()=" + totale() + "]";
    }
	
}
