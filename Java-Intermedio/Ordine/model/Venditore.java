package model;

/**
 * @author Simone Tugnetti
 */
public class Venditore extends Persona {

    private SettoreVendite settore;
	
    public Venditore() {
	super();
    }

    public Venditore(String nome, String cognome, double stipendio, 
                SettoreVendite settore) {
		
        super(nome,cognome,stipendio);
        this.settore = settore;
    }

    public SettoreVendite getSettore() {
	return settore;
    }

    public void setSettore(SettoreVendite settore) {
	this.settore = settore;
    }

    @Override
    public double tredicesima() {
	return super.getStipendio()*2;
    }

    @Override
    public String toString() {
	return "Venditore [" 
                    + (settore != null ? "settore=" + settore + ", " : "") 
                    + (super.toString() != null ? super.toString() : "") + "]";
    }
    	
}
