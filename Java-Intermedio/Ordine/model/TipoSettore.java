package model;

/**
 * @author Simone Tugnetti
 */
public enum TipoSettore {
    CARROZZERIA,MECCANICA,SPECIALISTA;
}
