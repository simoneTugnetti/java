package model;

import java.util.ArrayList;

/**
 * @author Simone Tugnetti
 */
public class ResponsabileVenditori extends Venditore {

    private ArrayList<Venditore> venditori;

    public ResponsabileVenditori() {
	super();
	venditori=new ArrayList<Venditore>();
    }

    public ResponsabileVenditori(String nome, String cognome, double stipendio, 
            SettoreVendite settore, ArrayList<Venditore> venditori) {
        
	super(nome, cognome, stipendio, settore);
	this.venditori=venditori;
    }

    public ArrayList<Venditore> getVenditori() {
	return venditori;
    }

    public void setVenditori(ArrayList<Venditore> venditori) {
	this.venditori = venditori;
    }
	
    public void aggiungiVenditore(Venditore venditori) {
	this.venditori.add(venditori);
    }
	
    public Venditore restituisciVenditore(int index) {
	return venditori.get(index);
    }
	
    public void cancellaVenditore(int index) {
	venditori.remove(index);
    }

    @Override
    public double tredicesima() {
	double bonus=0;
	for(Venditore v:venditori) {
            bonus+=((v.getStipendio()/24)*15)/100;
	}
	return super.tredicesima()+bonus;
    }

    @Override
    public String toString() {
	return "ResponabileVenditori [" 
                    + (venditori != null ? "venditori=" + venditori + ", " : "")
                    + (super.toString() != null ? super.toString() : "") + "]";
    }
	
}
