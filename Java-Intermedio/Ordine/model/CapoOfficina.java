package model;

import java.util.Vector;

/**
 * @author Simone Tugnetti
 */
public class CapoOfficina extends Meccanico {

    @SuppressWarnings("UseOfObsoleteCollectionType")
    private Vector<Ordine> ordini;

    public CapoOfficina() {
	super();
	ordini=new Vector<Ordine>();
    }

    public CapoOfficina(String nome, String cognome, double stipendio, 
            TipoSettore tipologia, Vector<Ordine> ordini) {
	super(nome, cognome, stipendio, tipologia);
	this.ordini=ordini;
    }

    public Vector<Ordine> getOrdini() {
    	return ordini;
    }

    public void setOrdini(Vector<Ordine> ordini) {
	this.ordini = ordini;
    }
	
    public void aggiungiOrdine(Ordine ordine) {
	ordini.add(ordine);
    }
	
    public int nOrdini() {
	return ordini.size();
    }

    @Override
    public double tredicesima() {
	double tot=0;
	for(Ordine o:ordini) {
            tot+=o.totale()*5/100;
        }
	return super.tredicesima()+tot;
    }

    @Override
    public String toString() {
	return "CapoOfficina [" 
                    + (ordini != null ? "ordini=" + ordini + ", " : "") 
                    + "nOrdini()=" + nOrdini() + ", "
                    + (super.toString() != null ? super.toString() : "") + "]";
    }
	
}
