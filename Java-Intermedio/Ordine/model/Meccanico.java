package model;

/**
 * @author Simone Tugnetti
 */
public class Meccanico extends Persona {

    private TipoSettore tipologia;

    public Meccanico() {
	super();
    }

    public Meccanico(String nome, String cognome, double stipendio, TipoSettore tipologia) {
	super(nome, cognome, stipendio);
	this.tipologia=tipologia;
    }

    public TipoSettore getTipologia() {
	return tipologia;
    }

    public void setTipologia(TipoSettore tipologia) {
	this.tipologia = tipologia;
    }

    @Override
    public double tredicesima() {
	return super.getStipendio()*2;
    }

    @Override
    public String toString() {
	return "Meccanico [" 
                    + (tipologia != null ? "tipologia=" + tipologia + ", " : "")
                    + (super.toString() != null ? super.toString() : "") + "]";
    }
	
}
