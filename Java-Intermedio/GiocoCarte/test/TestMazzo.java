package test;

import model.Mazzo;

/**
 * @author Simone Tugnetti
 */
public class TestMazzo {

    public static void main(String[] args) {
        Mazzo m1 = new Mazzo();
        m1.crea();
        System.out.println(m1);
        m1.mescola(5);
        System.out.println(m1);
    }
}
