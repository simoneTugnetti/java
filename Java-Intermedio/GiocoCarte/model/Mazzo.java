package model;

import java.util.ArrayList;
import java.util.Random;

/**
 * @author Simone Tugnetti
 */
public class Mazzo {

    private final ArrayList<Carta> mazzo;

    public Mazzo() {
        mazzo = new ArrayList<>();
    }

    public void crea() {
        for (Seme s : Seme.values()) {
            for (Valore v : Valore.values()) {
                mazzo.add(new Carta(v, s));
            }
        }
    }

    public void mescola(int numeroMescolamenti) {
        Random r = new Random();
        int primo, secondo;
        for (int i = 0; i < numeroMescolamenti; i++) {
            primo = r.nextInt(mazzo.size() - 1);
            secondo = r.nextInt(mazzo.size() - 1);
            mazzo.add(primo, mazzo.remove(primo));
            mazzo.add(secondo, mazzo.remove(secondo));
        }
    }

    @Override
    public String toString() {
        return "Mazzo: "
                + String.join("\n", mazzo.toString());
    }

}
