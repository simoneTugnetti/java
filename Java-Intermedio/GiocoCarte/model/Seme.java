package model;

/**
 * @author Simone Tugnetti
 */
public enum Seme {

    PICCHE, FIORI, CUORI, QUADRI;
}
