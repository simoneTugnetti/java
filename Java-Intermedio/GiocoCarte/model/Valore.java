package model;

/**
 * @author Simone Tugnetti
 */
public enum Valore {
    ASSO, DUE, TRE, QUATTRO, CINQUE, SEI, SETTE, OTTO, NOVE, DIECI, FANTE, 
    DONNA, RE;
}
