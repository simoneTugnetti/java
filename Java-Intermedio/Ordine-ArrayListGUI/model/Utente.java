package model;

/**
 * @author Simone Tugnetti
 */
public class Utente {
    
    private String nome;
    private String cognome;
    private String ragioneSociale;
    private String codiceFiscale;
    private String partitaIVA;
    private String telefono;
    private String mobile;
    private String email;

    public Utente() {}

    public Utente(String nome, String cognome, String ragioneSociale, 
            String codiceFiscale, String partitaIVA, String telefono, 
            String mobile, String email) {
        
        this.nome = nome;
        this.cognome = cognome;
        this.ragioneSociale = ragioneSociale;
        this.codiceFiscale = codiceFiscale;
        this.partitaIVA = partitaIVA;
        this.telefono = telefono;
        this.mobile = mobile;
        this.email = email;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String getRagioneSociale() {
        return ragioneSociale;
    }

    public void setRagioneSociale(String ragioneSociale) {
        this.ragioneSociale = ragioneSociale;
    }

    public String getCodiceFiscale() {
        return codiceFiscale;
    }

    public void setCodiceFiscale(String codiceFiscale) {
        this.codiceFiscale = codiceFiscale;
    }

    public String getPartitaIVA() {
        return partitaIVA;
    }

    public void setPartitaIVA(String partitaIVA) {
        this.partitaIVA = partitaIVA;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Dettagli Utente: \n"
                + "Nome: " + nome + "\n"
                + "Cognome: " + cognome + "\n"
                + "Ragione sociale: " + ragioneSociale + "\n"
                + "Codice fiscale: " + codiceFiscale + "\n"
                + "Partita IVA: " + partitaIVA + "\n"
                + "Telefono: " + telefono + "\n"
                + "Mobile: " + mobile + "\n"
                + "E-mail: " + email;
    }
    
    
}
