package model;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Random;

/**
 * @author Simone Tugnetti
 */
public class MyLibrary {
    
    
    /**
     * Funzione che restituisce una stringa alfanumerica di 8 caratteri
     * @return Stringa ottenuta
     */
    public static String alpha(){
        String caratteri = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        Random rnd = new Random();
        String s="";
        for(int i=0; i<8; i++){ 
            s+=caratteri.charAt(rnd.nextInt(caratteri.length()));
        }
        return s;
    }
    
    /**
     * Funzione che restituisce la data in formato GregorianCalendar
     * @return Data in GregorianCalendar
     */
    private static GregorianCalendar data(){
        GregorianCalendar cal=new GregorianCalendar();
        GregorianCalendar data=new GregorianCalendar(cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),cal.get(Calendar.DAY_OF_MONTH),
                cal.get(Calendar.HOUR_OF_DAY),cal.get(Calendar.MINUTE),
                cal.get(Calendar.SECOND));
	data.setLenient(false);
	return data;
    }
    
    /**
     * Funzione che restituisce la data effettiva per il percorso del file 
     * oppure la data e l'ora effettiva per le specifiche dell'ordine
     * @param file --> Scelta se utilizzare il pattern per il file o per l'ordine
     * @return Data con il pattern corretto
     */
    public static String dataEOra(boolean file){
        SimpleDateFormat sdf=new SimpleDateFormat();
        if(file){
            sdf.applyPattern("yyyy-MM-dd");
        }else{
            sdf.applyPattern("dd/MM/yyyy HH:mm:ss");
        }
	return sdf.format(data().getTime());
    }
    
    /**
     * Funzione che scrive un determinato messaggio all'interno di un file di testo
     * @param path --> Percorso del file
     * @param msg --> Messaggio da inserire
     * @throws Exception --> Nel caso venga riscontrato un errore durante la scrittura
     */
    public static void write(String path, String msg) throws Exception{
        FileWriter fw=null;
	PrintWriter pw=null;
	try {
            fw=new FileWriter(path,true);
            pw=new PrintWriter(fw);
            pw.write(msg);
            pw.flush();
            pw.close();
            fw.close();
	}catch(IOException e) {
            throw new Exception("Si sono verificati i seguenti errori:\n"+e.getMessage());
	}
    }
    
    /**
     * Funzione che restituisce un determinato messaggio all'interno di un file di testo
     * @param path --> Percorso del file
     * @return Messaggio contenuto all'interno del file di testo
     * @throws Exception --> Nel caso venga riscontrato un errore durante la lettura
     */
    public static String read(String path) throws Exception{
        FileReader fr=null;
        BufferedReader br=null;
        String s="";
        String st;
        try{
            fr=new FileReader(path);
            br=new BufferedReader(fr);
            while((st=br.readLine()) != null){ 
                s+=st+"\n";
            }
            fr.close();
            br.close();
            return s;
        }catch(IOException e){
            throw new Exception("Si sono verificati i seguenti errori:\n"+e.getMessage());
        }
    }
    
}