package model;

/**
 * @author Simone Tugnetti
 */
public class Spedizione {
    
    private String ViaSped;
    private int CAPSped;
    private String CittaSped;
    private String ProvinciaSped;

    public Spedizione() {
    }

    public Spedizione(String ViaSped, int CAPSped, String CittaSped, 
            String ProvinciaSped) {
        this.ViaSped = ViaSped;
        this.CAPSped = CAPSped;
        this.CittaSped = CittaSped;
        this.ProvinciaSped = ProvinciaSped;
    }

    public String getViaSped() {
        return ViaSped;
    }

    public void setViaSped(String ViaSped) {
        this.ViaSped = ViaSped;
    }

    public int getCAPSped() {
        return CAPSped;
    }

    public void setCAPSped(int CAPSped) {
        this.CAPSped = CAPSped;
    }

    public String getCittaSped() {
        return CittaSped;
    }

    public void setCittaSped(String CittaSped) {
        this.CittaSped = CittaSped;
    }

    public String getProvinciaSped() {
        return ProvinciaSped;
    }

    public void setProvinciaSped(String ProvinciaSped) {
        this.ProvinciaSped = ProvinciaSped;
    }

    @Override
    public String toString() {
        return "Indirizzo di spedizione: \n"
                + "Via: " + ViaSped + "\n"
                + "CAP: " + CAPSped + "\n"
                + "Città: " + CittaSped + "\n"
                + "Provincia: " + ProvinciaSped;
    }
    
    
}
