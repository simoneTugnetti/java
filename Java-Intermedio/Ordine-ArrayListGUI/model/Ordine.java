package model;

import java.util.ArrayList;

/**
 * @author Simone Tugnetti
 */
public class Ordine {
    
    private String codice;
    private String data;
    private ArrayList<Prodotto> prodotti;
    private Utente utente;
    private Fatturazione fatt;
    private Spedizione sped;

    public Ordine(ArrayList<Prodotto> prodotti) {
        codice = MyLibrary.alpha();
        data = MyLibrary.dataEOra(false);
        this.prodotti = prodotti;
    }

    public Ordine(ArrayList<Prodotto> prodotti, Utente utente, Fatturazione fatt,
            Spedizione sped) {
        codice = MyLibrary.alpha();
        data = MyLibrary.dataEOra(false);
        this.prodotti = prodotti;
        this.utente = utente;
        this.fatt = fatt;
        this.sped = sped;
    }

    public String getCodice() {
        return codice;
    }

    public String getData() {
        return data;
    }
    
    public ArrayList<Prodotto> getProdotti() {
        return prodotti;
    }

    public void setProdotti(ArrayList<Prodotto> prodotti) {
        this.prodotti = prodotti;
    }

    public Utente getUtente() {
        return utente;
    }

    public void setUtente(Utente utente) {
        this.utente = utente;
    }

    public Fatturazione getFatt() {
        return fatt;
    }

    public void setFatt(Fatturazione fatt) {
        this.fatt = fatt;
    }

    public Spedizione getSped() {
        return sped;
    }

    public void setSped(Spedizione sped) {
        this.sped = sped;
    }
    
    private double imponibile(){
        double somma=0;
        for(Prodotto p:prodotti){
            somma+=p.getPrezzo();
        }
        return somma;
    }
    
    private double iva(){
        return (imponibile()*22)/100;
    }
    
    private double totale(){
        return imponibile()+iva();
    }
    
    private int numProdotti(){
        int somma=0;
        for(Prodotto p:prodotti){
            somma+=p.getQuantita();
        }
        return somma;
    }

    /**
     * Metodo che restituisce il dettaglio dell'ordine appena effettuato
     * @return Dettaglio dell'ordine
     */
    public String stampaOrdine() {
        String txt="Codice: " + codice + "\n"
                    +"Data: " + data + "\n\n"
                    +"Prodotti: \n";
        for(Prodotto p:prodotti){
            txt+=p.toString()+"\n";
        }
        txt+="Numero di prodotti: "+prodotti.size()+"\n"
                +"Quantità di prodotti: "+numProdotti()+"\n\n"
                +"Imponibile: "+imponibile()+"\n"
                +"IVA: "+iva()+"\n"
                +"Totale: "+totale();
        return txt;
    }

    @Override
    public String toString() {
        return "Dettaglio Ordine: \n"
                + stampaOrdine()+"\n\n"
                + utente + "\n\n"
                + fatt + "\n\n"
                + sped;
    }

}
