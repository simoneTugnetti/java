package model;

/**
 * @author Simone Tugnetti
 */
public class Fatturazione {
    
    private String ViaFatt;
    private int CAPFatt;
    private String CittaFatt;
    private String ProvinciaFatt;

    public Fatturazione() {
    }

    public Fatturazione(String ViaFatt, int CAPFatt, String CittaFatt, String ProvinciaFatt) {
        this.ViaFatt = ViaFatt;
        this.CAPFatt = CAPFatt;
        this.CittaFatt = CittaFatt;
        this.ProvinciaFatt = ProvinciaFatt;
    }

    public String getViaFatt() {
        return ViaFatt;
    }

    public void setViaFatt(String ViaFatt) {
        this.ViaFatt = ViaFatt;
    }

    public int getCAPFatt() {
        return CAPFatt;
    }

    public void setCAPFatt(int CAPFatt) {
        this.CAPFatt = CAPFatt;
    }

    public String getCittaFatt() {
        return CittaFatt;
    }

    public void setCittaFatt(String CittaFatt) {
        this.CittaFatt = CittaFatt;
    }

    public String getProvinciaFatt() {
        return ProvinciaFatt;
    }

    public void setProvinciaFatt(String ProvinciaFatt) {
        this.ProvinciaFatt = ProvinciaFatt;
    }

    @Override
    public String toString() {
        return "Indirizzo di fatturazione: \n"
                + "Via: " + ViaFatt + "\n"
                + "CAP: " + CAPFatt + "\n"
                + "Città: " + CittaFatt + "\n"
                + "Provincia: " + ProvinciaFatt;
    }
    
    
}
