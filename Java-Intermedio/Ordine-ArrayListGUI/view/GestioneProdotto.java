package view;

import java.util.ArrayList;
import javax.swing.JOptionPane;
import model.Ordine;
import model.Prodotto;

/**
 * @author Simone Tugnetti
 * Gestione Ordini tramite GUI
 */
public class GestioneProdotto extends javax.swing.JFrame {

    private ArrayList<Prodotto> prodotti;
    
    /**
     * Creates new form Prodotto
     */
    public GestioneProdotto() {
        initComponents();
        prodotti = new ArrayList<Prodotto>();
    }
    
    private void resetForm(){
        txtNome.setText("");
        txtPrezzo.setText("");
        txtQuantita.setText("");
    }

    private boolean verifica(){
        if(txtNome.getText().equals("") || txtPrezzo.getText().equals("") || 
                txtQuantita.getText().equals("")){
            JOptionPane.showMessageDialog(this, "I campi devono essere compilati!");
        }else{
            return true;
        }
        return false;
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblTitolo = new javax.swing.JLabel();
        lblNome = new javax.swing.JLabel();
        txtNome = new javax.swing.JTextField();
        lblPrezzo = new javax.swing.JLabel();
        txtPrezzo = new javax.swing.JTextField();
        lblQuantita = new javax.swing.JLabel();
        txtQuantita = new javax.swing.JTextField();
        btnCarica = new javax.swing.JButton();
        btnSvuota = new javax.swing.JButton();
        btnCrea = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtLista = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Gestione Prodotti");

        lblTitolo.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblTitolo.setText("Gestione Prodotti");

        lblNome.setText("Nome");

        lblPrezzo.setText("Prezzo");

        lblQuantita.setText("Quantità");

        btnCarica.setText("Carica");
        btnCarica.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCaricaActionPerformed(evt);
            }
        });

        btnSvuota.setText("Svuota lista");
        btnSvuota.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSvuotaActionPerformed(evt);
            }
        });

        btnCrea.setText("Crea ordine");
        btnCrea.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCreaActionPerformed(evt);
            }
        });

        txtLista.setEditable(false);
        txtLista.setColumns(20);
        txtLista.setRows(5);
        jScrollPane1.setViewportView(txtLista);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblQuantita)
                            .addComponent(lblPrezzo)
                            .addComponent(lblNome))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(txtPrezzo, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 220, Short.MAX_VALUE)
                            .addComponent(txtNome, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtQuantita)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(91, 91, 91)
                        .addComponent(lblTitolo)))
                .addGap(43, 43, 43))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnCarica)
                        .addGap(35, 35, 35)
                        .addComponent(btnSvuota)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 37, Short.MAX_VALUE)
                        .addComponent(btnCrea))
                    .addComponent(jScrollPane1))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblTitolo)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblNome)
                    .addComponent(txtNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblPrezzo)
                    .addComponent(txtPrezzo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(21, 21, 21)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblQuantita)
                    .addComponent(txtQuantita, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(32, 32, 32)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCarica)
                    .addComponent(btnSvuota)
                    .addComponent(btnCrea))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 34, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCaricaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCaricaActionPerformed
        if(verifica()){
            prodotti.add(new Prodotto(txtNome.getText(),
                    Double.parseDouble(txtPrezzo.getText()),
                    Integer.parseInt(txtQuantita.getText())));
            txtLista.setText("");
            for(Prodotto p:prodotti){
                txtLista.setText(txtLista.getText()+p.toString()+"\n");
            }
            resetForm();
        }
    }//GEN-LAST:event_btnCaricaActionPerformed

    private void btnSvuotaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSvuotaActionPerformed
        if(JOptionPane.showConfirmDialog(this, 
                "Si desidera procedere con l'eliminazione?") == 0){
            prodotti.clear();
            txtLista.setText("");
            resetForm();
        }
    }//GEN-LAST:event_btnSvuotaActionPerformed

    private void btnCreaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCreaActionPerformed
        if(prodotti.isEmpty()){
            JOptionPane.showMessageDialog(this, "Non è presente alcun prodotto!");
        }else{
            Ordine o=new Ordine(prodotti);
            GestioneOrdine go=new GestioneOrdine();
            go.setOrdine(o);
            go.getTxtOrdine().setText(o.stampaOrdine());
            go.setVisible(true);
        }
        resetForm();
    }//GEN-LAST:event_btnCreaActionPerformed

    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(GestioneProdotto.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(GestioneProdotto.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(GestioneProdotto.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(GestioneProdotto.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new GestioneProdotto().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCarica;
    private javax.swing.JButton btnCrea;
    private javax.swing.JButton btnSvuota;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblNome;
    private javax.swing.JLabel lblPrezzo;
    private javax.swing.JLabel lblQuantita;
    private javax.swing.JLabel lblTitolo;
    private javax.swing.JTextArea txtLista;
    private javax.swing.JTextField txtNome;
    private javax.swing.JTextField txtPrezzo;
    private javax.swing.JTextField txtQuantita;
    // End of variables declaration//GEN-END:variables
}
