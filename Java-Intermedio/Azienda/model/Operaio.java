package model;

/**
 * @author Simone Tugnetti
 */
public class Operaio extends Dipendente {

    private Settore settore;

    public Operaio() {}

    public Operaio(String nominativo, int oreLavorate, double pagaOraria, Settore settore) {
	super(nominativo, oreLavorate, pagaOraria);
	this.settore=settore;
    }

    public Settore getSettore() {
	return settore;
    }

    public void setSettore(Settore settore) {
	this.settore = settore;
    }

    @Override
    public double stipendio() {
	return super.getPagaOraria()*super.getOreLavorate()+settore.getValore();
    }

    @Override
    public String toString() {
	return "" 
		+ (settore != null ? "settore=" + settore + ", " : "")
		+ (super.toString() != null ? super.toString() : "");
    }
	
}
