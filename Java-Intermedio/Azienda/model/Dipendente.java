package model;

/**
 * @author Simone Tugnetti
 */
public abstract class Dipendente {
	
    private String nominativo;
    private int oreLavorate;
    private double pagaOraria;
	
    public Dipendente() {}

    public Dipendente(String nominativo, int oreLavorate, double pagaOraria) {
	this.nominativo = nominativo;
	this.oreLavorate = oreLavorate;
	this.pagaOraria = pagaOraria;
    }

    public String getNominativo() {
	return nominativo;
    }

    public void setNominativo(String nominativo) {
	this.nominativo = nominativo;
    }

    public int getOreLavorate() {
	return oreLavorate;
    }

    public void setOreLavorate(int oreLavorate) {
	this.oreLavorate = oreLavorate;
    }

    public double getPagaOraria() {
    	return pagaOraria;
    }

    public void setPagaOraria(double pagaOraria) {
	this.pagaOraria = pagaOraria;
    }
	
    public abstract double stipendio();

    @Override
    public String toString() {
	return "" 
		+ (nominativo != null ? "nominativo=" + nominativo + ", " : "") 
                + "oreLavorate=" + oreLavorate 
		+ ", pagaOraria=" + pagaOraria 
		+ ", stipendio()=" + stipendio();
    }
	
}
