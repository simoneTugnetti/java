package model;

/**
 * @author Simone Tugnetti
 */
public class Amministrativo extends Dipendente {

    private Ruolo ruolo;
	
    public Amministrativo() {}

    public Amministrativo(String nominativo, int oreLavorate, double pagaOraria,
            Ruolo ruolo) {
	super(nominativo, oreLavorate, pagaOraria);
	this.ruolo=ruolo;
    }

    public Ruolo getRuolo() {
	return ruolo;
    }

    public void setRuolo(Ruolo ruolo) {
	this.ruolo = ruolo;
    }

    @Override
    public double stipendio() {
	return super.getPagaOraria()*super.getOreLavorate()+ruolo.getValore();
    }

    @Override
    public String toString() {
	return "" 
		+ (ruolo != null ? "ruolo=" + ruolo + ", " : "")
		+ (super.toString() != null ? super.toString() : "");
    }
	
}
