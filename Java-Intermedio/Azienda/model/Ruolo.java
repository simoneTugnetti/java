package model;

/**
 * @author Simone Tugnetti
 */
public enum Ruolo {
	
    CONTABILE(50),RISORSEUMANE(120),DIRETTORE(230);
	
    private double valore;
	
    Ruolo(double valore){
	this.valore=valore;
    }
	
    public double getValore() {
	return this.valore;
    }
    
}
