package model;

/**
 * @author Simone Tugnetti
 */
public enum Settore {

    INSTALLATORE(150),MANUTENTORE(180),;
	
    private double valore;
	
    Settore(double valore){
	this.valore=valore;
    }
	
    public double getValore() {
	return this.valore;
    }
    
}
