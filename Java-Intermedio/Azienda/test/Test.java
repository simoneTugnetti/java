package test;

import java.util.ArrayList;
import model.*;

/**
 * @author Simone Tugnetti
 * Gestionale di un'azienda
 */
public class Test {

    public static void main(String[] args) {
	// Test
		
	ArrayList<Dipendente> lista=new ArrayList<Dipendente>();
		
	Amministrativo a1=new Amministrativo("Pino",50,35.5,Ruolo.CONTABILE);
	Amministrativo a2=new Amministrativo("Gino",40,40.5,Ruolo.RISORSEUMANE);
	Amministrativo a3=new Amministrativo("Pagliaccio",100,50,Ruolo.DIRETTORE);
		
	lista.add(a1);
	lista.add(a2);
	lista.add(a3);
		
		
	lista.add(new Operaio("Lino",35,75,Settore.INSTALLATORE));
	lista.add(new Operaio("Nino",55,60,Settore.MANUTENTORE));
		
	lista.add(new OperaioSpecializzato("Dino",35,75,Settore.INSTALLATORE,3,400));
	lista.add(new OperaioSpecializzato("Gianpiero",55,60,Settore.MANUTENTORE,2,650));
		
	for(Dipendente d:lista) {
            if(d instanceof Amministrativo) {
		System.out.println(((Amministrativo)d).getNominativo());
            }
            if(d instanceof Operaio) {
		System.out.println(d);
            }
	}
    }

}
