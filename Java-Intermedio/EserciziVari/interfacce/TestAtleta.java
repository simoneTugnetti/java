package interfacce;

/**
 * @author Simone Tugnetti
 */
public class TestAtleta {

    public static void main(String[] args) {
        //Test

        Atleta a1 = new Atleta("Gino", "Pagliaccio", "Respirare");
        System.out.println(a1);

        Calciatore c1 = new Calciatore("Scemo", "Pagliaccio", "Calcio", 12, 
                "Pagliacci", 4321, 12);
        System.out.println(c1);

        Calciatore c2 = c1;
        c2.setCognome("Elefante");
        System.out.println(c1);

        Calciatore c3 = null;

        try {
            c3 = (Calciatore) c1.clone();
        } catch (CloneNotSupportedException e) {
            System.out.println(e.getMessage());
        }

        if (c3 != null) {
            c3.setCognome("Essenza");
            c3.setNome("Lezzo");

            if (c1.equals(c3)) {
                System.out.println("c1 e c3 sono identici!!!");
            } else {
                System.out.println("c1 e c3 sono diversi!!!");
            }

            System.out.println(c3);

            if (c3.compareTo(c1) == 1) {
                System.out.println("vince " + c1.getCognome());
            } else if (c3.compareTo(c1) == -1) {
                System.out.println("vince " + c1.getCognome());
            }

        }

        c1.setPartiteGiocate(0);
        Calciatore c4 = null;
        try {
            c4 = (Calciatore) c1.clone();
            System.out.println(c4);
        } catch (CloneNotSupportedException e) {
            System.out.println(e.getMessage());
        }

    }

}
