package interfacce;

/**
 * @author Simone Tugnetti
 */
public interface ITennista {

    public String dritto();

    public String rovescio();
}
