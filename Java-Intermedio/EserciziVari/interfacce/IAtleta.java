package interfacce;

/**
 * @author Simone Tugnetti
 */
public interface IAtleta {

    public String corro();

    public String salto();

}
