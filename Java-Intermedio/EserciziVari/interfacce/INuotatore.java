package interfacce;

/**
 * @author Simone Tugnetti
 */
public interface INuotatore {

    public String nuoto();
}
