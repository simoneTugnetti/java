package interfacce;

/**
 * @author Simone Tugnetti
 */
public class Calciatore extends Atleta implements Cloneable, Comparable<Calciatore> {

    private int numeroMaglia;
    private String squadra;
    private int goalSegnati;
    private int partiteGiocate;

    public Calciatore(String nome, String cognome, String disciplina, 
            int numeroMaglia, String squadra, int goalSegnati, int partiteGiocate) {
        super(nome, cognome, disciplina);
        this.numeroMaglia = numeroMaglia;
        this.squadra = squadra;
        this.goalSegnati = goalSegnati;
        this.partiteGiocate = partiteGiocate;
    }

    public Calciatore(String nome, String cognome, String disciplina) {
        super(nome, cognome, disciplina);
    }

    public int getNumeroMaglia() {
        return numeroMaglia;
    }

    public void setNumeroMaglia(int numeroMaglia) {
        this.numeroMaglia = numeroMaglia;
    }

    public String getSquadra() {
        return squadra;
    }

    public void setSquadra(String squadra) {
        this.squadra = squadra;
    }

    public int getGoalSegnati() {
        return goalSegnati;
    }

    public void setGoalSegnati(int goalSegnati) {
        this.goalSegnati = goalSegnati;
    }

    public int getPartiteGiocate() {
        return partiteGiocate;
    }

    public void setPartiteGiocate(int partiteGiocate) {
        this.partiteGiocate = partiteGiocate;
    }

    public float mediaGoalSegnati() {
        return (float) goalSegnati / partiteGiocate;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        if (partiteGiocate == 0) {
            throw new CloneNotSupportedException(
                    "Impossibile clonare per partite giocate = " 
                            + partiteGiocate + "!");
        }
        return super.clone();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + goalSegnati;
        result = prime * result + numeroMaglia;
        result = prime * result + partiteGiocate;
        result = prime * result + ((squadra == null) ? 0 : squadra.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Calciatore other = (Calciatore) obj;
        if (goalSegnati != other.goalSegnati) {
            return false;
        }
        if (numeroMaglia != other.numeroMaglia) {
            return false;
        }
        if (partiteGiocate != other.partiteGiocate) {
            return false;
        }
        if (squadra == null) {
            if (other.squadra != null) {
                return false;
            }
        } else if (!squadra.equals(other.squadra)) {
            return false;
        }
        return true;
    }

    @Override
    public int compareTo(Calciatore obj) {
        Calciatore other = obj;
        if (this.mediaGoalSegnati() > other.mediaGoalSegnati()) {
            return 1;
        } else if (this.mediaGoalSegnati() < other.mediaGoalSegnati()) {
            return -1;
        }
        return 0;
    }

    @Override
    public String toString() {
        return ""
                + "numeroMaglia=" + numeroMaglia + ", "
                + (squadra != null ? "squadra=" + squadra + ", " : "")
                + "goalSegnati=" + goalSegnati + ", "
                + "partiteGiocate=" + partiteGiocate + ", "
                + "MediaPartite=" + partiteGiocate + ", "
                + (getNome() != null ? "Nome=" + getNome() + ", " : "")
                + (getCognome() != null ? "Cognome=" + getCognome() + ", " : "")
                + (getDisciplina() != null ? "Disciplina=" + getDisciplina() + ", " : "")
                + (corro() != null ? corro() + ", " : "")
                + (salto() != null ? salto() + ", " : "")
                + (mangio() != null ? mangio() + ", " : "")
                + (bevo() != null ? bevo() + ", " : "")
                + "NumeroMaglia=" + getNumeroMaglia() + ", "
                + (getSquadra() != null ? "Squadra=" + getSquadra() + ", " : "")
                + "GoalSegnati=" + getGoalSegnati() + ", "
                + "PartiteGiocate=" + getPartiteGiocate();
    }

}
