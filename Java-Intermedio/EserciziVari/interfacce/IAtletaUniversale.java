package interfacce;

/**
 * @author Simone Tugnetti
 */
public interface IAtletaUniversale extends IAtleta, INuotatore, ITennista {

    public String mangio();

    public String bevo();
}
