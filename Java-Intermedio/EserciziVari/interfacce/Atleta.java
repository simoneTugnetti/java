package interfacce;

/**
 * @author Simone Tugnetti
 */
public class Atleta implements IAtletaUniversale {

    private String nome;
    private String cognome;
    private String disciplina;

    public Atleta(String nome, String cognome, String disciplina) {
        this.nome = nome;
        this.cognome = cognome;
        this.disciplina = disciplina;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String getDisciplina() {
        return disciplina;
    }

    public void setDisciplina(String disciplina) {
        this.disciplina = disciplina;
    }

    @Override
    public String corro() {
        return "Sto correndo!!!";
    }

    @Override
    public String salto() {
        return "Sto saltando!!!";
    }

    @Override
    public String nuoto() {
        return "Sto nuotando!!!";
    }

    @Override
    public String dritto() {
        return "Ho eseguito un dritto!!!";
    }

    @Override
    public String rovescio() {
        return "Ho eseguito un rovescio!!!";
    }

    @Override
    public String mangio() {
        return "Sto mangiando!!!";
    }

    @Override
    public String bevo() {
        return "Sto bevendo!!!";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((cognome == null) ? 0 : cognome.hashCode());
        result = prime * result + ((disciplina == null) ? 0 : disciplina.hashCode());
        result = prime * result + ((nome == null) ? 0 : nome.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Atleta other = (Atleta) obj;
        if (cognome == null) {
            if (other.cognome != null) {
                return false;
            }
        } else if (!cognome.equals(other.cognome)) {
            return false;
        }
        if (disciplina == null) {
            if (other.disciplina != null) {
                return false;
            }
        } else if (!disciplina.equals(other.disciplina)) {
            return false;
        }
        if (nome == null) {
            if (other.nome != null) {
                return false;
            }
        } else if (!nome.equals(other.nome)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return ""
                + (nome != null ? "nome=" + nome + ", " : "")
                + (cognome != null ? "cognome=" + cognome + ", " : "")
                + (disciplina != null ? "disciplina=" + disciplina + ", " : "")
                + (corro() != null ? corro() + ", " : "")
                + (nuoto() != null ? nuoto() + ", " : "")
                + (dritto() != null ? dritto() + ", " : "")
                + (rovescio() != null ? rovescio() + ", " : "")
                + (mangio() != null ? mangio() + ", " : "")
                + (bevo() != null ? bevo() + ", " : "")
                + (salto() != null ? salto() : "");
    }

}
