package threading;

/**
 * @author Simone Tugnetti
 */
public class PingPongV2 implements Runnable {

    private String frase;
    private int riposo;

    public PingPongV2(String frase, int riposo) {
        this.frase = frase;
        this.riposo = riposo;
        Thread t = new Thread(this);
        t.start();
    }

    public String getFrase() {
        return frase;
    }

    public void setFrase(String frase) {
        this.frase = frase;
    }

    public int getRiposo() {
        return riposo;
    }

    public void setRiposo(int riposo) {
        this.riposo = riposo;
    }

    @Override
    public void run() {
        while (true) {
            System.out.println(frase);
            try {
                Thread.sleep(riposo);
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    @Override
    public String toString() {
        return "PingPongV2 [" + (frase != null ? "frase=" + frase + ", " : "") 
                + "riposo=" + riposo + "]";
    }

}
