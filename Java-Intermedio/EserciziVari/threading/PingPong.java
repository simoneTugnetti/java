package threading;

/**
 * @author Simone Tugnetti
 */
public class PingPong extends Thread {

    private String frase;
    private int riposo;  //millisecondi

    public PingPong(String frase, int riposo) {
        this.frase = frase;
        this.riposo = riposo;
        start();  //Parte il thread
    }

    public String getFrase() {
        return frase;
    }

    public void setFrase(String frase) {
        this.frase = frase;
    }

    public int getRiposo() {
        return riposo;
    }

    public void setRiposo(int riposo) {
        this.riposo = riposo;
    }

    @Override
    public void run() {
        // Esecuzione del thread

        while (true) {
            System.out.println(frase);
            try {
                Thread.sleep(riposo);
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    @Override
    public String toString() {
        return "PingPong [" + (frase != null ? "frase=" + frase + ", " : "") 
                + "riposo=" + riposo + "]";
    }

}
