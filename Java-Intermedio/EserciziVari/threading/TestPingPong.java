package threading;

/**
 * @author Simone Tugnetti
 */
public class TestPingPong {

    public static void main(String[] args) {
        //Test

        /*
		PingPong p1=new PingPong("ping",2000);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		PingPong p2=new PingPong("pong",2000);
         */
        PingPongV2 p1 = new PingPongV2("ping", 2000);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        PingPongV2 p2 = new PingPongV2("pong", 2000);
    }

}
