package funzioni;

/**
 * @author Simone Tugnetti
 */
public class LancioDadiDiversi {

    private static int dado() {
        return dado(6);
    }

    private static int dado17() {
        return dado(17);
    }

    private static int dado(int numeroFacce) {
        return (int) (Math.random() * numeroFacce + 1);
    }

    public static void main(String[] args) {
        // Lancio di n dadi a facce diverse

        int dado = dado();
        System.out.println("dado6: " + dado);
        System.out.println("dado17: " + dado17());
        System.out.println("dadoNFacce: " + dado(40));
    }

}
