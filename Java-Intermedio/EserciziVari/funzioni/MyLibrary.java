package funzioni;

/**
 * @author Simone Tugnetti
 */
public class MyLibrary {

    public static int dado(int numeroFacce) {
        return (int) (Math.random() * numeroFacce + 1);
    }

    public static int casuale(int inf, int sup) {
        return (int) (Math.random() * sup + inf);
    }
}
