package funzioni;

/**
 * @author Simone Tugnetti
 */
public class LancioNDadiCasuali {

    public static void main(String[] args) {
        // Lancio N dadi con numero di facce casuali

        int n = MyLibrary.casuale(1, 10);
        int f;

        for (int i = 0; i < n; i++) {
            f = MyLibrary.casuale(4, 54);
            System.out.println("dado(" + f + "): " + MyLibrary.dado(f));
        }
    }

}
