package classi;

/**
 * @author Simone Tugnetti
 */
public class Cerchio {

    //Attributo
    public double raggio;

    //Costruttore di default
    public Cerchio() {
    }

    //Metodi
    public double diametro() {
        return 2 * this.raggio;
    }

    public double circonferenza() {
        return Math.PI * this.diametro();
    }

    public double area() {
        return Math.PI * Math.pow(this.raggio, 2);
    }

    //Metodo consumer (usa e getta)
    public String stampa() {
        return ""
                + "Raggio: " + this.raggio
                + "\nDiametro: " + this.diametro()
                + "\nCirconferenza: " + this.circonferenza()
                + "\nArea: " + this.area();
    }
}
