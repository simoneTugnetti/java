package classi;

import java.io.*;
import enumerazioni.Alimentazione;

/**
 * @author Simone Tugnetti
 */
public class TestAutomaAuto {

    public static void main(String[] args) {
        // Test Automa Auto
        AutomaAuto a1 = new AutomaAuto();
        int n = 0;
        a1.setAlimentazione(Alimentazione.BENZINA);
        a1.setCilindrata(2000);

        System.out.println("velocita': " + a1.getVelocita() + " km/h");

        InputStreamReader input = new InputStreamReader(System.in);
        BufferedReader tastiera = new BufferedReader(input);

        String mnu = "Scegli una delle seguenti opzioni:"
                + "\n1 --> accellera"
                + "\n2 --> frena"
                + "\n3 --> Vedi velocita'"
                + "\n0 --> esci";

        do {

            System.out.println(mnu);
            try {
                n = Integer.parseInt(tastiera.readLine());
            } catch (IOException e) {
                System.out.println("Errore!");
            } catch (NumberFormatException e) {
                System.out.println("Errore!");
            }

            if (n == 0) {
                break;
            } else if (n == 1) {
                a1.accellera();
            } else if (n == 2) {
                a1.frena();
            } else if (n == 3) {
                System.out.println("velocita': " + a1.getVelocita() + " km/h");
            } else {
                System.out.println("\nErrore inserimento scelta!");
            }

        } while (true);
    }

}
