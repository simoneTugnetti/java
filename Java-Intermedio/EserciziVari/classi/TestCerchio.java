package classi;

/**
 * @author Simone Tugnetti
 */
public class TestCerchio {

    public static void main(String[] args) {
        // Test per oggetti cerchio

        Cerchio c1 = new Cerchio();
        c1.raggio = 1.25;
        System.out.println("c1: " + c1.stampa());

        Cerchio c2 = c1;
        c2.raggio = 2.25;
        System.out.println("\nc2: " + c2.stampa());
        System.out.println("\nc1: " + c1.stampa());
    }

}
