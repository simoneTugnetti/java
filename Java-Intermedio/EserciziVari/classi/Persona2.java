package classi;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import enumerazioni.Sesso;

/**
 * @author Simone Tugnetti
 */
public class Persona2 {

    //attributi
    private String nome;
    private String cognome;
    private GregorianCalendar dataNascita;
    private String luogoNascita;
    private Sesso sesso;

    public Persona2() {
    }

    public Persona2(String nome, String cognome) {
        this.nome = nome;
        this.cognome = cognome;
    }

    public Persona2(String nome, String cognome, GregorianCalendar dataNascita) {
        this.nome = nome;
        this.cognome = cognome;
        this.dataNascita = dataNascita;
    }

    public Persona2(String nome, String cognome, GregorianCalendar dataNascita, 
            String luogoNascita) {
        this.nome = nome;
        this.cognome = cognome;
        this.dataNascita = dataNascita;
        this.luogoNascita = luogoNascita;
    }

    public Persona2(String nome, String cognome, GregorianCalendar dataNascita, 
            String luogoNascita, Sesso sesso) {
        this.nome = nome;
        this.cognome = cognome;
        this.dataNascita = dataNascita;
        this.luogoNascita = luogoNascita;
        this.sesso = sesso;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public GregorianCalendar getDataNascita() {
        return dataNascita;
    }

    public void setDataNascita(GregorianCalendar dataNascita) {
        this.dataNascita = dataNascita;
    }

    public String getLuogoNascita() {
        return luogoNascita;
    }

    public void setLuogoNascita(String luogoNascita) {
        this.luogoNascita = luogoNascita;
    }

    public Sesso getSesso() {
        return sesso;
    }

    public void setSesso(Sesso sesso) {
        this.sesso = sesso;
    }

    public int eta() {
        GregorianCalendar oggi = new GregorianCalendar(2019, 3, 21);
        int e = oggi.get(Calendar.YEAR) - dataNascita.get(Calendar.YEAR);
        if (dataNascita.get(Calendar.MONTH) > oggi.get(Calendar.MONTH)) {
            e--;
        } else if (dataNascita.get(Calendar.MONTH) == oggi.get(Calendar.MONTH) 
                && dataNascita.get(Calendar.DAY_OF_MONTH) > oggi.get(Calendar.DAY_OF_MONTH)) {
            e--;
        }
        return e;
    }

    @Override
    public String toString() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy ");

        return "Persona ["
                + (nome != null ? "nome=" + nome : "")
                + (cognome != null ? ", cognome=" + cognome : "")
                + (dataNascita != null ? ", dataNascita=" + sdf.format(dataNascita.getTime()) : "")
                + (luogoNascita != null ? ", luogoNascita=" + luogoNascita : "")
                + (sesso != null ? ", sesso=" + sesso : "")
                + (dataNascita != null ? ", eta()=" + eta() : "")
                + "]";
    }

}
