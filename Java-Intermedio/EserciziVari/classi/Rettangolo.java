package classi;

/**
 * @author Simone Tugnetti
 */
public class Rettangolo {

    //Attributi
    private double base, altezza;

    //Costruttore
    public Rettangolo(double base, double altezza) {
        if (base > 0) {
            this.base = base;
        }
        if (altezza > 0) {
            this.altezza = altezza;
        }
    }

    //Metodi
    public double perimetro() {
        return (this.base + this.altezza) * 2;
    }

    public double area() {
        return this.base * this.altezza;
    }

    public double diagonale() {
        return Math.sqrt(Math.pow(this.base, 2) + Math.pow(this.altezza, 2));
    }

    //Overloading - Metodi con firme diverse
    public String stampa() {
        return this.stampa("\n");
    }

    public String stampa(String separatore) {
        return separatore + "Base: " + this.base
                + separatore + "Altezza: " + this.altezza
                + separatore + "Perimetro: " + this.perimetro()
                + separatore + "Area: " + this.area()
                + separatore + "Diagonale: " + this.diagonale();
    }
}
