package classi;

import enumerazioni.MonetaEuro;

/**
 * @author Simone Tugnetti
 */
public class TestMonetaEuro {

    public static void main(String[] args) {
        // Test di monete

        MonetaEuro[] lista = MonetaEuro.values();

        for (MonetaEuro m : lista) {
            System.out.println(m + " valore: " + m.getValore());
        }

        System.out.println();

    }

}
