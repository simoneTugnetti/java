package classi;

import java.util.GregorianCalendar;
import enumerazioni.Sesso;

/**
 * @author Simone Tugnetti
 */
public class TestPersona2 {

    public static void main(String[] args) {
        // Test di oggetti Persona

        Persona2 pino = new Persona2();
        System.out.println(pino);
        Persona2 lino = new Persona2("Lino", "Delfino");
        System.out.println(lino);

        GregorianCalendar dataDino = new GregorianCalendar(1999, 5, 4);
        Persona2 dino = new Persona2("Dino", "Mandarino", dataDino, "Torino");
        System.out.println(dino);

        Persona2 mina = new Persona2("Mina", "Delillo", 
                new GregorianCalendar(2000, 7, 25), "Collegno", Sesso.F);
        System.out.println(mina);

    }

}
