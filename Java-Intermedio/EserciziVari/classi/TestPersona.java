package classi;

import enumerazioni.Sesso;

/**
 * @author Simone Tugnetti
 */
public class TestPersona {

    public static void main(String[] args) {
        // Test di oggetti Persona

        Persona pino = new Persona();
        System.out.println(pino);
        Persona lino = new Persona("Lino", "Delfino");
        System.out.println(lino);

        Data dataDino = new Data(4, 5, 1999);
        Persona dino = new Persona("Dino", "Mandarino", dataDino, "Torino");
        System.out.println(dino);

        Persona mina = new Persona("Mina", "Delillo", new Data(25, 7, 2000), "Collegno", Sesso.F);
        System.out.println(mina);

    }

}
