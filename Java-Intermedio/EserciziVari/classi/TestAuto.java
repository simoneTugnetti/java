package classi;

import enumerazioni.Alimentazione;

/**
 * @author Simone Tugnetti
 */
public class TestAuto {

    public static void main(String[] args) {
        // Auto

        Auto bmw = new Auto("BMW", "X5", 2498, Alimentazione.BENZINA, "verde");
        System.out.println(bmw);

    }

}
