package classi;

/**
 * @author Simone Tugnetti
 */
public class TestQuadrato {

    public static void main(String[] args) {
        // Test per oggetti Quadrato

        //Dichiarazione dell'oggetto
        Quadrato q = null;

        //Istanza dell'oggetto
        q = new Quadrato();

        q.lato = 2.25;

        System.out.println("Lato: " + q.lato); //lettura
        System.out.println("Perimetro: " + q.perimetro());
        System.out.println("Area: " + q.area());
        System.out.println("Diagonale: " + q.diagonale());

        Quadrato q1 = new Quadrato();
        q1.lato = 1.25;
        System.out.println("\nLato: " + q1.lato); //lettura
        System.out.println("Perimetro: " + q1.perimetro());
        System.out.println("Area: " + q1.area());
        System.out.println("Diagonale: " + q1.diagonale());

    }
}
