package classi;

/**
 * @author Simone Tugnetti
 */
public class Quadrato {

    //Attributi o campi --> Caratteristiche
    //modificatore di accesso - tipo - identificatore
    //Public --> Accessibile
    //Private --> Visibilita' solo all'interno della classe
    public double lato;

    //Metodi --> Funzionalit�
    public double perimetro() {
        return this.lato * 4;
    }

    public double area() {
        return this.lato * this.lato;
    }

    public double diagonale() {
        return this.lato * Math.sqrt(2);
    }

}
