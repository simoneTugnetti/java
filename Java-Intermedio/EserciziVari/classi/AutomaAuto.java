package classi;

import enumerazioni.Alimentazione;

/**
 * @author Simone Tugnetti
 */
public class AutomaAuto {

    private Auto auto; //Associazione
    private double velocita;

    public AutomaAuto() {
        this.auto = new Auto();
        this.velocita = 50;
    }

    public AutomaAuto(String marca, String modello, int cilindrata, 
            Alimentazione alimentazione, String colore) {
        this(); //Fa riferimento al costruttore di default
        this.auto.setMarca(marca);
        this.auto.setModello(modello);
        this.auto.setCilindrata(cilindrata);
        this.auto.setAlimentazione(alimentazione);
        this.auto.setColore(colore);
    }

    public AutomaAuto(String marca, String modello, int cilindrata, 
            Alimentazione alimentazione, String colore, double velocita) {
        this(marca, modello, cilindrata, alimentazione, colore); 
        this.velocita = velocita;
    }

    public String getMarca() {
        return auto.getMarca();
    }

    public void setMarca(String marca) {
        this.auto.setMarca(marca);
    }

    public String getModello() {
        return auto.getModello();
    }

    public void setModello(String modello) {
        this.auto.setModello(modello);
    }

    public int getCilindrata() {
        return auto.getCilindrata();
    }

    public void setCilindrata(int cilindrata) {
        this.auto.setCilindrata(cilindrata);
    }

    public Alimentazione getAlimentazione() {
        return auto.getAlimentazione();
    }

    public void setAlimentazione(Alimentazione alimentazione) {
        this.auto.setAlimentazione(alimentazione);
    }

    public String getColore() {
        return auto.getColore();
    }

    public void setColore(String colore) {
        this.auto.setColore(colore);
    }

    public double getVelocita() {
        return velocita;
    }

    public void setVelocita(double velocita) {
        this.velocita = velocita;
    }

    public void accellera() {
        this.velocita += 10;
        if (this.velocita > this.velocitaMax()) {
            this.velocita = this.velocitaMax();
        }
    }

    public void frena() {
        this.velocita -= 5;
        if (this.velocita == 0) {
            this.velocita = 0;
        }
    }

    public double velocitaMax() {
        return auto.velocitaMax();
    }

    @Override
    public String toString() {
        return "Auto ["
                + "marca=" + this.getMarca() + ", "
                + "modello=" + this.getModello() + ", "
                + "cilindrata=" + this.getCilindrata() + ", "
                + "alimentazione=" + this.getAlimentazione() + ", "
                + "colore=" + this.getColore() + ", "
                + "velocitaMax()=" + this.velocitaMax() + "]";
    }

}
