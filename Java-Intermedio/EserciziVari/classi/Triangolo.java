package classi;

/**
 * @author Simone Tugnetti
 */
public class Triangolo {

    //attributi
    private double lato1;
    private double lato2;
    private double lato3;

    //Costruttore
    public Triangolo() {
    } //Esplicita la costruzione dell'oggetto

    public Triangolo(double lato1, double lato2, double lato3) {
        this.setLato1(lato1);
        this.setLato2(lato2);
        this.setLato3(lato3);
    }

    public Triangolo(Punto p1, Punto p2, Punto p3) {
        this.setLato1(p1.distanza(p2));
        this.setLato2(p2.distanza(p3));
        this.setLato3(p3.distanza(p1));
    }

    //getters e setters
    //lettura
    public double getLato1() {
        return this.lato1;
    }

    //scrittura
    public void setLato1(double lato1) {
        this.lato1 = lato1;
    }

    public double getLato2() {
        return this.lato2;
    }

    public void setLato2(double lato2) {
        this.lato2 = lato2;
    }

    public double getLato3() {
        return this.lato3;
    }

    public void setLato3(double lato3) {
        this.lato3 = lato3;
    }

    //Metodi
    public boolean isCostruibile() {
        //Somma di due lati di un triangolo deve essere sempre maggiore della misura del terzo

        return ((this.lato1 + this.lato2 > this.lato3)
                && (this.lato2 + this.lato3 > this.lato1)
                && (this.lato3 + this.lato1 > this.lato2));
    }

    public double perimetro() {
        return this.lato1 + this.lato2 + this.lato3;
    }

    //http://www.ripmat.it/mate/i/ie/ieacb.html
    public double area() {
        if (!this.isCostruibile()) {
            return -1;
        }
        double sp = this.perimetro() / 2;
        return Math.sqrt(sp * (sp - this.lato1) * (sp - this.lato2) 
                * (sp - this.lato3));
    }

    public String tipo() {
        if (this.lato1 == this.lato2 && this.lato1 == this.lato3) {
            return "equilatero";
        } else if (this.lato1 == this.lato2 || this.lato1 == lato2 || 
                this.lato2 == this.lato3) {
            return "isoscele";
        }
        return "scaleno";
    }

    public String stampa() {
        if (!this.isCostruibile()) {
            return "Dati non coerenti!";
        }
        return "Lato1: " + this.lato1
                + "\nLato2: " + this.lato2
                + "\nLato3: " + this.lato3
                + "\nPerimetro: " + this.perimetro()
                + "\nArea: " + this.area()
                + "\nTipo: " + this.tipo();
    }

    @Override
    public String toString() {
        if (!this.isCostruibile()) {
            return "Dati non coerenti!";
        }
        return "Lato1: " + this.lato1
                + "\nLato2: " + this.lato2
                + "\nLato3: " + this.lato3
                + "\nPerimetro: " + this.perimetro()
                + "\nArea: " + this.area()
                + "\nTipo: " + this.tipo();
    }
}
