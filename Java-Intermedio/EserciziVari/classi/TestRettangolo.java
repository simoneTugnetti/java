package classi;

/**
 * @author Simone Tugnetti
 */
public class TestRettangolo {

    public static void main(String[] args) {
        // Test Rettangolo
        Rettangolo r1 = new Rettangolo(2.25, 1.25);
        System.out.println("r1:" + r1.stampa());

        Rettangolo r2 = new Rettangolo(-2.25, 1.25);
        System.out.println("\nr2:" + r2.stampa("\n"));
    }

}
