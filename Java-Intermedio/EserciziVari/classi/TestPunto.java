package classi;

/**
 * @author Simone Tugnetti
 */
public class TestPunto {

    public static void main(String[] args) {
        // Test Punto

        Punto p = new Punto();
        p.setEtichetta("P");
        p.setX(-2.3);
        p.setY(1.6);
        System.out.println(p);

        Punto q = new Punto("Q", -1.2, 2.1);
        System.out.println(q);

        System.out.println("PO: " + p.distanza());
        System.out.println("QO: " + q.distanza());
        System.out.println("PQ: " + p.distanza(q));
        System.out.println("QP: " + q.distanza(p));

        Punto r = new Punto("R", 1.1, -2.5);

        Triangolo t = new Triangolo(p, q, r);
        System.out.println("\n" + t);
    }

}
