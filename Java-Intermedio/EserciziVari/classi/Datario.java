package classi;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * @author Simone Tugnetti
 */
public class Datario {

    public static void main(String[] args) {
        // Gestione di una data tramite GregorianCalendar

        GregorianCalendar cal = new GregorianCalendar();

        // stampa --> gg/mm/aaaa hh:mm:ss
        String msg = ""
                + cal.get(Calendar.DAY_OF_MONTH)
                + "/"
                + cal.get(Calendar.MONTH)
                + "/"
                + cal.get(Calendar.YEAR)
                + " "
                + cal.get(Calendar.HOUR_OF_DAY)
                + ":"
                + cal.get(Calendar.MINUTE)
                + ":"
                + cal.get(Calendar.SECOND);
        System.out.println(msg);

        GregorianCalendar data = new GregorianCalendar(1999, 5, 24, 12, 23, 96);
        SimpleDateFormat sdf = new SimpleDateFormat();
        sdf.applyPattern("dd/MM/yyyy HH:mm:ss");
        System.out.println("data: " + sdf.format(data.getTime()));
    }

}
