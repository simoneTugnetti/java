package classi;

/**
 * @author Simone Tugnetti
 */
public class Punto {

    //Attributi
    private String etichetta; //P, Q, ecc...
    private double x;
    private double y;

    //Costruttori
    //default
    public Punto() {
    }

    //passaggio due parametri
    public Punto(double x, double y) {
        this.x = x;
        this.y = y;
    }

    //passaggio tre parametri
    public Punto(String etichetta, double x, double y) {
        this.etichetta = etichetta;
        this.x = x;
        this.y = y;
    }

    //getters e setters
    public String getEtichetta() {
        return etichetta;
    }

    public void setEtichetta(String etichetta) {
        this.etichetta = etichetta;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double distanza() {
        Punto origine = new Punto(0, 0);
        return this.distanza(origine);
    }

    public double distanza(Punto p) {
        return Math.sqrt(Math.pow(this.x - p.getX(), 2) + Math.pow(this.y - p.getY(), 2));
    }

    @Override
    public String toString() {
        return etichetta + " = (" + x + "," + y + ")";
    }

}
