package sequenza;

/**
 * @author Simone Tugnetti
 */
public class Wrapper {

    public static void main(String[] args) {
        // Classi Wrapper

        int n1 = 5;

        Integer n2 = 5;

        System.out.println("\nn1: " + n1);
        System.out.println("\nn2: " + n2);
        int n3 = Integer.parseInt("5");
        System.out.println("\nn3: " + n3);
        double d1 = 5.25;
        Double d2 = 5.25;
        System.out.println("\nd1: " + d1);
        System.out.println("\nd2: " + d2);
        double d3 = Double.parseDouble("5.25");
        System.out.println("\nd3: " + d3);
    }

}
