package sequenza;

/**
 * @author Simone Tugnetti
 */
public class NumeroCasuale {

    public static void main(String[] args) {
        // Determinare un numero casuale
        // Esempio per lancio di un dado a 6 facce

        double c1 = Math.random();
        double c2 = c1 * 6; //Valore massimo da recuperare
        double c3 = c2 + 1; //Valore minimo da recuperare
        int c4 = (int) c3;
        System.out.println(c1);
        System.out.println(c2);
        System.out.println(c3);
        System.out.println(c4);
    }
}
