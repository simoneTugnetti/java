package sequenza;

/**
 * @author Simone Tugnetti
 */
public class Cerchio {

    public static void main(String[] args) {
        // Dato un raggio di un cerchio, calcolare il diametro, la circonferenza e l'area

        double raggio = 10.25;
        double diametro = 2 * raggio;
        double circonferenza = 2 * Math.PI * raggio;
        double area = Math.PI * Math.pow(raggio, 2);

        System.out.println("Raggio --> " + raggio);
        System.out.println("Diametro --> " + diametro);
        System.out.println("Circonferenza --> " + circonferenza);
        System.out.println("Area --> " + area);

        String msg = "";
        msg = msg + "\nRaggio: " + raggio;
        msg += ", Diametro: " + diametro;
        msg += ", Circonferenza: " + circonferenza;
        msg += ", Area: " + area + ".";

        System.out.println(msg);
    }

}
