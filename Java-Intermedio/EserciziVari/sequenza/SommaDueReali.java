package sequenza;

/**
 * @author Simone Tugnetti
 */
public class SommaDueReali {

    public static void main(String[] args) {
        // Somma di due numeri reali

        float a = 12.75F, b = 7.43f;
        double s = a + b;

        System.out.println(a + " + " + b + " = " + s);
    }

}
