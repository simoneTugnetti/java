package sequenza;

/**
 * @author Simone Tugnetti
 */
public class DivisioneDueInteri {

    public static void main(String[] args) {
        // Dati due numeri interi, calcolare il quoziente intero, il resto e il quoziente reale

        //float -32bit
        //double -64bit  -  default
        int a = 12, b = 7;
        int qi = a / b; //Quoziente intero
        int r = a % b;	//Resto della divisione
        float qr = (float) a / b; //Quoziente reale

        System.out.println("Dividendo --> " + a);
        System.out.println("Divisore --> " + b);
        System.out.println("Quoziente intero --> " + qi);
        System.out.println("Resto --> " + r);
        System.out.println("Quoziente reale --> " + qr);
    }

}
