package sequenza;

/**
 * @author Simone Tugnetti
 */
public class PrezzoQuantita {

    public static void main(String[] args) {
        // Dato il prezzo e la quantita' di acquisto di un prodotto, stampare
        // il totale da pagare

        double prezzo = 10.25;
        int quantita = 4;
        double totale = prezzo * quantita;

        System.out.println("Prezzo --> " + prezzo);
        System.out.println("Quantita' --> " + quantita);
        System.out.println("Totale --> €" + totale);

    }

}
