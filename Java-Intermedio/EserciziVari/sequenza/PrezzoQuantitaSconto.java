package sequenza;

/**
 * @author Simone Tugnetti
 */
public class PrezzoQuantitaSconto {

    public static void main(String[] args) {
        // Dato il prezzo e la quantita' di acquisto di un prodotto, 
        // stampare il totale da pagare

        double prezzo = 10.25;
        int quantita = 4;
        int percentualeSconto = 30;
        double subTotale = prezzo * quantita;
        double sconto = subTotale * percentualeSconto / 100;
        double totale = subTotale - sconto;

        System.out.println("Prezzo --> " + prezzo);
        System.out.println("Quantita' --> " + quantita);
        System.out.println("Sconto --> " + percentualeSconto);
        System.out.println("Subtotale --> €" + subTotale);
        System.out.println("Sconto effettuato --> €" + sconto);
        System.out.println("Totale sconto --> €" + totale);
    }

}
