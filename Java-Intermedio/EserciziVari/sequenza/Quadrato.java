package sequenza;

/**
 * @author Simone Tugnetti
 */
public class Quadrato {

    public static void main(String[] args) {
        // Dato il lato calcolare di un quadrato, calcolare il perimentro, l'area e la diagonale

        double lato = 1.25;

        //Operazioni
        double perimetro = lato * 4;
        double area = lato * lato;
        double diagonale = lato * Math.sqrt(2);

        System.out.println("Lato --> " + lato);
        System.out.println("Perimetro --> " + perimetro);
        System.out.println("Area --> " + area);
        System.out.println("Diagonale --> " + diagonale);

    }

}
