package sequenza;

/**
 * @author Simone Tugnetti
 */
public class SommaDueInteri {

    public static void main(String[] args) {
        /*
            Dati in input due numeri interi, calcolarne la somma e stampare a video
		
            Tipi interi
            byte	-8bit [-128,127] [-2^7,2^7-1]
            short	-16bit
            int		-32bit
            long	-64bit
         */

        int a = 12, b = 7, s = a + b;
        System.out.println(a + " + " + b + " = " + s);
    }

}
