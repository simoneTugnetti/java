package abstractclass;

/**
 * @author Simone Tugnetti
 */
public class Cubo extends Solido {

    //Il cubo e' un solido?
    //SI
    
    private double lato;

    public Cubo(double lato, Materiale materiale) {
        super(materiale);
        this.lato = lato;
    }

    public double getLato() {
        return lato;
    }

    public void setLato(double lato) {
        this.lato = lato;
    }

    @Override
    public double volume() {
        return Math.pow(lato, 3);
    }

    @Override
    public String toString() {
        return ""
                + "lato=" + lato + " dm, "
                + (super.toString() != null ? super.toString() : "");
    }

}
