package abstractclass;

/**
 * @author Simone Tugnetti
 */
public class TestSolido {

    public static void main(String[] args) {
        //Test Solido

        //riferimento per i pesi specifici
        //https://www.oppo.it/tabelle/pesi_specifici.html
        //Non si puo' istanziare un oggetto di classe astratta
        //Solido s=null;
        //s=new Solido(pesoSpecificoAcciaio);
        //s.volume();
        
        double lato = 1;
        Cubo c1 = new Cubo(lato, Materiale.ACCIAIO);
        System.out.println(c1);

        Cubo c2 = new Cubo(lato, Materiale.ALLUMINIO);
        System.out.println(c2);

        double raggio = 1;
        Sfera s1 = new Sfera(raggio, Materiale.ACCIAIO);
        System.out.println(s1);

        //Calcolare il solido dell'esacisicosaedro di tungsteno
    }

}
