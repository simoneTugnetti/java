package abstractclass;

/**
 * @author Simone Tugnetti
 */
public abstract class Solido {

    private Materiale materiale;

    public Solido(Materiale materiale) {
        this.materiale = materiale;
    }

    public void setMateriale(Materiale materiale) {
        this.materiale = materiale;
    }

    public Materiale getMateriale() {
        return materiale;
    }

    public double peso() {
        return this.materiale.getPesoSpecifico() * volume();
    }

    public abstract double volume();

    @Override
    public String toString() {
        return ""
                + "pesoSpecifico=" + materiale.getPesoSpecifico() + " kg/dm^3"
                + ", peso()=" + peso() + " kg"
                + ", volume()=" + volume() + " dm^3";
    }

}
