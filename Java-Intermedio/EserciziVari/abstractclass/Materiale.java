package abstractclass;

/**
 * @author Simone Tugnetti
 */
public enum Materiale {

    ACCIAIO(7.85), ALLUMINIO(2.60);

    private double pesoSpecifico;

    Materiale(double pesoSpecifico) {
        this.pesoSpecifico = pesoSpecifico;
    }

    public double getPesoSpecifico() {
        return this.pesoSpecifico;
    }
}
