package abstractclass;

/**
 * @author Simone Tugnetti
 */
public class Sfera extends Solido {

    private double raggio;

    public Sfera(double raggio, Materiale materiale) {
        super(materiale);
        this.raggio = raggio;
    }

    public double getRaggio() {
        return raggio;
    }

    public void setRaggio(double raggio) {
        this.raggio = raggio;
    }

    @Override
    public double volume() {
        return 4 * Math.PI * Math.pow(this.raggio, 3) / 3;
    }

    @Override
    public String toString() {
        return ""
                + "raggio=" + raggio + ", "
                + (super.toString() != null ? super.toString() : "") + "]";
    }

}
