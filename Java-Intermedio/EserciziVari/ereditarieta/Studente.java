package ereditarieta;

import java.util.GregorianCalendar;
import enumerazioni.Sesso;

/**
 * @author Simone Tugnetti
 */
public final class Studente extends Persona {

    //Attributi
    private String matricola;
    private String corso;

    public Studente() {
    }

    public Studente(String nome, String cognome) {
        super(nome, cognome);
    }

    public Studente(String nome, String cognome, GregorianCalendar dataNascita) {
        super(nome, cognome, dataNascita);
    }

    public Studente(String nome, String cognome, GregorianCalendar dataNascita, 
            String luogoNascita) throws Exception {
        super(nome, cognome, dataNascita, luogoNascita);
    }

    public Studente(String nome, String cognome, GregorianCalendar dataNascita, 
            String luogoNascita, Sesso sesso) throws Exception {
        super(nome, cognome, dataNascita, luogoNascita, sesso);
    }

    public Studente(String nome, String cognome, GregorianCalendar dataNascita, 
            String luogoNascita, Sesso sesso, String matricola, String corso) 
            throws Exception {
        super(nome, cognome, dataNascita, luogoNascita, sesso);
        this.matricola = matricola;
        this.corso = corso;
    }

    public String getMatricola() {
        return matricola;
    }

    public void setMatricola(String matricola) {
        this.matricola = matricola;
    }

    public String getCorso() {
        return corso;
    }

    public void setCorso(String corso) {
        this.corso = corso;
    }

    @Override
    public String toString() {
        return "\nStudente"
                + (getNome() != null ? "\nNome = " + getNome() : "")
                + (getCognome() != null ? "\nCognome = " + getCognome() : "")
                + (getDataNascita() != null ? "\nData di nascita = " + getDataNascita() : "")
                + (getLuogoNascita() != null ? "\nLuogo di nascita = " + getLuogoNascita() : "")
                + (getSesso() != null ? "\nSesso = " + getSesso() : "")
                + (getDataNascita() != null ? "\nEta' = " + eta() : "")
                + (matricola != null ? "\nMatricola = " + matricola : "")
                + (corso != null ? "\nCorso = " + corso : "");
    }

}
