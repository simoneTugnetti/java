package ereditarieta;

/**
 * @author Simone Tugnetti
 */
public class Rettangolo extends Quadrilatero {

    public Rettangolo(double base, double altezza) {
        super(base, altezza, base, altezza);
    }

    public double area() {
        return super.getLato1() * super.getLato2();
    }

    public double diagonale() {
        return Math.sqrt(Math.pow(getLato1(), 2) + Math.pow(getLato2(), 2));
    }

    @Override
    public String toString() {
        return "\nRettangolo"
                + "\nBase = " + getLato1()
                + "\nAltezza = " + getLato2()
                + "\nPerimetro = " + perimetro()
                + "\nArea = " + area()
                + "\nDiagonale = " + diagonale();
    }

}
