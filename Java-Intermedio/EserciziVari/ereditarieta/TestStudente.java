package ereditarieta;

import java.util.GregorianCalendar;

/**
 * @author Simone Tugnetti
 */
public class TestStudente {

    public static void main(String[] args) {
        // Test

        Persona pino = new Persona();
        pino.setNome("Pino");
        try {
            pino.setDataNascita(new GregorianCalendar(1963, 1, 30));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        System.out.println(pino);

        Studente s = new Studente();
        s.setNome("Sergio");
        s.setMatricola("AS-1234");
        s.setCorso("Java for ever");
        System.out.println(s);
    }

}
