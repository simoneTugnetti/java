package ereditarieta;

/**
 * @author Simone Tugnetti
 */
public class Quadrato extends Rettangolo {

    public Quadrato(double lato) {
        super(lato, lato);
    }

    @Override
    public String toString() {
        return "\nQuadrato"
                + "\nLato = " + getLato1()
                + "\nPerimetro = " + perimetro()
                + "\nArea = " + area()
                + "\nDiagonale = " + diagonale();
    }

}
