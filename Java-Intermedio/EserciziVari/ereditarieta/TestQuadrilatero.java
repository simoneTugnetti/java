package ereditarieta;

/**
 * @author Simone Tugnetti
 */
public class TestQuadrilatero {

    public static void main(String[] args) {
        // Test

        Quadrilatero q1 = new Quadrilatero(2.5, 3.9, 2.0, 7.5);
        System.out.println(q1);

        Rettangolo r = new Rettangolo(1.25, 1.30);
        System.out.println(r);

        Quadrato q2 = new Quadrato(1.25);
        System.out.println(q2);
    }

}
