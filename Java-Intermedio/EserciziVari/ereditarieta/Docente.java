package ereditarieta;

import java.util.GregorianCalendar;
import enumerazioni.Sesso;

/**
 * @author Simone Tugnetti
 */
public class Docente extends Persona {

    private String materia;

    public Docente() {
        super();
    }

    public Docente(String nome, String cognome, GregorianCalendar dataNascita, 
            String luogoNascita, Sesso sesso) throws Exception {
        super(nome, cognome, dataNascita, luogoNascita, sesso);
    }

    public Docente(String nome, String cognome, GregorianCalendar dataNascita, 
            String luogoNascita) throws Exception {
        super(nome, cognome, dataNascita, luogoNascita);
    }

    public Docente(String nome, String cognome, GregorianCalendar dataNascita) {
        super(nome, cognome, dataNascita);
    }

    public Docente(String nome, String cognome) {
        super(nome, cognome);
    }

    public Docente(String nome, String cognome, GregorianCalendar dataNascita, 
            String luogoNascita, Sesso sesso, String materia) throws Exception {
        super(nome, cognome, dataNascita, luogoNascita, sesso);
        this.materia = materia;
    }

    public String getMateria() {
        return materia;
    }

    public void setMateria(String materia) {
        this.materia = materia;
    }

    @Override
    public String toString() {
        return "\nDocente"
                + (getNome() != null ? "\nNome = " + getNome() : "")
                + (getCognome() != null ? "\nCognome = " + getCognome() : "")
                + (getDataNascita() != null ? "\nData di nascita = " + getDataNascita() : "")
                + (getLuogoNascita() != null ? "\nLuogo di nascita = " + getLuogoNascita() : "")
                + (getSesso() != null ? "\nSesso = " + getSesso() : "")
                + (getDataNascita() != null ? "\nEta = " + eta() : "")
                + (materia != null ? "\nMateria = " + materia : "");
    }

}
