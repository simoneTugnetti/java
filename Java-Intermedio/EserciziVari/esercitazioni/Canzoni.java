package esercitazioni;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Simone Tugnetti
 */
public class Canzoni {

    //Attributi
    private String[] nome;
    private int[] durata;
    private int capienza;
    private int tot;

    public Canzoni() {
        tot = 0;
        capienza = 79;
        nome = new String[10];
        durata = new int[10];
    }

    public int getCapienza() {
        return capienza;
    }

    public void setCapienza(int capienza) {
        this.capienza = capienza;
    }

    public int getTot() {
        return tot;
    }

    public void setTot(int tot) {
        this.tot = tot;
    }

    public Canzoni(String[] nome, int[] durata) {
        this.nome = nome;
        this.durata = durata;
    }

    public String[] getNome() {
        return nome;
    }

    public String getNome(int i) {
        return nome[i];
    }

    public void setNome(String[] nome) {
        this.nome = nome;
    }

    public void setNome(int i, String nome) {
        this.nome[i] = nome;
    }

    public int[] getDurata() {
        return durata;
    }

    public int getDurata(int i) {
        return durata[i];
    }

    public void setDurata(int[] durata) {
        this.durata = durata;
    }

    public void setDurata(int i, int durata) {

        this.durata[i] = durata;
    }

    public void ModNome(String nome, String nuovoNome) throws Exception {
        boolean trov = false;
        for (int i = 0; i < this.nome.length; i++) {
            if (this.nome[i] == nome) {
                this.nome[i] = nuovoNome;
                trov = true;
            }
        }
        if (trov == false) {
            throw new Exception("Non e' stato trovato alcun cantante!");
        }
    }

    public void ModDurata(String nome, int durata) throws Exception {
        boolean trov = false;
        for (int i = 0; i < this.nome.length; i++) {
            if (this.nome[i] == nome) {
                this.durata[i] = durata;
                trov = true;
            }
        }
        if (trov == false) {
            throw new Exception("Non e' stato trovato alcun cantante!");
        }
    }

    public String Ora(String nome) {
        for (int i = 0; i < this.nome.length; i++) {
            if (this.nome[i] == nome) {
                String sdf = new SimpleDateFormat("HH:mm:ss")
                        .format(new Date(durata[i]));
                return sdf;
            }
        }
        return "Non e' stato trovato alcun cantante!";
    }

    @Override
    public String toString() {
        return "Canzoni [" + (nome != null ? "nome=" + nome + ", " : "") 
                + (durata != null ? "durata=" + durata : "")
                + "]";
    }

}
