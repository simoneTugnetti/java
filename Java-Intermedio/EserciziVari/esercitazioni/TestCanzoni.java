package esercitazioni;

import java.util.Scanner;

/**
 * @author Simone Tugnetti
 */
public class TestCanzoni {

    public static void main(String[] args) {
        // Test Canzoni

        String[] nomi = new String[10];
        int[] durate = new int[10];
        int durata = 0;
        String nome;

        Canzoni cd = new Canzoni();
        Scanner tastiera = new Scanner(System.in);
        for (int i = 0; i < 10; i++) {
            do {
                System.out.println("Inserisci il nome: ");
                nome = tastiera.nextLine();
                System.out.println("Inserisci la durata: ");
                durata = tastiera.nextInt();
            } while (durata + cd.getTot() > cd.getCapienza());
            nomi[i] = nome;
            durate[i] = durata;
            cd.setTot(durata + cd.getTot());
        }
        System.out.println(cd);
        System.out.println(cd.Ora("Bella"));
        tastiera.close();
    }

}
