package fileOggetti;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

/**
 * @author Simone Tugnetti
 */
public class ScritturaOggetto {

    public static void main(String[] args) {
        // Scrivo oggetto su file di binari

        String path = "src/fileOggetti/elenco.dat";

        //Creare un oggetto di tipo prodotto
        Prodotto p1 = new Prodotto(1, "Mouse", "E' bello", 10.25, 100);
        Prodotto p2 = new Prodotto(2, "Tastiera", "E' brutto", 4.70, 90);
        Prodotto p3 = new Prodotto(3, "Monitor", "E' discreto", 22.65, 10);

        //Classi per salvare gli oggetti su disco
        FileOutputStream fos = null; //Consente l'accesso al file
        ObjectOutputStream oos = null; //Consente di eseguire le operazioni sul file

        try {
            fos = new FileOutputStream(path);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(p1);
            oos.writeObject(p2);
            oos.writeObject(p3);
            oos.flush();
            oos.close();
            fos.close();
            System.out.println("Operazione eseguita con successo!");
        } catch (IOException e) {
            System.out.println("Errore:\n" + e.getMessage());
        }
    }

}
