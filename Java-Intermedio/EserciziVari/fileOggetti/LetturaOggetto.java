package fileOggetti;

import java.io.EOFException;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

/**
 * @author Simone Tugnetti
 */
public class LetturaOggetto {

    public static void main(String[] args) {
        // Lettura da file di oggetti binari

        String path = "src/fileOggetti/elenco.dat";

        Prodotto p1 = null;
        String txt = "";

        FileInputStream fis = null;
        ObjectInputStream ois = null;

        try {
            fis = new FileInputStream(path);
            ois = new ObjectInputStream(fis);
            while (true) {
                try {
                    p1 = (Prodotto) ois.readObject();
                    txt += "\n\n" + p1.toString();
                } catch (EOFException | ClassNotFoundException e) {
                    break;
                }
            }
            System.out.println(txt);
            fis.close();
            ois.close();
        } catch (IOException e) {
            System.out.println("Errori:\n" + e.getMessage());
        }
    }

}
