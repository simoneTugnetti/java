package enumerazioni;

/**
 * @author Simone Tugnetti
 */
public class TestEnumerazione {

    public static void main(String[] args) {
        // tipo enumeration

        System.out.println(Alimentazione.BENZINA);

        Alimentazione[] elenco = Alimentazione.values();

        for (int i = 0; i < elenco.length; i++) {
            System.out.println("\n" + elenco[i]);
        }
    }

}
