package enumerazioni;

/**
 * @author Simone Tugnetti
 */
public enum Sesso {
    ALTRO, F, M;
}
