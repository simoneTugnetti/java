package enumerazioni;

/**
 * @author Simone Tugnetti
 */
public enum MonetaEuro {
    UNCENTESIMO(0.01), DUECENTESIMI(0.02), CINQUECENTESIMI(0.05),
    DIECICENTESIMI(0.10), VENTICENTESIMI(0.20), CINQUANTACENTESIMI(0.50),
    UNOEURO(1.00), DUEEURO(2.00), CINQUEEURO(5.00), DIECIEURO(10.00), 
    VENTIEURO(20.00), CINQUANTAEURO(50.00), CENTOEURO(100.00), 
    DUECENTOEURO(200.00), CINQUECENTOEURO(500.00), MILLEEURO(1000.00);

    private double valore;

    MonetaEuro(double valore) {
        this.valore = valore;
    }

    public double getValore() {
        return valore;
    }

}
