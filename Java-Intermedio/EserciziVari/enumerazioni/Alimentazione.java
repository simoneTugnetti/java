package enumerazioni;

/**
 * @author Simone Tugnetti
 */
public enum Alimentazione {
    BENZINA, DIESEL, GPL, METANO, ELETTRICA;
}
