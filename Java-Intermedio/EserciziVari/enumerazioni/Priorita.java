package enumerazioni;

/**
 * @author Simone Tugnetti
 */
public enum Priorita {
    BASSA, NORMALE, ALTA;
}
