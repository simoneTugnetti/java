package collezioni;

import classi.Persona;
import classi.Punto;
import classi.Quadrato;
import java.util.Vector;

/**
 * @author Simone Tugnetti
 */
public class GestioneVector {

    public static void main(String[] args) {
        // Gestione di un Vector

        Vector elenco = new Vector(5, 2);
        System.out.println("Capacita': " + elenco.capacity());
        System.out.println("Dimensione: " + elenco.size());

        elenco.add(new Punto());
        elenco.add(new Punto());
        elenco.add(new Punto());
        elenco.add(new Quadrato());
        elenco.add(12);
        elenco.add(12.75);
        elenco.add(12.25F);
        elenco.add("Pino");
        elenco.add(false);
        elenco.add('$');
        elenco.add(new Persona());

        System.out.println("\nCapacita': " + elenco.capacity());
        System.out.println("Dimensione: " + elenco.size());

        System.out.println();

        for (int i = 0; i < elenco.size(); i++) {
            System.out.println(elenco.get(i));
        }

        System.out.println();

        elenco.add(4, 11.05);
        elenco.set(6, -125);
        Object o = elenco.remove(8);
        System.out.println("E' stato rimosso l'elemento " + o);
        Persona pino = new Persona("Pino", "Caluso");
        elenco.add(pino);
        boolean trovato = elenco.remove(pino);
        if (trovato) {
            System.out.println("E' stato rimosso l'elemento: " + pino);
        } else {
            System.out.println("Non e' stato rimosso l'elemento: " + pino);
        }

        System.out.println();

        for (Object obj : elenco) {
            System.out.println(obj);
        }

    }

}
