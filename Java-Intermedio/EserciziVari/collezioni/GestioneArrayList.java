package collezioni;

import classi.Persona;
import classi.Punto;
import java.util.ArrayList;

/**
 * @author Simone Tugnetti
 */
public class GestioneArrayList {

    public static void main(String[] args) {
        // ArrayList

        ArrayList lista = new ArrayList();
        System.out.println("Dimensione: " + lista.size());

        lista.add(new Persona("Pino", "Deluso"));
        lista.add(new Persona("Rino", "Alcaro"));
        lista.add(new Persona("Mino", "Bocchio"));
        //lista.add(new Auto());
        lista.add(new Punto("P", 1, 2));
        lista.add(new Punto("Q", -1, -2));
        lista.add("Stringa a caso");
        System.out.println("Dimensione: " + lista.size());

        for (Object obj : lista) {
            System.out.println(obj);
        }

        lista.clear();

        for (Object obj : lista) {
            System.out.println(obj);
        }

        System.out.println("Dimensione: " + lista.size());

        ArrayList<Persona> persone = new ArrayList<Persona>();
        persone.add(new Persona("Pino", "Deluso"));
        persone.add(new Persona("Rino", "Alcaro"));
        persone.add(new Persona("Mino", "Bocchio"));

        for (Persona p : persone) {
            System.out.println(p);
        }
    }

}
