package collezioni;

import classi.Punto;

/**
 * @author Simone Tugnetti
 */
public class ArrayOggetti {

    public static void main(String[] args) {
        // Collezione di dati

        Punto[] elenco = new Punto[3];

        for (Punto p : elenco) {
            System.out.println(p);
        }

        Punto p1 = new Punto("P1", 1.25, 1.5);
        Punto p2 = new Punto("P1", -1.25, 1.5);
        Punto p3 = new Punto("P1", 1.25, -1.5);
        elenco[0] = p1;
        elenco[1] = p2;
        elenco[2] = p3;

        for (int i = 0; i < elenco.length; i++) {
            System.out.println(elenco[i]);
        }

    }

}
