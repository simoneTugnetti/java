package fileTesto;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author Simone Tugnetti
 */
public class WriteMessage {

    public static void main(String[] args) {
        Message m = new Message("xxx@xxx.it", "yyy@yyy.it", "zzz@zzz.it", 
                "www@www.it", "Saluti", "Saluti e salve", Priority.HIGH);
        FileWriter fw = null; //Accesso al file in modalita' scrittura
        PrintWriter pw = null; //Esegue le operazioni di modifica su file

        String path = "src/fileTesto/Messaggi.txt";

        try {
            fw = new FileWriter(path, true); //Modalita' append
            pw = new PrintWriter(fw);
            pw.write(m.toString()); //Scrivo su file
            pw.flush(); //Salvo il contenuto del flusso all'interno del file
            pw.close();
            fw.close();
            System.out.println("Operazione avvenuta con successo!");
        } catch (IOException e) {
            System.out.println("Si sono verificati i seguenti errori:\n" 
                    + e.getMessage());
        }
    }
}
