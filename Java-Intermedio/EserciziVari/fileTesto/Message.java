package fileTesto;

/**
 * @author Simone Tugnetti
 */
public class Message {

    private String form;
    private String to;
    private String cc;
    private String bcc;
    private String object;
    private String text;
    private Priority priority;

    public Message(String form, String to, String object, String text) {
        this.form = form;
        this.to = to;
        this.object = object;
        this.text = text;
        this.priority = Priority.NORMAL;
    }

    public Message(String form, String to, String cc, String bcc, String object, 
            String text) {
        this(form, to, object, text);
        this.cc = cc;
        this.bcc = bcc;
    }

    public Message(String form, String to, String cc, String bcc, String object,
            String text, Priority priority) {
        this(form, to, cc, bcc, object, text);
        this.priority = priority;
    }

    public String getForm() {
        return form;
    }

    public void setForm(String form) {
        this.form = form;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getCc() {
        return cc;
    }

    public void setCc(String cc) {
        this.cc = cc;
    }

    public String getBcc() {
        return bcc;
    }

    public void setBcc(String bcc) {
        this.bcc = bcc;
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Priority getPriority() {
        return priority;
    }

    public void setPriority(Priority priority) {
        this.priority = priority;
    }

    @Override
    public String toString() {
        return "Message [\n"
                + (form != null ? "form=" + form + ",\n " : "")
                + (to != null ? "to=" + to + ",\n " : "")
                + (cc != null ? "cc=" + cc + ",\n " : "")
                + (bcc != null ? "bcc=" + bcc + ",\n" : "")
                + (object != null ? "object=" + object + ", \n" : "")
                + (text != null ? "text=" + text + ", \n" : "")
                + (priority != null ? "priority=" + priority : "")
                + "\n]";
    }

}
