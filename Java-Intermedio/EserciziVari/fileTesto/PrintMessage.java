package fileTesto;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * @author Simone Tugnetti
 */
public class PrintMessage {

    public static void main(String[] args) {
        //Lettura su file di testo

        String path = "src/fileTesto/Messaggi.txt";

        File f = new File(path);

        if (!f.exists()) {
            System.out.println("File non trovato!");
            return;
        }

        FileReader fr = null; //Accesso al file in modalita' lettura
        BufferedReader br = null; //Buffer per la gestione dei dati

        try {
            fr = new FileReader(path);
            br = new BufferedReader(fr);
            String line = br.readLine();
            while (line != null) {
                System.out.println(line);
                line = br.readLine();
            }
        } catch (IOException e) {
            System.out.println("Errore:\n" + e.getMessage());
        }

    }

}
