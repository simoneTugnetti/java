package fileTesto;

/**
 * @author Simone Tugnetti
 */
public enum Priority {
    LOW, NORMAL, HIGH;
}
