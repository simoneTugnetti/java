package fileTesto;

import java.io.File;
import java.io.IOException;

/**
 * @author Simone Tugnetti
 */
public class CreateFile {

    public static void main(String[] args) {
        //Creare un file di testo se non esiste gia'

        String path = "src/fileTesto/file.txt";

        File f = new File(path);

        if (f.exists()) {
            System.out.println("File trovato!");
        } else {
            try {
                if (f.createNewFile()) {
                    System.out.println("File creato!");
                }
            } catch (IOException e) {
                System.out.println("Si sono verificati i seguenti errori:\n" 
                        + e.getMessage());
            }
        }
    }
}
