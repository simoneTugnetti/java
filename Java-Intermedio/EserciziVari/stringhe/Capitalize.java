package stringhe;

import java.util.Scanner;

/**
 * @author Simone Tugnetti
 */
public class Capitalize {

    public static void main(String[] args) {
        // Trasformare ogni iniziale maiuscola di una stringa

        Scanner tastiera = new Scanner(System.in);

        System.out.println("Inserisci una frase!!");
        String tmp = tastiera.nextLine();
        tastiera.close();
        System.out.println();

        String capitalize = tmp.substring(0, 1).toUpperCase() +
                tmp.substring(1).toLowerCase();

        System.out.println(capitalize);

    }

}
