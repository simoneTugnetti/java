package stringhe;

import java.util.Random;
import java.util.Scanner;

/**
 * @author Simone Tugnetti
 */
public class GeneraPasswordCasuale {

    public static void main(String[] args) {
        // Genera password casuale

        String numbers = "0123456789";
        String caratchers = "abcdefghijklmnopqrstuvwxyz";
        String specials = "$&@_!+*";

        String pattern = caratchers + caratchers.toUpperCase() + numbers + specials;
        System.out.println(pattern);

        Scanner tastiera = new Scanner(System.in);
        System.out.println("Lunghezza password: ");
        int length = tastiera.nextInt();
        tastiera.close();
        System.out.println();

        String psw = "";
        Random r = new Random();

        for (int i = 0; i < length; i++) {
            psw += pattern.charAt(r.nextInt(pattern.length() - 1));
        }

        System.out.println("Password --> " + psw);
    }

}
