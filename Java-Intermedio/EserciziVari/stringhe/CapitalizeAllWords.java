package stringhe;

import java.util.Scanner;

/**
 * @author Simone Tugnetti
 */
public class CapitalizeAllWords {

    public static void main(String[] args) {
        // Trasformare ogni iniziale maiuscola di una stringa

        Scanner tastiera = new Scanner(System.in);

        System.out.println("Inserisci una frase!!");
        String tmp = tastiera.nextLine();
        tastiera.close();
        System.out.println();

        String[] words = tmp.split(" ");
        for (int i = 0; i < words.length; i++) {
            words[i] = words[i].substring(0, 1).toUpperCase() +
                    words[i].substring(1).toLowerCase();
        }

        System.out.println(String.join(" ", words));

    }

}
