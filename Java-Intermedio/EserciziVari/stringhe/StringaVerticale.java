package stringhe;

import java.util.Scanner;

/**
 * @author Simone Tugnetti
 */
public class StringaVerticale {

    public static void main(String[] args) {
        // Stampa di una stringa in verticale

        Scanner tastiera = new Scanner(System.in);

        System.out.println("Inserisci una frase!!");
        String tmp = tastiera.nextLine();
        tastiera.close();
        System.out.println();

        String space = " ";
        for (int i = 0; i < tmp.length(); i++) {
            for (int j = i; j > 0; j--) {
                System.out.print(space);
            }
            System.out.println(tmp.charAt(i));
        }
    }

}
