package array;

/**
 * @author Simone Tugnetti
 */
public class Array5Reali {

    public static void main(String[] args) {
        // Array di 5 numeri reali

        double[] numeri = new double[5];

        numeri[0] = -1;
        numeri[1] = 12.75;

        //Lettura
        for (int i = 0; i < 5; i++) {
            System.out.println("Indice: " + i + " - valore: " + numeri[i]);
        }

    }

}
