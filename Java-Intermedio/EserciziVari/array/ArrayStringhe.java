package array;

/**
 * @author Simone Tugnetti
 */
public class ArrayStringhe {

    public static void main(String[] args) {
        // Array di stringhe

        //Dichiarazione lineare
        String[] frutti = {"pompelmo", "limone", "bergamotto", "arancia", "mandarino"};
        for (int i = 0; i < frutti.length; i++) {
            System.out.println(frutti[i]);
        }

    }

}
