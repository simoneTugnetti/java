package array;

/**
 * @author Simone Tugnetti
 */
public class ArrayCinqueInteri {

    public static void main(String[] args) {
        // Array di 5 numeri interi

        //Dichiarazione
        //tipo [] identificatore = new tipo[dimensione]
        int[] numeri = new int[5];

        //Caricamento
        numeri[0] = 12;
        numeri[1] = 1;
        numeri[2] = 4;
        numeri[3] = 6;
        numeri[4] = 3;

        //Lettura
        System.out.println("Indice: 2; valore : " + numeri[2]);
        System.out.println("Indice: 4; valore : " + numeri[4]);
    }

}
