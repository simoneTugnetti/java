package array;

import funzioni.MyLibrary;

/**
 * @author Simone Tugnetti
 */
public class RuoteLotto {

    public static void main(String[] args) {
        // Ruote del lotto

        String[] ruote = {"Bari", "Cagliari", "Firenze", "Genova"};

        int[] estrazioni = new int[5];

        estrazioni[0] = MyLibrary.casuale(1, 90);
        boolean flag;
        int e = 0;
        //Riempimento dell'array
        for (int i = 1; i < estrazioni.length; i++) {
            //Controllo
            do {
                flag = false;
                e = MyLibrary.casuale(1, 90);
                for (int j = 0; j < i; j++) {
                    if (estrazioni[j] == e) {
                        flag = true;
                        break;
                    }
                }
            } while (flag);
            estrazioni[i] = e;
        }
        System.out.print("Ruota: " + ruote[MyLibrary.casuale(1, ruote.length - 1)]);
        for (int i : estrazioni) {
            System.out.print(" " + i);
        }

        //Ciclo for migliorato - foreach
        for (String s : ruote) {
            System.out.println(s);
        }
    }

}
