package selezione;

/**
 * @author Simone Tugnetti
 */
public class ControlloData {

    public static void main(String[] args) {
        // Dato il giorno, il mese e l'anno di una data
        //Stampare la sequenza gg/mm/aaaa e dire se corretta o errata

        int g = 22, m = 3, a = 1985; //Dal 1500 in poi - calendario gregoriano

        String msg = ""
                + (g > 0 && g < 10 ? "0" : "") + g + "/"
                + (m > 0 && m < 10 ? "0" : "") + m + "/"
                + a;

        msg += " ";

        if (g < 1 || g < 31 || m < 1 || m > 12 || a < 1500) {
            msg += "errata";
        } else if ((m == 4 || m == 6 || m == 9 || m == 11) && g == 31) {
            msg += "errata";
        } else if (m == 2 && g > 29) {
            msg += "errata";
        } else if (m == 2 && a % 4 != 0 && g == 29) {
            msg += "errata";
        } else if (m == 2 && a % 100 == 0 && a % 400 != 0 && g == 29) {
            msg += "errata";
        } else {
            msg += "corretta";
        }
        System.out.println(msg);
    }
}
