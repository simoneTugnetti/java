package selezione;

/**
 * @author Simone Tugnetti
 */
public class Saluto {

    public static void main(String[] args) {
        // Data l'ora, stabilire il saluto da stampare
        //Buon giorno (6 - 14) 
        //Buon pomeriggio (14 - 18) 
        //Buona sera (18 - 22)
        //Buona notte (22 - 6)

        int h = 16;
        String msg;
        if (h < 0 || h > 23) {
            msg = "errore!";
        } else if (h >= 6 && h < 14) {
            msg = "Buon giorno!";
        } else if (h >= 14 && h < 18) {
            msg = "Buon pomeriggio!";
        } else if (h >= 18 && h < 22) {
            msg = "Buona sera!";
        } else {
            msg = "Buona notte!";
        }
        System.out.println(msg);
    }
}
