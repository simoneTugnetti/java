package selezione;

/**
 * @author Simone Tugnetti
 */
public class PrezzoQuantitaDeterminaPercentualeSconto {

    public static void main(String[] args) {
        // Dato il prezzo e la quantita' di acquisto di un prodotto, 
        // stampare il totale da pagare

        //Determinare la percentuale di sconto in base ai seguenti intervalli
        // 0-3 percentuale:0
        // 4-8 percentuale:7
        // 9-13 percentuale:11
        // 14-19 percentuale:17
        // 20+ percentuale:25
        double prezzo = 10.25;
        int quantita = 4;
        int percentualeSconto = 0;

        //Quantita' strettamente positiva
        if (quantita >= 4 && quantita <= 8) {
            percentualeSconto = 7;
        } else if (quantita >= 9 && quantita <= 13) {
            percentualeSconto = 11;
        } else if (quantita >= 14 && quantita <= 19) {
            percentualeSconto = 17;
        } else if (quantita >= 20) {
            percentualeSconto = 25;
        }

        double subTotale = prezzo * quantita;
        double sconto = subTotale * percentualeSconto / 100;
        double totale = subTotale - sconto;

        System.out.println("Prezzo --> " + prezzo);
        System.out.println("Quantita' --> " + quantita);
        System.out.println("Sconto --> " + percentualeSconto);
        System.out.println("Subtotale --> €" + subTotale);
        System.out.println("Sconto effettuato --> €" + sconto);
        System.out.println("Totale sconto --> €" + totale);
    }

}
