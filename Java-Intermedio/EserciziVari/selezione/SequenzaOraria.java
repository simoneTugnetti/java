package selezione;

/**
 * @author Simone Tugnetti
 */
public class SequenzaOraria {

    public static void main(String[] args) {
        // Dati ora, minuti e secondi, stampare la sequenza oraria e verificare se corretta o errata
        // es. HH:MM:SS corretta o errata
        //Uso operatore ternario ? --> Espressione ? true : false
        int h = 17, m = 25, s = 3;
        String msg = "corretta!";

        if ((h < 0 || h > 23) || (m < 0 || m > 59) || (s < 0 || s > 59)) {
            msg = "errata!";
        }
        String sequenza = (h < 10 ? "0" : "") + h + ":"
                + (m < 10 ? "0" : "") + m + ":"
                + (s < 10 ? "0" : "") + s;
        System.out.println("\nLa data" + sequenza + " e' " + msg);
    }

}
