package selezione;

/**
 * @author Simone Tugnetti
 */
public class PositivoNegativo {

    public static void main(String[] args) {
        // Dato un numero intero, stabilire se e' positivo o negativo

        int n = 12;
        if (n >= 0) {
            System.out.println(n + " e' positivo!");
        } else {
            System.out.println(n + " e' negativo!");
        }
    }

}
