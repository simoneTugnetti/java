package selezione;

/**
 * @author Simone Tugnetti
 */
public class PariDispari {

    public static void main(String[] args) {
        // Dato un numero intero, stabilire se e' pari o dispari

        int n = 7;

        if (n % 2 == 0) {
            System.out.println(n + " e' pari!");
        } else {
            System.out.println(n + " e' dispari!");
        }
    }

}
