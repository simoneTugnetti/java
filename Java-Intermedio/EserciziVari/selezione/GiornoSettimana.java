package selezione;

/**
 * @author Simone Tugnetti
 */
public class GiornoSettimana {

    public static void main(String[] args) {
        // Dato un numero intero [1-7]
        //Stampare il nome del giorno della settimana corrispondente
        //es. 1=lunedi', 2=martedi', ...

        int giorno = 7;
        String msg;
        switch (giorno) {
            case 1:
                msg = "lunedi'!";
                break;
            case 2:
                msg = "martedi'!";
                break;
            case 3:
                msg = "mercoledi'!";
                break;
            case 4:
                msg = "giovedi'!";
                break;
            case 5:
                msg = "venerdi'!";
                break;
            case 6:
                msg = "sabato!";
                break;
            case 7:
                msg = "domenica!";
                break;
            default:
                msg = "nessun giorno!";
                break;
        }
        System.out.println("Oggi e' " + msg);

    }

}
