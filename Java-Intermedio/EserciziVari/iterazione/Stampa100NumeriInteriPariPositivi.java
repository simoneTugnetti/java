package iterazione;

/**
 * @author Simone Tugnetti
 */
public class Stampa100NumeriInteriPariPositivi {

    public static void main(String[] args) {
        // Stampa 100 numeri pari interi positivi

        //for
        for (int i = 0; i < 100; i++) {
            System.out.println(i * 2);
        }
    }
}
