package iterazione;

/**
 * @author Simone Tugnetti
 */
public class FrequenzaLancioNDadi {

    public static void main(String[] args) {
        // Lancio di un dado a 6 facce n volte

        int dado;
        int n = 10;
        int conta1 = 0;
        int conta2 = 0;
        int conta3 = 0;
        int conta4 = 0;
        int conta5 = 0;
        int conta6 = 0;

        for (int i = 0; i < n; i++) {
            dado = (int) (Math.random() * 6) + 1;

            //Conta le uscite random
            switch (dado) {
                case 1:
                    conta1++;
                    break;
                case 2:
                    conta2++;
                    break;
                case 3:
                    conta3++;
                    break;
                case 4:
                    conta4++;
                    break;
                case 5:
                    conta5++;
                    break;
                case 6:
                    conta6++;
                    break;
            }
        }
        System.out.println("Numeri di lanci eseguiti: " + n);
        
        System.out.println("1: " + conta1 + "; frequenza: " + 
                ((double) conta1 / n * 100) + "%");
        
        System.out.println("2: " + conta2 + "; frequenza: " + 
                ((double) conta2 / n * 100) + "%");
        
        System.out.println("3: " + conta3 + "; frequenza: " + 
                ((double) conta3 / n * 100) + "%");
        
        System.out.println("4: " + conta4 + "; frequenza: " + 
                ((double) conta4 / n * 100) + "%");
        
        System.out.println("5: " + conta5 + "; frequenza: " + 
                ((double) conta5 / n * 100) + "%");
        
        System.out.println("6: " + conta6 + "; frequenza: " + 
                ((double) conta6 / n * 100) + "%");
    }
}
