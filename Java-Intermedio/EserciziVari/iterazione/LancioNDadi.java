package iterazione;

/**
 * @author Simone Tugnetti
 */
public class LancioNDadi {

    public static void main(String[] args) {
        // Lancio di un dado a 6 facce n volte

        int dado;
        int n = 10;

        for (int i = 0; i < n; i++) {
            dado = (int) (Math.random() * 6) + 1;
            System.out.println(dado);
        }
    }
}
