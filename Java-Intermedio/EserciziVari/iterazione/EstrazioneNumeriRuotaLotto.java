package iterazione;

/**
 * @author Simone Tugnetti
 */
public class EstrazioneNumeriRuotaLotto {

    public static void main(String[] args) {
        // Estrazione numeri ruota lotto

        //I numeri non possono essere ripetuti
        int e1 = (int) (Math.random() * 90) + 1;
        int e2 = 0;
        int e3 = 0;
        int e4 = 0;
        int e5 = 0;

        do {
            e2 = (int) (Math.random() * 90) + 1;
        } while (e2 == e1);
        do {
            e3 = (int) (Math.random() * 90) + 1;
        } while (e3 == e1 || e3 == e2);
        do {
            e4 = (int) (Math.random() * 90) + 1;
        } while (e4 == e1 || e4 == e2 || e4 == e3);
        do {
            e5 = (int) (Math.random() * 90) + 1;
        } while (e5 == e1 || e5 == e2 || e5 == e3 || e5 == e4);

        System.out.println(e1 + " " + e2 + " " + e3 + " " + e4 + " " + e5);
    }

}
