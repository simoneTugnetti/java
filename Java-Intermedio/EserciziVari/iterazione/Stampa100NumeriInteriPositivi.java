package iterazione;

/**
 * @author Simone Tugnetti
 */
public class Stampa100NumeriInteriPositivi {

    public static void main(String[] args) {
        // Stampa di 100 numeri interi positivi

        //Utilizzo di iterazione indefinita
        //while
        int count = 0;
        while (count < 100) {
            System.out.println(count);
            count++; //Incremento
        }
    }

}
