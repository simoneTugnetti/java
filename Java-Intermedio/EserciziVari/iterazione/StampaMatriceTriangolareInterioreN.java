package iterazione;

/**
 * @author Simone Tugnetti
 */
public class StampaMatriceTriangolareInterioreN {

    public static void main(String[] args) {
        // Data la misura della base, costruire la matrice triangolare 

        /*
		 * 0
		 * 00
		 * 000
		 * 0000
		 * 00000
		 * 000000
         */
        int n = 6;
        char c = '*';

        for (int i = 0; i < n; i++) { //Ciclo delle righe
            for (int j = 0; j <= i; j++) {
                System.out.print(c);
            }
            System.out.println();
        }
    }
}
