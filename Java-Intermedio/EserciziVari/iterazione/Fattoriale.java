package iterazione;

/**
 * @author Simone Tugnetti
 */
public class Fattoriale {

    public static void main(String[] args) {
        // Fattoriale di un numero naturale

        //5!=120;
        //5!=1*2*3*4*5=120
        int n = 5, j = 1;
        String msg = n + "!=";
        String txt = "";
        for (int i = n; i >= 1; i--) {
            txt += i + (i > 1 ? "*" : "=");
            j *= i;
        }
        System.out.println(msg + j);
        System.out.println(msg + txt + j);
    }
}
