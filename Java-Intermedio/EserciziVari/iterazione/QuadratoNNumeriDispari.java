package iterazione;

/**
 * @author Simone Tugnetti
 */
public class QuadratoNNumeriDispari {

    public static void main(String[] args) {
        // Quadrato di un numero intero positivo
        //Dato in input come somma di numeri dispari positivi creascenti

        // 11 => 121=1+3+5+...
        int n = 11; //input

        int s = 0;
        String txt = "";
        for (int i = 0; i < n; i++) {
            txt += (i == 0 ? "=" : "+") + (2 * i + 1);
            s += 2 * i + 1;
        }
        System.out.println(s + txt);
    }

}
