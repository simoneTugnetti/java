package matrici;

import funzioni.MyLibrary;

/**
 * @author Simone Tugnetti
 */
public class MatriceRettangolareInteri {

    public static void main(String[] args) {
        // Matrice di interi 3x2

        //Dichiarazione
        int[][] m = new int[3][2];

        //Caricamento di dati casuali interi positivi
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 2; j++) {
                m[i][j] = MyLibrary.casuale(1, 100);
            }
        }

        //Stampa
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 2; j++) {
                System.out.print(m[i][j] + "\t");
            }
            System.out.println();
        }
    }
}
