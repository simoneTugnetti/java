package matrici;

import funzioni.MyLibrary;

/**
 * @author Simone Tugnetti
 */
public class Milione {

    public static void main(String[] args) {
        int[][] matrice;
        matrice = new int[2][];
        matrice[0] = new int[3];
        matrice[1] = new int[5];

        for (int i = 0; i < matrice.length; i++) {
            for (int j = 0; j < matrice[i].length; j++) {
                matrice[i][j] = MyLibrary.casuale(1, 9);
            }
        }

        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < matrice[i].length; j++) {
                System.out.print(matrice[i][j]);
            }
            System.out.println();
        }

    }
}
