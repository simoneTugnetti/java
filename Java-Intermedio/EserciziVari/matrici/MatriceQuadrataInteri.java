package matrici;

/**
 * @author Simone Tugnetti
 */
public class MatriceQuadrataInteri {

    public static void main(String[] args) {
        // Matrice di numeri interi quadrata 2x2 (righe x colonne)

        /*
		 * 3 -2
		 * 1 12
         */
        //Dichiarazione di una matrice
        int righe = 2, colonne = 2;
        int[][] m = new int[righe][colonne];

        //Caricamento dei dati
        m[0][0] = 3;
        m[0][1] = -2;
        m[1][0] = 1;
        m[1][1] = 12;

        //Fase di stampa
        for (int i = 0; i < righe; i++) {
            for (int j = 0; j < colonne; j++) {
                System.out.print(m[i][j] + "\t");
            }
            System.out.println();
        }

    }

}
