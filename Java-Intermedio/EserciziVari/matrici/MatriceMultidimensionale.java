package matrici;

import funzioni.MyLibrary;

/**
 * @author Simone Tugnetti
 */
public class MatriceMultidimensionale {

    public static void main(String[] args) {
        // Matrice multidimensionale

        /*
		 * 7 2 3 7 90 12
		 * 9 8 5
		 * 45 98 4 7 2 1 6 3 5 7
		 * 1
         */
        //Dichiarazione della matrice
        int[][] m = new int[MyLibrary.casuale(1, 10)][];

        for (int i = 0; i < m.length; i++) {
            m[i] = new int[MyLibrary.casuale(1, 10)];
            for (int j = 0; j < m[i].length; j++) {
                m[i][j] = MyLibrary.casuale(1, 99);
            }
        }

        //Fase di lettura
        for (int i = 0; i < m.length; i++) {
            for (int j = 0; j < m[i].length; j++) {
                System.out.print(m[i][j] + " ");
            }
            System.out.println();
        }
    }

}
