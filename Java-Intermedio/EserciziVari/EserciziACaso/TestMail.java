package EserciziACaso;

import java.util.Scanner;
import enumerazioni.Priorita;

/**
 * @author Simone Tugnetti
 */
public class TestMail {

    public static void main(String[] args) {

        Scanner tastiera = new Scanner(System.in);
        System.out.print("Mittente: ");
        String mittente = tastiera.nextLine();
        System.out.print("Destinatario: ");
        String destinatario = tastiera.nextLine();
        System.out.print("Oggetto: ");
        String oggetto = tastiera.nextLine();
        System.out.print("Testo: ");
        String testo = tastiera.nextLine();
        System.out.println("Priorita': ");
        for (int i = 0; i < Priorita.values().length; i++) {
            System.out.println(i + ": " + Priorita.values()[i]);
        }
        int priorita = tastiera.nextInt();

        tastiera.close();

        Mail m = new Mail(mittente, destinatario, oggetto, testo);
        if (priorita == 0) {
            m.setPriorita(Priorita.BASSA);
        } else if (priorita == 2) {
            m.setPriorita(Priorita.ALTA);
        }

        System.out.println(m);
    }

}
