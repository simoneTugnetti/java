package EserciziACaso;

import enumerazioni.Priorita;

/**
 * @author Simone Tugnetti
 */
public class Mail {

    private String mittente;
    private String destinatario;
    private String oggetto;
    private String testo;
    private Priorita priorita;

    public Mail(String mittente, String destinatario, String oggetto, String testo) {
        this.mittente = mittente;
        this.destinatario = destinatario;
        this.oggetto = oggetto;
        this.testo = testo;
        this.priorita = Priorita.NORMALE;
    }

    public Mail(String mittente, String destinatario, String testo, 
            String oggetto, Priorita priorita) {
        this(mittente, destinatario, oggetto, testo);
        this.priorita = priorita;
    }

    public String getMittente() {
        return mittente;
    }

    public void setMittente(String mittente) {
        this.mittente = mittente;
    }

    public String getDestinatario() {
        return destinatario;
    }

    public void setDestinatario(String destinatario) {
        this.destinatario = destinatario;
    }

    public Priorita getPriorita() {
        return priorita;
    }

    public void setPriorita(Priorita priorita) {
        this.priorita = priorita;
    }

    public String getOggetto() {
        return oggetto;
    }

    public void setOggetto(String oggetto) {
        this.oggetto = oggetto;
    }

    @Override
    public String toString() {
        return "\n"
                + (destinatario != null ? "A: " + destinatario : "")
                + (mittente != null ? "\nDA: " + mittente : "")
                + (oggetto != null ? "\nOggetto: " + oggetto : "")
                + (testo != null ? "\nTesto: " + testo : "")
                + (priorita != null ? "\nPriorita': " + priorita : "");
    }

}
