package EserciziACaso;

import java.util.Scanner;

/**
 * @author Simone Tugnetti
 */
public class TestBanca {

    public static void main(String[] args) {
        // Test Banca

        Banca conto = new Banca(2310775);
        String msg = "\nCosa desidera?"
                + "\n1: Prelevare"
                + "\n2: Depositare"
                + "\n3: Visionare il conto"
                + "\n0: Uscire";
        Scanner tastiera = new Scanner(System.in);
        int scelta;
        do {
            System.out.println(msg);
            scelta = tastiera.nextInt();
            if (scelta == 1) {
                System.out.println("Quanto vuoi prelevare?");
                try {
                    conto.preleva(tastiera.nextInt());
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            } else if (scelta == 2) {
                System.out.println("Quanto vuoi depositare?");
                try {
                    conto.deposita(tastiera.nextInt());
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            } else if (scelta == 3) {
                System.out.println(conto);
            } else if (scelta == 0) {
                break;
            } else {
                System.out.println("Richiesta non valida!");
            }
        } while (true);
        tastiera.close();
        System.out.println("Arrivederci");

    }

}
