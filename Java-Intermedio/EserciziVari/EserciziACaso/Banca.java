package EserciziACaso;

/**
 * @author Simone Tugnetti
 */
public class Banca {

    private int conto;
    private int fondo;

    public Banca(int conto) {
        this.conto = conto;
        fondo = 0;
    }

    public int getConto() {
        return conto;
    }

    public int getFondo() {
        return fondo;
    }

    public void preleva(int prelievo) throws Exception {
        if (fondo >= prelievo) {
            fondo -= prelievo;
        } else {
            throw new Exception("Importo non valido");
        }
    }

    public void deposita(int deposito) throws Exception {
        if (deposito > 0 && deposito <= 1000) {
            fondo += deposito;
        } else {
            throw new Exception("Importo non valido");
        }
    }

    @Override
    public String toString() {
        return "\n"
                + "Numero di conto: " + getConto()
                + "\nFondo: " + getFondo();
    }
}
