package input;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

/**
 * @author Simone Tugnetti
 */
public class TestInput {

    public static void main(String[] args) {

        System.out.println();

        /**
         * Lettura da tastiera tramite Flusso
         * 
         * InputStream -> Viene letto il flusso di dati inseriti tramite input e salvati poi 
         * all'interno della variabile input
         * 
         * Buffered -> Viene letto il flusso di dati per poi essere restituito 
         * in un formato accettabile all'utente
        */
        InputStreamReader input = new InputStreamReader(System.in);
        BufferedReader tastiera = new BufferedReader(input);
        String nome = "";

        /**
         * Per poter eseguire la lettura dell'input, bisogna gestire l'eccezione
         * all'interno del metodo readLine()
         * 
         * Verranno inseriti i dati in formato String
         * 
         * IOException e' l'eccezione che gestisce gli Input e gli Output 
         * all'interno di un programma
        */
        try {
            nome = tastiera.readLine();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        /**
         * Lettura da tastiera tramite Scansione
        */
        Scanner secondaTastiera = new Scanner(System.in);
        String cognome = secondaTastiera.nextLine();
        int eta = secondaTastiera.nextInt();

        /**
         * Per poter reinserire una stringa dopo aver utilizzato nextInt(), 
         * nextDouble(), ... , bisogna prima utilizzare nextLine() senza alcuna 
         * istanza per poi procedere
        */
        secondaTastiera.nextLine();

        String luogoNascita = secondaTastiera.nextLine();

        /**
         * Bisogna sempre terminare il processo di scannerizzazione quando non
         * vi e' piu' necessita'
         */
        secondaTastiera.close(); 

        System.out.println(nome + ", " + cognome + ", " + eta + ", " + luogoNascita);
    }

}
