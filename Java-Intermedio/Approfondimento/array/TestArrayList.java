package array;

import java.util.ArrayList;

import classi.Triangolo;

/**
 * @author Simone Tugnetti
 */
public class TestArrayList {

    public static void main(String[] args) {

        /**
         * Viene creata un'istanza della classe ArrayList
         * In questo caso non si tratta pero' di una normale istanza, 
         * bensi' della creazione di un array di tipo Object avente come nome "lista"
         * Altrimenti denominato "generic array"
        */
        ArrayList lista = new ArrayList();

        /**
         * Il metodo size() e' direttamente paragonabile al metodo length e 
         * determina la grandezza dell'array partendo da 1 e non da 0
        */
        System.out.println("Dimensione: " + lista.size());

        /**
         * Il metodo add() permette l'inserimento di informazioni all'interno 
         * dell'array con conseguente aumento di dimensione
        */
        lista.add(new Triangolo(12, 16));
        lista.add(new Triangolo(1, 167));
        lista.add(12);
        lista.add(14.5);
        lista.add(true);
        lista.add("Stringa bella");

        System.out.println("\nStampa for:");

        //Stampa dell'intero array utilizzando un ciclo for
        for (int i = 0; i < lista.size(); i++) {

            /**
             * Il metodo get() restituise l'informazione contenuta nell'indice 
             * inserito tra parentesi, esattamente come se fosse inserito lista[i]
            */
            System.out.println(lista.get(i));

        }

        //La dimensione risultera' differente rispetto a prima
        System.out.println("\nDimensione: " + lista.size());

        System.out.println("\nStampa for migliorato");

        /**
         * Stampa dell'intero array utilizzando un ciclo for migliorato
         * La variabile deve essere di tipo Object in questo caso, cioe' generic
        */
        lista.forEach((obj) -> {
            System.out.println(obj);
        });

        System.out.println("\nStampa solo stringhe utilizzando instanceof");

        /**
         * Utilizzando la dicitura instanceof, si prendono in considerazione 
         * solo gli elementi che sono istanze di una determinata classe, 
         * in questo caso String
        */
        for (Object obj : lista) {
            if (obj instanceof String) {
                System.out.println(obj);
            }
        }

        /**
         * Verranno cancellate tutte le informazioni salvate all'interno dell'array
        */
        lista.clear();

        /**
         * In questo caso verra' creato un array dinamico di interi, 
         * modificando il tipo da Object a Integer
        */
        ArrayList<Integer> interi = new ArrayList<>();

        /**
         * Possono cosi' essere inseriti solo valori interi all'interno dell'array
        */
        interi.add(12);
        interi.add(145);
        interi.add(132);

        System.out.println("\nStampa solo interi utilizzando parentesi angolari");

        /**
         * Ora possono essere utilizzate solo variabili intere all'interno del 
         * for migliorato per stampare a video gli elementi
        */
        interi.forEach((val) -> {
            System.out.println(val);
        });

        /**
         * In questo caso verra' creato un array dinamico di numeri reali, 
         * modificando il tipo da Object a Double
        */
        ArrayList<Double> reali = new ArrayList<>();

        reali.add(12.65);
        reali.add(35.86);

        System.out.println("\nStampa solo reali, cioe' double, utilizzando parentesi angolari");
        for (int i = 0; i < reali.size(); i++) {
            System.out.println(reali.get(i));
        }

    }

}
