package array;

import classi.Triangolo;

/**
 * @author Simone Tugnetti
 */
public class TestArrayOggetti {

    public static void main(String[] args) {

        /**
         * Crea un array di Oggetti di tipo Triangolo con grandezza 3
         */
        Triangolo[] t3 = new Triangolo[3];

        /**
         * Ogni cella di memoria deve essere istanziata con il costruttore dello
         * stesso tipo dell'array In questo modo ci possono essere piu' istanze
         * di tipo Triangolo in base alle necessita'
         */
        for (int i = 0; i < t3.length; i++) {  //t3.length restituisce la grandezza dell'array t3
            t3[i] = new Triangolo();
        }

        /**
         * Verra' richiamato in automatico il metodo toString() per la stampa
         * delle informazioni nella cella di memoria corrispondente
         */
        for (Triangolo t31 : t3) {
            System.out.println(t31);
        }

    }

}
