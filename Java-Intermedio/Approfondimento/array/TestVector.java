package array;

import java.util.Vector;
import classi.Triangolo;

/**
 * @author Simone Tugnetti
 */
public class TestVector {

    public static void main(String[] args) {

        /**
         * Viene creata un'istanza della classe Vector In questo caso non si
         * tratta pero' di una normale istanza, bensi' della creazione di un
         * array di tipo Object avente come nome "elenco" Altrimenti denominato
         * "generic array" In questo caso, viene creata a priori la dimensione
         * dell'array come primo parametro Come secondo parametro, invece, viene
         * specificato di quanto deve aumentare la sua grandezza non appena
         * viene raggiunto il limite massimo In questo caso, di due in due
         */
        Vector elenco = new Vector(5, 2);

        /**
         * Il metodo capacity() ritorna la grandezza massima di cui dispone
         * l'array Il metodo size() ritorna il numero di celle occupate
         * all'interno dell'array Entrambi conteggiano partendo da 1 e non da 0
         */
        System.out.println("Capacit�: " + elenco.capacity());
        System.out.println("Dimensione: " + elenco.size());

        /**
         * Il metodo add() permette l'inserimento di informazioni all'interno
         * dell'array L'array aumentera' di dimensione solo quando verra'
         * superata la capacita' massima
         */
        elenco.add(new Triangolo(96, 71));
        elenco.add(12);
        elenco.add(12.75);
        elenco.add(45.89);
        elenco.add("Pino");

        /**
         * In questo caso, la capacit� e la dimensione avranno lo stesso valore
         */
        System.out.println("\nCapacit�: " + elenco.capacity());
        System.out.println("Dimensione: " + elenco.size());

        elenco.add(false);
        Triangolo t1 = new Triangolo();
        elenco.add(t1);

        /**
         * Ora, invece, la capacita' sara' aumentata di due unita' e la
         * dimensione dell'array, cioe' gli elementi al suo interno, sara' 7
         */
        System.out.println("\nCapacita': " + elenco.capacity());
        System.out.println("Dimensione: " + elenco.size());

        /**
         * Il medodo add(int,Object) viene definito per inserire all'interno di
         * una posizione specifica un determinato valore con conseguente scalata
         * dei dati preesistenti Come primo parametro si inserisce la posizione
         * e come secondo il valore
         */
        elenco.add(4, 11.05);

        /**
         * Il metodo set(int,Object) viene definito per modificare un valore
         * all'interno di una determinata posizione dell'array Come primo
         * parametro si inserisce la posizione e come secondo il nuovo valore
         * assegnato a tale posizione
         */
        elenco.set(6, -125);

        System.out.println("\nStampa tramite ciclo for");
        for (int i = 0; i < elenco.size(); i++) {
            System.out.println(elenco.get(i));
        }

        /**
         * Il metodo remove() viene utilizzato per poter eliminare un dato
         * all'interno di una determinata posizione dell'array Il metodo, in
         * questo caso, restituisce un valore di tipo Generics
         */
        Object o = elenco.remove(6);

        /**
         * In questo caso, il metodo remove() restituira' un booleano, in quanto
         * e' stata fatta richiesta di eliminare uno specifico oggetto
         * all'interno dell'array Cosi da poter definire se l'oggetto e' stato
         * eliminato o meno
         */
        boolean trov = elenco.remove(t1);

        System.out.println("\nStampa tramite ciclo for migliorato");
        elenco.forEach((obj) -> {
            System.out.println(obj);
        });

    }

}
