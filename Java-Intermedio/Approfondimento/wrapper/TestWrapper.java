package wrapper;

/**
 * @author Simone Tugnetti
 */
public class TestWrapper {

    public static void main(String[] args) {

        String valore = "1";
        int numero = Integer.parseInt(valore);  //convertira' '1' in 1

        //convertira' '1' in 1.0
        double puntoSpecifico = Double.parseDouble(valore);

        //convertira' '1' in 1.0 ma con una grandezza dopo il punto piu' 
        //limitata rispetto al double
        float punto = Float.parseFloat(valore);

        System.out.println(valore + ", " + numero + ", " + puntoSpecifico + ", " + punto);

    }

}
