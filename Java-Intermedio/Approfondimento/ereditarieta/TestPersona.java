package ereditarieta;

/**
 * @author Simone Tugnetti
 */
public class TestPersona {

    public static void main(String[] args) {
        Persona p1 = new Persona("Gianni", "Poggi", 21, 200);
        System.out.println(p1);
        Studente s1 = new Studente("Gianni", "Poggi", 21, 200, "Informatica", 21);
        System.out.println(s1);
        System.out.println(s1.acquistoCibo(300)); //Risultera' errato
        System.out.println(s1);
        System.out.println(s1.acquistoCibo(75)); //Risultera' corretto
        System.out.println(s1);
    }

}
