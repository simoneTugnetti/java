package ereditarieta;

/**
 * Questa classe ereditera' tutti i metodi all'interno di Persona, la classe
 * Studente puo' ereditare pero' una sola classe
 *
 * @author Simone Tugnetti
 */
public class Studente extends Persona { //Sottoclasse

    private String corso;
    private int livDisperazione;

    /**
     * Viene richiamato il costruttore senza passaggio di parametro all'interno
     * di Persona
     */
    public Studente() {
        super();
    }

    /**
     * Viene richiamato il costruttore con passaggio di parametro all'interno di
     * Persona, dove verranno inseriti i primi 4 parametri
     *
     * @param nome
     * @param cognome
     * @param eta
     * @param soldi
     * @param corso
     * @param livDisperazione
     */
    public Studente(String nome, String cognome, int eta, int soldi, String corso, int livDisperazione) {
        super(nome, cognome, eta, soldi);
        this.corso = corso;
        this.livDisperazione = livDisperazione;
    }

    public String getCorso() {
        return corso;
    }

    public void setCorso(String corso) {
        this.corso = corso;
    }

    public int getLivDisperazione() {
        return livDisperazione;
    }

    public void setLivDisperazione(int livDisperazione) {
        this.livDisperazione = livDisperazione;
    }

    /**
     * Metodo che sottrae i soldi in base all'acquisto effettuato
     *
     * Viene provato il metodo contenente l'eccezione per verificare se
     * incorrono errori o meno
     *
     * @param sottrai
     * @return Messaggio soldi sottratti
     */
    public String acquistoCibo(int sottrai) {
        String msg;

        try {

            //Viene utilizzato super per definire la superclasse dove e' 
            //contenuto il metodo sottraiSoldi
            super.sottraiSoldi(sottrai);

            msg = "Sono stati sottratti €" + sottrai + " dal tuo portafogli";

        } catch (Exception e) { //Viene presa l'eccezione solo se quest'ultima si verifica

            //Viene restituito il messaggio dell'Eccezione
            msg = e.getMessage();

            //Per poter visualizzare il messaggio tramite errore si deve 
            //utilizzare la dicitura e.printStackTrace()
        }
        return msg;
    }

    /**
     * Viene richiamato il metodo toString all'interno della superclasse
     * Persona, essendo questo contenente tutti gli attributi necessari
     * all'identificazione dello studente
     *
     * @return Riassunto classe
     */
    @Override
    public String toString() {
        return "Studente ["
                + (corso != null ? "corso=" + corso + ", " : "")
                + "livDisperazione=" + livDisperazione + ", "
                + (super.toString() != null ? super.toString() : "") + "]";
    }

}
