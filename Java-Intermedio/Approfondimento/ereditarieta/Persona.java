package ereditarieta;

/**
 * Persona puo' essere ereditato verso piu' classi
 *
 * @author Simone Tugnetti
 */
public class Persona { //Superclasse

    private String nome;
    private String cognome;
    private int eta;
    private int soldi;

    public Persona() {
    }

    public Persona(String nome, String cognome, int eta, int soldi) {
        this.nome = nome;
        this.cognome = cognome;
        this.eta = eta;
        this.soldi = soldi;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public int getEta() {
        return eta;
    }

    public void setEta(int eta) {
        this.eta = eta;
    }

    public int getSoldi() {
        return soldi;
    }

    public void setSoldi(int soldi) {
        this.soldi = soldi;
    }

    /**
     * Metodo usato per sottrarre i soldi in base ad un acquisto
     * Approfondimento! Viene utilizzato protected per essere visionato solo
     * dalle classi che eredita come alternativa al public (usare con cautela!)
     *
     * Viene utilizzato throws per specificare che l'eccezione deve essere
     * gestita verso l'esterno
     *
     * Quando occorrera', verra' creata una nuova eccezione con un determinato
     * messaggio da stampare su schermo
     *
     * Le Eccezioni sono un'ottima alternativa ai System.out.println(), essendo
     * che all'interno dei metodi, quest'ultimi, sono meno accetti
     *
     * @param soldiMeno
     * @throws Exception
     */
    protected void sottraiSoldi(int soldiMeno) throws Exception {
        if ((soldi - soldiMeno) >= 0) {
            soldi = soldi - soldiMeno;
        } else {
            throw new Exception("Non te lo puoi permettere!!");
        }
    }

    @Override
    public String toString() {
        return "Persona [" + (nome != null ? "nome=" + nome + ", " : "")
                + (cognome != null ? "cognome=" + cognome + ", " : "")
                + "eta=" + eta + ", "
                + "soldi=" + soldi + "]";
    }

}
