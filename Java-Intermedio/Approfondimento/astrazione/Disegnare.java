package astrazione;

/**
 * @author Simone Tugnetti
 */
public class Disegnare extends Penna {

    private String disegno;

    /**
     * Si dovranno sempre ereditare gli attributi della superclasse astratta
     * all'interno del costruttore della sottoclasse che eredita
     *
     * @param colore
     * @param tappo
     * @param disegno
     */
    public Disegnare(String colore, boolean tappo, String disegno) {
        super(colore, tappo);
        this.disegno = disegno;
    }

    public String getDisegno() {
        return disegno;
    }

    public void setDisegno(String disegno) {
        this.disegno = disegno;
    }

    /**
     * Il metodo astratto deve quindi essere riscritto, tramite Overriding, per
     * diventare cosi' operativo e richiamabile
     *
     * @return Il disegno
     */
    @Override
    public String azione() {
        disegno += "disegnare un cavallo";
        return disegno;
    }

    /**
     * Avendo cosi' incapsulato le informazioni all'interno del metodo azione(),
     * sara' possibile restituire tutti i dati senza incorrere in alcun errore
     *
     * @return La stringa riassuntiva
     */
    @Override
    public String toString() {
        return "" + super.toString();
    }

}
