package astrazione;

/**
 * @author Simone Tugnetti
 */
public class TestPenna {

    public static void main(String[] args) {

        String msg = "fare qualcosa di interessante come ";

        /*
		Non e' possibile istanziare una classe Astratta
			
		Penna p1=new Penna("arancione",true);
			
		Quest'ultima dar� errore!!
         */
        
        /**
         * E' possibile invece istanziare una classe che eredita la classe 
         * astratta passando come parametro sia gli attributi della classe 
         * astratta e sia gli attributi propri della classe Disegnare
        */
        Disegnare d1 = new Disegnare("giallo", true, msg);
        System.out.println(d1);

        System.out.println();

        Dipingere dip1 = new Dipingere("rosso", false, msg);
        System.out.println(dip1);

    }

}
