package astrazione;

/**
 * @author Simone Tugnetti
 */
public class Dipingere extends Penna {

    private String dipinto;

    public Dipingere(String colore, boolean tappo, String dipinto) {
        super(colore, tappo);
        this.dipinto = dipinto;
    }

    public String getDipinto() {
        return dipinto;
    }

    public void setDipinto(String dipinto) {
        this.dipinto = dipinto;
    }

    @Override
    public String azione() {
        dipinto += "dipingere un anatra";
        return dipinto;
    }

    @Override
    public String toString() {
        return "" + super.toString();
    }

}
