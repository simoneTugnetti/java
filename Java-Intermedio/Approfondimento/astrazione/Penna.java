package astrazione;

/**
 * Una classe Astratta si denota tramite la dicitura abstract dopo il metodo di
 * accesso
 *
 * @author Simone Tugnetti
 */
public abstract class Penna {

    private String colore;
    private boolean tappo;

    public Penna(String colore, boolean tappo) {
        this.colore = colore;
        this.tappo = tappo;
    }

    public String getColore() {
        return colore;
    }

    public void setColore(String colore) {
        this.colore = colore;
    }

    /**
     * Il metodo isTappo() e' l'equivalente di getTappo() solo che, di norma,
     * viene utilizzato per restituire gli attributi di tipo booleano
     *
     * @return Il tappo aperto o chiuso
     */
    public boolean isTappo() {
        return tappo;
    }

    public void setTappo(boolean tappo) {
        this.tappo = tappo;
    }

    /**
     * Questo metodo dovra' essere implementato attraverso la classe dove verra'
     * ereditato
     *
     * @return Azione della penna
     */
    public abstract String azione();

    @Override
    public String toString() {
        return "Sto usando una penna di colore " + colore + " "
                + (tappo == true ? "di cui non ho perso il tappo"
                        : "di cui ho perso il tappo")
                + " per " + azione() + "!!!";
    }

}
