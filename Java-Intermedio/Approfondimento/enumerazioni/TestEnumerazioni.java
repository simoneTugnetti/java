package enumerazioni;

/**
 * @author Simone Tugnetti
 */
public class TestEnumerazioni {

    public static void main(String[] args) {
        
        /**
         * Viene creato un array con gli elementi, cioe' le costanti, 
         * all' interno di Sesso
        */
        Sesso[] s1 = Sesso.values();

        /**
         * Stampera' tutti gli elementi all'interno dell'array s1
        */
        for (int i = 0; i < s1.length; i++) {
            System.out.println(s1[i]);
        }

        /**
         * Stampera' la costante all'interno dell'Enum FasciaEta denominata 
         * BAMBINO
        */
        System.out.println("\n" + FasciaEta.BAMBINO);

        /**
         * Stampera' il valore della costante ADULTO all'interno dell'Enum 
         * FasciaEta, cioe' 40
        */
        System.out.println(FasciaEta.ADULTO.getFasciaPersona());
    }

}
