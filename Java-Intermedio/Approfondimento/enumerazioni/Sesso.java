package enumerazioni;

/**
 * @author Simone Tugnetti
 */
public enum Sesso {
    
    /**
     * Le costanti devono essere sempre scritte in Maiuscolo
     */
    MASCHIO, FEMMINA, ALTRO;
}
