package enumerazioni;

/**
 * @author Simone Tugnetti
 */
public enum FasciaEta {
    /**
     * Le costanti, in questo caso, possono essere viste come costruttori che
     * passano come parametro il valore corrispondente all'interno delle
     * parentesi
     */
    BAMBINO(3), ADOLESCENTE(16), ADULTO(40), ANZIANO(70);

    private final int FasciaPersona;

    /**
     * Costruttore delle costanti, dove vengono assegnati all'attributo i valori
     * di default
     */
    FasciaEta(int FasciaPersona) {
        this.FasciaPersona = FasciaPersona;
    }

    public int getFasciaPersona() {
        return FasciaPersona;
    }
}
