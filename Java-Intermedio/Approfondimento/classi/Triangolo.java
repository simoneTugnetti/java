package classi;

/**
 * @author Simone Tugnetti
 */
public class Triangolo {

    private int a;
    private int b;

    /**
     * Costruttore di default, cioe' senza passaggi di parametro
     */
    public Triangolo() {
    }

    /**
     * Costruttore avente passaggi di parametro this e' il puntatore alla classe
     * sul quale e' inserito (cioe' Triangolo) con riferimento all' attributo b.
     *
     * @param parametroA
     * @param parametroB
     */
    public Triangolo(int parametroA, int parametroB) {
        this.a = parametroA;
        this.b = parametroB;
    }

    /**
     * Metodo getter, utilizzato cio� per la restituzione dei valori all'interno
     * degli attributi
     *
     * @return A
     */
    public int getA() {
        return a;
    }

    /**
     * Metodo setter, utilizzato cio� per la modifica dei valori all'interno
     * degli attributi
     *
     * @param nuovoA
     */
    public void setA(int nuovoA) {
        this.a = nuovoA;
    }

    public int getB() {
        return b;
    }

    public void setB(int nuovoB) {
        this.b = nuovoB;
    }

    public String stampa() {
        return a + " " + b;
    }

    /**
     * Metodo di default per la stampa delle informazioni all'interno della
     * classe
     *
     * @return Riassunto classe
     */
    @Override
    public String toString() {
        return a + " " + b;
    }

}
