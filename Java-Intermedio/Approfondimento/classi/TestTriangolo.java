package classi;

/**
 * @author Simone Tugnetti
 */
public class TestTriangolo {

    public static void main(String[] args) {

        /**
         * Istanza di un oggetto di tipo Triangolo che utilizza il costruttore
         * senza passaggi di parametro
         */
        Triangolo t1 = new Triangolo();

        /**
         * Istanza di un oggetto di tipo Triangolo che utilizza il costruttore
         * con passaggi di parametro
         */
        Triangolo t2 = new Triangolo(12, 15);

        /**
         * t2 e' il puntatore alla classe Triangolo con riferimento al metodo
         * stampa()
         */
        System.out.println(t1.stampa());

        /**
         * Verra' richiamato in automatico il metodo toString() per la stampa
         * delle informazioni.
         */
        System.out.println(t2);

    }

}
