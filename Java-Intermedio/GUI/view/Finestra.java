package view;

import java.awt.Label;
import java.awt.event.WindowEvent;
import javax.swing.JFrame;

/**
 * @author Simone Tugnetti
 */
public class Finestra {

    public static void main(String[] args) {
        JFrame f = new JFrame();
        f.dispatchEvent(new WindowEvent(f, WindowEvent.WINDOW_CLOSING));
        f.setSize(800, 600);
        f.setLocation(100, 250);
        f.setTitle("La mia finestra");
        f.setVisible(true);

        Label l = new Label("Testo di etichetta");
        l.setVisible(true);

        f.add(l);
    }
}
