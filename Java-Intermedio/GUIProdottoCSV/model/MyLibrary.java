package model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 * @author Simone Tugnetti
 */
public class MyLibrary {
    
    //Lettura da file CSV
    public static ArrayList<Prodotto> read(String path) throws Exception{
        ArrayList<Prodotto> lista = new ArrayList<Prodotto>();
        
        //Riempire la lista di oggetti di tipo Prodotto
        File f=new File(path);
        if(!f.exists()){
            throw new Exception("File non trovato");
        }
        
        //Lettura da file di testo
        FileReader fr=null;
        BufferedReader br=null;
        try{
            Prodotto p=null;
            fr=new FileReader(path);
            br=new BufferedReader(fr);
            String[] words;
            String line=br.readLine();
            while(line!=null){
                words=line.split(";"); //Separa una stringa in sottostringhe delimitate da un carattere
                p=new Prodotto();
                p.setCodice(words[0]);
                p.setNome(words[1]);
                p.setDescrizione(words[2]);
                p.setPrezzo(Double.parseDouble(words[3]));
                p.setGiacenza(Integer.parseInt(words[4]));
                lista.add(p);
                line=br.readLine();
            }
        }catch(IOException e){
            throw new Exception(e.getMessage());
        }
        fr.close();
        br.close();
        
        //Costruzione di oggetti
        
        
        //Restituire l'elenco creato
        
        return lista;
    }
    
    
    
    //Scrittura su dile CSV
    public static void write(String path, ArrayList<Prodotto> elenco) 
            throws Exception{
        
        FileWriter fw=null;
        PrintWriter pw=null;
        try{
            fw=new FileWriter(path,true);
            pw=new PrintWriter(fw);
            String msg="";
            for(Prodotto p:elenco){
                msg+=p.getCodice()+";";
                msg+=p.getNome()+";";
                msg+=p.getDescrizione()+";";
                msg+=p.getPrezzo()+";";
                msg+=p.getGiacenza()+";";
                msg+="\n";
            }
            pw.write(msg);
            pw.flush();
            pw.close();
            fw.close();
        }catch(IOException e){
            throw new Exception(e.getMessage());
        }
    }
    
}
