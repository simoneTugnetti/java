import java.util.Scanner;

/**
 * @author Simone Tugnetti
 * Gestione di un magazzino utilizzando Pila e Coda
 */
public class main {

    static Scanner tastiera = new Scanner(System.in);
    static int scelta, scelta2, scelta3, codicePacco;
     
    static Coda coda = new Coda();
    static Pila pila1 = new Pila();
    static Pila pila2 = new Pila();
    static Pila pila3 = new Pila();
    
    public static void main(String args[]) {
     
        System.out.println("Benvenuti in Factory!");
        System.out.println("Ci sono dei pacchi da mettere in ordine!");
     
        do{
            System.out.println("Che cosa desideri fare?");
            System.out.println("1 - Inserire pacchi nella coda");
            System.out.println("2 - Visualizzare la coda");
            System.out.println("3 - Assegnare un pacco ad una pila");
            System.out.println("4 - Visualizzare le pile "
                    + "(prima assegnare tutti i pacchi alle pile) "
                    + "(dopo averlo visualizzato sarà poi consegnato)");
            System.out.println("5 - Uscire dalla fabbrica");
            
            scelta = tastiera.nextInt();
            switch(scelta) {
                case 1:
                    System.out.println("Inserisci il codice del pacco!");
                    codicePacco = tastiera.nextInt();
                    coda.push(codicePacco);
                    break;
                case 2:
                    if(coda.vuota() == false){
                        coda.visualizza();
                    } else {
                        System.out.println("La coda è vuota!");
                    }
                    break;
                case 3:
                    assign();
                    break;
                case 4:
                    visual();
                    break;  
            }
        }while(scelta != 5);
    }
    
    
    
    private static void assign() {
        if(!coda.vuota()){
            System.out.println("A quale pila vuoi che venga "
                        + "assegnato il pacco? (1 , 2 o 3)");
            scelta2 = tastiera.nextInt();
            switch(scelta2){
                case 1:
                    pila1.push(coda.visual());
                    coda.pop();
                    break;
                case 2:
                    pila2.push(coda.visual());
                    coda.pop();
                    break;
                case 3:
                    pila3.push(coda.visual());
                    coda.pop();
                    break;
            }
        } else {
            System.out.println("La coda è vuota!");
        }
        
    }
    
    private static void visual() {
        System.out.println("Di quale pila vuoi visualizzarne i "
                            + "codici dei pacchi? (1 , 2 o 3)");
        scelta3 = tastiera.nextInt();
        switch(scelta3){
            case 1:
                if(!pila1.vuota()){
                    pila1.visualizza();
                } else {
                    System.out.println("Devi prima inserire un "
                                + "pacco nella prima pila!");
                }
                break;
            case 2:
                if(!pila2.vuota()){
                    pila2.visualizza();
                } else {
                    System.out.println("Devi prima inserire un "
                                + "pacco nella seconda pila!");
                }
                break;
            case 3:
                if(!pila3.vuota()){
                    pila3.visualizza();
                } else {
                    System.out.println("Devi prima inserire un "
                                + "pacco nella terza pila!");
                }
                break;
        }
        
    }
    
}
