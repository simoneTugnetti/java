import java.util.ArrayList;

/**
 * @author Simone Tugnetti
 */
public class Pila {
    
    private final ArrayList scatole;
    
    public Pila(){
        scatole = new ArrayList(6);
    }
    
    public void push(Object obj){
        scatole.add(obj);
    }
    
    public Object pop(){
        Object obj = null;
        int size = scatole.size();
        if (size > 0){
            obj = scatole.get(size - 1);
            scatole.remove(size - 1);
        }
        return obj;
    }
    
    public Object top(){
        Object obj = null;
        int size = scatole.size();
        if (size > 0){
            obj = scatole.get(size-1);
        }
        return obj;
    }
    
    public boolean vuota(){
        return scatole.size() <= 0;
    }
    
    public int size(){
        return scatole.size();
    }
    
    public void visualizza(){
        int i = 0;
        do {
            System.out.println("Ecco il " + (i+1) + "° pacco nella pila!");
            System.out.println(top());
            pop();
            i++;
        } while(!vuota());
    }
    
}
