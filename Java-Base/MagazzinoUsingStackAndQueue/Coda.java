import java.util.ArrayList;

/**
 * @author Simone Tugnetti
 */
public class Coda {

    private final ArrayList pacchi;

    public Coda(){
        pacchi = new ArrayList(5);
    }
    
    public void push(int obj){
        pacchi.add(obj);
    }
    
    public Object pop(){
        Object obj = null;
        int size = pacchi.size();
        if(size > 0){
            obj = pacchi.get(0);
            pacchi.remove(0);
        }
        return obj;
    }
    
    public boolean vuota(){
        return pacchi.size() <= 0;
    }
    
    public int size(){
        return pacchi.size();
    }
    
    public void visualizza(){
        for(int i = 0; i < size() ; i++){
            System.out.println("Ecco il " + (i+1) + "° pacco della coda!");
            System.out.println(pacchi.get(i));
        }
    }
    
    public Object visual(){
        return pacchi.get(0);
    }
    
}
