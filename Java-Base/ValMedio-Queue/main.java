/**
 * @author Simone Tugnetti
 * Utilizzo della modalità Pila
 */
public class main {

    public static void main(String args[]){
        int somma = 0;
        double media;
    
        Pila pila = new Pila();
    
        for(int i=0; i<100; i++){
            pila.push(i);
        }
        
        for(int i=0; i<pila.size(); i++){
            somma += (int)pila.top();
            pila.pop();
        }
        
        media = somma/pila.size();
        System.out.println("La media è: "+media);
    }
    
}
