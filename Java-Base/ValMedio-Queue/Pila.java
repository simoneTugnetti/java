import java.util.ArrayList;

/**
 * @author Simone Tugnetti
 */
public class Pila {

    private final ArrayList numeri;

    public Pila(){
        numeri = new ArrayList();
    }
    
    public void push(Object obj){
        numeri.add(obj);
    }
    
    public Object pop(){
        Object obj = null;
        int size = numeri.size();
        if (size>0){
            obj = numeri.get(size - 1);
            numeri.remove(size - 1);
        }
        return obj;
    }
    
    public Object top(){
        Object obj = null;
        int size = numeri.size();
        if (size>0){
           obj=numeri.get(size - 1);
        }
       return obj;
    }
    
    public boolean vuota(){
        return numeri.size() <= 0;
    }
    
    public int size(){
        return numeri.size();
    }
    
}
