import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author Simone Tugnetti
 * Calcolo della media di N Voti
 */
public class Main {

    public static void main(String args[]) {
        InputStreamReader input = new InputStreamReader(System.in);
        BufferedReader tastiera = new BufferedReader(input);
        
        int nVoti = 0, i = 0, somma = 0;
        double media;
        
        try {
            do {
                System.out.println("Inserisci N voti da inserire:");
                nVoti = Integer.parseInt(tastiera.readLine());
            }while(nVoti <= 0);
        } catch(IOException | NumberFormatException e) {
            System.out.println("Errore: " + e.getMessage());
        }
        
        do {
            try {
                System.out.println("Inserisci voto: ");
                int voto = Integer.parseInt(tastiera.readLine());
                somma = somma + voto;
                i++;
            } catch(IOException | NumberFormatException e) {
                System.out.println("Errore: " + e.getMessage());
            }
            
        } while(i < nVoti);
        
        media = somma / nVoti;
        System.out.println("La media e' " + media);
    }

}
