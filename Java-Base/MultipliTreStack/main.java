/**
 * @author Simone Tugnetti
 * Salvataggio numeri tramite modalità Pila
 */
public class main {

    public static void main(String args[]){
        Pila pila = new Pila();
        for(int i=3; i<=60; i+=3){
            pila.push(i);
            System.out.println(pila.top());
        }
    }
}
