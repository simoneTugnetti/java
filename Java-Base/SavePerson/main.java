

import java.util.Scanner;

/**
 * @author Simone Tugnetti
 * Salvataggio dei dati di una persona tramite Classe
 */
public class main {

    public static void main(String args[]){

        Scanner tastiera = new Scanner(System.in);
        Persona persona = new Persona();
    
        System.out.println("Qual è il tuo nome?");
        persona.setNome(tastiera.next());
        
        System.out.println("Qual è il tuo cognome?");
        persona.setCognome(tastiera.next());
        
        System.out.println("Qual è la tua professione?");
        persona.setProfessione(tastiera.next());
        
        System.out.println("Qual è il tuo sesso?");
        persona.setSesso(tastiera.next());
        
        System.out.println("Quanti anni hai?");
        persona.setEta(tastiera.nextInt());
        
        System.out.println(persona.toString());
        
    }
    
}
