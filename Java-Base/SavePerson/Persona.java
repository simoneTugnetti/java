

/**
 * @author Simone Tugnetti
 */
public class Persona {

    private String nome, cognome, sesso, professione;
    private int eta;
    
    Persona() { }

    public String getNome(){
        return nome;
    }

    public String getCognome(){
        return cognome;
    }


    public String getSesso(){
        return sesso;
    }


    public String getProfessione(){
        return professione;
    }

    public int getEta(){
        return eta;
    }

    public void setNome(String nome){
        this.nome = nome;
    }

    public void setCognome(String cognome){
        this.cognome = cognome;
    }

    public void setSesso(String sesso){
        this.sesso = sesso;
    }

    public void setProfessione(String professione){
        this.professione = professione;
    }

    public void setEta(int eta){
        this.eta = eta;
    }

    @Override
    public String toString(){
        return "Sono una persona di nome: " + nome + ", cognome: " + cognome +
                ", sesso: " + sesso + ", eta: " + eta + " e professione: " + 
                professione + ".";
    }


}
