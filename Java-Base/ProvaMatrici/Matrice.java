import java.util.Random;

/**
 * @author Simone Tugnetti
 */
public class Matrice {
    
    private final int[][] matrice;
    private final Random random;
    private final int[] max;
    private final int[] min;
    
    public Matrice(){
        min = new int[5];
        max = new int[5];
        random = new Random();
        matrice = new int[5][5];
    }
    
    public void inserimento(){
        for(int i=0; i<5; i++){
            for(int j=0; j<5; j++){
                matrice[i][j] = random.nextInt(100) + 1;
            }
        }
        for(int i=0; i<5; i++){
            System.out.println("Questi sono i valori della "+i+"° riga:");
            for(int j=0; j<5; j++){
                System.out.println(matrice[i][j]);
            }
        }
    }
    
    public void min(){
        for(int i=0; i<5; i++){
            min[i] = matrice[i][0];
            for(int j=0; j<5; j++){
                if(matrice[i][j] < min[i]){
                    min[i] = matrice[i][j];
                }
            }
            System.out.println("Il valore minimo della "+i+"° riga è: "+min[i]);
        }
    }
    
    public void max(){
        for(int i=0; i<5; i++){
            max[i] = matrice[i][0];
            for(int j=0; j<5; j++){
                if(matrice[i][j]>max[i]){
                    max[i] = matrice[i][j];
                }
            }
            System.out.println("Il valore massimo della "+i+"° riga è: "+max[i]);
        }
    }
    
}
