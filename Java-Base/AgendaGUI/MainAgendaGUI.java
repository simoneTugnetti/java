import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 * @author Simone Tugnetti
 * Agenda GUI
 */
public class MainAgendaGUI extends javax.swing.JFrame {

    /**
     * Creates new form ProgettoTugnettiGUI
     */
    public MainAgendaGUI() {
        initComponents();
    }
    
    public ArrayList array = new ArrayList();
    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelInsert = new javax.swing.JPanel();
        stato = new javax.swing.JLabel();
        lblNome = new javax.swing.JLabel();
        lblTel = new javax.swing.JLabel();
        lblTipo = new javax.swing.JLabel();
        nome = new javax.swing.JTextField();
        tel = new javax.swing.JTextField();
        tipo = new javax.swing.JTextField();
        cancella = new javax.swing.JButton();
        aggiungi = new javax.swing.JButton();
        panelVisual = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        lista = new javax.swing.JList();
        visual = new javax.swing.JButton();
        cerca = new javax.swing.JButton();
        ordina = new javax.swing.JButton();
        svuota = new javax.swing.JButton();
        leggi = new javax.swing.JButton();
        salva = new javax.swing.JButton();
        esci = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        panelInsert.setBorder(javax.swing.BorderFactory.createTitledBorder("Inserimento Contatto"));
        panelInsert.setToolTipText("Inserimento Contatto");

        stato.setText("Contatti presenti in rubrica: 0 su 100");
        stato.setToolTipText("");

        lblNome.setText("Nome");

        lblTel.setText("Telefono");

        lblTipo.setText("Tipo (casa, cell, uff, ecc.):");

        nome.setToolTipText("");

        tel.setToolTipText("");

        cancella.setText("Cancella");

        aggiungi.setText("Aggiungi");
        aggiungi.setToolTipText("");
        aggiungi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                aggiungiActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelInsertLayout = new javax.swing.GroupLayout(panelInsert);
        panelInsert.setLayout(panelInsertLayout);
        panelInsertLayout.setHorizontalGroup(
            panelInsertLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelInsertLayout.createSequentialGroup()
                .addGroup(panelInsertLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelInsertLayout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(stato))
                    .addGroup(panelInsertLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(panelInsertLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(panelInsertLayout.createSequentialGroup()
                                .addComponent(aggiungi)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cancella))
                            .addGroup(panelInsertLayout.createSequentialGroup()
                                .addGroup(panelInsertLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblNome, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(lblTel, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(lblTipo, javax.swing.GroupLayout.Alignment.TRAILING))
                                .addGap(18, 18, 18)
                                .addGroup(panelInsertLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(nome)
                                    .addComponent(tel)
                                    .addComponent(tipo, javax.swing.GroupLayout.DEFAULT_SIZE, 186, Short.MAX_VALUE))))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panelInsertLayout.setVerticalGroup(
            panelInsertLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelInsertLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(stato)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelInsertLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblNome)
                    .addComponent(nome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelInsertLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblTel)
                    .addComponent(tel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelInsertLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblTipo)
                    .addComponent(tipo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(panelInsertLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cancella)
                    .addComponent(aggiungi))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        panelVisual.setBorder(javax.swing.BorderFactory.createTitledBorder("Visualizzazione Contatti"));

        lista.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        jScrollPane1.setViewportView(lista);

        visual.setText("Visualizza");
        visual.setToolTipText("");

        cerca.setText("Cerca");
        cerca.setToolTipText("");

        ordina.setText("Ordina");

        svuota.setText("Svuota");

        leggi.setText("Leggi");
        leggi.setToolTipText("");
        leggi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                leggiActionPerformed(evt);
            }
        });

        salva.setText("Salva");
        salva.setToolTipText("");

        javax.swing.GroupLayout panelVisualLayout = new javax.swing.GroupLayout(panelVisual);
        panelVisual.setLayout(panelVisualLayout);
        panelVisualLayout.setHorizontalGroup(
            panelVisualLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelVisualLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelVisualLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(visual, javax.swing.GroupLayout.DEFAULT_SIZE, 111, Short.MAX_VALUE)
                    .addComponent(cerca, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(ordina, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(svuota, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(leggi, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(salva, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        panelVisualLayout.setVerticalGroup(
            panelVisualLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelVisualLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelVisualLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane1)
                    .addGroup(panelVisualLayout.createSequentialGroup()
                        .addComponent(visual)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cerca)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ordina)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(svuota)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(leggi)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(salva)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        esci.setText("Esci");
        esci.setToolTipText("");
        esci.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                esciActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelInsert, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelVisual, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(esci)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelInsert, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(panelVisual, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 11, Short.MAX_VALUE)
                .addComponent(esci)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    private void aggiungiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_aggiungiActionPerformed
        if(nome.getText().equals("") || tel.getText().equals("") || 
            tipo.getText().equals("")){
                
            if(nome.getText().equals("")){
                JOptionPane.showMessageDialog(null, 
                            "Devi prima inserire il nome!", "info", 
                            JOptionPane.INFORMATION_MESSAGE);
            }
                
            if(tel.getText().equals("")){
                JOptionPane.showMessageDialog(null, 
                            "Devi prima inserire il numero di telefono!", 
                            "info", JOptionPane.INFORMATION_MESSAGE);
            }
                
            if(tipo.getText().equals("")){
                JOptionPane.showMessageDialog(null, 
                            "Devi prima inserire il tipo di contatto!", "info", 
                            JOptionPane.INFORMATION_MESSAGE);
            }
                
        } else {
            array.add(nome.getText() + "; " + tel.getText() + "; " + 
                        tipo.getText() + ".");
            nome.setText("");
            tel.setText("");
            tipo.setText("");
            JOptionPane.showMessageDialog(null, 
                        "I dati del contatto sono stati aggiunti nella rubrica",
                        "info", JOptionPane.INFORMATION_MESSAGE);
        }
    }//GEN-LAST:event_aggiungiActionPerformed

    private void leggiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_leggiActionPerformed
        if(array.isEmpty()){
            lista.setListData(array.toArray());
            JOptionPane.showMessageDialog(null, 
                        "La rubrica è vuota!","info", 
                        JOptionPane.INFORMATION_MESSAGE);
        } else {
            stato.setText("Contatti presenti in rubrica: " + array.size() + 
                        " su 100");
            lista.setListData(array.toArray());
        }
    }//GEN-LAST:event_leggiActionPerformed

    private void esciActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_esciActionPerformed
        System.exit(0);
    }//GEN-LAST:event_esciActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainAgendaGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainAgendaGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainAgendaGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainAgendaGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new MainAgendaGUI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton aggiungi;
    private javax.swing.JButton cancella;
    private javax.swing.JButton cerca;
    private javax.swing.JButton esci;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblNome;
    private javax.swing.JLabel lblTel;
    private javax.swing.JLabel lblTipo;
    private javax.swing.JButton leggi;
    private javax.swing.JList lista;
    private javax.swing.JTextField nome;
    private javax.swing.JButton ordina;
    private javax.swing.JPanel panelInsert;
    private javax.swing.JPanel panelVisual;
    private javax.swing.JButton salva;
    private javax.swing.JLabel stato;
    private javax.swing.JButton svuota;
    private javax.swing.JTextField tel;
    private javax.swing.JTextField tipo;
    private javax.swing.JButton visual;
    // End of variables declaration//GEN-END:variables
}
