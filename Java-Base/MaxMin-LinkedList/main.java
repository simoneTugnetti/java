import java.util.LinkedList;

/**
 * @author Simone Tugnetti
 * Utilizzo di una LinkedList
 */
public class main {

    public static void main(String args[]){
        LinkedList<Integer> list = new <Integer>LinkedList();
        int min, max;
        
        for(int i=0; i<100; i++){
            list.offer(i);
        }
        
        min = list.peek();
        max = list.peek();
    
        for (Integer value : list) {
            if(value > max){
                max = value;
            }
            if(value < min){
                min = value;
            }
        }
        System.out.println("Il valore minimo della coda: "+min);
        System.out.println("Il valore massimo della coda: "+max);
    }
    
}
