

/**
 * @author Simone Tugnetti
 * Simulazione di un cellulare
 */
public class Main {

    public static void main (String args[]){
        Cellulare cell = new Cellulare();
        cell.setNumeroCell("3284921443");
        cell.setCredito(12.92);
        cell.setNumeroMex(98);
        cell.setStato(false);

        System.out.println("Numero: " + cell.getNumeroCell());
        System.out.println("Credito: " + cell.getCredito());
        System.out.println("Numero Mex: " + cell.getNumeroMex());
        System.out.println("Stato: " + cell.getStato());
        
    }

}
