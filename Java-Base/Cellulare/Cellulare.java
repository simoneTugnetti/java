

/**
 * @author Simone Tugnetti
 */
class Cellulare {
    
    private String numeroCell;
    private boolean stato;
    private int numeroMessaggi;
    private double credito;
    
    public Cellulare() {
        numeroCell = "";
        stato = false;
        numeroMessaggi = 0;
        credito = 0;
    }

    void setNumeroCell(String numeroCell){
        this.numeroCell = numeroCell;
    }
    
    void setStato(boolean stato){
        this.stato = stato;
    }
    
    void setNumeroMex(int numeroMessaggi){
        if(this.numeroMessaggi>100){
            this.numeroMessaggi = 0;
        } else {
            this.numeroMessaggi = numeroMessaggi;
        }
    }
    
    void setCredito(double credito){
        this.credito = credito;
    }

    String getNumeroCell(){
        return numeroCell;
    }
    
    String getStato(){
        if (stato == false){
            return "spento";
        } else {
            return "acceso";
        }
    }
    
    int getNumeroMex(){
        return numeroMessaggi;
    }
    
    double getCredito(){
        return credito;
    }

    void accendi(){
        stato = true;
    }
    
    void spegni(){
        stato = false;
    }

    void invioMsg(){
        credito -= 0.10;
    }

    void ricarica(double importo) {
        credito += importo;
    }

}
