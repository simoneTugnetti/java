

/**
 * @author Simone Tugnetti
 */
public class Triangolo {

    private double base;
    private double altezza;

    Triangolo() {}
    
    double getBase(){
        return base;
    }

    void setBase(double base){
        this.base = base;
    }

    double getAltezza(){
        return altezza;
    }

    void setAltezza(double altezza){
       this.altezza = altezza;
    }

    double area(){
        return (base*altezza)/2;
    }

    double perimetro(){
        return base*3;
    }
}
