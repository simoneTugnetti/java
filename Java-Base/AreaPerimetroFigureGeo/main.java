

import java.util.Scanner;

/**
 * @author Simone Tugnetti
 * Calcolo di Area e perimetro di figure geometriche usando le classi
 */
public class main{

    public static void main (String args[]) {

        System.out.println("1 - Quadrato");
        System.out.println("2 - Triangolo");
        System.out.println("3 - Rettangolo");
        System.out.println("4 - Rett Maggiore");

        Scanner tastiera = new Scanner(System.in);
        
        int scelta = tastiera.nextInt();
        double valore;

        switch(scelta){
            case 1:
                System.out.println("Inserisci il lato:");
                Quadrato q = new Quadrato();
                
                valore = tastiera.nextDouble();
                q.setLato(valore);
                
                System.out.println("AREA Quadrato: "+q.area());
                System.out.println("Perimetro Quadrato: "+q.perimetro());
                break;
            case 2:
                System.out.println("Inserisci base:");
                Triangolo t = new Triangolo();
                
                valore = tastiera.nextDouble();
                t.setBase(valore);
                
                System.out.println("Inserisci altezza:");
                valore = tastiera.nextDouble();
                t.setAltezza(valore);
                
                System.out.println("Area triangolo:"+t.area());
                System.out.println("Perimetro triangolo:"+t.perimetro());
                break;
            case 3:
                System.out.println("Inserisci base:");
                Rettangolo r = new Rettangolo();
                
                valore = tastiera.nextDouble();
                r.setBase(valore);
            
                System.out.println("Inserisci altezza:");
                valore = tastiera.nextDouble();
                r.setAltezza(valore);

                System.out.println("Area rettangolo:"+r.area());
                System.out.println("Perimetro rettangolo:"+r.perimetro());
                break;
            case 4:
                System.out.println("Inserisci BASE e ALTEZZA di due rettangoli");
                Rettangolo r1 = new Rettangolo();
                Rettangolo r2 = new Rettangolo();

                System.out.println("BASE 1:");
                valore = tastiera.nextDouble();
                r1.setBase(valore);
                System.out.println("BASE 2:");
                valore = tastiera.nextDouble();
                r2.setBase(valore);
                System.out.println("ALTEZZA 1:");
                valore = tastiera.nextDouble();
                r1.setAltezza(valore);
                System.out.println("ALTEZZA 2:");
                valore = tastiera.nextDouble();
                r2.setAltezza(valore);

                System.out.println("AREA 1: "+r1.area());
                System.out.println("AREA 2: "+r2.area());

                System.out.println("PERIMETRO 1: "+r1.perimetro());
                System.out.println("PERIMETRO 2: "+r2.perimetro());

                if(r1.getBase() > r2.getBase() && r1.getAltezza() > r2.getAltezza()) {
                    System.out.println("Valori maggiori:");
                    System.out.println("Base: "+r1.getBase()+"Altezza: "+r1.getAltezza());
                } else {
                    System.out.println("Valori maggiori:");
                    System.out.println("Base: "+r2.getBase()+"Altezza: "+r2.getAltezza());

                }
                break;
        }

    }

}
