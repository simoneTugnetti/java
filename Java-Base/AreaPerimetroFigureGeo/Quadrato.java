

/**
 * @author Simone Tugnetti
 */
public class Quadrato {
    
    private double lato;
   
    Quadrato() { }
    
    public double getLato(){
        return lato;
    }
    
    public void setLato(double lato){
        this.lato = lato;
    }
    
    public double area(){
        return lato * lato;
    }
    
    public double perimetro(){
        return lato * 4;
    }
    
}
