

/**
 * @author Simone Tugnetti
 */
public class Rettangolo {

    private double base;
    private double altezza;

    Rettangolo() {}

    public double getBase(){
        return base;
    }
    
    public void setBase(double base){
        this.base = base;
    }
    
    public double getAltezza(){
        return altezza;
    }
    
    public void setAltezza(double altezza){
        this.altezza = altezza;
    }

    public double area(){
        return base * altezza;
    }
    
    public double perimetro(){
        return (base + altezza) * 2;
    }
    
}
