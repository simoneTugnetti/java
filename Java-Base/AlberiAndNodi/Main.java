import java.util.Scanner;

/**
 * @author Simone Tugnetti
 * Funzionamento della modalita Alberi - Nodi
 */
final class Main {
    
    Scanner tastiera = new Scanner(System.in);
    Albero albero;
  
    public Main(){
        int scelta;

        albero = new Albero();

        do {
            System.out.println();
            System.out.println("***** MENU *****");
            System.out.println("1 - Aggiunta di un dato");
            System.out.println("2 - Eliminazione di un dato");
            System.out.println("3 - Ricerca di un dato");
            System.out.println("4 - Stampa ordinata");
            System.out.println("5 - Stampa in preordine");
            System.out.println("6 - Stampa in postordine");
            System.out.println("\n0 - Fine\n");
            System.out.print("Scelta: ");

            scelta = tastiera.nextInt();

            switch(scelta) {
                case 1:
                    aggiunta();
                    break;
                case 2:
                    eliminazione();
                    break;
                case 3:
                    ricerca();
                    break;
                case 4:
                    albero.stampaInordine();
                    break;
                case 5:
                    albero.stampaPreordine();
                    break;
                case 6:
                    albero.stampaPostordine();
                    break;
                default:
                    System.out.println("Scelta non corretta.");
                    break;
            }
        } while (scelta != 0);
    }

    /**
     * In questa procedura, si aggiunge un nuovo nodo dove verrà 
     * inserito il codice e la descrizione dell'elemento per poi infine 
     * aggiungerlo nell'albero.
     */
    public void aggiunta() {
        Nodo nodo;
        int codice;
        String descrizione;

        System.out.print("Codice: ");
        codice = tastiera.nextInt();
        System.out.print("Descrizione: ");
        descrizione = tastiera.next();
        nodo = new Nodo(codice, descrizione);
        albero.inserisci(nodo);
        
    }

    /**
     * In questa procedura, si elimina dall'albero l'elemento avente lo stesso 
     * codice inserito dall'utente.
     */
    public void eliminazione(){ 
        int codice;

        System.out.print("Codice: ");
        codice = tastiera.nextInt();
        albero.elimina(codice);
        
    }
       
    /**
     * In questa procedura, il programma cerca il nodo nell'albero avente 
     * lo stesso codice chiesto all'utente per poi stamparlo a video
     */
    public void ricerca() {
        Nodo nodo;
        int codice;

        System.out.print("Codice: ");
        codice = tastiera.nextInt();
        nodo = albero.contiene(codice);
    
        if (nodo != null) {
            System.out.println("Nodo trovato:");
            nodo.stampa();
        } else {
            System.out.println("Codice inesistente.");
        }
        
    }

    
    @SuppressWarnings("ResultOfObjectAllocationIgnored")
    public static void main(String argv[]) {
        new Main();
    }
    
}
