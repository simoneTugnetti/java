/**
 * @author Simone Tugnetti
 */
class Nodo {
  
    private final int codice;
    private final String descrizione;
    private boolean eliminato;

    private Nodo sinistro;
    private Nodo destro;

    public Nodo(int codice, String descrizione) {
        this.codice = codice;
        this.descrizione = descrizione;

        eliminato = false;
        sinistro = null;
        destro = null;
    }

    public void stampa() {
        System.out.print(codice + " - " + descrizione);
        if (eliminato) {
            System.out.print(" (eliminato)");
        }
        System.out.println();
    }

    public int getCodice() {
        return codice;
    }

    public boolean isEliminato() {
        return eliminato;
    }

    public void setEliminato(boolean eliminato) {
        this.eliminato = eliminato;
    }

    public Nodo getSinistro() {
        return sinistro;
    }

    public void setSinistro(Nodo sinistro) {
        this.sinistro = sinistro;
    }

    public Nodo getDestro() {
        return destro;
    }

    public void setDestro(Nodo destro) {
        this.destro = destro;
    }
    
    
}
