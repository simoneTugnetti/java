/**
 * @author Simone Tugnetti
 */
class Albero {
  
    private Nodo radice;
    
    public Albero() {
        radice = null;
    }
 
    public void inserisci(Nodo nodo) {
        if (radice == null) {
            radice = nodo;
        } else {
            inserisci(nodo, radice);
        }
        
    }
 
    private void inserisci(Nodo firstNodo, Nodo secondNodo) {
        if (firstNodo.getCodice() == secondNodo.getCodice()) {
            secondNodo.setEliminato(false);
        } else if (firstNodo.getCodice() < secondNodo.getCodice()) {
            
            if (secondNodo.getSinistro() == null) {
                secondNodo.setSinistro(firstNodo);
            } else {
                inserisci(firstNodo, secondNodo.getSinistro());
            }
            
        } else {
            
            if (secondNodo.getDestro() == null) {
                secondNodo.setDestro(firstNodo);
            } else {
                inserisci(firstNodo, secondNodo.getDestro());
            }
            
        }
        
    }

    public void elimina(int elim) {
        elimina(elim, radice);
    }

    private void elimina(int elim, Nodo nodo) {
        if (nodo != null) {
            if (elim == nodo.getCodice()) {
                nodo.setEliminato(true);
                System.out.println("Nodo eliminato.");
            } else if (elim < nodo.getCodice()) {
                elimina(elim, nodo.getSinistro());
            } else {
                elimina(elim, nodo.getDestro());
            }
        } else {
            System.out.println("Nodo non trovato!");
        }
    }
 
    public Nodo contiene(int cerca) {
        return contiene(cerca, radice);
    }

    private Nodo contiene(int cerca, Nodo nodo) {
        if (nodo != null) {
            if (cerca == nodo.getCodice()) {
                
                if (nodo.isEliminato()) {
                    return null;
                } else {
                    return nodo;
                }
            
            } else if (cerca < nodo.getCodice()) {
                return contiene(cerca, nodo.getSinistro());
            } else {
                return contiene(cerca, nodo.getDestro());
            }
        } else {
            return null;
        }
        
    }

    public void stampaInordine() {
        stampaInordine(radice);
    }

    private void stampaInordine(Nodo nodo) {
        if (nodo != null) {
            stampaInordine(nodo.getSinistro());
            nodo.stampa();
            stampaInordine(nodo.getDestro());
        }
    }

    public void stampaPreordine() {
        stampaPreordine(radice);
    }
 
    private void stampaPreordine(Nodo nodo) {
        if (nodo != null) {
            nodo.stampa();
            stampaPreordine(nodo.getSinistro());
            stampaPreordine(nodo.getDestro());
        }
    }

    public void stampaPostordine() {
        stampaPostordine(radice);
    }

    private void stampaPostordine(Nodo nodo) {
        if (nodo != null) {
            stampaPostordine(nodo.getSinistro());
            stampaPostordine(nodo.getDestro());
            nodo.stampa();
        }
    }
    
}
