import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * @author Simone Tugnetti
 */
public class Frame {
    
    private LinkedList linkedList;
    private JButton btnInserisci;
    private JButton btnElimina;
    private JButton btnVuota;
    private JButton btnSize;
    private JLabel lblInserisci;
    private JLabel lblElimina;
    private JLabel lblSize;
    private JLabel lblVuota;
    private JTextField info;
    private JTextField insertInfo;
    
    public Frame(){}
    
    public void gestione(){
        linkedList = new LinkedList();
        JFrame frame = new JFrame("Gestione di una coda");
        JPanel panel = new JPanel();
        btnInserisci = new JButton("Inserisci");
        btnElimina = new JButton("Elimina");
        btnVuota = new JButton("Vuota");
        btnSize = new JButton("Size");
        lblInserisci = new JLabel();
        lblElimina = new JLabel();
        lblVuota = new JLabel();
        lblSize = new JLabel();
        info = new JTextField();
        insertInfo = new JTextField();
        panel.setLayout(null);
        panel.add(btnInserisci);
        panel.add(btnElimina);
        panel.add(btnVuota);
        panel.add(btnSize);
        panel.add(info);
        panel.add(lblInserisci);
        panel.add(lblElimina);
        panel.add(lblVuota);
        panel.add(lblSize);
        panel.add(insertInfo);
        btnInserisci.setBounds(10,10,100,50);
        lblInserisci.setBounds(180,20,300,50);
        btnElimina.setBounds(10,70,100,50);
        lblElimina.setBounds(180,80,300,50);
        btnVuota.setBounds(10,130,100,50);
        lblVuota.setBounds(180,140,300,50);
        btnSize.setBounds(10,190,100,50);
        lblSize.setBounds(180,200,300,50);
        info.setBounds(200, 270, 250, 150);
        insertInfo.setBounds(420,10,200,50);
        frame.addWindowListener(new GestioneFinestra());
        btnInserisci.addActionListener(new pulsanti());
        btnElimina.addActionListener(new pulsanti());
        btnVuota.addActionListener(new pulsanti());
        btnSize.addActionListener(new pulsanti());
        info.setEditable(false);
        frame.getContentPane().add(panel);
        frame.setSize(700,500);
        frame.setVisible(true);
    }
    
    class pulsanti implements ActionListener{
        
        public pulsanti(){}
  
        @Override
        public void actionPerformed(ActionEvent e) {
            String pulsante = e.getActionCommand();
    
            if (pulsante.equals("Inserisci")) {
                if(insertInfo.getText().equals("")){
                    info.setText("Devi inserire qualcosa nella pila!");
                } else {
                    linkedList.offer(insertInfo.getText());
                    info.setText(insertInfo.getText());
                    lblInserisci.setText("La frase è stata aggiunta nella pila!");   
                }
            }
            
            if (pulsante.equals("Elimina")) {
                if(linkedList.isEmpty()){
                    info.setText("Devi prima inserire qualcosa nella pila!");
                } else {
                    linkedList.poll();
                    lblElimina.setText("E' stato eliminato un elemento nella pila!");
                }
            }
            
            if (pulsante.equals("Vuota")){
                if(linkedList.isEmpty()){
                    lblVuota.setText("La pila è vuota!!!!");
                } else {
                    lblVuota.setText("La pila non è vuota!!!!");
                }
            } 
    
            if (pulsante.equals("Size")) {
                lblSize.setText("Gli elementi esistenti nella pila sono: " + 
                        linkedList.size());
            }
        }
        
    }
    
}