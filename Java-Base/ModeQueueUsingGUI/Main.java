/**
 * @author Simone Tugnetti
 * Utilizzo della modalità Coda visualizzando una GUI
 */
public class Main {
    
    public static void main(String args[]){
        Frame frame = new Frame();
        frame.gestione();
    }
    
}
