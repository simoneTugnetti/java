/**
 * @author Simone Tugnetti
 */
public class Linea18 {
    
    private final String descrizione;
    private final String cap1;
    private final String cap2;
    
    public Linea18(){
        descrizione = "In tutto ci sono 46 fermate urbane";
        cap1 = "Caio Mario cap.";
        cap2 = "Sofia cap.";
    }
    
    @Override
    public String toString(){
        return "Il primo capolinea è: "+cap1+";  "
                + "Il secondo capolinea è: "+cap2+";  "
                + "La descrizione del percorso: "+descrizione;
    }
    
    public void visualizza(){
        System.out.println(toString());
    }
    
}
