/**
 * @author Simone Tugnetti
 */
public class Linea14 {
    
    private final String descrizione;
    private final String cap1;
    private final String cap2;
    
    public Linea14(){
        descrizione = "In tutto ci sono 28 fermate urbane";
        cap1 = "Solferino cap.";
        cap2 = "Barile cap.";
    }
    
    @Override
    public String toString(){
        return "Il primo capolinea è: "+cap1+";  "
                + "Il secondo capolinea è: "+cap2+";  "
                + "La descrizione del percorso: "+descrizione;
    }
    
    public void visualizza(){
        System.out.println(toString());
    }
    
}
