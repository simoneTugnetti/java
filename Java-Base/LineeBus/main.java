import java.util.Scanner;

/**
 * @author Simone Tugnetti
 * Info linee pubbliche utilizzando le Classi
 */
public class main {
    
    public static void main(String args[]){
        int scelta;
        Scanner tastiera = new Scanner(System.in);
        
        do{
            System.out.println("Di quale linea vuoi sapere i dati?");
            System.out.println("1 - 35");
            System.out.println("2 - 14");
            System.out.println("3 - 18");
            scelta = tastiera.nextInt();
            switch(scelta){
                case 1:
                    Linea35 l35 = new Linea35();
                    l35.visualizza();
                    break;
                case 2:
                    Linea14 l14 = new Linea14();
                    l14.visualizza();
                    break;
                case 3:
                    Linea18 l18 = new Linea18();
                    l18.visualizza();
                    break;
            }
            
            System.out.println("Desiseri uscire? (1 - SI; 0 - NO)");
            scelta = tastiera.nextInt();
        }while(scelta != 0);
    }
    
}
