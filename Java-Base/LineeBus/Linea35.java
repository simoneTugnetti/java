/**
 * @author Simone Tugnetti
 */
public class Linea35{
    
    private final String descrizione;
    private final String cap1;
    private final String cap2;
    
    public Linea35(){
        descrizione = "In tutto ci sono 11 fermate urbane";
        cap1 = "M - Carducci";
        cap2 = "Amendola cap.";
    }
    
    @Override
    public String toString(){
        return "Il primo capolinea è: "+cap1+";  "
             + "Il secondo capolinea è: "+cap2+";  "
             + "La descrizione del percorso: "+descrizione;
    }
    
    public void visualizza(){
        System.out.println(toString());
    }
    
}
