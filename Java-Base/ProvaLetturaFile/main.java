import java.io.IOException;

/**
 * @author Simone Tugnetti
 * Lettura da file
 */
public class main {
    
    public static void main(String args[]) throws IOException{
        LetturaFile lettura = new LetturaFile();
        lettura.leggiFile();
    }
}
