import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * @author Simone Tugnetti
 */
public class LetturaFile {
    
    private FileReader f;
    private BufferedReader fIN;
    private String s;
    private int i;
    
    public LetturaFile(){
        f = null;
        fIN = null;
        s = "";
        i = 0;
    }
    
    public void leggiFile() throws FileNotFoundException, IOException{
        f = new FileReader("Frasi.txt");
        fIN = new BufferedReader(f);
        s = fIN.readLine();
        while(s != null) {
            i++;
            s = fIN.readLine();
        }
        System.out.println("Nel file ci sono " + i + " frasi");
        f.close();
    }
    
}
