/**
 * @author Simone Tugnetti
 */
public class Stringa {

    private String testo;
    private int vocali;
    private int spazi;
    
    Stringa() {}

    public void analizza(){
        testo = testo.toLowerCase();
        for (int i=0; i<testo.length(); i++){
            switch(testo.charAt(i)){
                case 'a':
                    vocali++;
                    break;
                case 'e':
                    vocali++;
                    break;
                case 'i':
                    vocali++;
                    break;
                case 'o':
                    vocali++;
                    break;
                case 'u':
                    vocali++;
                    break;
                case 'è':
                    vocali++;
                    break;
                case 'ù':
                    vocali++;
                    break;
                case 'ò':
                    vocali++;
                    break;
                case ' ':
                    spazi++;
                    break;
            }
        }
    }
    
    public int getVocali(){
        return vocali;
    }

    public int getSpazi(){
        return spazi;
    }

    public double getFreqSpazi(){
        return ((double) spazi/ (double) testo.length());
    }

    public double getFreqVocali(){
        return ((double)vocali/(double)testo.length());
    }

    public void setTesto(String testo) {
        this.testo = testo;
    }
    
}