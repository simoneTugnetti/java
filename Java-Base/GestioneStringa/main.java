import java.util.Scanner;

/**
 * @author Simone Tugnetti
 * Gestione di una Stringa utilizzando le Classi
 */
public class main {

    public static void main(String args[]){
        Scanner tastiera = new Scanner(System.in);
        Stringa s = new Stringa ();
        s.setTesto(tastiera.next());
        s.analizza();
        System.out.println(s.getVocali());
        System.out.println(s.getSpazi());
        System.out.println(s.getFreqSpazi());
        System.out.println(s.getFreqVocali());
    }
    
}
