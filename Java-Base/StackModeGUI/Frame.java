import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * @author Simone Tugnetti
 */
public class Frame {
    
    private Pila pila;
    private JButton btnIns;
    private JButton btnElim;
    private JButton btnVuota;
    private JButton btnSize;
    private JLabel lblIns;
    private JLabel lblElim;
    private JLabel lblSize;
    private JLabel lblVuota;
    private JTextField info;
    private JTextField insInfo;
     
    public Frame(){}
    
    public void gestione(){
        pila = new Pila();
        JFrame frame = new JFrame("Gestione di una pila");
        JPanel panel = new JPanel();
        btnIns = new JButton("Inserisci");
        btnElim = new JButton("Elimina");
        btnVuota = new JButton("Vuota");
        btnSize = new JButton("Size");
        lblIns = new JLabel();
        lblElim = new JLabel();
        lblVuota = new JLabel();
        lblSize = new JLabel();
        info = new JTextField();
        insInfo = new JTextField();
        
        panel.setLayout(null);
        panel.add(btnIns);
        panel.add(btnElim);
        panel.add(btnVuota);
        panel.add(btnSize);
        panel.add(info);
        panel.add(lblIns);
        panel.add(lblElim);
        panel.add(lblVuota);
        panel.add(lblSize);
        panel.add(insInfo);
        
        btnIns.setBounds(10,10,100,50);
        lblIns.setBounds(180,20,300,50);
        btnElim.setBounds(10,70,100,50);
        lblElim.setBounds(180,80,300,50);
        btnVuota.setBounds(10,130,100,50);
        lblVuota.setBounds(180,140,300,50);
        btnSize.setBounds(10,190,100,50);
        lblSize.setBounds(180,200,300,50);
        info.setBounds(200, 270, 250, 150);
        insInfo.setBounds(420,10,200,50);
        
        frame.addWindowListener(new GestioneFinestra());
        btnIns.addActionListener(new pulsanti());
        btnElim.addActionListener(new pulsanti());
        btnVuota.addActionListener(new pulsanti());
        btnSize.addActionListener(new pulsanti());
        
        info.setEditable(false);
        frame.getContentPane().add(panel);
        frame.setSize(700,500);
        frame.setVisible(true);
    }
    
    class pulsanti implements ActionListener{
  
        public pulsanti(){}
        
        @Override
        public void actionPerformed(ActionEvent e) {
            String pulsante = e.getActionCommand();
    
            if (pulsante.equals("Inserisci")) {
                if(insInfo.getText().equals("")){
                    info.setText("Devi inserire qualcosa nella pila!");
                } else {
                    pila.push(insInfo.getText());
                    info.setText(insInfo.getText());
                    lblIns.setText("La frase è stata aggiunta nella pila!");   
                }
            }
    
            if (pulsante.equals("Elimina")) {
                if(pila.vuota()){
                    info.setText("Devi prima inserire qualcosa nella pila!");
                } else {
                    pila.pop();
                    lblElim.setText("E' stato eliminato un elemento nella pila!");
                }
            }
            
            if (pulsante.equals("Vuota")){
                if(pila.vuota()){
                    lblVuota.setText("La pila è vuota!");
                } else {
                    lblVuota.setText("La pila non è vuota!");
                }
            }
            
            if (pulsante.equals("Size")) {
                lblSize.setText("Gli elementi esistenti nella pila sono: " + 
                        pila.size());
            }
        }
        
    }
    
}
