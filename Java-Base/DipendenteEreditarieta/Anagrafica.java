import java.util.Scanner;

/**
 * @author Simone Tugnetti
 */
public class Anagrafica{
    
    private int stipendio;
    protected int livello;
    private int scelta;
    private int aumento;
    private final String nome;
    
    public Anagrafica(){
        stipendio = 1200;
        livello = 0;
        aumento = 0;
        nome = "Gianfranco";
    }
    
    public void incremento(){
        Scanner tastiera = new Scanner(System.in);
        
        System.out.println("Questo dipendente sta facendo bene il suo lavoro? "
                + "(1 - SI; 0 - NO)");
        scelta = tastiera.nextInt();
        
        if(scelta == 1){
            System.out.println("Allora gli aumenteremo il suo livello e gli "
                    + "daremo un aumento del 10% sul suo stipendio!");
            livello++;
            aumento = (stipendio * 10) / 100;
            stipendio += aumento;
        } else {
            System.out.println("Con il tempo sicuramente migliorerà!");
        }
        
    }
    
    public void visualizza(){
        System.out.println("Il nome del dipendente è: "+nome);
        System.out.println("Il livello del dipendente è: "+livello);
        System.out.println("Il suo stipendio è di: " + stipendio + "€");
    }
    
}
