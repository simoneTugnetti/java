/**
 * @author Simone Tugnetti
 */
class DipendenteSpecializzato extends Anagrafica{
    
    public DipendenteSpecializzato(){
        super();
    }
    
    public void premio(){
        if(livello == 5){
         System.out.println("Essendo che il livello del dipendente è arrivato "
                 + "a 5, gli verrà dato un premio di 1000€");
        }
    }
    
    @Override
    public void visualizza(){
        super.visualizza();
        if(livello >= 5){
            System.out.println("Gli è stato dato il premio dal valore di 1000€!");
        }
    }
    
}
