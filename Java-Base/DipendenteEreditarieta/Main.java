import java.util.Scanner;

/**
 * @author Simone Tugnetti
 * Simulazione feedback dipendente usando l'Ereditarietà
 */
public class Main{
    
    public static void main(String args[]){
        Scanner tastiera = new Scanner(System.in);
        int scelta;
        
        System.out.println("Benvenuto!");
        System.out.println("Potresti rispondere solo ad un breve sondaggio su "
                + "un nostro dipendente?");
        
        DipendenteSpecializzato dipSpec = new DipendenteSpecializzato();
        
        do{
            dipSpec.incremento();
            dipSpec.premio();
            System.out.println("Desideri controllare ancora se il dipendente "
                    + "fa o non fa un buon lavoro? (1 - SI; 0 - NO)");
            scelta = tastiera.nextInt();
        }while(scelta == 1);
        
        System.out.println("Desideri visualizzare i suoi dati? (1 - SI; 0 - NO)");
        scelta = tastiera.nextInt();
        
        if(scelta == 1){
            dipSpec.visualizza();
        } else {
            System.out.println("Arrivederci!");
        }
        
    }
    
}
