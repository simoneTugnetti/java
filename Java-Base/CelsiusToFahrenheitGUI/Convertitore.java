/**
 * @author Simone Tugnetti
 * Convertitore da Celsius a Fahrenheit usando la GUI
 */
class Convertitore {
  
    public static void main(String argv[]){
        ConvertFrame frame = new ConvertFrame();
        frame.pack();
        frame.setVisible(true);
    }
    
}
