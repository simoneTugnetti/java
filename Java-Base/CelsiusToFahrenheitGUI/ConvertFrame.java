import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


/**
 * @author Simone Tugnetti
 */
class ConvertFrame extends JFrame implements ActionListener {
  
    private final JPanel firstPanel;
    private final JPanel secondPanel;
    private final JTextField txtCentigradi;
    private final JTextField txtFahrenheit;
    private final JButton btnConverti;

    @SuppressWarnings({"OverridableMethodCallInConstructor", 
        "LeakingThisInConstructor"})
    public ConvertFrame() {
        super("Convertitore Centigradi -> Fahrenheit");
        this.btnConverti = new JButton("Converti");
        this.txtFahrenheit = new JTextField(15);
        this.txtCentigradi = new JTextField(15);
        this.secondPanel = new JPanel();
        this.firstPanel = new JPanel();

        addWindowListener(new GestoreFinestra());

        firstPanel.add(new JLabel("Gradi Centigradi: "));
        firstPanel.add(txtCentigradi);
        secondPanel.add(new JLabel("Gradi Fahrenheit: "));
        secondPanel.add(txtFahrenheit);

        setLayout(new GridLayout(3,1,5,10));
        add(firstPanel);
        add(btnConverti);
        add(secondPanel);
        btnConverti.addActionListener(this);
        
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String pulsante = e.getActionCommand();
        double cent, fahr;

        if (pulsante.equals("Converti")){
            
            try {
                String numeroLetto = txtCentigradi.getText();
                cent = Double.parseDouble(numeroLetto);
                fahr = 32 + (cent/100) * 180;
                txtFahrenheit.setText(""+fahr);
            } catch(NumberFormatException ex) {
                txtFahrenheit.setText(ex.getMessage());
            }
        }
    }
    
}
