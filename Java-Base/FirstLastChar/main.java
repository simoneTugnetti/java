/**
 * @author Simone Tugnetti
 * Salvataggio e conteggio di una stringa tramite classe
 */
public class main {

    public static void main(String args[]) {
        Parola parola = new Parola();
        parola.inserimento();
        parola.visualizza();
    }
}
