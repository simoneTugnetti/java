import java.util.Scanner;

/**
 * @author Simone Tugnetti
 */
public class Parola {

    private String riga;
    private int size;

    public Parola(){
        riga = "";
        size = 0;
    }
    

    public void inserimento() {
        Scanner tastiera = new Scanner(System.in);
        
        System.out.println("Inserisci una parola!");
        riga = tastiera.next();
        size = riga.length() - 1;
    }
    
    public void visualizza(){
        System.out.println("Il primo carattere della parola è: "+riga.charAt(0));
        System.out.println("L'ultima lettera della parola è: "+riga.charAt(size));
    }
    
}
