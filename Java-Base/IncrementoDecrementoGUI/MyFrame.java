import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * @author Simone Tugnetti
 */
public class MyFrame {
    
    public MyFrame(){}
    
    public void gestione(){
        JFrame f = new JFrame("Incremento e decremento");
        JPanel p = new JPanel();
        JButton incr = new JButton("Incrementare");
        JButton decr = new JButton("Decrementare");
        JTextField info = new JTextField();
        p.setLayout(new BorderLayout());
        p.add(incr, "North");
        p.add(info, "Center");
        p.add(decr, "South");

        f.addWindowListener(new GestoreFinestra());
        incr.addActionListener(new GestionePulsanti(info));
        decr.addActionListener(new GestionePulsanti(info));

        info.setEditable(true);

        f.getContentPane().add(p);
        f.setSize(450,300);
        f.setVisible(true);
    }
    
}
