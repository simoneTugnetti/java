import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JTextField;

/**
 * @author Simone Tugnetti
 */
public class GestionePulsanti implements ActionListener{
    
    private final JTextField info;
    private int risultato;
  
    public GestionePulsanti(JTextField info) {
        this.info = info;
        risultato = 0;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String pulsante = e.getActionCommand();

        if(info.getText().equals("")) {
            info.setText("Inserisci un valore!");
        } else {
            if (pulsante.equals("Incrementare")) {
                risultato = Integer.parseInt(info.getText()) + 10;
                info.setText(String.valueOf(risultato));
            }
    
            if (pulsante.equals("Decrementare")) {
                risultato = Integer.parseInt(info.getText()) - 10;
                info.setText(String.valueOf(risultato));
            }
        }
        
    }
    
}
