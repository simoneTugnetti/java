import java.util.Scanner;

/**
 * @author Simone Tugnetti
 */
class Voce {
  
    private final String nome;
    private final String telefono;

    public Voce(){
        Scanner tastiera = new Scanner(System.in);
        
        System.out.print("Nome: ");
        nome = tastiera.nextLine();
            
        System.out.print("Telefono: ");
        telefono = tastiera.nextLine();
    }

    public void stampa() {
        System.out.println(nome+" tel.: " + telefono);
    }

    @Override
    public String toString() {
        return "Voce -> " + "nome: " + nome + ", telefono=" + telefono;
    }
    
    
    
}
