import java.util.Scanner;

/**
 * @author Simone Tugnetti
 */
class Menu {
    
    private int indice, scelta;
    
    Menu() {}
  
    private void mostraMenu() {
        System.out.println();
        System.out.println("1 - Aggiungi voce");
        System.out.println("2 - Elimina voce");
        System.out.println("3 - Visualizza rubrica");
        System.out.println("4 - Esci");
    }

    public int scelta(){
        Scanner tastiera = new Scanner(System.in);
        mostraMenu();
        System.out.print("\n-> ");
        
        scelta = tastiera.nextInt();
  
        return scelta;
    }

    public int leggiIndice() {
        Scanner tastiera = new Scanner(System.in);

        System.out.print("\nVoce da eliminare: ");
        indice = tastiera.nextInt();

        return indice;
    }
    
}
