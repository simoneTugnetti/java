import java.util.ArrayList;

/**
 * @author Simone Tugnetti
 */
class Rubrica {
  
    private final ArrayList elenco; 
  
    public Rubrica() {
        elenco = new ArrayList();
    }

    public void aggiungiVoce(Voce v){
        elenco.add(v);
    }

    public void eliminaVoce(int indice) {
        elenco.remove(indice);
        System.out.println("Eliminazione effettuata.");
    }

    public void visualizza() {
        Voce v;
        System.out.println("\nRUBRICA");
    
        for(int i=0; i<elenco.size(); i++){
            System.out.println("posizione " + i + " -> ");
            v = (Voce) elenco.get(i);
            System.out.print(v);
        }
    }
    
}
