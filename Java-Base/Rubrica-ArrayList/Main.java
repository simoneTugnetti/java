/**
 * @author Simone Tugnetti
 * Gestione Rubrica utilizzando ArrayList
 */
class Main {
  
    public static void main(String argv[]) {
        Rubrica miaRubrica = new Rubrica();
        Menu mioMenu = new Menu();
        int scelta;

        scelta = mioMenu.scelta();

        while (scelta != 4) {
            switch (scelta) {
                case 1:
                    Voce v = new Voce();
                    miaRubrica.aggiungiVoce(v);
                    break;
                case 2:
                    int indice = mioMenu.leggiIndice();
                    miaRubrica.eliminaVoce(indice);
                    break;
                case 3:
                    miaRubrica.visualizza();
                    break;
                default:
                    break;
            }

            scelta = mioMenu.scelta();
        }

        System.out.println("Fine programma.");
        
    }
}
