import java.util.Scanner;

/**
 * @author Simone Tugnetti
 * Simulazione Città - Lettura e scrittura da File
 */
public class Main {
    
    public static void main(String args[]){
        Scanner tastiera = new Scanner(System.in);
        int scelta = 0;
        Citta citta = new Citta(), nuovaCitta = null;
        
        do {
            System.out.println("Che cosa intendi fare?");
            System.out.println("1 - Inserimento");
            System.out.println("2 - Leggi file");
            System.out.println("0 - Esci");
        
            scelta = tastiera.nextInt();
    
            switch(scelta){
                case 1:
                    nuovaCitta = citta.lettura();
                    System.out.println("Verranno ora scritti i dati sul file!!!");
                    ScritturaNelFile scriviFile = new ScritturaNelFile();
                    scriviFile.scrivi(nuovaCitta);
                    break;
                case 2:
                    if(nuovaCitta == null) {
                        System.out.println("Non vi sono città!");
                    } else {
                        LetturaFile leggiFile = new LetturaFile();
                        leggiFile.leggeToken();
                    }
                    break;
            }
        
        } while (scelta != 0);
        
    }
    
}
