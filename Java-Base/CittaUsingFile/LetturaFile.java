import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;

/**
 * @author Simone Tugnetti
 */
class LetturaFile {

    LetturaFile(){ }

    void leggeToken() {
        FileReader f;
        BufferedReader fIN;
        Citta citta;
        String s;
        StringTokenizer st;

        try {
            f = new FileReader("citta.txt");
            fIN = new BufferedReader(f);
    
            citta = new Citta();
            
            s = fIN.readLine();
        
            while(s != null) {
                st = new StringTokenizer(s, ";");
                citta.setNomeCitta(st.nextToken());
                citta.setProvincia(st.nextToken());
                citta.setnAbit(Integer.parseInt(st.nextToken()));
                citta.stampa();
                
                s = fIN.readLine();
            }

            f.close();
    
        } catch (IOException e) {
            System.out.println("Errore: " + e.getMessage());
            System.exit(0);
        }
    }
    
}
