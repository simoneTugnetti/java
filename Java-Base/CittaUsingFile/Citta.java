import java.util.Scanner;

/**
 * @author Simone Tugnetti
 */
class Citta {

    private String nomeCitta;
    private String provincia;
    private int nAbit;
    
    Citta(){ }

    Citta lettura(){
        Scanner tastiera = new Scanner(System.in);
        
        System.out.println("Inserisci il nome della città (* per finire): ");
        nomeCitta = tastiera.next();
        
        if(nomeCitta.equals("*")) {
            return null;
        }
        
        System.out.println("Inserisci la provincia: ");
        provincia = tastiera.next();
        System.out.println("Inserisci il numero di abitanti: ");
        nAbit = tastiera.nextInt();
        
        return this;
    }

    String getNomeCitta() {
        return nomeCitta;
    }

    String getProvincia() {
        return provincia;
    }

    int getnAbit() {
        return nAbit;
    }

    public void setNomeCitta(String nomeCitta) {
        this.nomeCitta = nomeCitta;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public void setnAbit(int nAbit) {
        this.nAbit = nAbit;
    }
    
    void stampa(){
        System.out.println("Nome città: " + nomeCitta);
        System.out.println("Provincia: " + provincia);
        System.out.println("Numero di abitanti: " + nAbit);
    }
    
}
