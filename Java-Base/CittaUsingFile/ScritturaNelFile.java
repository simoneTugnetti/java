import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author Simone Tugnetti
 */
class ScritturaNelFile {
    
    ScritturaNelFile(){ }
    
    void scrivi(Citta citta){
        FileWriter f;
        PrintWriter fOUT;
        
        try {
            f = new FileWriter("citta.txt");
            fOUT = new PrintWriter(f);
            
            if (citta != null) {
                fOUT.println(citta.getNomeCitta() + ";" + 
                        citta.getProvincia() + ";" + citta.getnAbit());
                fOUT.flush();
            }
            
            f.close();
            
        } catch(IOException e){
            System.out.println("Errore: " + e.getMessage());
            System.exit(0);
        }
    }
    
}
