/**
 * @author Simone Tugnetti
 * Funzionamento della modalità Pila
 */
public class Main {
    
    public static void main(String argv[]) {
        Pila pila = new Pila();
        int num;
        
        for(int i=0; i<5; i++) {
            num = (int)(Math.random() * 100);
            System.out.print(num + " ");
            pila.push(num);
        }
        
        System.out.println("\nElementi nella pila: " + pila.size());
        
        System.out.println("L'estrazione della pila avviene nel seguente modo");
        
        while(!pila.vuota()) {
            num = (int) pila.pop();
            System.out.print(num + " ");
        }
        
        System.out.println();
    }
}
