import java.util.ArrayList;

/**
 * @author Simone Tugnetti
 */
public class Pila {
    
    private final ArrayList elementi;
        
    public Pila(){
        elementi = new ArrayList();            
    }
    
    public void push(Object obj) {
        elementi.add(obj);
    }
    
    public Object pop() {
        Object obj = null;
        int size = elementi.size();
            
        if (size>0) {
            obj = elementi.get(size - 1);
            elementi.remove(size - 1);
        }
        return obj;
    }
    
    public Object top() {
        Object obj = null;
        int size =elementi.size();
            
        if (size>0) {
            obj=elementi.get(size - 1);
        }
        return obj;
    }
        
    public boolean vuota() {
        return elementi.size() <= 0;
    }
    
    public int size() {
        return elementi.size();
    }
    
}    
