

import java.util.Scanner;

/**
 * @author Simone Tugnetti
 */
class ContoLimit extends Conto{
    
    private final double limite;
    private double saldo;
    private int scelta;
    private final Scanner tastiera;
    
    public ContoLimit(String nome, double saldoConto){
        super(nome, saldoConto);
        limite = 100000;
        scelta = 0;
        tastiera = new Scanner(System.in);
    }
    
    public void contoLimitato(){ 
        System.out.println("Il limite di questo conto è di " + limite + " euro!");
        
        if(saldo >= limite){
            System.out.println("Non puoi più depositare denaro!");
            System.out.println("Può però ritirarne!");
            super.prelievo();
        } else {
            System.out.println("Desidera ritirare o depositare? "
                    + "(1 - Ritirare; 0 - Depositare)");
            scelta = tastiera.nextInt();
            if(scelta == 1){
                super.prelievo();
            } else {
                super.deposito();
            }
        }
    }
    
}