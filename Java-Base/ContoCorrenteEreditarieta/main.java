

/**
 * @author Simone Tugnetti
 * Gestione conto usando l'Ereditarietà
 */
public class main {

    public static void main(String args[]){
        Conto conto = new Conto("Giovanni", 30000);
        System.out.println("Benvenuto!");
        conto.inizio();
        conto.visualizza();
        
        ContoLimit contoLimit = new ContoLimit("Anna", 50000);
        System.out.println("Il suo conto limitato!");
        
        contoLimit.contoLimitato();
        contoLimit.visualizza();
        contoLimit.chiusura();
    }
    
}