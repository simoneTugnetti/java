

import java.util.Scanner;

/**
 * @author Simone Tugnetti
 */
public class Conto {

    private double saldo;
    private final String nomeProprietario;
    private int scelta;
    private double soldi;
    private final Scanner tastiera;
    

    public Conto(String nomeProprietario, double saldo){
        this.nomeProprietario = nomeProprietario;
        this.saldo = saldo;
        scelta = 0;
        soldi = 0;
        tastiera = new Scanner(System.in);
    }
    
    public void inizio() {
        System.out.println("Vuole depositare o vuole ritirare? "
                + "(1 - Depositare; 0 - Ritirare)");
        
        scelta = tastiera.nextInt();
        
        if(scelta == 1){
            deposito();
        } else {
            prelievo();
        }
        
    }
    
    public void deposito(){
        do{
            System.out.println("Quanti soldi vorrebbe depositare?");
            soldi = tastiera.nextDouble();
            saldo += soldi;
            System.out.println("Vuole depopsitarne ancora? (1 - SI; 0 - NO)");
            scelta = tastiera.nextInt();
        }while(scelta == 1);
    }
    
    public void prelievo(){
        do{
            System.out.println("Quanti soldi vorrebbe ritirare?");
            soldi = tastiera.nextDouble();
            saldo -= soldi;
            System.out.println("Vuole ritirarne ancora? (1 - SI; 0 - NO)");
            scelta = tastiera.nextInt();
        }while(scelta == 1);
    }
    
    public void visualizza(){
        System.out.println("Il suo nome è: " + nomeProprietario);
        System.out.println("Il saldo sul suo conto è di: " + saldo);
    }
    
    public void chiusura(){
        System.out.println("Vuole uscire dalla nostra banca? (1 - SI; 0 - NO)");
        
        scelta = tastiera.nextInt();
 
        if(scelta == 1){
            System.out.println("Arrivederci!");
        } else {
            inizio();
        }
    }
    
}