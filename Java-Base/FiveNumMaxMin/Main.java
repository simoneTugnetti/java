

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author Simone Tugnetti
 * Calcolo minimo e Massimo degli elementi all'interno di un array
 */
public class Main {
    
    public static void main(String args[]) {
        
        InputStreamReader input = new InputStreamReader(System.in);
        BufferedReader tastiera = new BufferedReader(input);

        int[] numeri = new int[5];
        int max = 0;
        int min = 0;
    
        for(int i=0;i<numeri.length;i++) {
        
            try {
                System.out.println((i+1) + "°: ");
                numeri[i] = Integer.parseInt(tastiera.readLine());
            } catch(NumberFormatException | IOException | 
                    ArrayIndexOutOfBoundsException e) {
                System.out.println("Error: " + e.getMessage());
            }
        
        }
        
        for(int i=0;i<numeri.length;i++) {
            for(int j=i+1;j<numeri.length;j++) {
                if (numeri[i]>numeri[j]) {
                    max = numeri[i];
                } else if (numeri[i]<numeri[j]) {
                    min = numeri[i];
                }
            }
        }
        
        System.out.println("Valore Max: " + max);
        System.out.println("Valore Min: " + min);
        
    }
}
