import java.util.ArrayList;

/**
 * @author Simone Tugnetti
 */
public class Coda {

    private final ArrayList ordinaz;

    public Coda(){
        ordinaz = new ArrayList(4);
    }
    
    public void push(Object obj){
        ordinaz.add(obj);
    }
        
    public Object pop(){
        Object obj = null;
        int size = ordinaz.size();
        if(size > 0){
            obj = ordinaz.get(size - 1);
            ordinaz.remove(size - 1);
        }
        return obj;
    }
    
    public boolean vuota(){
        return ordinaz.size() <= 0;
    }
    
}
