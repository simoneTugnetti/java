import java.util.Scanner;

/**
 * @author Simone Tugnetti
 * Gestione di un ristorante usando ArrayList e Coda
 */
public class main {

    public static void main(String args[]) {
        Scanner tastiera = new Scanner(System.in);
        
        int scelta;
        Ordinazione ord = null;
        Coda coda = new Coda();
        Vet vet = new Vet();
        
        System.out.println("Benvenuti in Restaurant!");
        System.out.println("Ci sono dei clienti!");
     
        do{
            System.out.println("Che cosa desideri fare?");
            System.out.println("1 - Prendere l'ordinazione");
            System.out.println("2 - Erogare ordinazione al cliente");
            System.out.println("3 - Dare il conto al cliente");
            System.out.println("4 - Uscire dal ristorante");
            scelta = tastiera.nextInt();
            
            switch(scelta){
                case 1:
                    ord = new Ordinazione();
                    coda.push(ord);
                    break;
                case 2:
                    if(!coda.vuota()){
                        System.out.println("L'ordinazione è arrivata al tavolo "
                                + "del cliente!");
                        vet.aggiungi(ord);
                        coda.pop();
                    } else {
                        System.out.println("Non è ancora stato ordinato niente!");
                    }
                    break;
                case 3:
                    if(!vet.vuota()){
                        System.out.println("Ecco il suo conto!");
                        vet.visualizza(0);
                        vet.elimina(0);
                    } else {
                        System.out.println("Visto che non ha ordinato niente,"
                                + " il suo conto totale è 0 €");
                    }
                    break;
            }
        } while(scelta != 4);
    }
    
}