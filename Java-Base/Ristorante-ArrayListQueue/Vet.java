import java.util.ArrayList;

/**
 * @author Simone Tugnetti
 */
public class Vet {

    private final ArrayList scontrini;

    public Vet(){
        scontrini = new ArrayList(4);
    }
    
    public void aggiungi(Object obj){
        scontrini.add(obj);
    }
    
    public void elimina(int i){
        scontrini.remove(i);
    }
    
    public void visualizza(int i){
        Object obj;
        obj = scontrini.get(i);
        System.out.println(obj);
    }
    
    public boolean vuota(){
        return scontrini.size() <= 0;
    }
    
}
