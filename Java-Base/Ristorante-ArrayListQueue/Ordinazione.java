import java.util.Scanner;

/**
 * @author Simone Tugnetti
 */
public class Ordinazione {
    
    private int nTavolo;
    private String bevande;
    private String primi;
    private String secondi;
    private double prezzo;
    private double prezzo2;
    private double prezzo3;
    private final double totale;
    private int scelta;
    private final Scanner tastiera;
    
    public Ordinazione() {
        tastiera = new Scanner(System.in);
        
        do{
            System.out.println("A quale tavolo desidera sedersi? "
                    + "(Le ricordo che non ci sono più di 30 tavoli)");
            nTavolo = tastiera.nextInt();
        }while(nTavolo > 30);
        
        bevanda();
        
        primo();
        
        secondo();
        
        totale = prezzo + prezzo2 + prezzo3;
    }
    
    private void bevanda() {
        System.out.println("Quale bevanda gradisce ordinare?");
        System.out.println("0 - Nulla");
        System.out.println("1 - Coca cola");
        System.out.println("2 - Fanta");
        System.out.println("3 - Sprite");
        do{
            scelta = tastiera.nextInt();
        }while(scelta > 3);
        switch(scelta){
            case 1:
                bevande = "Coca cola";
                prezzo = 2.50;
                break;
            case 2:
                bevande = "Fanta";
                prezzo = 2;
                break;
            case 3:
                bevande = "Sprite";
                prezzo = 2.70;
                break;
            case 0:
                bevande = "Niente da bere";
                prezzo = 0;
                break;
        }
    }
    
    private void primo() {
        System.out.println("Quale primo piatto desidera ordinare?");
        System.out.println("0 - Nulla");
        System.out.println("1 - Pasta al ragù");
        System.out.println("2 - Insalata di riso");
        System.out.println("3 - Lasagne al forno");
        do{
            scelta = tastiera.nextInt();
        }while(scelta > 3);
        switch(scelta){
            case 1:
                primi = "Pasta al ragù";
                prezzo2 = 5.50;
                break;
            case 2:
                primi = "Insalata di riso";
                prezzo2 = 4.30;
                break;
            case 3:
                primi = "Lasagne al forno";
                prezzo2 = 6;
                break;
            case 0:
                primi = "Niente primo";
                prezzo2 = 0;
                break;
        }
    }
    
    private void secondo() {
        System.out.println("Che cosa gradisce ordinare di secondo???");
        System.out.println("0-Nulla");
        System.out.println("1-Spezzatino con patate");
        System.out.println("2-Coniglio al vino bianco");
        System.out.println("3-Pollo al rosto");
        do{
            scelta = tastiera.nextInt();
        }while(scelta > 3);
        switch(scelta){
            case 1:
                secondi = "Spezzatino con patate";
                prezzo3 = 4.20;
                break;
            case 2:
                secondi = "Coniglio al vino bianco";
                prezzo3 = 7.90;
                break;
            case 3:
                secondi = "Pollo al rosto";
                prezzo3 = 5.10;
                break;
            case 0:
                secondi = "Niente secondo";
                prezzo3 = 0;
                break;
        }
    }
    
    @Override
    public String toString(){
        return "Numero tavolo: "+nTavolo+";"
                + " Primo: "+primi+";"
                + " Secondo: "+secondi+";"
                + " Bevande: "+bevande+";"
                + " Totale: "+totale;
    }
  }