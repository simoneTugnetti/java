import java.util.Scanner;

/**
 * @author Simone Tugnetti
 */
public class Stringhe {

    private int i;
    private String righe;

    public Stringhe(){
        i = 0;
        righe = "";
    }
    
    public void Inserimento(){
        Scanner tastiera = new Scanner(System.in);
        do{
            System.out.println("Inserisci una parola o una frase!");
            righe = tastiera.next();
            i++;
        }while(!righe.equals(""));
        i--;
    }
    
    public void visualizza(){
        System.out.println("Il numero di righe è: "+i);
    }
    
}
