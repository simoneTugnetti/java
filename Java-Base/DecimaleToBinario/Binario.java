/**
 * @author Simone Tugnetti
 */
public class Binario {
    
    private String ris;
    private int numero;
    
    public Binario(){
        numero = 0;
        ris = "";
    }
    
    public String risultato(int num){
        numero = num;
        do{
            ris += (num%2);
            num /= 2;
        }while(num>1);
        return 1 + ris;
    }
    
    public int getNumero() {
        return numero;
    }
    
    public void SetNumero(int numero){
        this.numero = numero;
    }
    
}

