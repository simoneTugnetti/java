/**
 * @author Simone Tugnetti
 * Convertitore da decimale a binario
 */
public class Main {

    public static void main(String args[]){
        Binario n1 = new Binario();
        System.out.println("Il programma sta convertendo il numero già preimpostato");
        System.out.println("Binario: " + n1.risultato(14));
        System.out.println("Decimale " + n1.getNumero());
    }

}
