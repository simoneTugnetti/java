import java.util.Scanner;

/**
 * @author Simone Tugnetti
 */
public class Cartoleria {
    
    private int nprodotti, tot;
    private String nomeprodotto2;
    private String[] nomeprodotto;
    private int[] prezzoprodotto;
    private final Scanner tastiera;

    public Cartoleria(){
        nprodotti = 0;
        nomeprodotto2 = "";
        tastiera = new Scanner(System.in);
    }
    
    public void carica(){
        System.out.println("Inserisci il numero di prodotti:");
    
        nprodotti = tastiera.nextInt();
        nomeprodotto = new String[nprodotti];
        prezzoprodotto = new int[nprodotti];
            
        for(int i=0; i<nprodotti; i++){
            System.out.println("Inserisci il nome del prodotto:");
            nomeprodotto[i] = tastiera.next();
                
            System.out.println("Inserisci il prezzo del prodotto:");
            prezzoprodotto[i] = tastiera.nextInt();
        }
        
    }
    
    public void valoreprodotto(){
        for(int i=0; i<nprodotti; i++){
            tot = tot + prezzoprodotto[i];
        }
        System.out.println("Il costo totale è: "+tot);
    }
    
    public void stampa(){
        System.out.println("Il numero di prodotti è: " + nprodotti);
        
        for(int i=0; i<nprodotti; i++){
            System.out.println("Il nome del prodotto è: " + nomeprodotto[i]);
            System.out.println("Il prezzo del prodotto è: "+prezzoprodotto[i]);
        }
    }
    
    public void costoprodotto(){
        System.out.println("Dimmi il nome del prodotto:");
        
        nomeprodotto2 = tastiera.next();
        int i = 0;
        
        boolean trov = false;
        while(i<nprodotti && !trov) {
            if(nomeprodotto2.equals(nomeprodotto[i])){
                trov = true;
            } else {
                i++;
            }
        }
        
        if(trov){
            System.out.println("Il costo del prodotto è: " + prezzoprodotto[i]);
        } else {
            System.out.println("Il prodotto non esiste!");
        }
        
    }
    
}
