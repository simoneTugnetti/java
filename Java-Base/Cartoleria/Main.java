/**
 * @author Simone Tugnetti
 Simulazione di una Cartoleria
 */
public class Main {
    
    public static void main(String args[]){
        Cartoleria a1 = new Cartoleria();
        a1.carica();
        a1.valoreprodotto();
        a1.stampa();
        a1.costoprodotto();
    }
}
