import java.util.LinkedList;
import java.util.Scanner;

/**
 * @author Simone Tugnetti
 * Gestione di un Ristorante usando LinkedList e ArrayList
 */
public class main {

    public static void main(String args[]) throws Exception{
        Scanner tastiera = new Scanner(System.in);
        int scelta;
        int i = 0;
        
        LinkedList<Ordinazione> ordList = new LinkedList<Ordinazione>();
        Ordinazione[] ordArray = new Ordinazione[1000];
        Vet vet = new Vet();
     
        System.out.println("Benvenuti in Restaurant ExSTremis!!!");
        System.out.println("Ci sono dei clienti!");
     
        do{
            System.out.println("Che cosa desideri fare?");
            System.out.println("1 - Prendere l'ordinazione");
            System.out.println("2 - Erogare ordinazione al cliente");
            System.out.println("3 - Dare il conto al cliente");
            System.out.println("4 - Uscire dal ristorante");
            scelta = tastiera.nextInt();
     
            switch(scelta){
                case 1:
                    ordArray[i] = new Ordinazione();
                    ordList.offer(ordArray[i]);
                    vet.aggiungi(ordArray[i]);
                    vet.aggiungiScontr(ordArray[i]);
                    i++;
                    break;
                case 2:
                    if(!vet.vuota()){
                        System.out.println("L'ordinazione è arrivata al tavolo"
                                + " del cliente!");
                        vet.elimina(0);
                    } else {
                        System.out.println("Non è ancora stato ordinato niente!");
                    }
                    break;
                case 3:
                    if(!vet.vuotaScontr()){
                        System.out.println("Ecco il suo conto!");
                        System.out.println(ordList.peek());
                        ordList.poll();
                        vet.eliminaScontr(0);
                    } else {
                        System.out.println("Visto che non ha ordinato niente,"
                                + " il suo conto totale è 0 €");
                    }
                    break;
            }
        }while(scelta != 4);
        
    }
    
}
