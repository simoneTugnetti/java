import java.util.ArrayList;

/**
 * @author Simone Tugnetti
 */
public class Vet {

    private final ArrayList ord;
    private final ArrayList scontrino;

    public Vet(){
        ord = new ArrayList(4);
        scontrino = new ArrayList(4);
    }
    
    public void aggiungi(Object obj){
        ord.add(obj);
    }
    
    public void elimina(int i){
        ord.remove(i);
    }
    
    public boolean vuota(){
        return ord.size() <= 0;
    }
    
    public void aggiungiScontr(Object obj){
        scontrino.add(obj);
    }
    
    public void eliminaScontr(int i){
        scontrino.remove(i);
    }
 
    public boolean vuotaScontr(){
        return scontrino.size() <= 0;
  }
}
