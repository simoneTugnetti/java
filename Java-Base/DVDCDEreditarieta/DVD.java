import java.util.Scanner;

/**
 * @author Simone Tugnetti
 */
class DVD {
    
    public int[] codice;
    public int[] durata;
    public double[] prezzo;
    public String[] titolo;

    public DVD(){
        codice = new int[10];
        durata = new int[10];
        prezzo = new double[10];
        titolo = new String[10];
    }
    
    public void aggiunta(){
        Scanner tastiera = new Scanner(System.in);
        for(int i = 0; i<10; i++){
            System.out.println("Inserisci il codice del DVD: ");
            codice[i] = tastiera.nextInt();
                
            System.out.println("Inserisci la durata: ");
            durata[i] = tastiera.nextInt();
                
            System.out.println("Il prezzo del DVD: ");
            prezzo[i] = tastiera.nextDouble();
                
            System.out.println("Il prezzo del titolo: ");
            titolo[i] = tastiera.next();
        }
    
    }

    public void stampa(){
        for(int i=0; i<10; i++){
            System.out.println("Ecco i dati: "+codice[i]+" "+durata[i]+" "+
                    prezzo[i]+" "+titolo[i]);
        }
    }
    
    public int[] getCodice(){
        return codice;
    }

    public int[] getDurata(){
        return durata;
    }
    
    public double[] getPrezzo(){
        return prezzo;
    }

    public String[] getTitolo(){
        return titolo;
    }
    
}
