import java.util.Scanner;

/**
 * @author Simone Tugnetti
 */
class CD extends DVD{
    
    private int nbrani;

    public CD() {
        nbrani = 0;
    }
    
    public void aggiunta (int nBrani) {
        Scanner tastiera = new Scanner(System.in);
        
        System.out.println("Scrivi il numero di brani");
        nbrani = tastiera.nextInt();
    }
    
    @Override
    public void stampa() {
        System.out.println("Questo è il numero di brani: " + nbrani);
    }
    
    public int getBrani(){
        return nbrani;
    }
    
}
