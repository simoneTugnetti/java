import java.util.Scanner;

/**
 * @author Simone Tugnetti
 * Gestione di Impiegati tramite i Nodi
 */
public class main {
 
    public static void main (String[] args) {
        Scanner tastiera = new Scanner(System.in);
        
        int scelta;
        String cogn;
        String mansione;
        String nome;
        int numImpiegati = 0;
        int aumento;
        Gestione gestione = new Gestione();
        Impiegato impiegato;
        
        System.out.println("Benvenuti in Factory!");
        System.out.println("Ci sono dei pacchi da mettere in ordine!");
     
        do{
            System.out.println("Che cosa desideri fare?");
            System.out.println("1 - Inserire un nuovo impiegato");
            System.out.println("2 - Modifica della mansione di un impiegato");
            System.out.println("3 - Stampa di tutti gli impiegati con una certa"
                    + " mansione");
            System.out.println("4 - Licenziare un impiegato");
            System.out.println("5 - Aumento dello stipendio di tutti gli "
                    + "impiegati");
            System.out.println("6 - Visualizzare la lista");
            System.out.println("7 - Uscire dalla fabbrica");
            
            scelta = tastiera.nextInt();
            switch(scelta){
                case 1:
                    impiegato = new Impiegato();
                    System.out.println("Inserisci il nome!");
                    impiegato.setNome(tastiera.next());
                    System.out.println("Inserisci il cognome!");
                    impiegato.setCognome(tastiera.next());
                    System.out.println("Inserisci il numero di matricola!");
                    impiegato.setMatricola(tastiera.nextInt());
                    System.out.println("Inserisci la sua mansione!");
                    impiegato.setMansione(tastiera.next());
                    System.out.println("Inserisci il suo stipendio mensile!");
                    impiegato.setStipendioMensile(tastiera.nextInt());
             
                    if(numImpiegati == 0){
                        gestione.inserisciInTesta(impiegato);
                    } else {
                        gestione.inserisciInCoda(impiegato);
                    }
                    numImpiegati++;
                    break;
                case 2:
                    if(gestione.getElementi() > 0) {
                        System.out.println("Inserisci il cognome dell'impiegato!");
                        cogn = tastiera.next();
                        for(int i = 0; i < gestione.getElementi(); i++){
                            impiegato = gestione.getLinkPosizione(i).getInfo();
                            if(impiegato.getCognome().equals(cogn)){
                                System.out.println("Inserisci la nuova mansione!");
                                impiegato.setMansione(tastiera.next());
                            }
                        }
                    } else {
                        System.out.println("Non ci sono elementi nella lista!");
                    }
                    break;
                case 3:
                    if(gestione.getElementi() > 0){
                        System.out.println("Inserisci la mansione da ricercare!");
                        mansione = tastiera.next();
                        for(int i = 0; i < gestione.getElementi(); i++){
                            impiegato = gestione.getLinkPosizione(i).getInfo();
                            if(mansione.equals(impiegato.getMansione())){
                                System.out.println("Ecco l'impiegato "
                                        + "avente questa mansione!");
                                impiegato.toString();
                            }
                        }
                    }else{
                        System.out.println("Non ci sono elementi nella lista!");
                    }
                    break;
                case 4:
                    if(gestione.getElementi() > 0){
                        System.out.println("Inserisci il nome dell'impiegato "
                                + "da licenziare!");
                        nome = tastiera.next();
                        for(int i = 0; i < gestione.getElementi(); i++){
                            impiegato = gestione.getLinkPosizione(i).getInfo();
                            if(nome.equals(impiegato.getNome())){
                                System.out.println("L'impiegato verrà ora "
                                        + "licenziato!");
                                gestione.eliminaInPosizione(i);
                            }
                        }
                        numImpiegati--;
                    }else{
                        System.out.println("Non ci sono elementi nella lista!");
                    }
                    break;
                case 5:
                    if(gestione.getElementi() > 0){
                        System.out.println("Inserisci di quanto vuoi aumentare "
                                + "lo stipendio agli impiegati!");
                        aumento = tastiera.nextInt();
                        for(int i = 0; i < gestione.getElementi(); i++){
                            impiegato = gestione.getLinkPosizione(i).getInfo();
                            impiegato.setStipendioMensile(
                                    impiegato.getStipendioMensile() + aumento);
                            gestione.inserisciInPosizione(impiegato, i);
                        }
                        System.out.println("Lo stipentio di tutti gli impiegati"
                                + " è stato aumentato!");
                    } else {
                        System.out.println("Non ci sono elementi nella lista!");
                    }
                    break;
                case 6:
                    if(gestione.getElementi() > 0){
                        gestione.toString();
                    } else {
                        System.out.println("Non ci sono elementi nella lista!");
                    }
                    break;
                }
        } while(scelta != 7);
    }
    
}