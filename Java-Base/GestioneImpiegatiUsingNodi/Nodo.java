/**
 * @author Simone Tugnetti
 */
public class Nodo {
    
    private Impiegato info;
    private Nodo link;

    public Nodo(Impiegato persona){
        info = new Impiegato(persona);
        link = null;
    }

    public void setInfo(Impiegato persona){
        info = new Impiegato(persona);
    }

    public Impiegato getInfo(){
        return new Impiegato(info);
    }

    protected void setLink(Nodo link){
        this.link = link;
    }

    public Nodo getLink(){
        return link;
    }
    
}
