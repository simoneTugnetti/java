/**
 * @author Simone Tugnetti
 */
public class Impiegato {
    
    private String nome;
    private int matricola;
    private String cognome;
    private String mansione;
    private int stipendioMensile;

    public Impiegato(){
        nome = "";
        cognome = "";
        matricola = 0;
        mansione = "";
        stipendioMensile = 0;
    }

    public Impiegato(Impiegato impiegato){
        this.nome = impiegato.getNome();
        this.cognome = impiegato.getCognome();
        this.matricola = impiegato.getMatricola();
        this.mansione = impiegato.getMansione();
        this.stipendioMensile=impiegato.getStipendioMensile();
    }
 
    public void setNome(String nome){
        this.nome = nome;
    }
    
    public String getNome(){
        return nome;
    }
    
    public void setCognome(String cognome){
        this.cognome = cognome;
    }
    
    public String getCognome(){
        return cognome;
    }
    
    public void setMatricola(int matricola){
        this.matricola = matricola;
    }
    
    public int getMatricola(){
        return matricola;
    }
    
    public void setMansione(String mansione){
        this.mansione = mansione;
    }
    
    public String getMansione(){
        return mansione;
    }
    
    public void setStipendioMensile(int stipendioMensile){
        this.stipendioMensile = stipendioMensile;
    }
    
    public int getStipendioMensile(){
        return stipendioMensile;
    }
    
    @Override
    public String toString(){
        return "Dati dell'impiegato: "
             + "nome --> " + nome + "; "
             + "cognome --> " + cognome + "; "
             + "numero matricola --> " + matricola + "; "
             + "mansione --> " + mansione + "; "
             + "stipendio mensile --> " + stipendioMensile + ".";
    }
    
}
