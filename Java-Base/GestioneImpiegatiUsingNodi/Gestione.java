/**
 * @author Simone Tugnetti
 */
public class Gestione {
 
    private Nodo head;
    private int elementi;
 
    public Gestione() {
        head = null;
        elementi = 0;
    }
    
    public Nodo getLinkPosizione(int posizione) {
        int n = 1;
        Nodo nodo = head;
        if (head != null){
            if ((posizione < elementi) || (posizione > 1)){
                while ((nodo.getLink() != null) && (n < posizione)) {
                    nodo = nodo.getLink();
                    n++;
                }
            } else {
                System.out.println("La posizione è errata!");
            }
        } else {
            System.out.println("La lista è vuota!");
        }
        return nodo;
    }
    
    private Nodo creaNodo(Impiegato persona, Nodo link) {
        Nodo nuovoNodo = new Nodo(persona);
        nuovoNodo.setLink(link);
        return nuovoNodo;
    }
    
    public int getElementi() {
        return elementi;
    }
    
    public void inserisciInTesta(Impiegato persona) {
        Nodo nodo = creaNodo(persona, head);
        head = nodo;
        elementi++;
    }
    
    public void inserisciInCoda(Impiegato persona){
        if (head == null){
            inserisciInTesta(persona);
        } else {
            Nodo nodo = getLinkPosizione(elementi);
            nodo.setLink(creaNodo(persona, null));
            elementi++;
        }
    }
 
    public void inserisciInPosizione(Impiegato persona, int posizione){
        if (posizione<=1){
            inserisciInTesta(persona);
        } else {
            if (elementi < posizione){
                inserisciInCoda(persona);
            } else {
                Nodo nodo = getLinkPosizione(posizione - 1);
                nodo.setLink(creaNodo(persona, nodo.getLink()));
                elementi++;
            }
        }
    }
    
    public void eliminaInTesta() {
        if (head != null){
            head = head.getLink();
            elementi--;
        } else {
            System.out.println("La lista è vuota!");
        }
    }

    public void eliminaInCoda() {
        if (head != null){
            Nodo nodo = getLinkPosizione(elementi - 1);
            nodo.setLink(null);
            elementi--;
        } else {
            System.out.println("La lista è vuota!");
        }
        
    }
 
    public void eliminaInPosizione(int posizione) {
        if (posizione == 1){
            eliminaInTesta();
        } else {
            if (posizione == elementi){
                eliminaInCoda();
            } else {
                Nodo firstNodo = getLinkPosizione(posizione);
                Nodo secondNodo = getLinkPosizione(posizione - 1);
                secondNodo.setLink(firstNodo.getLink());
                elementi--;
            }
        }
    }
    
    private String visita(Nodo p) {
        if (p == null) {
            return "";
        }
        return p.getInfo().toString() + "\n" + visita(p.getLink());
    }
 
    public String elenco() {
        return visita(head);
    }
 
    @Override
    public String toString() {
        Nodo nodo = head;
        String lista = "head->";
        if (nodo == null) {
            return lista + "null";
        }
        while (nodo != null) {
            lista = lista + "[" + nodo.getInfo().toString() + "|";
            if (nodo.getLink() == null) {
                lista = lista + "null]";
            } else {
                lista = lista + "+]->";
            }
            nodo = nodo.getLink();
        }
        return lista;
    }
    
}
