import java.util.Scanner;

/**
 * @author Simone Tugnetti
 * Gestione Rubrica usando Vettori Dinamici
 */
public class main {

    public static void main(String args[]){
        Scanner tastiera = new Scanner(System.in);
        int scelta;
        Vettori vet = new Vettori();
    
        do{
            System.out.println("Cosa vorresti fare?");
            System.out.println("1 - Aggiungi una persona alla rubrica");
            System.out.println("2 - Visualizza quante persone sono nella rubrica");
            System.out.println("3 - Ricerca una persona nella rubrica");
            scelta = tastiera.nextInt();
    
            switch(scelta){
                case 1:
                    Rubrica r = new Rubrica();
                    vet.aggiungiElenco(r);
                    break;
                case 2:
                    vet.visual();
                    break;
                case 3:
                    vet.ricerca();
                    break;
            }
    
            System.out.println("Vuoi continuare? (1 - SI; 0 - NO)");
            scelta = tastiera.nextInt();
    
        }while(scelta == 1);
    }
    
}