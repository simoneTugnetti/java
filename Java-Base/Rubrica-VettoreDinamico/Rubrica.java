import java.util.Scanner;

/**
 * @author Simone Tugnetti
 */
public class Rubrica{

    private final String nome;
    private final long numtel;

    public Rubrica() {
        Scanner tastiera = new Scanner(System.in);
        
        System.out.println("Inserisci il nome!!!");
        nome = tastiera.next();
        
        System.out.println("Inserisci il numero di telefono!!!");
        numtel = tastiera.nextLong();
    }
    
    public String getNome() {
        return nome;
    }
    
}