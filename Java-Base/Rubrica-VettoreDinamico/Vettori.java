import java.util.ArrayList;
import java.util.Scanner;

/**
 * @author Simone Tugnetti
 */
public class Vettori{

    private final ArrayList elenco;
    private boolean f;
    private String pers;
    
    public Vettori(){
        elenco = new ArrayList(5);
        f = false;
        pers = "";
    }
    
    public void aggiungiElenco(Rubrica r){
        elenco.add(r);
    }
    
    public void ricerca(){
        Scanner tastiera = new Scanner(System.in);
        Rubrica r;
    
        System.out.println("Inserisci il nome da ricercare!");
        pers = tastiera.next();
    
        for(int i=0; i<elenco.size(); i++){
            r = (Rubrica) elenco.get(i);
            
            if(r.getNome().equals(pers)){
                f = true;
            }
            
            if(f){
                System.out.println(r);
            }
        }
    }
    
    public void visual(){
        System.out.println("Il numero di persone aggiunte è "+elenco.size());
    }
    
}