import java.util.Scanner;

/**
 * @author Simone Tugnetti
 * Ricerca minimo e massimo tra due numeri
 */
public class main {

    public static void main(String args[]){
        Scanner tastiera = new Scanner(System.in);
        Controllo contr = new Controllo();
        contr.setNumero1(tastiera.nextDouble());
        contr.setNumero2(tastiera.nextDouble());
        contr.min();
        contr.max();
    }
    
}
