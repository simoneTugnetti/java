/**
 * @author Simone Tugnetti
 */
public class Controllo {
    
    private double num1;
    private double num2;

    public Controllo(){
        num1 = 0;
        num2 = 0;
    }
    
    public void setNumero1(double num1){
        this.num1 = num1;
    }
    
    public void setNumero2(double num2){
        this.num2 = num2;
    }
    
    public void min(){
        if(num1 < num2){
            System.out.println("il numero minimo è: "+num1);
        } else {
            System.out.println("il numero minimo è: "+num2);
        }
    }
    
    public void max(){
        if(num1 > num2){
            System.out.println("il numero massimo è: "+num1);
        } else {
            System.out.println("il numero massimo è: "+num2);
        }
    }
    
}
