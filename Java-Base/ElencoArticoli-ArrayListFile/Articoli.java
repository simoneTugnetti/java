import java.util.Scanner;

/**
 * @author Simone Tugnetti
 */
public class Articoli {
 
    private final String codice;
    private final String descrizione;
    private final double prezzo;

    public Articoli(){
        Scanner tastiera = new Scanner(System.in);
        
        System.out.print("Codice: ");
        codice = tastiera.next();
        System.out.print("Descrizione: ");
        descrizione = tastiera.next();
        System.out.print("Prezzo: ");
        prezzo = tastiera.nextDouble();
    }

    public String stampa() {
        return codice+", "+descrizione+", "+prezzo;
    }
    
}
