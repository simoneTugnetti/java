import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;

/** 
 * @author Simone Tugnetti
 */
public class Catalogo {
    
    private final ArrayList lista;
    public FileReader f;
    public BufferedReader fIN;
    private String s;
    public FileWriter r;
    public PrintWriter fOUT;

    public Catalogo() {
        lista = new ArrayList(2);
    }

    public void aggiungiGestione(Articoli articoli){
        lista.add(articoli);
    }

    public void eliminaGestione(int indice){
        lista.remove(indice);
        System.out.println("Eliminazione effettuata.");
    }

    public void visualizza(){
        Articoli articoli;
        System.out.println("\nCATALOGO");
        for(int i=0; i<lista.size(); i++){
            articoli = (Articoli)lista.get(i);
            System.out.println(articoli.stampa());
        }
    }
    
    public void letturaDati() throws Exception{
        f = new FileReader("dati.txt");
        fIN = new BufferedReader(f);
        s = fIN.readLine();
        lista.add(s);
        f.close();
    }
    
    public void salvataggioDati() throws Exception{
        r = new FileWriter("Dati.txt");
        fOUT = new PrintWriter(r);
        fOUT.println(lista);
        fOUT.flush();
        r.close();
    }
    
}
