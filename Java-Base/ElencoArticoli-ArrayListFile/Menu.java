import java.util.Scanner;

/**
 * @author Simone Tugnetti
 */
public class Menu {
    
    private void mostraMenu() {
        System.out.println("1 - Inserimento dei dati");
        System.out.println("2 - Cancellazione di un indice");
        System.out.println("3 - Visualizzazione dei dati");
        System.out.println("4 - Esci");
    }

    public int scelta(){
        Scanner tastiera = new Scanner(System.in);
        int scelta;
        
        mostraMenu();
        System.out.print("\n -> ");
        scelta = tastiera.nextInt();
        return scelta;
    }

    public int leggiIndice(){
        Scanner tastiera = new Scanner(System.in);
        int indice;
        System.out.print("\nVoce da eliminare: ");
        indice = tastiera.nextInt();
        return indice;
    }
    
}
