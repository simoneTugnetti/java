/**
 * @author Simone Tugnetti
 * Gestione infromazioni Articoli tramite Vettori e file
 */
public class main {
    
    public static void main(String args[]) throws Exception{
        Catalogo mioCatalogo = new Catalogo();
        Menu mioMenu = new Menu();
        int scelta;
        scelta = mioMenu.scelta();
        
        while (scelta != 4) {
            switch (scelta) {
                case 1:
                    Articoli articoli = new Articoli();
                    mioCatalogo.aggiungiGestione(articoli);
                    break;
                case 2:
                    int indice = mioMenu.leggiIndice();
                    mioCatalogo.eliminaGestione(indice);
                    break;
                case 3:
                    mioCatalogo.visualizza();
                    break;
                default:
                    break;
            }
            scelta = mioMenu.scelta();
        }
        mioCatalogo.salvataggioDati();
    }
    
}