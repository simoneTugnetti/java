import java.util.Scanner;

/**
 * @author Simone Tugnetti
 * Calcolo della divisione dati due numeri in input
 */
public class main {

    public static void main(String args[]){
    
        Scanner tastiera = new Scanner(System.in);
        Divisione div = new Divisione();
       
        System.out.println("Inserisci il primo valore: ");
        int a = tastiera.nextInt();
        
        System.out.println("Inserisci il secondo valore: ");
        int b = tastiera.nextInt();
        
        div.calcola(a, b);
        
        div.visualizza();
        
    }
}
