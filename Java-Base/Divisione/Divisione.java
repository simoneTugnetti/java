/**
 * @author Simone Tugnetti
 */
public class Divisione {

    private double tot;

    public Divisione(){
        tot = 0;
    }
    
    public void calcola(double num1, double num2){
        if(num2 == 0){
            System.out.println("Impossibile dividere per 0");
        } else {
            tot = num1 / num2;
        }
    }
    
    public double getTotale(){
        return tot;
    }
    
    public void visualizza(){
        System.out.println("Il risultato è: " + tot);
    }
    
}
