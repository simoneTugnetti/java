/**
 * @author Simone Tugnetti
 * Simulazione di una partita a dadi
 */
public class Main {
    
    public static void main(String args[]) throws Exception{
        Dadi dadi = new Dadi();
        dadi.giocata();
        dadi.risultato();
    }
    
}
