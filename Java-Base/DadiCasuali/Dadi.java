import java.util.Random;
import java.util.Scanner;

/**
 * @author Simone Tugnetti
 */
public class Dadi {
    
    private int dado;
    private final Random random = new Random();
    private String giocatore1;
    private String giocatore2;
    public int risultato;
    public int risultato2;
    public int turni;
    private final int nDado;
    
    public Dadi(){
        dado = 0;
        giocatore1 = "";
        giocatore2 = "";
        risultato = 0;
        risultato2 = 0;
        turni = 0;
        nDado = 5;
    }
    
    public void giocata() throws Exception{
        Scanner tastiera = new Scanner(System.in);
        
        System.out.println("Inserisci il nome del primo giocatore:");
        giocatore1 = tastiera.next();
        
        System.out.println("Inserisci il nome del secondo giocatore:");
        giocatore2 = tastiera.next();
        
        do{
            System.out.println("E' il turno di " + giocatore1 + "!");
            dado = random.nextInt(nDado) + 1;
            System.out.println("E' uscito il numero: " + dado);
            risultato = risultato + dado;
        
            System.out.println("E' il turno di " + giocatore2 + "!");
            dado = random.nextInt(nDado) + 1;
            System.out.println("E' uscito il numero: " + dado);
            risultato2 = risultato2 + dado;
            turni++;
        }while(turni < 3);
    }
    
    public void risultato(){
        if(risultato > risultato2){
            System.out.println("Il vincitore è: "+giocatore1);
            System.out.println("Con un punteggio di: "+risultato);
        } else {
            System.out.println("Il vincitore è: "+giocatore2);
            System.out.println("Con un punteggio di: "+risultato2);
        }
    }
}
