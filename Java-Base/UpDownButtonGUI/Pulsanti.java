import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

/**
 * @author Simone Tugnetti
 * Prova Bottoni Up/Down usando l'Interfaccia Grafica
 */
class Pulsanti {
  
    public static void main(String argv[]){
        JFrame frame = new JFrame("Pulsanti");
        JPanel panel = new JPanel();
        JButton sup = new JButton("Superiore");
        JButton inf = new JButton("Inferiore");
        JTextArea info = new JTextArea(50,10);

        panel.setLayout(new BorderLayout());
        panel.add(sup, "North");
        panel.add(info, "Center");
        panel.add(inf, "South");

        frame.addWindowListener(new GestoreFinestra());
        sup.addActionListener(new GestorePulsante(info));
        inf.addActionListener(new GestorePulsante(info));

        info.setEditable(false);

        frame.getContentPane().add(panel);
        frame.setSize(450,300);
        frame.setVisible(true);
    }
    
}
