import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JTextArea;

class GestorePulsante implements ActionListener {
  
    private final JTextArea info;

    public GestorePulsante(JTextArea info) {
        this.info = info;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String pulsante = e.getActionCommand();

        if (pulsante.equals("Superiore")) {
            info.append("E' stato premuto il pulsante *superiore*.\n");
        }
    
        if (pulsante.equals("Inferiore")) {
            info.append("E' stato premuto il pulsante *inferiore*.\n");
        }
        
    }
    
}
