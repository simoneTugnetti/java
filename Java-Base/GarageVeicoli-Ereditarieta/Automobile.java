/**
 * @author Simone Tugnetti
 */
class Automobile extends VeicoliAMotore{

    private int numPorte;

    public Automobile(){
        super();
        numPorte = 0;
    }

    public int getNumPorte(){
        return numPorte;
    }
    
    public void setNumPorte(int p){
        numPorte = p;
    }

    @Override
    public String toString() {
        return "Automobile: \n" + super.toString() + "\n "
                + "Numero porte: " + numPorte;
    }
    
}
