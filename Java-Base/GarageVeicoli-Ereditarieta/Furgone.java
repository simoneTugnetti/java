/**
 * @author Simone Tugnetti
 */
class Furgone extends VeicoliAMotore{

    private int capacitaCarico;

    public Furgone(){
        super();
        capacitaCarico = 0;
    }

    public int getCapacita(){
        return capacitaCarico;
    }
    
    public void setCapacita(int c){
        capacitaCarico = c;
    }

    @Override
    public String toString() {
        return "Furgone: \n" + super.toString() + " \n "
                + "Capacità di carico: " + capacitaCarico;
    }
    
}
