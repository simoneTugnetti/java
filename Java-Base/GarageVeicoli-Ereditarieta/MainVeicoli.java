import java.util.Scanner;

/**
 * @author Simone Tugnetti
 * Gestione di Veicoli a Motore usando l'Ereditarietà
 */
public class MainVeicoli {

    public static void main(String[] args){
        Scanner tastiera = new Scanner(System.in);
        Furgone fur[];
        fur = new Furgone[50];
        Automobile auto[];
        auto = new Automobile[50];
        Moto moto[];
        moto = new Moto[50];
        Garage garage = new Garage();
        int scelta, numFur = 0, numAuto = 0, numMoto = 0;
        
        do{
            
            System.out.println("Questo veicolo è un Furgone, un Auto o una Moto? "
                    + "(1 - Furgone; 2 - Auto; 3 - Moto; 4 - Visualizza Garage)");
            scelta = tastiera.nextInt();
            
            switch(scelta) {
                case 1:
                    fur[numFur] = new Furgone();
                    
                    System.out.println("Anno immatricolazione:");
                    fur[numFur].setAnnoImm(tastiera.nextInt());
            
                    System.out.println("Marca:");
                    fur[numFur].setMarca(tastiera.next());
            
                    System.out.println("Tipo di alimentazione:");
                    fur[numFur].setTipoAlimentaz(tastiera.next());
            
                    System.out.println("Cilindrata:");
                    fur[numFur].setCilindrata(tastiera.nextInt());
                    
                    System.out.println("Capacità carico:");
                    fur[numFur].setCapacita(tastiera.nextInt());
                    
                    System.out.println(fur[numFur]);
                    
                    garage.immettiNuovoVeicolo(fur[numFur]);
                    
                    numFur++;
                    break;
                case 2:
                    auto[numAuto] = new Automobile();
                    
                    System.out.println("Anno immatricolazione:");
                    auto[numAuto].setAnnoImm(tastiera.nextInt());
            
                    System.out.println("Marca:");
                    auto[numAuto].setMarca(tastiera.next());
            
                    System.out.println("Tipo di alimentazione:");
                    auto[numAuto].setTipoAlimentaz(tastiera.next());
            
                    System.out.println("Cilindrata:");
                    auto[numAuto].setCilindrata(tastiera.nextInt());
                    
                    System.out.println("Numero di porte:");
                    auto[numAuto].setNumPorte(tastiera.nextInt());
                    
                    System.out.println(auto[numAuto]);
                    
                    garage.immettiNuovoVeicolo(auto[numAuto]);
                    
                    numAuto++;
                    break;
                case 3:
                    moto[numMoto] = new Moto();
                    
                    System.out.println("Anno immatricolazione:");
                    moto[numMoto].setAnnoImm(tastiera.nextInt());
            
                    System.out.println("Marca:");
                    moto[numMoto].setMarca(tastiera.next());
            
                    System.out.println("Tipo di alimentazione:");
                    moto[numMoto].setTipoAlimentaz(tastiera.next());
            
                    System.out.println("Cilindrata:");
                    moto[numMoto].setCilindrata(tastiera.nextInt());
                    
                    System.out.println("Tipologia:");
                    moto[numMoto].setTipologia(tastiera.next());
                    
                    System.out.println("Tempi motore:");
                    moto[numMoto].setTempMotore(tastiera.nextInt());
                    
                    System.out.println(moto[numMoto]);
                    
                    garage.immettiNuovoVeicolo(moto[numMoto]);
                    
                    numMoto++;
                    break;
                case 4:
                    garage.StampaSituazionePosti();
                    break;
            }
            
            System.out.println("Vuoi continuare? (1 - SI; 0 - NO)");
            scelta = tastiera.nextInt();
            
        }while(scelta != 0);
    }
}
