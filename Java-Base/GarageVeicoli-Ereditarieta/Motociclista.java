/**
 * @author Simone Tugnetti
 */
class Moto extends VeicoliAMotore{

    private String tipologia;
    private int numTempiMotore;

    public Moto(){
        super();
        tipologia = "";
        numTempiMotore = 0;
    }

    public String getTipologia(){
        return tipologia;
    }
    
    public void setTipologia(String t){
        tipologia = t;
    }
    
    public int getTempiMotore(){
        return numTempiMotore;
    }
    
    public void setTempMotore(int t){
        numTempiMotore = t;
    }

    @Override
    public String toString() {
        return "Moto: \n" + super.toString() + "\n "
                + "Tipologia: " + tipologia + "\n "
                + "Tempi motore: " + numTempiMotore;
    }
    
    
}
