import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author Simone Tugnetti
 */
public class Garage {
    
    private final VeicoliAMotore[] veicoli;
    private int numPosti;
    
    public Garage(){
        numPosti = 0;
        veicoli = new VeicoliAMotore[100];
    }
    
    public int immettiNuovoVeicolo(VeicoliAMotore v){
        FileWriter f;
        PrintWriter fOUT;
        
        veicoli[numPosti] = v;
        
        try {
            f = new FileWriter("aggiuntaAuto.txt");
            fOUT = new PrintWriter(f);
            fOUT.println(v);
            fOUT.flush();
        } catch (IOException e) {
            System.out.println("Errore: " + e.getMessage());
            System.exit(0);
        }
          return numPosti++;
    }
    
    public VeicoliAMotore estraiVeicolo(int posto){
        if(posto > numPosti) {
            return null;
        } else {
            numPosti--;
            return veicoli[posto];
        }
    }
    
    public void StampaSituazionePosti(){
        for(int i=0; i<numPosti; i++) {
            System.out.println(veicoli[i]);
        }
    }

}
