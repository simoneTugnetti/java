/**
 * @author Simone Tugnetti
 */
public class Cerchio {

    private double raggio;
    
    public void setRaggio(double r) {
        raggio = r;
    }
    
    public double area() {
        return raggio * raggio * Math.PI;
    }
    
}
