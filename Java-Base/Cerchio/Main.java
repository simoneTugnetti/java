/**
 * @author Simone Tugnetti
 * Calcolo area del cerchio
 */
public class Main {
    
    public static void main(String args[]){
        // Dichiarazione dell'oggetto
        Cerchio tavolo;
       
        // Creazione dell'istanza
        tavolo = new Cerchio();
        tavolo.setRaggio(3.8);
        System.out.println("Area del tavolo vale "+tavolo.area());
    }

}
