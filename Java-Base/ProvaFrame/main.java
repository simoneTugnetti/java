import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * @author Simone Tugnetti
 * Prova utilizzo Frame
 */
public class main {

    @SuppressWarnings("SleepWhileInLoop")
    public static void main(String args[]) throws InterruptedException {
        final Frame f = new Frame("First Frame");
        
        f.setSize(320,240);
        f.setLocation(10,10);
    
        WindowAdapter wa = new WindowAdapter(){
        
            @Override
            public void windowClosing(WindowEvent we){
                f.dispose();
            }
        };
    
        f.addWindowListener(wa);
        f.setVisible(true);
    
        for(int i=0; i<10; i++){
            System.out.println(f.getWidth()+", "+f.getHeight());
            Thread.sleep(2000);
        }
        
        System.exit(0);
    }
    
}
