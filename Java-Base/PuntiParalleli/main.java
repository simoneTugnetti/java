import java.util.Scanner;

/**
 * @author Simone Tugnetti
 * Distanza tra due punti usando le Classi
 */
public class main {
 
    public static void main(String args[]) {
        Scanner tastiera = new Scanner(System.in);
        Punti punti = new Punti();
        
        System.out.println("Scrivi il primo punto:");
        punti.setPrima(tastiera.nextDouble());
        
        System.out.println("Scrivi il secondo punto:");
        punti.setSeconda(tastiera.nextDouble());
        punti.distanza();
        punti.visualizza();
    }
    
}
