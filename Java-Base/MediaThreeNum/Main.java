import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author Simone Tugnetti
 * Media dati tre numeri in input
 */
public class Main {

    public static void main(String args[]){
        InputStreamReader input = new InputStreamReader(System.in);
        BufferedReader tastiera = new BufferedReader(input);
    
        int somma = 0;
        double media;
        
        for(int i=0; i<3; i++) {
            System.out.println("Inserisci la " + (i+1) + "° età");
            try {
                int valore = Integer.parseInt(tastiera.readLine());
                somma += valore;
            } catch(IOException| NumberFormatException e) {
                System.out.println("Errore: " + e.getMessage());
            }
        }
        
        media = somma / 3;
        System.out.println("Età media: " + media);
    }

}
