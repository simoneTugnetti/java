

/**
 * @author Simone Tugnetti
 */
public class LibroBiblioteca {

    private int numeroCopie;
    
    public LibroBiblioteca() {
        numeroCopie = 10;
    }

    public void setNumeroCopie(int n){
        numeroCopie = n;
    }
    
    public int getNumeroCopie(){
        return numeroCopie;
    }

    public void prestito(){
        if (numeroCopie != 0) {
            System.out.println("Prestito effettuato!");
            numeroCopie--;
        } else {
            System.out.println("Non è possibile effettuare un prestito");
        }
    }

    public void restituzione(){
        numeroCopie++;
        System.out.println("Restituito!");
    }

}
