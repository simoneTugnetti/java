

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author Simone Tugnetti
 * Simulazione di una biblioteca tramite numero di copie
 */
public class Main {

    public static void main(String args[]){
        InputStreamReader input = new InputStreamReader(System.in);
        BufferedReader tastiera = new BufferedReader(input);
        LibroBiblioteca libro = new LibroBiblioteca();
        int scelta = 0;
        boolean run = true;
        
        do {
            System.out.println("1 - Prendi in prestito");
            System.out.println("2 - Restituisci");
            System.out.println("3 - Esci");
            System.out.println("Numero Copie disponibili: " + 
                    libro.getNumeroCopie());
        
            try {
                scelta = Integer.parseInt(tastiera.readLine());
            } catch(IOException e){
                System.out.println("Errore: " + e.getMessage());
            }
            
            switch(scelta){
                case 1:
                    libro.prestito();
                break;
                case 2:
                    libro.restituzione();
                break;
                case 3:
                    run = false;
                break;
            }
            
        } while(run);
    }

}
