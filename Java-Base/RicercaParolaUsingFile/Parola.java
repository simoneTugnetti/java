import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 * @author Simone Tugnetti
 */
public class Parola {

    private FileReader f;
    private BufferedReader fIN;
    private FileWriter fr;
    private PrintWriter fOUT;
    private String readString;
    private int contParole;
    private String parola;
    private final String[] par;

    public Parola(){
        par = new String[100];
        f = null;
        fIN = null;
        fr = null;
        fOUT = null;
        readString = "";
        parola = "";
    }
    
    public void lettura() throws FileNotFoundException, IOException{
        Scanner tastiera = new Scanner(System.in);
        contParole = 0;
        System.out.println("Quale parola devi cercare?");
        parola = tastiera.next();
        f = new FileReader("parole.txt");
        fIN = new BufferedReader(f);
        readString = fIN.readLine();
        while(readString != null){
            if(readString.equals(parola)){
                contParole++;
            }
            readString = fIN.readLine();
        }
        System.out.println("La parola compare " + contParole + " volte");
        f.close();
    }
    
    public void letturaeScrittura() throws FileNotFoundException, IOException{
        contParole = 0;
        Scanner tastiera = new Scanner(System.in);
    
        System.out.println("Qual è la parola da sostituire?");
        parola = tastiera.next();
        f = new FileReader("parole.txt");
        fIN = new BufferedReader(f);
        readString = fIN.readLine();
        while(readString != null){
            if(readString.equals(parola)){
                System.out.println("Quale parola vuoi scrivere?");
                par[contParole] = tastiera.next();
            } else {
                readString = fIN.readLine();
                par[contParole] = readString;
            }
            contParole++;
        }
        
        f.close();
        fr = new FileWriter("parole.txt");
        fOUT = new PrintWriter(fr);
        for(int j=0; j < contParole-1; j++){
            fOUT.println(par[j]);
            fOUT.flush();
        }
        fr.close();
    }
    
}
