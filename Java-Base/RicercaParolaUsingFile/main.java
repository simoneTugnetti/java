import java.io.IOException;
import java.util.Scanner;

/**
 * @author Simone Tugnetti
 * Lettura e scrittura di un file data una parola
 */
public class main {

    public static void main(String args[]) throws IOException{
        Scanner tastiera = new Scanner(System.in);
        int scelta;
        Parola parola = new Parola();
        
        do {
            System.out.println("Che cosa vuoi fare?");
            System.out.println("1 - Leggere e contare una parola");
            System.out.println("2 - Leggere e cambiare una parola");
            scelta = tastiera.nextInt();
            switch(scelta){
                case 1:
                    parola.lettura();
                    break;
                case 2:
                    parola.letturaeScrittura();
                    break;
                }
            System.out.println("Vuoi continuare? (1 - SI; 0 - NO)");
            scelta = tastiera.nextInt();
        } while(scelta == 1);
    }
    
}
