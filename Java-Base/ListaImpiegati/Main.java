import java.util.Scanner;

/**
 * @author Simone Tugnetti
 * Gestione di Impiegati usando una LinkedList
 */
public class Main {
    
    public static void main(String args[]) {
        String nome, cognome, mansione, matricola, cercaMatricola;
        float stipendio;
        GestioneListaImpiegati gestione = new GestioneListaImpiegati();
        Scanner tastiera = new Scanner(System.in);
        
        while(true) {
            
            System.out.println("1 - Aggiungi nuovo impiegato");
            System.out.println("2 - Modifica la mansione di un impiegato");
            System.out.println("3 - Stampa lista degli impiegati");
            System.out.println("4 - Elimina un impiegato");
            System.out.println("5 - Aumenta lo stipendio degli impiegati");
            System.out.println("6 - Esci");
            System.out.print("Inserisci scelta: ");
            
            switch(tastiera.nextInt()){
                case 1:
                    System.out.print("Inserisci nome: ");
                    nome = tastiera.next();
                    System.out.print("Inserisci cognome: ");
                    cognome = tastiera.next();
                    System.out.print("Inserisci matricola: ");
                    matricola = tastiera.next();
                    System.out.print("Inserisci stipendio: ");
                    stipendio = tastiera.nextFloat();
                    System.out.print("Inserisci mansione: ");
                    mansione = tastiera.next();
                    gestione.nuovoImpiegato(new Impiegato(
                            matricola, nome, cognome, mansione, stipendio));
                    System.out.println("Impiegato aggiunto correttamente");
                    break;
                case 2:
                    if(gestione.getImpiegati().isEmpty()) {
                        System.out.println("Non esistono impiegati.");
                    } else {
                        System.out.print("Inserisci matricola dell'impiegato: ");
                        cercaMatricola = tastiera.next();
                        System.out.print("Insersci nuova masione dell'impiegato: ");
                        mansione = tastiera.next();
                        gestione.modificaMansione(cercaMatricola, mansione);
                        System.out.println("Mansione modificata.");
                    }
                    break;
                case 3:
                    if(gestione.getImpiegati().isEmpty()) {
                        System.out.println("Non esistono impiegati.");
                    } else {
                        System.out.print("Inserisci la mansione di cui vuoi"
                                + " stampare gli impiegati (o premi invio per"
                                + " stamparli tutti): ");
                        gestione.stampaImpiegati(tastiera.next());
                    }
                    break;
                case 4:
                    if(gestione.getImpiegati().isEmpty()) {
                        System.out.println("Non esistono impiegati.");
                    } else {
                        System.out.print("Inserisci il numero dell'impiegato"
                                + " che vuoi eliminare: ");
                        gestione.eliminaImpiegato(tastiera.nextInt());
                    }
                    break;
                case 5:
                    if(gestione.getImpiegati().isEmpty()) {
                        System.out.println("Non esistono impiegati.");
                    } else {
                        System.out.print("Inserisci la percentuale dell'aumento: ");
                        gestione.aumentaStipendi(tastiera.nextInt());
                    }
                    break;
                case 6:
                    System.exit(0);
                    break;
                default:
                    System.out.println("Scelta non valida.");
                    break;
            }
        }
    }
}
