

import java.util.Scanner;

/**
 * @author Simone Tugnetti
 */
class Libro extends Pubblicazione{

    private int copieVendute;
    private int scelta;

    public Libro(){
        copieVendute = 0;
        scelta = 0;
    }
    
    @Override
    public void aumento(){
        Scanner tastiera = new Scanner(System.in);
        
        System.out.println("Vuoi acquistare questo libro? (1 - SI; 0 - NO)");
    
        scelta = tastiera.nextInt();   
        
        while(scelta == 1) {
            System.out.println("Grazie dell'acquisto");
            copieVendute++;
                
            System.out.println("Vuoi acquistare di nuovo? (1 - SI; 0 - NO)");
            scelta = tastiera.nextInt();
        }
        
    }
    
    @Override
    public void visualizza(){
        super.visualizza();
        System.out.println("Questo è il numero di libri venduti: " + copieVendute);
    }
    
    public int getNcopie(){
        return copieVendute;
    }
    
}
