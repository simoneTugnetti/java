

/**
 * @author Simone Tugnetti
 * Simulazione Casa editrice usando l'Ereditarietà
 */
public class Main {

    public static void main(String args[]){
        Pubblicazione pub = new Pubblicazione();
        pub.aumento();
        pub.getPrezzo();
        pub.visualizza();
    
        Libro libro = new Libro();
        libro.aumento();
        libro.getNcopie();
        libro.visualizza();
    
        Periodico per = new Periodico();
        per.aumento();
        per.getDirettore();
        per.visualizza();
        
    }
}
