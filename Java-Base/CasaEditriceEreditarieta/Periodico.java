

import java.util.Scanner;

/**
 * @author Simone Tugnetti
 */
class Periodico extends Libro{

    private String direttore;

    public Periodico(){
        direttore = "";
    }
    
    @Override
    public void aumento(){
        Scanner tastiera = new Scanner(System.in);
        System.out.println("Qual'è il nome del direttore?");
        direttore = tastiera.next();
    }
    
    @Override
    public void visualizza(){
        super.visualizza();
        System.out.println("Questo è il nome del direttore: " + direttore);
    }
    
    public String getDirettore(){
        return direttore;
    }
    
}
