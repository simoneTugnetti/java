

import java.util.Scanner;

/**
 * @author Simone Tugnetti
 * 
 */
class Pubblicazione {
    
    private int scelta;
    private int prezzo;
    private String titolo;
    private int aumen;
    
    public Pubblicazione(){
        prezzo = 0;
        titolo = "";
        scelta = 0;
        aumen = 0;
    }
    
    public void aumento(){
        Scanner tastiera = new Scanner(System.in);
        
        do {    
            System.out.println("Inserisci il titolo: ");
            titolo = tastiera.next();
            
            System.out.println("Di quanto si dovrebbe aumentare il prezzo "
                    + "del libro?");
            aumen = tastiera.nextInt();   
            prezzo = prezzo + aumen;
                
            System.out.println("Ti sembra un prezzo equo o è meglio "
                        + "aumentarlo? (1 - aumenta; 0 - questo prezzo va bene)");
            scelta = tastiera.nextInt();
        }while(scelta == 1);
        
    }
    
    public void visualizza(){
        System.out.println("Titolo: " + titolo + "\nPrezzo: " + prezzo);
    }
    
    public String getTitolo(){
        return titolo;
    }
    
    public int getPrezzo(){
        return prezzo;
    }
    
}
