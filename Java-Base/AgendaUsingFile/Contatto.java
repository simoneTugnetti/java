import java.util.Scanner;

/**
 * @author Simone Tugnetti
 */
public class Contatto {

    private String cognome;
    private String nome;
    private String telefono;

    public Contatto(){ }

    public Contatto leggi(){
        Scanner tastiera = new Scanner(System.in);
        
        System.out.println("Inserisci il cognome (* per finire): ");
        cognome = tastiera.next();
        if(cognome.equals("*")) {
            return null;
        }
        System.out.println("Inserisci il nome: ");
        nome = tastiera.next();
        System.out.println("Inserisci il telefono: ");
        telefono = tastiera.next();
        
        return this;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
    
    public void stampa(){
        System.out.println(cognome + " " + nome + "   "+ telefono);
    }
}

