/**
 * @author Simone Tugnetti
 * Lettura e Scrittura Agenda su File
 */
public class ProgAgenda {

    public static void main(String argv[]) {
        ScriviAgenda scrAgenda = new ScriviAgenda();
        scrAgenda.scrivi();
        LeggiAgenda lgAgenda = new LeggiAgenda();
        lgAgenda.leggeToken();
    }
    
}
