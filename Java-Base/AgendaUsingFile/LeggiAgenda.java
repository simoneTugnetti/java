import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;

/**
 * @author Simone Tugnetti
 */
public class LeggiAgenda {

    public LeggiAgenda(){}

    public void leggeToken(){
        FileReader f;
        BufferedReader fIN;
        Contatto c;
        String s;
        StringTokenizer st;

        try{
            f = new FileReader("agenda.txt");
            fIN = new BufferedReader(f);
            c = new Contatto();
            
            s = fIN.readLine();
            while(s != null) {
                st = new StringTokenizer(s, ";");
                c.setCognome(st.nextToken());
                c.setNome(st.nextToken());
                c.setTelefono(st.nextToken());
                c.stampa();
                s = fIN.readLine();
            }
            f.close();
        
        } catch (IOException e) {
            System.out.println("Errore: " + e.getMessage());
            System.exit(0);
        }
    }
    
}
