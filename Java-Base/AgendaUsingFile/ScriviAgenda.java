import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author Simone Tugnetti
 */
public class ScriviAgenda {

    public ScriviAgenda() {}

    public void scrivi(){
        Contatto c;
        FileWriter f;
        PrintWriter fOUT;
    
        try {              
            f = new FileWriter("agenda.txt");
            fOUT = new PrintWriter(f);
            c = new Contatto();
            while(c.leggi() != null) {
                fOUT.println(c.getCognome() + ";" + c.getNome() 
                        + ";" + c.getTelefono());
                fOUT.flush();
            }
            f.close();
        } catch(IOException e){
            System.out.println("Errore: " + e.getMessage());
            System.exit(0);
        }
        
    }
    
}
