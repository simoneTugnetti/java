/**
 * @author Simone Tugnetti
 * Utilizzo dei Nodi per l'organizzazione di una festa
 */
class Main {
 
    private Nodo head;
    private int elementi;
 
    public Main() {
        head = null;
        elementi = 0;
    }
 
    private Nodo getLinkPosizione(int posizione) throws FestaException {
        int n = 1;
        Nodo nodo = head;
        
        if (head == null){
            throw new FestaException("Lista vuota");
        }
        
        if ((posizione>elementi) || (posizione<1)){
            throw new FestaException("Posizione errata");
        }
        
        while ((nodo.getLink() != null) && (n < posizione)) {
            nodo = nodo.getLink();
            n++;
        }
        
        return nodo;
    }
 
    private Nodo creaNodo(Invitato persona, Nodo link) {
        Nodo nuovoNodo = new Nodo(persona);
        nuovoNodo.setLink(link);
        return nuovoNodo;
    }

    public int getElementi() {
        return elementi;
    }

    public void inserisciInTesta(Invitato persona) {
        Nodo nodo = creaNodo(persona, head);
        head = nodo;
        elementi++;
    }
 
    public void inserisciInCoda(Invitato persona) {
        if (head==null){
            inserisciInTesta(persona);
        } else {
            try {
                Nodo nodo = getLinkPosizione(elementi);
                nodo.setLink(creaNodo(persona, null));
                elementi++;
            } catch (FestaException e){
                System.out.println("Errore: " + e.getError());
            }
        }
    }
 
    public void inserisciInPosizione(Invitato persona, int posizione) 
            throws FestaException {
     
        if (posizione <= 1) {
            inserisciInTesta(persona);
        } else {
            if (elementi < posizione) {
                inserisciInCoda(persona);
            } else {
                Nodo nodo = getLinkPosizione(posizione - 1);
                nodo.setLink(creaNodo(persona, nodo.getLink()));
                elementi++;
            }
        }
    } 
 
    public void eliminaInTesta() throws FestaException {
        if (head == null){
            throw new FestaException("Lista vuota");
        }
        head = head.getLink();
        elementi--;
    }

    public void eliminaInCoda() throws FestaException {
        if (head==null) {
            throw new FestaException("Lista vuota");
        }
        Nodo nodo = getLinkPosizione(elementi - 1);
        nodo.setLink(null);
        elementi--;
    }
 
    public void eliminaInPosizione(int posizione) throws FestaException {
        if (posizione == 1) {
            eliminaInTesta();
        } else {
            if (posizione == elementi) {
                eliminaInCoda();
            } else {
                Nodo firstNodo = getLinkPosizione(posizione);
                Nodo secondNodo = getLinkPosizione(posizione - 1);
                secondNodo.setLink(firstNodo.getLink());
                elementi--;
            }
        }
    }
 
    private String visita(Nodo nodo) {
        if (nodo == null) {
            return "";
        }
        return nodo.getInfo().toString() + "\n" + visita(nodo.getLink());
    }
 
    public String elenco() {
        return visita(head);
    }
 
    @Override
    public String toString() {
        Nodo nodo = head;
        String lista = "head->";
        if (nodo == null) {
            return lista + "null";
        }
        while (nodo != null) {
            lista = lista + "[" + nodo.getInfo().toString() + "|";
            if (nodo.getLink() == null) {
                lista = lista + "null]";
            } else {
                lista = lista + "+]->";
            }
            nodo = nodo.getLink();
        }
        return lista;
    }

    public static void main (String[] args) {
        Invitato i1 = new Invitato("Bianchi Giovanni",'M',"0586 854822");
        Invitato i2 = new Invitato("Rossi Marta",'F',"0586 844853");
        Invitato i3 = new Invitato("Neri Marco",'M',"0586 444722");
        Invitato i4 = new Invitato("Verdi Roberta",'F',"0586 974824");
        
        Main festa = new Main();
        festa.inserisciInTesta(i1) ;
        festa.inserisciInTesta(i2) ;
        festa.inserisciInCoda(i3) ;
 
        try {
            festa.inserisciInPosizione(i4, 2);
        } catch (FestaException e) {
            System.out.println("Errore: " + e.getError());
        }
        
        System.out.println("Visita ricorsiva: ");
        
        System.out.println(festa.elenco());
 
        System.out.println("------------------");
 
        System.out.println(festa.toString());
 
        System.out.println("------------------");
 
        try {
            festa.eliminaInPosizione(2);
        } catch (FestaException e) {
            System.out.println("Errore: " + e.getError());
        }
        System.out.println(festa.toString());
 
        System.out.println("------------------");
 
        try {
            festa.eliminaInCoda();
        } catch (FestaException e) {
            System.out.println("Errore: " + e.getError());
        }
        System.out.println(festa.toString());
 
        System.out.println("------------------");
 
        try { 
            festa.eliminaInTesta();
        } catch (FestaException e) {
            System.out.println("Errore: " + e.getError());
        }
        System.out.println(festa.toString());
 
        System.out.println("------------------");
 
        try { 
            festa.eliminaInPosizione(5);
        } catch (FestaException e) {
            System.out.println("Errore: " + e.getError());
        }
        System.out.println(festa.toString());
 
        System.out.println("------------------");
 
        try { 
            festa.eliminaInPosizione(1);
        } catch (FestaException e) {
            System.out.println("Errore:" + e.getError());
        }
        System.out.println(festa.toString());
 
        System.out.println("------------------");
 
        try { 
            festa.eliminaInPosizione(1);
        } catch (FestaException e) {
            System.out.println("Errore: " + e.getError());
        }
    }
    
}