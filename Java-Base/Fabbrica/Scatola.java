

import java.util.Random;

/**
 * @author Simone Tugnetti
 */
public class Scatola {

    private int codice;

    public Scatola(){
        codice = new Random().nextInt() % 1000000;
        if(codice < 0){
            codice *= -1;
        }
    }

    public int getCodice(){
        return codice;
    }
}
