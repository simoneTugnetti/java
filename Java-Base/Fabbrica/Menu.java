

import java.io.IOException;
import java.util.Scanner;

/**
 * @author Simone Tugnetti
 */
public class Menu {
    
    private final String[] voci;

    public Menu(String[] voci){
        this.voci = voci;
    }

    public void mostra(){
        System.out.println("\nInserisci la tua scelta");
        for(int i = 0; i < voci.length; i++){
            System.out.println((i + 1) + " - " + voci[i]);
        }
        System.out.println((voci.length + 1) + " - " + "Esci");
    }

    public static void pulisciSchermo(){
        System.out.println("Premi un tasto per continuare...");
        try{
            System.in.read();
            for(int i = 0; i < 50; i++)
                System.out.println();
        }
        catch(IOException e){
            System.out.println("Errore: " + e.getMessage());
        }
    }
    
    public int scelta(){
        Scanner tastiera = new Scanner(System.in);
        int scelta = 0;
        while(scelta == 0){
            System.out.println("Inserisci una scelta: ");
            scelta = tastiera.nextInt();
            if(scelta <= 0 || scelta > voci.length + 1){
                System.out.println("Hai inserito una scelta non valida");
                scelta = 0;
            }
        }
        return scelta;
    }

    public int uscita(){
        return voci.length + 1;
    }
}
