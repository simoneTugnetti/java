import java.io.FileWriter;
import java.io.PrintWriter;

/**
 * @author Simone Tugnetti
 */
public class ScritturaFile {
    
    private FileWriter f;
    private PrintWriter fOUT;
    private final int max;
    
    public ScritturaFile(){
        max = 1000;
    }
    
    public void scrittura() throws Exception{
        f = new FileWriter("Numeri.txt");
        fOUT = new PrintWriter(f);
        int d;
        for (int n = 2; n < max; n++) {
            d = 2;
            while ((n%d != 0) && (d < n)){
                d++;
            }
            if (d == n) {
                fOUT.println(n);
            }
        }
        fOUT.flush();
        f.close();
    }
    
}
