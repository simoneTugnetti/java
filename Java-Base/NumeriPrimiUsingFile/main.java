/**
 * @author Simone Tugnetti
 * Scrittura su file dei numeri primi da un determinato valore
 */
public class main {
    
    public static void main(String args[]) throws Exception{
        ScritturaFile scrFile = new ScritturaFile();
        scrFile.scrittura();
    }
}
