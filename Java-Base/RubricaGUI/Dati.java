/**
 * @author Simone Tugnetti
 */
public class Dati {
    
    private final String nome;
    private final String cognome;
    private final int ntel;
            
    public Dati(Dati d){
        nome = d.getNome();
        cognome = d.getCognome();
        ntel = d.getNTel();
    }
    
    public Dati(String nome, String cognome, int ntel){
        this.nome = nome;
        this.cognome = cognome;
        this.ntel = ntel;
    }
    
    public String getNome(){
        return nome;
    }
    
    public String getCognome(){
        return cognome;
    }
    
    public int getNTel(){
        return ntel;
    }
    
    @Override
    public String toString(){
        return getNome()+"; "+getCognome()+"; "+getNTel();
    }
    
}