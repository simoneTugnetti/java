import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 * @author Simone Tugnetti
 */
public class Frame {
    
    private JButton ins;
    private JButton elim;
    private JButton ricerca;
    private JMenu menu;
    private JMenuItem salva;
    private JMenuItem chiudi;
    private JMenuItem apri;
    private String nome;
    private String cognome;
    private int telefono;
    private JFrame frame;
    private Lista lista;
     
    public Frame(){}
    
    public void gestione(){
    
        frame = new JFrame("Rubrica");
        JPanel panel = new JPanel();
        JMenuBar mb = new JMenuBar();
        lista = new Lista();
    
        frame.setJMenuBar(mb);
    
        ins = new JButton("Inserisci");
        elim = new JButton("Elimina");
        ricerca = new JButton("Ricerca");
        
        panel.setLayout(null);
    
        menu = new JMenu("File"); 
        mb.add(menu);
    
        salva = new JMenuItem("Salva");
        chiudi = new JMenuItem("Chiudi");
        apri = new JMenuItem("Apri");
    
        menu.add(salva);
        menu.add(apri);
        menu.add(chiudi);
        panel.add(ins);
        panel.add(elim);
        panel.add(ricerca);
        
        ins.setBounds(10,10,100,50);
        elim.setBounds(10,70,100,50);
        ricerca.setBounds(10,130,100,50);
        
        frame.addWindowListener(new GestioneFinestra());
        ins.addActionListener(new pulsanti());
        elim.addActionListener(new pulsanti());
        ricerca.addActionListener(new pulsanti());
        salva.addActionListener(new pulsanti());
        chiudi.addActionListener(new pulsanti());
        apri.addActionListener(new pulsanti());
    
        frame.getContentPane().add(panel);
        frame.setSize(200,300);
        frame.setVisible(true);
    }
    
    class pulsanti implements ActionListener{//Creo la classe per poter gestire i vari pulsanti
        
        private FileWriter fw;
        private PrintWriter fOUT;
        private String nome;
        private FileReader fr;
        private BufferedReader fIN;
        private String s;
        private int scelta;
  
        public pulsanti(){}
  
        @Override
        public void actionPerformed(ActionEvent e) {
            String pulsante = e.getActionCommand();
    
            if (pulsante.equals("Inserisci")){
                inserisci();
            }
            
            if (pulsante.equals("Elimina")){
                elimina();
            }
            
            if (pulsante.equals("Ricerca")){
                ricerca();
            }
    
            if(pulsante.equals("Salva")){
                salva();
            }
            
            if(pulsante.equals("Apri")){
                apri();
            }
            
            if(pulsante.equals("Chiudi")){
                frame.dispose();
                System.exit(0);
            }
        }
        
        private void inserisci() {
            nome = JOptionPane.showInputDialog(null, "Inserisci il nome!");
        
            while(nome.equals("")){
                JOptionPane.showMessageDialog(null, 
                        "Devi prima inserire il nome", "info", 
                        JOptionPane.INFORMATION_MESSAGE);
            
                nome = JOptionPane.showInputDialog(null,"Inserisci il nome!");
            }
                
            cognome = JOptionPane.showInputDialog(null, "Inserisci il cognome!");
        
            while(cognome.equals("")){
                JOptionPane.showMessageDialog(null, 
                        "Devi prima inserire il cognome", "info", 
                        JOptionPane.INFORMATION_MESSAGE);
            
                cognome = JOptionPane.showInputDialog(null,"Inserisci il cognome!");
            }
                
            telefono = Integer.parseInt(JOptionPane
                    .showInputDialog("Inserisci il numero di telefono!", 0));
        
            while(telefono == 0){
                JOptionPane.showMessageDialog(null, 
                        "Devi prima inserire il numero di telefono", "info", 
                        JOptionPane.INFORMATION_MESSAGE);
                    
                telefono = Integer.parseInt(JOptionPane
                        .showInputDialog(0, "Inserisci il numero di telefono!"));
            }
                
            lista.inserisci(new Dati(nome,cognome,telefono));
            
        }
        
        private void elimina() {
            scelta = Integer.parseInt(JOptionPane
                        .showInputDialog(null, 
                            "Desideri eliminare tutti i contatti o uno solo?"
                                    + " (1 - Tutti; 0 - Uno)"));
            if(scelta == 1){
                lista.elimina();
                try {
                    fw = new FileWriter("Dati.txt");
                    fOUT = new PrintWriter(fw);
                    fOUT.println();
                    fOUT.flush();
                    fw.close();
                } catch(IOException exception){
                    System.out.println("Errore: " + exception.getMessage());
                }
            } else {
                nome = JOptionPane.showInputDialog(null, 
                        "Inserisci il nome della persona da cancellare!");
                    
                for(int i=0; i<lista.size(); i++){
                    if(lista.getRubrica().get(i).getNome().equals(nome)){
                        lista.getRubrica().remove(i);
                        JOptionPane.showMessageDialog(null, 
                                "Il contatto è stato eliminato", "info", 
                                JOptionPane.INFORMATION_MESSAGE);
                    }
                }
            }
            
        }
        
        private void ricerca() {
            nome = JOptionPane.showInputDialog(null, 
                        "Inserisci il nome della persona da ricercare!");
      
            for(int i=0; i<lista.size(); i++){
                if(lista.getRubrica().get(i).getNome().equals(nome)){
                    nome = JOptionPane.showInputDialog(null, 
                            "Inserisci il nome!");
        
                    while(nome.equals("")){
                        JOptionPane.showMessageDialog(null, 
                                "Devi prima inserire il nome!", "info", 
                                    JOptionPane.INFORMATION_MESSAGE);
            
                        nome = JOptionPane.showInputDialog(null, 
                                    "Inserisci il nome!");
                    }
                        
                    cognome = JOptionPane.showInputDialog(null, 
                                "Inserisci il cognome!");
        
                    while(cognome.equals("")){
                        JOptionPane.showMessageDialog(null, 
                                "Devi prima inserire il cognome!", "info", 
                                    JOptionPane.INFORMATION_MESSAGE);
            
                        cognome = JOptionPane.showInputDialog(null, 
                                    "Inserisci il cognome!");
                    }
        
                    telefono = Integer.parseInt(JOptionPane
                                .showInputDialog(0, 
                                    "Inserisci il numero di telefono!"));
        
                    while(telefono == 0){
                        JOptionPane.showMessageDialog(null, 
                                "Devi prima inserire il numero di telefono",
                                    "info", JOptionPane.INFORMATION_MESSAGE);
            
                        telefono = Integer.parseInt(JOptionPane
                                    .showInputDialog(0, 
                                        "Inserisci il numero di telefono!"));
                    }
                        
                    lista.getRubrica().add(i, new Dati(nome,cognome,telefono));
             
                    JOptionPane.showMessageDialog(null, 
                                "Il contatto è stato modificato", "info", 
                                    JOptionPane.INFORMATION_MESSAGE);
                }
            }
            
        }
        
        private void salva() {
            for(int i=0; i<lista.size(); i++){
                for(int j=i+1; j<lista.size(); j++){
                    if(lista.getRubrica().get(i).getCognome()
                            .compareTo(lista.getRubrica().get(j)
                                    .getCognome()) > 0){
                            
                        Dati temp = lista.getRubrica().get(i);
                        lista.getRubrica().set(i, lista.getRubrica().get(j));
                        lista.getRubrica().set(j, temp);
                            
                    }
                }
            }
                
            try {
                fw = new FileWriter("Dati.txt");
                fOUT = new PrintWriter(fw);
                for(int i=0; i<lista.size(); i++){
                    fOUT.println(lista.getRubrica().get(i));
                }
                fOUT.flush();
                fw.close();
            } catch(IOException exception){
                System.out.println("Errore: " + exception.getMessage());
            }
            
        }
        
        private void apri() {
            try{
                fr = new FileReader("Dati.txt");
                fIN = new BufferedReader(fr);
                s = fIN.readLine();
                while(s != null){
                    JOptionPane.showMessageDialog(null, s, "info", 
                            JOptionPane.INFORMATION_MESSAGE);
                    s = fIN.readLine();
                }
            } catch(IOException exception){
                System.out.println("Errore: " + exception.getMessage());
            }
        }
        
    }
    
}