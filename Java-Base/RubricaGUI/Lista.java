import java.util.LinkedList;
import java.util.List;

/**
 * @author Simone Tugnetti
 */
public class Lista {
    
    private List<Dati> rubrica;
    
    public Lista(){
        rubrica = new LinkedList();
    }
    
    public void inserisci(Dati d){
        rubrica.add(d);
    }
    
    public void elimina(){
        rubrica.clear();
    }
    
    public int size(){
       return rubrica.size();
    }

    public List<Dati> getRubrica() {
        return rubrica;
    }
    
    public void setRubrica(List<Dati> rubrica) {
        this.rubrica = rubrica;
    }
    
}