/**
 * @author Simone Tugnetti
 */
public class Studente {

    private String cognome;
    private int nAssenze;
    private int nVoti;
    private double media, sommaVoti;
    private final double[] voti;

    Studente() {
        nAssenze = 0;
        nVoti = 0;
        voti = new double[30];
    }

    double getMedia(){
        return media;
    }

    String getCognome(){
        return this.cognome;
    }

    String setCognome(String cognome){
        return this.cognome = cognome;
    }
    
    void inserisciA(){
        nAssenze++;
    }
    
    void inserisciV(double voto){
        voti[nVoti] = voto;
        nVoti++;
    }
    
    void media(){
        for(int i=0; i<nVoti; i++){
            sommaVoti += voti[i];
            media = sommaVoti/nVoti;
        }
    }

}
