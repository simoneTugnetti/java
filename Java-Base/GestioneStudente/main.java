import java.util.Scanner;

/**
 * @author Simone Tugnetti
 * Gestione di studenti usando le Classi
 */
public class main {
    
    public static void main (String args[]) {
        Scanner tastiera = new Scanner(System.in);
        
        System.out.println("Inserisci il numero di studenti:");
        int nStudenti = tastiera.nextInt();
        
        Studente[] s = new Studente[nStudenti];
        for(int i=0; i<nStudenti; i++){
            System.out.println("Studente n°"+i);
    
            System.out.println("Nome: ");
            String cognome = tastiera.next();
            s[i].setCognome(cognome);
        }
        
        System.out.println("1 - Inserisci voto tramite cognome");
        System.out.println("2 - Inserisci assenza tramtie cognome");
        System.out.println("3 - Stampa media voti tramite cognome");
        int scelta = tastiera.nextInt();
        String cogn;
        boolean flag;
    
        switch(scelta){
            case 1:
                System.out.println("Inserisci cognome:");
                cogn = tastiera.next();
                flag = true;
                for(int i=0; i<nStudenti && flag; i++){
                    if(s[i].getCognome().equals(cogn)){
                        System.out.println("Inserisci voto");
                        int voto = tastiera.nextInt();
                        s[i].inserisciV(voto);
                        flag = false;
                    }
                }
                break;
            case 2:
                System.out.println("Inserisci cognome:");
                cogn = tastiera.next();
                flag = true;
                for(int i=0; i<nStudenti && flag; i++){
                    if(s[i].getCognome().equals(cogn)){
                        s[i].inserisciA();
                        flag = false;
                    }
                }
                break;
            case 3:
                System.out.println("Inserisci cognome:");
                cogn = tastiera.next();
                flag = true;
                for(int i=0; i<nStudenti && flag; i++){
                    if(s[i].getCognome().equals(cogn)){
                        s[i].media();
                        System.out.println("Media studente:"+s[i].getMedia());
                        flag = false;
                    }
                }
                break;
            }
        
    }
    
}
