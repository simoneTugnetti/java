import java.awt.Button;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


/**
 * @author Simone Tugnetti
 * Funzionamento dei bottoni all'interno di una GUI
 */
public class Azione extends Frame{

    Button pulsante1 = new Button("pulsante1");
    Button pulsante2 = new Button("pulsante2");
    Button pulsante3 = new Button("pulsante3");
    Button pulsante4 = new Button("pulsante4");
    Button pulsante5 = new Button("Esci");

    @SuppressWarnings("OverridableMethodCallInConstructor")
    public Azione () {
        setLayout(null);
        
        add(pulsante1);
        add(pulsante2);
        add(pulsante3);
        add(pulsante4);
        add(pulsante5);

        pulsante1.setBounds(10,30,60,30);
        pulsante2.setBounds(80,30,60,30);
        pulsante3.setBounds(150,30,60,30);
        pulsante4.setBounds(10,70,60,30);
        pulsante5.setBounds(80,70,60,30);
        
        pulsante1.addActionListener(new AscoActionListener());
        pulsante2.addActionListener(new AscoActionListener());
        pulsante3.addActionListener(new AscoActionListener());
        pulsante4.addActionListener(new AscoActionListener());
        pulsante5.addActionListener(new AscoActionListener());

        addWindowListener(new ChiudiWin());

        setTitle("Ascolto le azioni sui pulsanti!");
        setLocation(200,100);
        setSize(230,200);
        setVisible(true);
        
    }
    
    @SuppressWarnings("ResultOfObjectAllocationIgnored")
    public static void main (String[] s) {
        System.out.println("Ascolto i pulsanti!");
        new Azione();
    }
    
    public class AscoActionListener implements ActionListener{
        
        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == pulsante5) {
                System.out.println("ActionListener: Premuto Esci");
                System.exit(0);
            } else {
                System.out.println("ActionListener: premuto " + 
                        e.getActionCommand());
            }
        }
    }
    
}


