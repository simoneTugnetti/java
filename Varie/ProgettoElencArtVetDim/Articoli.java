//Simone Tugnetti                 4°AI
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.*;
/**
 *
 * @author 4AI
 */
public class Articoli {
 private String codice;
    private String descrizione;
    private double prezzo;

    public Articoli() throws Exception{
        InputStreamReader input = new InputStreamReader(System.in);
        BufferedReader tastiera = new BufferedReader(input);
        System.out.print("Codice: ");
            codice = tastiera.readLine();
        System.out.print("Descrizione: ");
            descrizione = tastiera.readLine();
        System.out.print("Prezzo: ");
            prezzo =Double.parseDouble(tastiera.readLine());
    }

    public void Stampa(){
        System.out.println(codice+", "+descrizione+", "+prezzo);
    }
    
}
