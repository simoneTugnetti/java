//Simone Tugnetti         4°AI
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author 4AI
 */
public class main {
    public static void main(String args[]) throws Exception{
    Catalogo mioCatalogo = new Catalogo(); // creazione oggetto mioCatalogo per la gestione del catalogo
    Menu mioMenu = new Menu();// creazione oggetto mioMenu per la gestione del menu
    mioCatalogo.letturaDati();
    int scelta;
    scelta = mioMenu.scelta();  // invoco metodo scelta
    while (scelta != 4){
      if (scelta == 1){
        Articoli g = new Articoli(); // creazione oggetto Articoli che inizializza i parametri dall'utente
        mioCatalogo.aggiungiGestione(g);
      }
      else if (scelta == 2){
        int indice = mioMenu.leggiIndice();
        mioCatalogo.eliminaGestione(indice);
      }
      else if (scelta == 3){
        mioCatalogo.Visualizza();
      }
      scelta = mioMenu.scelta(); // se non rientra fra i 3 numeri invoca nuovamente il metodo
    }
    mioCatalogo.salvataggioDati();
}
}