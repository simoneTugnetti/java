//Simone Tugnetti          4°AI
import java.util.*;
import java.io.*;

public class Catalogo {
    private Vector lista;
    public FileReader f=null;
    public BufferedReader fIN=null;
    private String s;
    public FileWriter r = null;
    public PrintWriter fOUT = null;

    public Catalogo(){
        lista = new Vector(2,2);
    }

    public void aggiungiGestione(Articoli g){
        lista.addElement(g);
    }

    public void eliminaGestione(int indice){
            lista.removeElementAt(indice);
        System.out.println("Eliminazione effettuata.");
    }

    public void Visualizza(){
        Articoli g;
        System.out.println("\nCATALOGO");
        for(int i=0; i<lista.size(); i++){
            g=(Articoli)lista.elementAt(i);
            System.out.println(g);
        }
}
    public void letturaDati() throws Exception{
    f=new FileReader("Dati.txt");
    fIN=new BufferedReader(f);
        s=fIN.readLine();
        lista.addElement(s);
        f.close();
}
    public void salvataggioDati() throws Exception{
        r=new FileWriter("Dati.txt");
        fOUT=new PrintWriter(r);
        fOUT.println(lista);
        fOUT.flush();
        r.close();
    }
}
