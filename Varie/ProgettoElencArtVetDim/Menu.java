//Simone Tugnetti        4°AI
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.*;
/**
 *
 * @author 4AI
 */
public class Menu {
  private void mostraMenu()
  {
    System.out.println("1) Inserimento dei dati");
    System.out.println("2) Cancellazione di un indice");
    System.out.println("3) Visualizzazione dei dati");
    System.out.println("4) Esci");
  }

  public int scelta() throws Exception{
    InputStreamReader input = new InputStreamReader(System.in);
    BufferedReader tastiera = new BufferedReader(input);
    int scelta;
    mostraMenu();
    System.out.print("\n-> ");
      scelta = Integer.parseInt(tastiera.readLine());
    return scelta;
  }

  public int leggiIndice() throws Exception{
    InputStreamReader input = new InputStreamReader(System.in);
    BufferedReader tastiera = new BufferedReader(input);
    int indice;
    System.out.print("\nVoce da eliminare: ");
      indice = Integer.parseInt(tastiera.readLine());
    return indice;
  }
}
