/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.*;
import java.net.*;
/**
 *
 * @author simone.tugnetti
 */
public class ServerThread extends Thread{
public static int counter=0;
private int id=++counter;
public Socket sockt;
private BufferedReader in;
public PrintWriter out;
//Costruttore
public ServerThread(Socket s) throws IOException{
        sockt =s;
        start();
        System.out.println("ServerThread "+id+": started");
}
public void run(){
    try{
    System.out.println("ServerThread "+id+"Connessione stabilita...");
    in=new BufferedReader(new InputStreamReader(sockt.getInputStream()));
    OutputStreamWriter osw=new OutputStreamWriter(sockt.getOutputStream());
    out=new PrintWriter(new BufferedWriter(osw), true);
    AgendaTelefonica at=new AgendaTelefonica();
    while(true){
    String nome=in.readLine();
    if(nome.equals("q")) break;
    String info=at.ricerca(nome);
    out.println(info);
    out.flush();
    }
    out.close();
    sockt.close();
    System.out.println("ServerThread "+id+"Connessione chiusa!");
    }
    catch(Exception e){}
}
}
