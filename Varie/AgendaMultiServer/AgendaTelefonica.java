/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.*;
/**
 *
 * @author 5ai
 */
public class AgendaTelefonica {
    //Definizione degli attributi
    String nome;
    String telefono;
    String agenda[][];
    int l;
    int k;
    public AgendaTelefonica(){
    agenda=carica(); //Se si utilizza carica scrivere --> agenda=carica()
    }
    public void inserimento() throws Exception{
        InputStreamReader input1=new InputStreamReader(System.in);
        BufferedReader tastiera1=new BufferedReader(input1);
        System.out.println("Inserisci prima il nome e poi il numero:");
        for(l=0;l<2;l++){
            System.out.println("Questo è l'utente n° "+(l+1));
            for(k=0;k<2;k++){
                agenda[l][k]=tastiera1.readLine();
            }
        }
    }
    public String[][] carica(){
        String [][]at={
            {"Gino","02887766"},
            {"Eva","067788"},
            {"Enzo","011334455"}
        };
        return at;
    }
    public String ricerca(String nome){
        //Variabili locali
        int i,n;
        String tel;
        boolean trovato=false;
        //Inizializzazione
        i=0;
        n=agenda.length;
        tel="Non trovato";
        while(i<n && !trovato){//Ricerca il nome nell'array 2D
            if(agenda[i][0].equalsIgnoreCase(nome)){
                tel=agenda[i][1];
                trovato=true;
            }
            i++;
        }
        return tel;
    }
}
