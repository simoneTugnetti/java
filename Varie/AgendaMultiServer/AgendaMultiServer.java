/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.*;
import java.net.*;
/**
 *
 * @author simone.tugnetti
 */

public class AgendaMultiServer{
    public static void main(String[] args){
        ServerSocket sSocket;
        Socket connessione;
        int porta=3333;
        //Gestione flusso in uscita
       
        try
        {
            
            sSocket=new ServerSocket(porta);
            while(true)
            {
                System.out.println("In attesa di connessione ...");
                connessione=sSocket.accept();
                System.out.println("Connessione stabilita..."+connessione);
                new ServerThread(connessione);
                
            }
        }
        catch(IOException e){}
    }
}
