//Simone Tugnetti        4°AI

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.*;
import java.util.*;
/**
 *
 * @author 4AI
 */
public class Parola {
private FileReader f;
private BufferedReader fIN;
private FileWriter s;
private PrintWriter fOUT;
private String z;
private int j;
private int i;
private StringTokenizer st;
private String parola;
private String parola2;
private String par[]=new String[100];
public Parola(){
    f=null;
    fIN=null;
    s=null;
    fOUT=null;
    z="";
    i=0;
    parola="";
    parola2="";
    j=0;
}
public void lettura() throws Exception{
    i=0;
    InputStreamReader input=new InputStreamReader(System.in);
    BufferedReader tastiera=new BufferedReader(input);
    System.out.println("Quale parola devi cercare???");
    parola=tastiera.readLine();//Chiedo la parola da contare
    f=new FileReader("parole.txt");//Apro il file in modalità lettura
    fIN=new BufferedReader(f);
    z=fIN.readLine();
    while(z!=null){//Cicla fino a quando la variabile che contiene le parole nel file non è nullo
        if(z.equals(parola)){//Se la variabile contiene la stessa parola chiesta al'utente, il contatore aumenta di 1
            i++;
        }
        z=fIN.readLine();
    }
    System.out.println("La parola compare "+i+" volte");//Mostra quante volte la parola è nel file
    f.close();//Chiudo il file
}
public void letturaeScrittura() throws Exception{
    i=0;
    InputStreamReader input=new InputStreamReader(System.in);
    BufferedReader tastiera=new BufferedReader(input);
    System.out.println("Qual'è la parola da sostituire????");
    parola=tastiera.readLine();//Chiedo all'utente la parola da sostituire
    f=new FileReader("parole.txt");//Apro il file in modalità lettura
    fIN=new BufferedReader(f);
    z=fIN.readLine();
    while(z!=null){
        st=new StringTokenizer(z,";");
        if(z.equals(parola)){//Se la parola esiste nel file, viene chiesto all'utente la nuova parola da inserire al posto di quella precedente
            System.out.println("Quale parola vuoi scrivere????");
            parola2=tastiera.readLine();
            parola2=st.nextToken();
        }
        z=fIN.readLine();
        par[i]=z;
        i++;
    }
    f.close();//Chiudo il file
    s=new FileWriter("parole.txt");//Apro il file in modalità scrittura
    fOUT=new PrintWriter(s);
    for(j=0;j<i-1;j++){
    fOUT.println(par[j]);//Scrivo la ricerca fatta in precedenza
    fOUT.flush();//Forzo la scrittura nel file
    }
s.close();//Chiudo il file
}
}
