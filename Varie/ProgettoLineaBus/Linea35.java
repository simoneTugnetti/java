/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author AleSimo76
 */
public class Linea35{
    public String descrizione;
    public String cap1;
    public String cap2;
    public Linea35(){
        descrizione="In tutto ci sono 11 fermate urbane";
        cap1="M - Carducci";
        cap2="Amendola cap.";
    }
    public String toString(){
      return("Il primo capolinea è: "+cap1+";     "
             + "Il secondo capolinea è: "+cap2+";     "
             + "La descrizione del percorso: "+descrizione);
    }
    public void visualizza(){
        System.out.println(toString());
    }
}
