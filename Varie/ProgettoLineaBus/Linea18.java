/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author AleSimo76
 */
public class Linea18 {
    public String descrizione;
    public String cap1;
    public String cap2;
    public Linea18(){
        descrizione="In tutto ci sono 46 fermate urbane";
        cap1="Caio Mario cap.";
        cap2="Sofia cap.";
    }
    public String toString(){
        return("Il primo capolinea è: "+cap1+";      "
                + "Il secondo capolinea è: "+cap2+";     "
                + "La descrizione del percorso: "+descrizione);
    }
    public void visualizza(){
        System.out.println(toString());
    }
}
