/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author AleSimo76
 */
public class Linea14 {
    public String descrizione;
    public String cap1;
    public String cap2;
    public Linea14(){
        descrizione="In tutto ci sono 28 fermate urbane";
        cap1="Solferino cap.";
        cap2="Barile cap.";
    }
    public String toString(){
        return("Il primo capolinea è: "+cap1+";     "
                + "Il secondo capolinea è: "+cap2+";      "
                + "La descrizione del percorso: "+descrizione);
    }
    public void visualizza(){
        System.out.println(toString());
    }
}
