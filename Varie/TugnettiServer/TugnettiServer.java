/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.*;
import java.net.*;
/**
 *
 * @author simone.tugnetti
 */
public class TugnettiServer {
    public static void main(String[] args){
        ServerSocket sSocket;
        Socket connessione;
        int porta=3333;
        //Gestione flusso in uscita
        OutputStream out;
        PrintWriter sOut;
        //Stream per gestione input dati
       InputStream in;
       InputStreamReader input;
       BufferedReader sIn;
       BufferedReader tastiera;
       String info;
       int scelta;
        try
        {
            sSocket=new ServerSocket(porta);
            while(true)
            {
                System.out.println("In attesa di connessione ...");
                connessione=sSocket.accept();
                System.out.println("Connessione stabilita...");
                //Istanzio flusso in ingress
                in=connessione.getInputStream();
                input=new InputStreamReader(in);
                sIn=new BufferedReader(input);
                do{
                String nome=sIn.readLine();
                out=connessione.getOutputStream();
                sOut=new PrintWriter(out);
                //Invia informazione al client
                if(nome.equals("ON")){
                info="Sistema Avviato";
                sOut.println(info);
                sOut.flush();
                }
                if(nome.equals("OFF")){
                info="Sistema Spento";
                sOut.println(info);
                sOut.flush();
                }
                
                System.out.println("Vuoi rimanere su questo server?? (1-SI;2-NO)");
                tastiera=new BufferedReader(new InputStreamReader(System.in));
                scelta=Integer.parseInt(tastiera.readLine());
                }while(scelta==1);
                sOut.close();
             connessione.close();
             System.out.println("Connessione chiusa.");   
            }
        }
        catch (Exception e)
                {
                   System.err.println(e);
            }
    }
}
