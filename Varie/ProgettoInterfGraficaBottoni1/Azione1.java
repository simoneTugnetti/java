/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import java.awt.*;
import java.awt.event.*;
/**
 *
 * @author 4AI
 */
public class Azione1 extends Frame{
//istanziamo i 5 oggetti di classe Button come attributi di classe
Button pulsante1 = new Button("pulsante1");
Button pulsante2 = new Button("pulsante2");
Button pulsante3 = new Button("pulsante3");
Button pulsante4 = new Button("pulsante4");
Button pulsante5 = new Button("pulsante5");
public Azione1 () {
setLayout(null);
//creiamo i 5 pulsanti
this.add(pulsante1);
this.add(pulsante2);
this.add(pulsante3);
this.add(pulsante4);
this.add(pulsante5);
pulsante1.setBounds(10,30,60,30);
pulsante2.setBounds(80,30,60,30);
pulsante3.setBounds(150,30,60,30);
pulsante4.setBounds(10,70,60,30);
pulsante5.setBounds(80,70,60,30);
//registriamo gli osservatori sui puls anti
pulsante1.addActionListener(new AscoActionListener());
pulsante2.addActionListener(new AscoActionListener());
pulsante3.addActionListener(new AscoActionListener());
pulsante4.addActionListener(new AscoActionListener());
pulsante5.addActionListener(new AscoActionListener());
//registriamo gli osservatori sulla finestra
this.addWindowListener(new ChiudiWin());
//dimensionamento finestra e visualizzazione
setTitle("Ascoltiamo le azioni sui pulsanti!");
setLocation(200,100);
setSize(230,200);
setVisible(true);
}
public static void main (String[] s) {
System.out.println("Ascolto i pulsanti!");
new Azione1();
}
public class AscoActionListener implements ActionListener{
public void actionPerformed(ActionEvent e) {
if (e.getSource()==pulsante5) {
System.out.println("ActionListener: Premuto Esci");
System.exit(9); //un numero qualsiasi si chiama exit code.
}
else{
System.out.println("ActionListener: premuto "+e.getActionCommand());
    }
}
}
}


