//Simone Tugnetti        4°AI
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.*;//Così posso utilizzare tutti i comandi di java
/**
 *
 * @author 4AI
 */
public class Conto {//Questa è la classe conto, dove chiedo all'utente se vuole depositare o ritirare soldi dal proprio conto bancario
public double saldo;//Creo gli attributi
public String nomeproprietario;
public int scelta;
public double soldi;
public Conto(String nomeproprietario, double saldo){//Costruttore con parametri, dove inizializzo gli attributi a 0
    this.nomeproprietario=nomeproprietario;
    this.saldo=saldo;
    scelta=0;
    soldi=0;
}
public void nomeutente(){//Qui chiedo il nome della persona che vuole creare il conto
    InputStreamReader input=new InputStreamReader(System.in);
    BufferedReader tastiera=new BufferedReader(input);
    System.out.println("Qual'è il suo nome???");
    try{
        nomeproprietario=tastiera.readLine();
    }
    catch(IOException t){
        System.out.println("ERROREEEEE!!!!!");
    }
}
public void inizio(){//Qui chedo se l'utente vuole ritirare o depositare dei soldi
    InputStreamReader input=new InputStreamReader(System.in);
    BufferedReader tastiera=new BufferedReader(input);
    System.out.println("Vuole depositare o vuole ritirare??? (1-Depositare; 2-Ritirare)");
    try{
         scelta=Integer.parseInt(tastiera.readLine());   
        }
        catch(IOException g){
            System.out.println("Errore!!");
        }
    if(scelta==1){
        deposito();
    }
    else{
        prelievo();
    }
}
public void deposito(){//Qui inizia la procedura per il deposito di soldi
    InputStreamReader input=new InputStreamReader(System.in);
    BufferedReader tastiera=new BufferedReader(input);
    do{
    System.out.println("Quanti soldi vorrebbe depositare???");
    try{
        soldi=Double.parseDouble(tastiera.readLine());
    }
    catch(IOException r){
        System.out.println("ERROREEEE!!!!!!!");
    }
    saldo=saldo+soldi;
        System.out.println("Vuole depopsitarne ancora??? (1-SI; 2-NO)");
        try{
         scelta=Integer.parseInt(tastiera.readLine());   
        }
        catch(IOException g){
            System.out.println("Errore!!");
        }
    }while(scelta==1);
}
public void prelievo(){//Qui inizia la procedura per il ritiro di denaro
    InputStreamReader input=new InputStreamReader(System.in);
    BufferedReader tastiera=new BufferedReader(input);
    do{
    System.out.println("Quanti soldi vorrebbe ritirare???");
    try{
        soldi=Double.parseDouble(tastiera.readLine());
    }
    catch(IOException r){
        System.out.println("ERROREEEE!!!!!!!");
    }
    saldo=saldo-soldi;
        System.out.println("Vuole ritirarne ancora??? (1-SI; 2-NO)");
        try{
         scelta=Integer.parseInt(tastiera.readLine());   
        }
        catch(IOException g){
            System.out.println("Errore!!");
        }
    }while(scelta==1);
}
public void visualizza(){//Qui viene visualizzato il saldo dell'utente
    System.out.println("Il saldo sul suo conto è di: "+saldo);
}
public double getSaldo(){//Qui ottengo il saldo dell'utente
    return saldo;
}
public void indecisione(){//Qui chiedo se l'utente vuole uscire dalla banca
    InputStreamReader input=new InputStreamReader(System.in);
    BufferedReader tastiera=new BufferedReader(input);
 System.out.println("Vuole uscire dalla nostra banca??? (1-SI; 2-NO)");
 try{
         scelta=Integer.parseInt(tastiera.readLine());   
        }
        catch(IOException g){
            System.out.println("Errore!!");
        }
 if(scelta==1){
     System.out.println("Arrivederci!!!");
 }
 else{
     inizio();
 }
}
}