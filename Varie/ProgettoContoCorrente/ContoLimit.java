//Simone Tugnetti          4°AI
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.*;
/**
 *
 * @author AleSimo76
 */
class ContoLimit extends Conto{//Questra è la classe ContoLimit, cioè la sottoclasse di Conto; Qui il proprietario può accedere al suo conto limitato
    public double limite;//Creo gli attributi
    public double saldo2;
    public int scelta2;
    public ContoLimit(){//Inizializzo gli attributi a 0, tranne il limite di denaro che è invece 100000 soldi
        super(nomeproprietario, saldo);
        limite=100000;
        saldo2=0;
        scelta=0;
    }
    public void contolimitato(){//In questa procedura non c'è niente di divero da quella precedente, tranne che se il conto bancario dell'utente raggiunge il limite, egli non è più in grado di depositare denaro, ma può sempre ritirarlo
        InputStreamReader input=new InputStreamReader(System.in);
        BufferedReader tastiera=new BufferedReader(input);
        System.out.println("Il limite di questo conto è di "+limite+" euro!!!");
        if(saldo2>=limite){
            System.out.println("Non puoi più depositare denaro!!!");
            System.out.println("Desideri prelevare o uscire??? (1-Prelevare; 2-Uscire)");
            try{
            scelta2=Integer.parseInt(tastiera.readLine());   
            }
            catch(IOException d){
                System.out.println("ERROREEEEE!!!!");
            }
            if(scelta2==1){
             do{
            System.out.println("Quanti soldi vorrebbe ritirare???");
            try{
                soldi=Double.parseDouble(tastiera.readLine());
            }
            catch(IOException u){
                System.out.println("ERROREEEE!!!!!!!");
            }
            saldo2=saldo2-soldi;
                System.out.println("Vuole ritirarne ancora??? (1-SI; 2-NO)");
                try{
                 scelta2=Integer.parseInt(tastiera.readLine());   
                }
                catch(IOException l){
                    System.out.println("Errore!!");
                }
            }while(scelta2==1);
            }
            else{
                System.out.println("Stai per uscire dal tuo conto limitato!!");
            }
        }
        else{
            System.out.println("Desidera ritirare o depositare??? (1-Ritirare; 2-Depositare)");
            try{
            scelta2=Integer.parseInt(tastiera.readLine());   
            }
            catch(IOException d){
                System.out.println("ERROREEEEE!!!!");
            }
            if(scelta2==1){
             do{
            System.out.println("Quanti soldi vorrebbe ritirare???");
            try{
                soldi=Double.parseDouble(tastiera.readLine());
            }
            catch(IOException u){
                System.out.println("ERROREEEE!!!!!!!");
            }
            saldo2=saldo2-soldi;
                System.out.println("Vuole ritirarne ancora??? (1-SI; 2-NO)");
                try{
                 scelta2=Integer.parseInt(tastiera.readLine());   
                }
                catch(IOException l){
                    System.out.println("Errore!!");
                }
            }while(scelta2==1);
            }
            else{
                do{
                System.out.println("Quanti soldi vorrebbe depositare???");
                try{
                    soldi=Double.parseDouble(tastiera.readLine());
                }
                catch(IOException x){
                    System.out.println("ERROREEEE!!!!!!!");
                }
                saldo2=saldo2+soldi;
                    System.out.println("Vuole depopsitarne ancora??? (1-SI; 2-NO)");
                    try{
                     scelta2=Integer.parseInt(tastiera.readLine());   
                    }
                    catch(IOException c){
                        System.out.println("Errore!!");
                    }
                }while(scelta2==1);
            }
        }
    }
}