//Simone Tugnetti        4°AI

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.*;
/**
 *
 * @author 4AI
 */
public class main {
public static void main(String args[]) throws Exception{
     InputStreamReader input=new InputStreamReader(System.in);
     BufferedReader tastiera=new BufferedReader(input);
     int scelta;
     int scelta2;
     int scelta3;
     int CodicePacco;
     Coda a1=new Coda();
     Pila a2=new Pila();//Istanzio 3 oggetti aventi la stessa classe Pila()
     Pila a3=new Pila();
     Pila a4=new Pila();
     System.out.println("Benvenuti in Tugnetti's Factory!!!");
     System.out.println("Ci sono dei pacchi da mettere in ordine!!");
     do{
     System.out.println("Che cosa desideri fare??");
     System.out.println("1-Inserire pacchi nella coda");
     System.out.println("2-Visualizzare la coda");
     System.out.println("3-Assegnare un pacco ad una pila");
     System.out.println("4-Visualizzare le pile (prima assegnare tutti i pacchi alle pile) (dopo averlo visualizzato sarà poi consegnato)");
     System.out.println("5-Uscire dalla fabbrica");
     scelta=Integer.parseInt(tastiera.readLine());
     switch(scelta){
         case 1:
             System.out.println("Inserisci il codice del pacco!!!!!");
             CodicePacco=Integer.parseInt(tastiera.readLine());
             a1.push(CodicePacco);//Inserisco il codice del pacco nella coda
             break;
         case 2:
             if(a1.vuota()==false){
             a1.visualizza();
             }
             else{
                 System.out.println("Devi prima inserire un pacco nella coda!!!!!");
             }
             break;
         case 3:
             if(a1.vuota()==false){
             System.out.println("A quale pila vuoi che venga assegnato il pacco???? (1,2 o 3)");
             scelta2=Integer.parseInt(tastiera.readLine());
             switch(scelta2){
                 case 1:
                     a2.push(a1.visual());//Inserisco il valore nella coda nella pila attraverso un metodo che mi ritorna il primo valore nella coda
                     a1.pop();
                     break;
                 case 2:
                     a3.push(a1.visual());
                     a1.pop();
                     break;
                 case 3:
                     a4.push(a1.visual());
                     a1.pop();
                     break;
             }
             }
             else{
                 System.out.println("Devi prima inserire un pacco nella coda!!!!");
             }
             break;
         case 4:
             System.out.println("Di quale pila vuoi visualizzarne i codici dei pacchi???? (1,2 o 3)");
             scelta3=Integer.parseInt(tastiera.readLine());
             switch(scelta3){
                 case 1:
                     if(a2.vuota()==false){
                     a2.visualizza();//Visualizzo la pila
                     }
                     else{
                         System.out.println("Devi prima inserire un pacco nella pila!!!");
                     }
                     break;
                 case 2:
                     if(a3.vuota()==false){
                     a3.visualizza();
                     }
                     else{
                         System.out.println("Devi prima inserire un pacco nella pila!!!");
                     }
                     break;
                 case 3:
                     if(a4.vuota()==false){
                     a4.visualizza();
                     }
                     else{
                         System.out.println("Devi prima inserire un pacco nella pila!!!");
                     }
                     break;
             }
             break;
     }
     }while(scelta!=5);//Se la scelta sarà diversa da 5, il programma continuerà a ciclare, altrimenti si terminerà
}
}
