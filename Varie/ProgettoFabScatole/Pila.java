//Simone Tugnetti      4°AI

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.*;
/**
 *
 * @author 4AI
 */
public class Pila {
private Vector scatole;
private int i;
public Pila(){
     scatole=new Vector(6,3);
     i=0;
}
public void push(Object a1){//Aggiungo un oggetto alla pila
     scatole.addElement(a1);
}
public Object pop(){//Elimino un oggetto dalla pila
     Object a2=null;
     int size=scatole.size();
     if (size>0){
        a2=scatole.elementAt(size-1);
        scatole.removeElementAt(size-1);
     }
     return a2;
}
public Object top(){//Ritorna l'ultimo valore nella pila
     Object a3=null;
     int size=scatole.size();
     if (size>0){
        a3=scatole.elementAt(size-1);
     }
     return a3;
}
public boolean vuota(){//Controllo se la pila è vuota con attributi booleani
     if(scatole.size()>0){
        return false;
     }
     else{
        return true;
     }
}
public int size(){//Restituisce la grandezza della pila
     return scatole.size();
}
public void visualizza(){//Visualizzo l'intera pila
    i=0;
    do{
        System.out.println("Ecco il "+(i+1)+"° pacco nella pila!!!");
        System.out.println(top());
        pop();
        i++;
    }while(vuota()==false);
  }
}
