//Simone Tugnetti       4°AI

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.*;
/**
 *
 * @author 4AI
 */
public class Coda {
private Vector pacchi;
private int i;
public Coda(){
    pacchi=new Vector(5,4);
    i=0;
}
public void push(int a3){//Aggiungo il valore alla coda
    pacchi.addElement(a3);
}
public Object pop(){//Elimino il valore alla coda
    Object a1=null;
    int size=pacchi.size();
    if(size>0){
        a1=pacchi.elementAt(0);
        pacchi.removeElementAt(0);
    }
    return a1;
}
public boolean vuota(){
    if(pacchi.size()>0){
        return false;
    }
    else{
        return true;
    }
  }
public int size(){
    return pacchi.size();
}
public void visualizza(){//Visualizzo l'intera coda
    for(i=0;i<size();i++){
        System.out.println("Ecco il "+(i+1)+"° pacco della coda!!!");
        System.out.println(pacchi.elementAt(i));
    }
}
public Object visual(){//Ritorno il primo valore della coda
    return pacchi.elementAt(0);
}
}
