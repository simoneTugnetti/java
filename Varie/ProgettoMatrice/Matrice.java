/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.*;
/**
 *
 * @author AleSimo76
 */
public class Matrice {
    private int[][] matrice=new int[5][5];
    private Random r=new Random();
    private int i;
    private int j;
    private int n;
    private int f;
    private int[] max=new int[5];
    private int[] min=new int[5];
    public Matrice(){
        i=0;
        j=0;
        n=0;
        f=0;
    }
    public void inserimento(){
        f=1;
        n=100;
        for(i=0;i<5;i++){
            for(j=0;j<5;j++){
                matrice[i][j]=r.nextInt(n)+f;
            }
        }
        for(i=0;i<5;i++){
            System.out.println("Questi sono i valori della "+i+"° riga:");
            for(j=0;j<5;j++){
                System.out.println(matrice[i][j]);
            }
    }
    }
    public void min(){
        for(i=0;i<5;i++){
            min[i]=matrice[i][0];
        for(j=0;j<5;j++){
                if(matrice[i][j]<min[i]){
                    min[i]=matrice[i][j];
                }
            }
        System.out.println("Il valore minimo della "+i+"° riga è: "+min[i]);
        }
    }
    public void max(){
        for(i=0;i<5;i++){
            max[i]=matrice[i][0];
        for(j=0;j<5;j++){
                if(matrice[i][j]>max[i]){
                    max[i]=matrice[i][j];
                }
            }
        System.out.println("Il valore massimo della "+i+"° riga è: "+max[i]);
        }
    }
}
