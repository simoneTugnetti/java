class ProgRub
{
  public static void main(String argv[])
  {
    Rubrica miaRubrica = new Rubrica(); // creazione oggetto miaRubrica per gestione rubrica
    Menu mioMenu = new Menu(); // creazione oggetto mioMenu per la gestione del menu
    int scelta;  // variabile per il costrutto switch-case del menu

    scelta = mioMenu.scelta();  // invoco metodo scelta

    while (scelta != 4)
    {
      if (scelta == 1)
      {
        Voce v = new Voce(); // creazione oggetto voce che inizializza i parametri dall'utente (vedere classe Voce)
        miaRubrica.aggiungiVoce(v); // invocazione metodo aggiungiVoce con passaggio di parametro
      }
      else if (scelta == 2)
      {
        int indice = mioMenu.leggiIndice();
        miaRubrica.eliminaVoce(indice);
      }
      else if (scelta == 3)
      {
        miaRubrica.visualizza();
      }

      scelta = mioMenu.scelta(); // se non rientra fra i 3 numeri invoca nuovamente mioMenu.scelta()
    }

    System.out.println("Fine programma.");
  }
}
