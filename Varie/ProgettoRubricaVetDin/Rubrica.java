import java.util.*;

class Rubrica
{
  private Vector elenco; // dichiarazione di un oggetto di tipo Vector di nome elenco 
  
  public Rubrica()
  {
    elenco = new Vector(1,1); //costruisco un vettore dinamico con capicità di un elemento ed estensione di un elemento alla volta
  }

  public void aggiungiVoce(Voce v)
  {
    elenco.addElement(v); // invocazione metodo aggiungi elemento
  }

  public void eliminaVoce(int indice)
  {
    try
    {
      elenco.removeElementAt(indice); // invocazione metodo elimina elemento (automaticamente compatta il vettore)
    }
    catch (Exception e) // se non esiste l'indice
    {
      System.out.println("Eliminazione non possibile.");
      return;
    }

    System.out.println("Eliminazione effettuata.");
  }

  public void visualizza()
  {
    Voce v; // dichiaro variabile v di tipo voce

    System.out.println("\nRUBRICA");
    for(int i=0; i<elenco.size(); i++)  // elenco.size metodo della classe vector (dimensione)
    {
      System.out.print("posizione " + i + " -> ");
      v = (Voce) elenco.elementAt(i); // restituisce un oggetto della classe Object che si trova nella posizione indicata
                                      // e deve necessariamente essere effettuato un casting, se non riesce il casting produce un eccezione
    }
  }
}
