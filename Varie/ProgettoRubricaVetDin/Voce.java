import java.io.*;

class Voce
{
  private String nome = new String(); // variabile classe String (funziona senza "new String()"? )
  private String telefono = new String();

  public Voce() // costruttore che inizializza dall'utente i suoi attributi
  {
    InputStreamReader input = new InputStreamReader(System.in);
    BufferedReader tastiera = new BufferedReader(input);

    System.out.print("Nome: ");
    try
    {
      nome = tastiera.readLine();
    }
    catch(IOException e) {}

    System.out.print("Telefono: ");
    try
    {
      telefono = tastiera.readLine();
    }
    catch(IOException e) {}
  }

  public void stampa()
  {
    System.out.println(nome+" tel.: " + telefono);
  }
}
