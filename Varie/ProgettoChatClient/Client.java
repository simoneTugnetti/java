//Simone Tugnetti        5°AI

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.*;
import java.net.*;
/**
 *
 * @author 5AI
 */
public class Client {
    public static void main(String args[]){
        Socket connessione;
        String server="localhost";
        int porta=3333;
        InputStream in;
        InputStreamReader input;
        BufferedReader sIN;
        OutputStream out;
        PrintWriter sOut;
        int scelta=0;
        try{
            connessione=new Socket(server,porta);
            System.out.println("Connessione aperta");
            do{
                in=connessione.getInputStream();
                //creiamo il nostro lettore di buffer
                BufferedReader br= new BufferedReader(new InputStreamReader(in));
                //creiamo una variabile dove salvare di volta in volta le varie righe dello stream
                String line="";
                while((line=br.readLine())!=null){
                System.out.println(line);
                }
            input=new InputStreamReader(System.in);
            sIN=new BufferedReader(input);
            String clock=sIN.readLine();
            sOut=new PrintWriter(clock);
            System.out.println(sOut);
            sOut.print(clock);
            System.out.println("Vuoi scrivere qualcos' altro????? (1-SI; 2-NO)");
            scelta=Integer.parseInt(sIN.readLine());
            }while(scelta==1);
            sOut.close();
            if(!connessione.isClosed()){
            connessione.close();
            System.out.println("Connessione chiusa");
            }
        }
        catch(IOException e){
            System.out.println("ERRORE!!!");
        }
    }
}
