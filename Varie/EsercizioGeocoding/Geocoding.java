/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.*;
import java.net.*;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import org.xml.sax.SAXException;
/**
 *
 * @author simone.tugnetti
 */

//Pag.167 in poi
public class Geocoding {
private String prefix ="http://maps.googleapis.com/maps/api/geocode/xml?address=";
private String suffix = "&sensor=false";
private String url;
private String filename;
private boolean saved = false;
private boolean parsed = false;
private double latitude;
private double longitude;
public Geocoding(String address, String filename){
    URL server;
    HttpURLConnection service;
    BufferedReader input;
    BufferedWriter output;
    int status;
    String line;
    this.filename = filename;
    try {
        url = prefix + URLEncoder.encode(address, "UTF-8") + suffix;
        server = new URL(url);
        service = (HttpURLConnection)server.openConnection();
        service.setRequestProperty("Host", "maps.googleapis.com");
        service.setRequestProperty("Accept", "application/xml");
        service.setRequestProperty("Accept-Charset", "UTF-8");
        service.setRequestMethod("GET");
        service.setDoInput(true);
        service.connect();
        status = service.getResponseCode();
        if (status != 200) {
            return;
        }
        input = new BufferedReader(new InputStreamReader(service.getInputStream(), "UTF-8"));
        output = new BufferedWriter(new FileWriter(filename));
        while ((line = input.readLine()) != null) {
            output.write(line);
            output.newLine();
        }
        input.close();
        output.close();
        saved = true;
    }
    catch(IOException e){
    }
}

private void parseXML() throws GeocodingException {
        if (!saved) {
            throw new GeocodingException();
        }
        try {
        DocumentBuilderFactory factory =DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(filename);
        Element root = document.getDocumentElement();
        NodeList list = root.getElementsByTagName("status");
        if (list != null && list.getLength() > 0) {
            if (list.item(0).getFirstChild().getNodeValue(). equalsIgnoreCase("OK")) {
                list = root.getElementsByTagName("location");
                if (list != null && list.getLength() > 0) {
                    Element loc = (Element)list.item(0);
                    NodeList lat = loc.getElementsByTagName("lat");
                    latitude = Double.parseDouble(lat.item(0).getFirstChild().getNodeValue());
                    NodeList lng = loc.getElementsByTagName("lng");
                    longitude = Double.parseDouble(lng.item(0).getFirstChild().getNodeValue());
                    parsed = true;
                }
            }
        }
        }
        catch (IOException e) {
            throw new GeocodingException();
        }
        catch (ParserConfigurationException e) {
            throw new GeocodingException();
        }
        catch (SAXException e) {
            throw new GeocodingException();
        }
}

public double getLongitude() throws GeocodingException {
if (!saved) {
throw new GeocodingException();
}
if (!parsed) {
parseXML();
}
return longitude;
}

public double getLatitude() throws GeocodingException {
if (!saved) {
throw new GeocodingException();
}
if (!parsed) {
parseXML();
}
return latitude;
}

class GeocodingException extends Exception {
}

    public static void main(String[] args){
    Geocoding zanichelli = new Geocoding("Via Pessinetto, 12 Torino, Italia", "file.xml");
    try {
    System.out.println("( " + zanichelli.getLatitude() + ";" +
    zanichelli.getLongitude() + ")");
    }
    catch (GeocodingException e) {
    System.out.println("Errore invocazione web-service!");
    }
    }
}
