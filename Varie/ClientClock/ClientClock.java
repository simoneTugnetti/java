/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clientclock;
import java.net.*;
import java.io.*;
/**
 *
 * @author Vincenzo
 */
public class ClientClock {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //Dichiarazione variabili
       Socket connessione;
       //Parametri di connessione
       String server="localhost";
       int porta=3333;
       //Stream per gestione input dati
       InputStream in;
       InputStreamReader input;
       BufferedReader sIn;
       try
       {
           connessione=new Socket(server,porta);//Costruttore connessione
           System.out.println("Connessione aperta");
           in=connessione.getInputStream();
           input=new InputStreamReader(in);
           sIn=new BufferedReader(input);
           String clock=sIn.readLine();
           System.out.println("La data ed ora restituita dal server e' :"+clock);
           sIn.close();
           connessione.close();
           System.out.println("Connessione chiusa");
       }
       catch(IOException e)
               {
                 System.err.println(e);
               }
    }
}
