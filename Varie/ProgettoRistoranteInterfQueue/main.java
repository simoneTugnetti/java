//Simone Tugnetti    4°AI

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.*;
import java.io.*;
/**
 *
 * @author 4AI
 */
public class main {
public static void main(String args[]) throws Exception{
     InputStreamReader input=new InputStreamReader(System.in);
     BufferedReader tastiera=new BufferedReader(input);
     int scelta;
     int i=0;
     LinkedList<Ordinazione> a2 = new LinkedList<Ordinazione>();//Inizializzo una lista
     Ordinazione[] a3 =new Ordinazione[1000];//Creo un array di oggetti
     vet a1=new vet();
     System.out.println("Benvenuti in Restaurant ExSTremis!!!");
     System.out.println("Ci sono delle persone che vorrebbero mangiare da noi!!");
     do{
     System.out.println("Che cosa desideri fare??");
     System.out.println("1-Prendere l'ordinazione");
     System.out.println("2-Erogare ordinazione al cliente");
     System.out.println("3-Dare il conto al cliente");
     System.out.println("4-Uscire dal ristorante");
     scelta=Integer.parseInt(tastiera.readLine());
     switch(scelta){
         case 1:
             a3[i]=new Ordinazione();//Inizializzo la classe Ordinazione()
             a2.offer(a3[i]);//Aggiungo alla coda utilizzando il metodo offer() gli attributi di Ordinazione()
             a1.aggiungi(a3[i]);
             a1.aggiungi2(a3[i]);
             i++;
             break;
         case 2:
             if(a1.vuota()==false){//Se il primo vettore dinamico è vuoto, vuol dire che non si è ancora passati ad ordinare
                 System.out.println("L'ordinazione è arrivata al tavolo del cliente!!!");
                 a1.elimina(0);
             }
             else{
                 System.out.println("Non è ancora stato ordinato niente!!!");
             }
             break;
         case 3:
             if(a1.vuota2()==false){
                 System.out.println("Ecco il suo conto!!!");
                 System.out.println(a2.peek());//Visualizzo la restituzione di quello che è presente nella coda al primo posto
                 a2.poll();//Elimino l'elemento nella coda
                 a1.elimina2(0);
             }
             else{
                 System.out.println("Visto che non ha ordinato niente, il suo conto totale è 0 €");
             }
             break;
     }
     }while(scelta!=4);
}
}
