/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author 4AI
 */
import java.util.LinkedList;
public class GestioneListaImpiegati
{
    LinkedList<Impiegato> impiegati = new LinkedList<Impiegato>();

    public void nuovoImpiegato(Impiegato i)
    {
        impiegati.add(i);
    }
    public void modificaMansione(String matricolaImpiegato, String nuovaMansione)
    {
        Boolean trovato = false;
        int i = 0;
        while(i < impiegati.size() && !trovato)
        {
            if(matricolaImpiegato.equals(impiegati.get(i).getMatricola()))
            {
                trovato = true;
            }
            else
            {
                i++;
            }
        }
        if(!trovato)
        {
            System.out.println("Non esiste nessun impiegato con questa matricola.");
        }
        else
        {
            Impiegato imp = impiegati.get(i);
            imp.setMansione(nuovaMansione);
            impiegati.set(i, imp);
        }
        
    }
    public void stampaImpiegati(String mansione)
    {
        if(impiegati.isEmpty())
        {
            System.out.println("Non ci sono impiegati.");
        }
        else
        {
            if(mansione.equals(""))
            {
                for(int i = 0; i < impiegati.size(); i++)
                {

                    System.out.println("\nMatricola: " + impiegati.get(i).getMatricola());
                    System.out.println("Cognome: " + impiegati.get(i).getCognome());
                    System.out.println("Nome: " + impiegati.get(i).getNome());
                    System.out.println("Mansione: " + impiegati.get(i).getMansione());
                    System.out.println("Stipendio: " + impiegati.get(i).getStipendio() + "\n");
                }
            }
            else
            {
                for(int i = 0; i < impiegati.size(); i++)
                {
                    if(mansione.equalsIgnoreCase(impiegati.get(i).getMansione()))
                    {
                        System.out.println("\nMatricola: " + impiegati.get(i).getMatricola());
                        System.out.println("Cognome: " + impiegati.get(i).getCognome());
                        System.out.println("Nome: " + impiegati.get(i).getNome());
                        System.out.println("Stipendio: " + impiegati.get(i).getStipendio() + "\n");
                    }
                }
            }
        }
    }
    public void eliminaImpiegato(int indiceImpiegato)
    {
        if(indiceImpiegato >= impiegati.size())
        {
            System.out.println("Questo impiegato non esiste");
        }
        else
        {
            impiegati.remove(indiceImpiegato);
        }
    }
    public void aumentaStipendi(float percentuale)
    {
        if(impiegati.isEmpty())
        {
            System.out.println("Non esistono impiegati.");
        }
        else
        {
            for(int i = 0; i < impiegati.size(); i++)
            {
                Impiegato tmp = impiegati.get(i);
                tmp.setStipendio(tmp.getStipendio() + (tmp.getStipendio() / 100 * percentuale));
                System.out.println("Nuovo stipendio dell'impiegato " + tmp.getCognome() + ": " + tmp.getStipendio());
                impiegati.set(i, tmp);
            }
        }
    }
}
