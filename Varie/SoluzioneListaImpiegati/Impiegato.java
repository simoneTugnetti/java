/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author 4AI
 */
public class Impiegato
{
    private String nome, cognome, mansione, matricola;
    private float stipendio;

    Impiegato(String matricola, String nome, String cognome, String mansione, float stipendio)
    {
        this.matricola = matricola;
        this.nome = nome;
        this.cognome = cognome;
        this.mansione = mansione;
        this.stipendio = stipendio;
    }
    public String getMatricola()
    {
        return matricola;
    }
    public String getNome()
    {
        return nome;
    }
    public String getCognome()
    {
        return cognome;
    }
    public String getMansione()
    {
        return mansione;
    }
    public float getStipendio()
    {
        return stipendio;
    }
    public void setStipendio(float stipendio)
    {
        this.stipendio = stipendio;
    }
    public void setMansione(String mansione)
    {
        this.mansione = mansione;
    }
}
