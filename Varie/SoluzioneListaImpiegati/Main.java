/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author 4ai
 */
import java.io.*;
public class Main
{
    public static void main(String args[]) throws IOException
    {
        String nome, cognome, mansione, matricola, cercaMatricola;
        float stipendio;
        GestioneListaImpiegati g = new GestioneListaImpiegati();
        BufferedReader tastiera = new BufferedReader(new InputStreamReader(System.in));
        while(true)
        {
            System.out.println("1 - Aggiungi nuovo impiegato");
            System.out.println("2 - Modifica la mansione di un impiegato");
            System.out.println("3 - Stampa lista degli impiegati");
            System.out.println("4 - Elimina un impiegato");
            System.out.println("5 - Aumenta lo stipendio degli impiegati");
            System.out.println("6 - Esci");
            System.out.print("Inserisci scelta: ");
            switch(Integer.parseInt(tastiera.readLine()))
            {
                case 1:
                    System.out.print("Inserisci nome: ");
                    nome = tastiera.readLine();
                    System.out.print("Inserisci cognome: ");
                    cognome = tastiera.readLine();
                    System.out.print("Inserisci matricola: ");
                    matricola = tastiera.readLine();
                    System.out.print("Inserisci stipendio: ");
                    stipendio = Float.parseFloat(tastiera.readLine());
                    System.out.print("Inserisci mansione: ");
                    mansione = tastiera.readLine();
                    g.nuovoImpiegato(new Impiegato(matricola, nome, cognome, mansione, stipendio));
                    System.out.println("Impiegato aggiunto correttamente");
                    break;
                case 2:
                    if(g.impiegati.isEmpty())
                    {
                        System.out.println("Non esistono impiegati.");
                    }
                    else
                    {
                        System.out.print("Inserisci matricola dell'impiegato: ");
                        cercaMatricola = tastiera.readLine();
                        System.out.print("Insersci nuova masione dell'impiegato: ");
                        mansione = tastiera.readLine();
                        g.modificaMansione(cercaMatricola, mansione);
                        System.out.println("Mansione modificata.");
                    }
                    break;
                case 3:
                    if(g.impiegati.isEmpty())
                    {
                        System.out.println("Non esistono impiegati.");
                    }
                    else
                    {
                        System.out.print("Inserisci la mansione di cui vuoi stampare gli impiegati(o premi invio per stamparli tutti): ");
                        g.stampaImpiegati(tastiera.readLine());
                    }
                    break;
                case 4:
                    if(g.impiegati.isEmpty())
                    {
                        System.out.println("Non esistono impiegati.");
                    }
                    else
                    {
                        System.out.print("Inserisci il numero dell'impiegato che vuoi eliminare: ");
                        g.eliminaImpiegato(Integer.parseInt(tastiera.readLine()));
                    }
                    break;
                case 5:
                    if(g.impiegati.isEmpty())
                    {
                        System.out.println("Non esistono impiegati.");
                    }
                    else
                    {
                        System.out.print("Inserisci la percentuale dell'aumento: ");
                        g.aumentaStipendi(Integer.parseInt(tastiera.readLine()));
                    }
                    break;
                case 6:
                    System.exit(0);
                default:
                    System.out.println("Scelta non valida.");
                    break;
            }
        }
    }
}
