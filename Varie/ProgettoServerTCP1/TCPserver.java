//Simone Tugnetti         5°AI

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.*;
import java.net.*;
/**
 *
 * @author 5AI
 */
public class TCPserver extends Thread{
    public final static int SERVER_PORT=3212;
    private ServerSocket server;
    
    public TCPserver(int port) throws IOException{
        server=new ServerSocket(port);
        server.setSoTimeout(1000); // 1000ms = 1s
    }
    public void run(){
        Socket connection=null;
        while(!Thread.interrupted()){
            try{
                //Attesa richiesta connessione da parte del client
                //Attsa massima 1s
                connection=server.accept();
                System.out.println("Connessione richiesta da: "+connection.getInetAddress().toString()+": "+connection.getPort());
                Thread client=new ClientThread(connection);
                client.start();
            }
            catch(SocketTimeoutException exception){
            }
            catch(IOException exception){
            }
        }
        //Chiusura socket di ascolto
        try{
            server.close();
        }
        catch(IOException exception){
        }
    }
    public static void main(String[] args){
        int c;
        try{
            TCPserver server=new TCPserver(SERVER_PORT);
            server.start();
            c=System.in.read();
            server.interrupt();
            server.join();
        }
        catch(IOException exception){
            System.err.println("ERRORE!");
        }
        catch(InterruptedException exception){
            System.err.println("ERRORE!");
        }
    }
}
