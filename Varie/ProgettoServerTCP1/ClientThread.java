//Simone Tugnetti      5°AI

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.*;
import java.net.*;
import java.nio.*;
import java.util.*;
/**
 *
 * @author 5AI
 */
public class ClientThread extends Thread{
    private Socket connection; //Socket di connessione con il client
    private InputStream input; //Stream di input del socket
    private OutputStream output; //Stream dell'output del socket
    
    public ClientThread(Socket connection) throws IOException{
        this.connection=connection;
        input=this.connection.getInputStream();
        output=this.connection.getOutputStream();
    }
    
    public void run(){
        int n;
        byte [] buffer=new byte[1024];
        
        try{
            //Ciclo di ricezione dei dati dal client
            while((n=input.read(buffer))!= -1){
                if(n>0){
                    //Invio dei dati ricevuti al client
                    output.write(buffer,0,n);
                    output.flush();
                }
            }
        }
        catch(IOException exception){
        }
        try{
            System.out.println("Connessione chiusa!");
            input.close();
            output.close();
            connection.shutdownInput();
            connection.shutdownOutput();
            connection.close();
        }
        catch(IOException _exception){
        }
    }
}
