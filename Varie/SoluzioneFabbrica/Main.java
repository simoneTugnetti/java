package fabbrica;
import java.util.Stack;
import java.util.LinkedList;

public class Main {

    static String[] voci = new String[] {"Prima pila",
                                        "Quarta pila",
                                        "Terza pila"};
    static Menu menuPile = new Menu(voci);

    static Stack primaPila = new Stack();
    static Stack secondaPila = new Stack();
    static Stack terzaPila = new Stack();
    
    static LinkedList<Scatola> codaScatole = new LinkedList<Scatola>();

    public static void main(String[] args) {

        String[] voci = new String[] {  "Inserire una scatola in coda",
                                        "Spostare una scatola dalla coda alla pila",
                                        "Estrai una scatola da una delle pile",
                                        "Stampare tutti i codici della coda"};
        Menu menu = new Menu(voci);
        int scelta = 0;
        while(scelta == 0 || scelta != menu.uscita()){
            menu.mostra();
            scelta = menu.scelta();
            Menu.pulisciSchermo();
            switch(scelta){
                case 1:
                    inserireScatolaInCoda();
                    break;
                case 2:
                    spostareScatolaInPila();
                    break;
                case 3:
                    estrarreScatolaDallaPila();
                    break;
                case 4:
                    stampaCodici();
                    break;
            }
        }
    }

    public static void inserireScatolaInCoda(){
        Scatola scatola = new Scatola();
        codaScatole.push(scatola);
        System.out.println("La scatola " + scatola.getCodice() + " è stata inserita nella coda.");
    }

    public static void spostareScatolaInPila(){
        if(codaScatole.size() > 0){
            int scelta = 0;
            System.out.println("In quale pila vuoi inserire?");
            scelta = menuPile.scelta();
            Scatola scatola = codaScatole.pop();
            switch(scelta){
                case 1:
                    primaPila.push(scatola);
                    System.out.println("La scatola " + scatola.getCodice() + " è stata inserita nella prima pila.");
                    break;
                case 2:
                    secondaPila.push(scatola);
                    System.out.println("La scatola " + scatola.getCodice() + " è stata inserita nella seconda pila.");
                    break;
                case 3:
                    terzaPila.push(scatola);
                    System.out.println("La scatola " + scatola.getCodice() + " è stata inserita nella terza pila.");
                    break;
                }
            }
        else
            System.out.println("Errore, non ci sono elementi nella coda.");
    }

    public static void estrarreScatolaDallaPila(){
        int scelta = 0;
        System.out.println("Da quale pila vuoi estrarre?");
        while(scelta == 0 || scelta != menuPile.uscita()){
            scelta = menuPile.scelta();
            switch(scelta){
                case 1:
                    if(primaPila.size() > 0)
                        primaPila.pop();
                    else
                        System.out.println("Errore, non ci sono elementi.");
                    break;
                case 2:
                    if(primaPila.size() > 0)
                        secondaPila.pop();
                    else
                        System.out.println("Errore, non ci sono elementi.");
                    break;
                case 3:
                    if(primaPila.size() > 0)
                        terzaPila.pop();
                    else
                        System.out.println("Errore, non ci sono elementi.");
                    break;
            }
        }
    }

    public static void stampaCodici(){
        LinkedList<Scatola> scatoleStampate = new LinkedList<Scatola>();
        int i = 1;
        while(codaScatole.size() > 0){
            Scatola scatola = codaScatole.pop();
            System.out.println(i + " - " + scatola.getCodice());
            scatoleStampate.push(scatola);
            i++;
        }
        while(scatoleStampate.size() > 0)
            codaScatole.push(scatoleStampate.pop());
    }

}
