package fabbrica;
import java.util.Random;

public class Scatola {

    private int codice;

    public Scatola(){
        codice = new Random().nextInt() % 1000000;
        if(codice < 0)
            codice *= -1;
    }

    public int getCodice(){
        return codice;
    }
}
