package fabbrica;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Menu {
    private String[] voci;

    public Menu(String[] voci){
        this.voci = voci;
    }

    public void mostra(){
        System.out.println("\nInserisci la tua scelta");
        for(int i = 0; i < voci.length; i++){
            System.out.println((i + 1) + " - " + voci[i]);
        }
        System.out.println((voci.length + 1) + " - " + "Esci");
    }

    public static void pulisciSchermo(){
        System.out.println("Premi un tasto per continuare...");
        try{
            System.in.read();
            for(int i = 0; i < 50; i++)
                System.out.println();
        }
        catch(Exception e){
        }
    }
    
    public int scelta(){
        BufferedReader tastiera = new BufferedReader(new InputStreamReader(System.in));
        int scelta = 0;
        while(scelta == 0){
            System.out.println("Inserisci una scelta: ");
            try{
                scelta = Integer.parseInt(tastiera.readLine());
                if(scelta <= 0 || scelta > voci.length + 1){
                    System.out.println("Hai inserito una scelta non valida");
                    scelta = 0;
                }
            }
            catch(Exception e){
                System.out.println("Hai inserito una scelta non valida");
                scelta = 0;
            }
        }
        return scelta;
    }

    public int uscita(){
        return voci.length + 1;
    }
}
