/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.File;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
/**
 *
 * @author simone.tugnetti
 */
public class DomParserDemo {
    public static void main(String[] args){

      try {	
         File inputFile = new File("input.xml");
         DocumentBuilderFactory dbFactory 
            = DocumentBuilderFactory.newInstance();
         DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
         Document doc = dBuilder.parse(inputFile);
         doc.getDocumentElement().normalize();
         System.out.println("Root element :" 
            + doc.getDocumentElement().getNodeName());
         NodeList nList = doc.getElementsByTagName("address_component");
         System.out.println("Indirizzo :" 
            + doc.getDocumentElement().getAttribute("formatted_address"));
         System.out.println("----------------------------");
         for (int temp = 0; temp < nList.getLength(); temp++) {
            Node nNode = nList.item(temp);
            System.out.println("\nCurrent Element :" 
               + nNode.getNodeName());
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
               Element eElement = (Element) nNode;
               System.out.println("Numero : " 
                  + eElement.getAttribute("formatted_address"));
               System.out.println("Nome lungo : " 
                  + eElement
                  .getElementsByTagName("long_name")
                  .item(0)
                  .getTextContent());
               System.out.println("Nome corto : " 
               + eElement
                  .getElementsByTagName("short_name")
                  .item(0)
                  .getTextContent());
               System.out.println(" Tipo: " 
               + eElement
                  .getElementsByTagName("type")
                  .item(0)
                  .getTextContent());
            }
         }
      } catch (Exception e) {
         e.printStackTrace();
      }
   }
}
