/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.*;
import java.net.*;
import javax.net.ssl.*;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import org.xml.sax.SAXException;
/**
 *
 * @author simone.tugnetti
 */

class WeatherException extends Exception {
}

public class Weather {
    private String prefix ="https://api.wunderground.com/api/9ace33058203ff62/conditions/q/";
    private String suffix = ".xml";
    private String url;
    private boolean parsed = false;
    private boolean saved = false;
    private String filename;
    private double temperature; // °C
    private double humidity; // %
    private double pressure; // millibar
    private double windspeed; // km/h
    private double rain; // mm/h
    private String weather;
public Weather(String country, String city, String filename) {
URL server;
HttpsURLConnection service;
BufferedReader input;
BufferedWriter output;
int status;
String line;
this.filename = filename;
try {
// costruzione dello URL di interrogazione del web-service
url = prefix + URLEncoder.encode(country, "UTF-8") + "/" + URLEncoder.encode(city, "UTF-8") + suffix;
server = new URL(url);
service = (HttpsURLConnection) server.openConnection();
// impostazione header richiesta
service.setRequestProperty("Host", "api.wunderground.com");
service.setRequestProperty("Accept", "application/xml");
service.setRequestProperty("Accept-Charset", "UTF-8");
// impostazione metodo di richiesta GET
service.setRequestMethod("GET");
// attivazione ricezione
service.setDoInput(true);
// connessione al web-service
service.connect();
// verifica stato risposta
status = service.getResponseCode();
if (status != 200) {
return; // non OK
}
// apertura stream di ricezione da risorsa web
input = new BufferedReader(new InputStreamReader(service.getInputStream(), "UTF-8"));
// apertura stream per scrittura su file
output = new BufferedWriter(new FileWriter(filename));
// ciclo di lettura da web e scrittura su file
while ((line = input.readLine()) != null) {
output.write(line);
output.newLine();
}
input.close();
output.close();
saved = true;
}
catch (IOException e) {
}
}

private void parseXML() throws WeatherException {
if (!saved) {
throw new WeatherException();
}
try {
DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
DocumentBuilder builder = factory.newDocumentBuilder();
// costruzione albero DOM del file XML
Document document = builder.parse(filename);
// estrazione elemento radice
Element root = document.getDocumentElement();
// ricerca elemento "feature"
NodeList list = root.getElementsByTagName("feature");
if (list != null && list.getLength() > 0) {
// verifica tipologia risposta ("conditions")
if ( list.item(0).getFirstChild().getNodeValue().equalsIgnoreCase("conditions")) {
// ricerca elemento "current_observation"
list = root.getElementsByTagName("current_observation");
if (list != null && list.getLength() > 0) {
Element data = (Element)list.item(0);
// ricerca elemento "temp_c"
NodeList t = data.getElementsByTagName("temp_c");
// conversione in valore numerico
temperature = Double.parseDouble(t.item(0).getFirstChild().getNodeValue());
// ricerca elemento "relative_humidity"
NodeList h = data.getElementsByTagName("relative_humidity");
String s = h.item(0).getFirstChild().getNodeValue().replace('%', ' ').trim();
// conversione in valore numerico
humidity = Double.parseDouble(s);
// ricerca elemento "pressure_mb"
NodeList p =data.getElementsByTagName("pressure_mb");
// conversione in valore numerico
pressure = Double.parseDouble(p.item(0).getFirstChild().getNodeValue());
// ricerca elemento "wind_kph"
NodeList w = data.getElementsByTagName("wind_kph");
// conversione in valore numerico
windspeed = Double.parseDouble(w.item(0).getFirstChild().getNodeValue());
// ricerca elemento "precipit_1hr_metric"
NodeList r = data.getElementsByTagName("precip_1hr_metric");
// conversione in valore numerico
rain = Double.parseDouble(r.item(0).getFirstChild().getNodeValue());
NodeList wh = data.getElementsByTagName("weather");
// conversione in valore numerico
weather = (wh.item(0).getFirstChild().getNodeValue());
parsed = true;
}
}
}
}
catch (IOException e) {
throw new WeatherException();
}
catch (ParserConfigurationException e) {
throw new WeatherException();
}
catch (SAXException e) {
throw new WeatherException();
}
}
public double getTemperature() throws WeatherException {
if (!saved) {
throw new WeatherException();
}
if (!parsed) {
parseXML();
}
return temperature;
}
public double getHumidity() throws WeatherException {
if (!saved) {
throw new WeatherException();
}
if (!parsed) {
parseXML();
}
return humidity;
}
public double getPressure() throws WeatherException {
if (!saved) {
throw new WeatherException();
}
if ( !parsed) {
parseXML();
}
return pressure;
}
public double getWindspeed() throws WeatherException {
if (!saved) {
throw new WeatherException();
}
if ( !parsed) {
parseXML();
}
return windspeed;
}
public double getRain() throws WeatherException {
if (!saved) {
throw new WeatherException();
}
if ( !parsed) {
parseXML();
}
return rain;
}
public String getWeather() throws WeatherException {
if (!saved) {
throw new WeatherException();
}
if ( !parsed) {
parseXML();
}
return weather;
}
public static void main(String args[]) {
Weather meteo = new Weather("Italy", "Torino", "file.xml");
try {
System.out.println("Temperatura: " + meteo.getTemperature() + "°C");
System.out.println("Umidità: " + meteo.getHumidity() + "%");
System.out.println("Pressione: " + meteo.getPressure() + "mb");
System.out.println("Vento: " + meteo.getWindspeed() + "km/h");
System.out.println("Pioggia: " + meteo.getRain() + "mm/h");
System.out.println("Weather: " + meteo.getWeather());
}
catch (WeatherException e) {
System.out.println("Errore invocazione web-service!");
}
}
}
