//Simone Tugnetti     4°AI

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author AT
 */
public class Consumer extends Thread{
    private CubbyHole cubbyhole;
    private int number;
    public Consumer(CubbyHole c, int number) {
	cubbyhole = c;
	this.number = number;
    }
    public void run() {
	int value = 0;
	for (int i = 0; i < 10; i++) {
	    value = cubbyhole.get();
	    System.out.println("Consumer #" + this.number + " got: " + value);
	    try {
		sleep((int) (Math.random() * 100));
	    } catch (InterruptedException e) {
	    }
	}
    }
}
