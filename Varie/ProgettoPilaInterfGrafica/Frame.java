/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 *
 * @author 4AI
 */
public class Frame {
    public Pila1 a1;
    public JButton ins;
    public JButton elim;
    public JButton vuota;
    public JButton size;
    public JLabel ins1;
    public JLabel elim1;
    public JLabel size1;
    public JLabel vuota1;
    public JTextField a;
    public JTextField a2;
     public Frame(){}
    public void gestione(){
    a1=new Pila1();
    JFrame f = new JFrame("Gestione di una pila");
    JPanel p = new JPanel();
    ins = new JButton("Inserisci");
    elim = new JButton("Elimina");
    vuota = new JButton("Vuota");
    size = new JButton("Size");
    ins1=new JLabel();
    elim1=new JLabel();
    vuota1=new JLabel();
    size1=new JLabel();
    a = new JTextField();
    a2=new JTextField();
    p.setLayout(null);
    p.add(ins);
    p.add(elim);
    p.add(vuota);
    p.add(size);
    p.add(a);
    p.add(ins1);
    p.add(elim1);
    p.add(vuota1);
    p.add(size1);
    p.add(a2);
    ins.setBounds(10,10,100,50);
    ins1.setBounds(180,20,300,50);
    elim.setBounds(10,70,100,50);
    elim1.setBounds(180,80,300,50);
    vuota.setBounds(10,130,100,50);
    vuota1.setBounds(180,140,300,50);
    size.setBounds(10,190,100,50);
    size1.setBounds(180,200,300,50);
    a.setBounds(200, 270, 250, 150);
    a2.setBounds(420,10,200,50);
    f.addWindowListener(new GestioneFinestra());
    ins.addActionListener(new pulsanti());
    elim.addActionListener(new pulsanti());
    vuota.addActionListener(new pulsanti());
    size.addActionListener(new pulsanti());
    a.setEditable(true);
    f.getContentPane().add(p);
    f.setSize(700,500);
    f.setVisible(true);
    }
    class pulsanti implements ActionListener{
  public pulsanti(){}
  public void actionPerformed(ActionEvent e)
  {
    String pulsante = e.getActionCommand();
    
    if (pulsante.equals("Inserisci"))
    {
        if(a2.getText().equals("")){
            a.setText("Devi inserire qualcosa nella pila!!!");
        }
        else{
            a1.push(a2.getText());
            a.setText(a2.getText());
            ins1.setText("La frase è stata aggiunta nella pila!!");   
        }
    }
    if (pulsante.equals("Elimina"))
    {
        if(a1.vuota()==true){
            a.setText("Devi prima inserire qualcosa nella pila!!!!");
        }
        else{
      a1.pop();
      elim1.setText("E' stato eliminato un elemento nella pila!!!!");
        }
        }
    if (pulsante.equals("Vuota")){
      if(a1.vuota()==true){
      vuota1.setText("La pila è vuota!!!!");
      }
      else{
          vuota1.setText("La pila non è vuota!!!!");
      }
      }
    if (pulsante.equals("Size"))
    {
      size1.setText("Gli elementi esistenti nella pila sono: "+a1.size());
    }
  }
}
}
