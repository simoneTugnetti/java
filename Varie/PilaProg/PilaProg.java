/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author daniele
 */
public class PilaProg {
    public static void main(String argv[])
    {
        Pila pila =new Pila();
        int num;
        Integer numObj;
        
        //aggiungo elementi alla pila
        for(int i=0; i<5; i++)
        {
            num = (int)(Math.random()*100);
            numObj = new Integer(num);
            System.out.print(numObj + " ");
            pila.push(numObj);
        }
        System.out.println("\nElementi nella pila: "+pila.size());
        
        //toglie gli elementi e li visualizza
        System.out.println("L'estrazione della pila avviene nel seguente modo");
        while(!pila.vuota())
        {
            numObj = (Integer) pila.pop();
            System.out.print(numObj + " ");
        }
        System.out.println();
    }
}
