/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author 4AI
 */
class Automobile extends VeicoliAMotore{
private int numPorte;
public Automobile(){
    numPorte=0;
}
public int getNumPorte(){
    return numPorte;
}
public void setNumPorte(int p){
    numPorte=p;
}
public String toString(){
    return("I dati del veicolo sono: "+getMarca()+" "+getAnnoImm()+" "+getTipoAlimentaz()+" "+getCilindrata()+" "+getNumPorte());
}
}
