/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author 4AI
 */
class Motociclista extends VeicoliAMotore{
private String tipologia;
private int numTempiMotore;
public Motociclista(){
    tipologia="";
    numTempiMotore=0;
}
public String getTipologia(){
    return tipologia;
}
public void setTipologia(String t){
    tipologia=t;
}
public int getTempiMotore(){
    return numTempiMotore;
}
public void setTempMotore(int t){
    numTempiMotore=t;
}
public String toString(){
    return("I dati del veicolo sono: "+getMarca()+" "+getAnnoImm()+" "+getTipoAlimentaz()+" "+getCilindrata()+" "+getTipologia()+" "+getTempiMotore());
}
}
