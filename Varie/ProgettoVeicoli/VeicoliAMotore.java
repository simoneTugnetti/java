/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author 4AI
 */
public class VeicoliAMotore {
private int annoImmatricolazione;
private String marca;
private String tipoAlimentazione;
private int cilindrata;
public VeicoliAMotore(){
    annoImmatricolazione=0;
    marca="";
    tipoAlimentazione="";
    cilindrata=0;
}
public int getAnnoImm(){
    return annoImmatricolazione;
}
public void setAnnoImm(int anno){
    annoImmatricolazione=anno;
}
public String getMarca(){
    return marca;
}
public void setMarca(String m){
    marca=m;
}
public String getTipoAlimentaz(){
    return tipoAlimentazione;
}
public void setTipoAlimentaz(String alimentazione){
    tipoAlimentazione=alimentazione;
}
public int getCilindrata(){
    return cilindrata;
}
public void setCilindrata(int cilindrata){
    this.cilindrata=cilindrata;
}
}
