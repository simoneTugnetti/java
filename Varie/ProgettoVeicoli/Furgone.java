/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author 4AI
 */
class Furgone extends VeicoliAMotore{
public int capacitaCarico;
public Furgone(){
    capacitaCarico=0;
}
public int getCapacita(){
    return capacitaCarico;
}
public void setCapacita(int c){
    capacitaCarico=c;
}
public String toString(){
    return("I dati del veicolo sono: "+getMarca()+" "+getAnnoImm()+" "+getTipoAlimentaz()+" "+getCilindrata()+" "+getCapacita());
}
}
