/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author AleSimo76
 */
public class FatturaVettore extends fattura{
    public String vettore;
    public double prezzo2;
    public double iva2;
    public FatturaVettore(){
        super();
    }
    public void NomeAzienda1(){
        vettore=("Agenzia Trasporti Carru'");
        prezzo2=15.50;
        iva2=(prezzo2*22)/100;
    }
    public void NomeAzienda2(){
        vettore=("Ar.Es Trasporti");
        prezzo2=13;
        iva2=(prezzo2*22)/100;
    }
     public void NomeAzienda3(){
        vettore=("Finesso Spa Trasporti Nazionali");
        prezzo2=18;
        iva2=(prezzo2*22)/100;
    }
      public String toString(){
          return ("Nome Azienda: "+vettore+";\n"+"Prezzo trasporto: "+prezzo2+";\n"+"IVA 22%: "+iva2);
      }
      public double getPrezzo2(){
          return prezzo2;
      }
      public double getIVA2(){
          return iva2;
      }
}
