
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import javax.swing.*;
/**
 *
 * @author AleSimo76
 */
public class Prodotto {
    public String cod[]=new String[100];
    public String descrizione[]=new String[100];
    public double prezzo[]=new double[100];
    public double iva[]=new double[100];
    public String eliminare;
    JFrame frame=new JFrame("Codice");
    public int i=0;
    public int j=0;
    public Prodotto(){
    }
    public void CodAlf(){//Creo il codice alfanumerico del prodotto
        cod[i]=JOptionPane.showInputDialog(frame, "Inserisci il codice alfanumerico del prodotto!!!");
        if(i>0){
            for(j=0;j<i;j++){
        if(cod[i].equals(cod[j])){
            JOptionPane.showMessageDialog(null, "Questo codice prodotto è già stato usato in precedenza!!!","info", JOptionPane.INFORMATION_MESSAGE);
             cod[i]=JOptionPane.showInputDialog(frame, "Inserisci il codice alfanumerico del prodotto!!!");
        }
        }
        }
    }
    public void Descrizione(){//Qui si assegna la descrizione del prodotto
        descrizione[i]=JOptionPane.showInputDialog(frame, "Inserisci la descrizione del prodotto!!!!");
    }
    public void Prezzo(){//Qui si assegna il prezzo al prodotto
        prezzo[i]=Double.parseDouble(JOptionPane.showInputDialog(frame, "Inserisci il prezzo del prodotto!!!!"));
    }
    public void IVA(){//Qui si calcola l'iva del prodotto
        iva[i]=(prezzo[i]*22)/100;
    }
    public void elimina(){//Qui si elimina un prodotto
         eliminare=JOptionPane.showInputDialog(frame, "Quale prodotto desideri eliminare??? (Utilizzare il codice prodotto)");
         for(i=0;i<j;i++){
             if(cod[i].equals(eliminare)){
                 cod[i]="";
                 descrizione[i]="";
                 prezzo[i]=0;
                 iva[i]=0;
             }
         }
    }
   
    public String getCodAlf(){
        return cod[i];
    }
    public String getDescrizione(){
        return descrizione[i];
    }
    public double getPrezzo(){
        return prezzo[i];
    }
    public double getIVA(){
        return iva[i];
    }
}
