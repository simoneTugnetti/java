

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import javax.swing.*;
/**
 *
 * @author AleSimo76
 */
public class Moto {
    public int moto;
    public int smotorizz;
    public int smodello;
    public String modelloScooter[]=new String[100];
    public int cilindrataScooter[]=new int[100];
    public String motorizzScooter[]=new String[100];
    public int prezzoScooter[]=new int[100];
    public String modelloStradale[]=new String[100];
    public int cilindrataStradale[]=new int[100];
    public String motorizzStradale[]=new String[100];
    public int prezzomotorizzScooter[]=new int[100];
    public int prezzocilindrataStradale[]=new int[100];
    public String modelloEnduro[]=new String[100];
    public int cilindrataEnduro[]=new int[100];
    public String motorizzEnduro[]=new String[100];
    public int prezzoEnduro[]=new int[100];
    public int prezzomotorizzEnduro[]=new int[100];
    public String modelloHypermotard[]=new String[100];
    public int cilindrataHypermotard[]=new int[100];
    public String motorizzHypermotard[]=new String[100];
    public int prezzocilindrataHypermotard[]=new int[100];
    public ScooterSpec a2=new ScooterSpec();
    public StradaleSpec a3=new StradaleSpec();
    public int i=0;
    public int j=0;
    public int y=0;
    public int w=0;
    public JFrame frame=new JFrame("Codice");
    public Moto(){
    }
    public void Scooter(){
        do{
        smodello=Integer.parseInt(JOptionPane.showInputDialog(frame, "Scegliere il modello dello scooter tra: "+"\n"+"1)Honda SH 300i"+"\n"+"2)Sym Symphony 125 ST"+"\n"+"3)Piaggio Beverly 300 S"));
        }while(smodello>3||smodello==0);
        switch(smodello){
            case 1:
                modelloScooter[i]="Honda SH 300i";
                prezzoScooter[i]=2675;
                break;
            case 2:
                modelloScooter[i]="Sym Symphony 125 ST";
                prezzoScooter[i]=2150;
                break;
            case 3:
                modelloScooter[i]="Piaggio Beverly 300 S";
                prezzoScooter[i]=3790;
                break;
        }
        cilindrataScooter[i]=50;
        do{
        smotorizz=Integer.parseInt(JOptionPane.showInputDialog(frame, "Scegliere la motorizzazione dello scooter tra: "+"\n"+"1)Due tempi"+"\n"+"2)Quattro tempi"+"\n"+"3)Elettrica"));
        }while(smotorizz>3||smotorizz==0);
        switch(smotorizz){
            case 1:
                motorizzScooter[i]="Due tempi";
                prezzomotorizzScooter[i]=prezzoScooter[i]-300;
                break;
            case 2:
                motorizzScooter[i]="Quattro tempi";
                prezzomotorizzScooter[i]=prezzoScooter[i]-250;
                break;
            case 3:
                motorizzScooter[i]="Elettrica";
                prezzomotorizzScooter[i]=prezzoScooter[i]-200;
                break;
        }
        a2.Bauletto();
        prezzomotorizzScooter[i]=prezzomotorizzScooter[i]+a2.getPrezzoBauletto();
        i++;
    }
    public void Stradali(){
        do{
        smodello=Integer.parseInt(JOptionPane.showInputDialog(frame, "Scegliere il modello di moto stradale tra: "+"\n"+"1)Honda CB F CB 125 F"+"\n"+"2)BMW Serie R GS R 1200"+"\n"+"3)Suzuki V-Strom 650 XT"));
        }while(smodello>3||smodello==0);
        switch(smodello){
            case 1:
                modelloStradale[j]="Honda CB F CB 125 F";
                break;
            case 2:
                modelloStradale[j]="BMW Serie R GS";
                break;
            case 3:
                modelloStradale[j]="Suzuki V-Strom 650 XT";
                break;
        }
        switch(smodello){
            case 1:
                cilindrataStradale[j]=125;
                prezzocilindrataStradale[j]=2490-150;
                break;
            case 2:
                cilindrataStradale[j]=1170;
                prezzocilindrataStradale[j]=16630-(150+(15*92));
                break;
            case 3:
                cilindrataStradale[j]=650;
                prezzocilindrataStradale[j]=8590-(150+(15*40));
                break;
        }
        do{
        smotorizz=Integer.parseInt(JOptionPane.showInputDialog(frame, "Scegliere la motorizzazione dello scooter tra: "+"\n"+"1)Due tempi"+"\n"+"2)Quattro tempi"+"\n"+"3)Elettrica"));
        }while(smotorizz>3||smotorizz==0);
        switch(smotorizz){
            case 1:
                motorizzStradale[j]="Due tempi";
                break;
            case 2:
                motorizzStradale[j]="Quattro tempi";
                break;
            case 3:
                motorizzStradale[j]="Elettrica";
                break;
        }
        a3.Carena();
        prezzocilindrataStradale[i]=prezzocilindrataStradale[i]+a3.getPrezzoCarena();
        j++;
    }
    public void Enduro(){
        do{
        smodello=Integer.parseInt(JOptionPane.showInputDialog(frame, "Scegliere il modello di moto Enduro tra: "+"\n"+"1)XTRAINER 300"+"\n"+"2)TM EN125 MY 2015"+"\n"+"3)TM EN-300 MY 2015"));
        }while(smodello>3||smodello==0);
        switch(smodello){
            case 1:
                modelloEnduro[y]="XTRAINER 300";
                prezzoEnduro[y]=6390;
                break;
            case 2:
                modelloEnduro[y]="TM EN-125 MY 2015";
                prezzoEnduro[y]=7440;
                break;
            case 3:
                modelloEnduro[y]="TM EN-300 MY 2015";
                prezzoEnduro[y]=7990;
                
                break;
        }
        switch(smodello){
            case 1:
                cilindrataEnduro[y]=300;
                break;
            case 2:
                cilindrataEnduro[y]=125;
                break;
            case 3:
                cilindrataEnduro[y]=300;
                break;
        }
        do{
        smotorizz=Integer.parseInt(JOptionPane.showInputDialog(frame, "Scegliere la motorizzazione dello scooter tra: "+"\n"+"1)Due tempi"+"\n"+"2)Quattro tempi"+"\n"+"3)Elettrica"));
        }while(smotorizz>3||smotorizz==0);
        switch(smotorizz){
            case 1:
                motorizzEnduro[y]="Due tempi";
                prezzomotorizzEnduro[y]=prezzoEnduro[y]-200;
                break;
            case 2:
                motorizzEnduro[y]="Quattro tempi";
                prezzomotorizzEnduro[y]=prezzoEnduro[y]-150;
                break;
            case 3:
                motorizzEnduro[y]="Elettrica";
                prezzomotorizzEnduro[y]=prezzoEnduro[y];
                break;
        }
        y++;
    }
    public void Hypermotard(){
        do{
        smodello=Integer.parseInt(JOptionPane.showInputDialog(frame, "Scegliere il modello di moto Ducati tra: "+"\n"+"1)Hypermotard"+"\n"+"2)Hypermotard SP"+"\n"+"3)Hyperstrada"));
        }while(smodello>3||smodello==0);
        switch(smodello){
            case 1:
                modelloHypermotard[w]="Hypermotard";
                break;
            case 2:
                modelloHypermotard[w]="Hypermotard SP";
                break;
            case 3:
                modelloHypermotard[w]="Hyperstrada";
                break;
        }
        switch(smodello){
            case 1:
                cilindrataHypermotard[w]=850;
                prezzocilindrataHypermotard[w]=11490-(50*10);
                break;
            case 2:
                cilindrataHypermotard[w]=850;
                prezzocilindrataHypermotard[w]=14740-(50*10);
                break;
            case 3:
                cilindrataHypermotard[w]=850;
                prezzocilindrataHypermotard[w]=12740-(50*10);
                break;
        }
        do{
        smotorizz=Integer.parseInt(JOptionPane.showInputDialog(frame, "Scegliere la motorizzazione dello scooter tra: "+"\n"+"1)Due tempi"+"\n"+"2)Quattro tempi"+"\n"+"3)Elettrica"));
        }while(smotorizz>3||smotorizz==0);
        switch(smotorizz){
            case 1:
                motorizzHypermotard[w]="Due tempi";
                break;
            case 2:
                motorizzHypermotard[w]="Quattro tempi";
                break;
            case 3:
                motorizzHypermotard[w]="Elettrica";
                break;
        }
        w++;
    }
    public String toString(){
        return ("Scooter"+"\n"+"Modello: Honda SH 300i (2675€), Sym Symphony 125 ST (2150€), Piaggio Beverly 300 S (3790€);"+"\n"+"Cilindrata: 50cc"+"\n"+"Motorizzazione: Due tempi (300€ di sconto), Quattro tempi (250€ si sconto), Elettrica (200€ di sconto);"+"\n"+"Bauletto: Grande, Medio, Piccolo, Assente."+"\n"+"\n"+"Stradali"+"\n"+"Modello: Honda CB F CB 125 F (2490€), BMW Serie R GS (16630€), Suzuki V-Strom 650 XT (8590€);"+"\n"+"Cilindrata (rispettivamente)(sconto di 150€ più 15 € per ogni 10 cc di cilindrata oltre i 250cc): 125cc, 1170cc, 650cc;"
                +"\n"+ "Motorizzazione: Due tempi, Quattro tempi, Elettrica;"+"\n"+"Carena: Semicarena, Cupolino, Integrale, Naked."+"\n"+"\n"+"Enduro"+"\n"+"Modello: XTRAINER 300 (6390€), TM EN-125 MY 2015 (7440€), TM EN-300 MY 2015 (7990€);"+"\n"+"Cilindrata (rispettivamente): 300cc, 125cc, 300cc;"+"\n"+"Motorizzazzione: Due tempi (200€ di sconto), Quattro tempi (150€ di sconto), Elettrica."
                + "\n"+"\n"+"Hypermotard"+"\n"+"Modelli: Hypermotard (11490€), Hypermotard SP (14740€), Hyperstrada (12740€);"+"\n"+"Cilindrata (50 € ogni 50cc di cilindrata oltre i 350cc): 850cc;"+"\n"+"Motorizzazione: Due tempi, Quattro tempi, Elettrica.");
    }
    public String getModelloScooter(){
        return modelloScooter[i];
    }
    public int getPrezzoScooter(){
        return prezzoScooter[i];
    }
    public int getCilindrataScooter(){
        return cilindrataScooter[i];
    }
    public String getMotorizzScooter(){
        return motorizzScooter[i];
    }
    public int getPrezzoMotorizzScooter(){
        return prezzomotorizzScooter[i];
    }
    public String getModelloStradale(){
        return modelloStradale[j];
    }
    public int getCilindrataStradale(){
        return cilindrataStradale[j];
    }
    public int getPrezzoCilindrataStradale(){
        return prezzocilindrataStradale[j];
    }
    public String getMotorizzStradale(){
        return motorizzStradale[j];
    }
    public String getModelloEnduro(){
        return modelloEnduro[y];
    }
    public int getPrezzoEnduro(){
        return prezzoEnduro[y];
    }
    public int getCilindrataEnduro(){
        return cilindrataEnduro[y];
    }
    public String getMotorizzEnduro(){
        return motorizzEnduro[y];
    }
    public int getPrezzoMotorizzEnduro(){
        return prezzomotorizzEnduro[y];
    }
    public String getModelloHypermotard(){
        return modelloHypermotard[w];
    }
    public int getCilindrataHypermotard(){
        return cilindrataHypermotard[w];
    }
    public int getPrezzoCilindrataHypermotard(){
        return prezzocilindrataHypermotard[w];
    }
    public String getMotorizzHypermotard(){
        return motorizzHypermotard[w];
    }
}
