//Simone Tugnetti       4°AI

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author AleSimo76
 */
public class nodo {
    private impiegato info;
    private nodo link;

 public nodo (impiegato persona){//Costruttore per avere le informazioni di una persona atrraverso un oggetto
     info=new impiegato(persona);
     link=null;
 }

 public void setInfo(impiegato persona){
     info=new impiegato(persona);
 }

 public impiegato getInfo(){
     return new impiegato(info);
 }

 protected void setLink(nodo link){
     this.link=link;
 }

 public nodo getLink(){
     return link;
 }
}
