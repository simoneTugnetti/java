//Simone Tugnetti      4°AI

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.*;
/**
 *
 * @author AleSimo76
 */
public class main {
 public static void main (String[] args) throws Exception{
     InputStreamReader input=new InputStreamReader(System.in);
     BufferedReader tastiera=new BufferedReader(input);
     int scelta;
     String cogn;
     String mansione1;
     String nome1;
     int i=0;
     int aumento;
     Gestione f = new Gestione();
     impiegato a1=null;
     System.out.println("Benvenuti in Tugnetti's Factory!!!");
     System.out.println("Ci sono dei pacchi da mettere in ordine!!");
     do{
     System.out.println("Che cosa desideri fare??");
     System.out.println("1-Inserire un nuovo impiegato");
     System.out.println("2-Modifica della mansione di un impiegato");
     System.out.println("3-Stampa di tutti gli impiegati con una certa mansione");
     System.out.println("4-Licenziare un impiegato");
     System.out.println("5-Aumento dello stipendio di tutti gli impiegati");
     System.out.println("6-Visualizzare la lista");
     System.out.println("7-Uscire dalla fabbrica");
     scelta=Integer.parseInt(tastiera.readLine());
     switch(scelta){
         case 1:
             a1=new impiegato();
             System.out.println("Inserisci il nome!!!!");
             a1.setNome(tastiera.readLine());
             System.out.println("Inserisci il cognome!!!!!");
             a1.setCognome(tastiera.readLine());
             System.out.println("Inserisci il numero di matricolazione!!!");
             a1.setMatricola(Integer.parseInt(tastiera.readLine()));
             System.out.println("Inserisci la sua mansione!!!!");
             a1.setMansione(tastiera.readLine());
             System.out.println("Inserisci il suo stipendio mensile!!!");
             a1.setStipendioMensile(Integer.parseInt(tastiera.readLine()));
             if(i==0){
                 f.inserisciInTesta(a1);
             }
             else{
                 f.inserisciInCoda(a1);
             }
             i++;
              break;
         case 2:
             if(f.getElementi()!=0){
             System.out.println("Inserisci il cognome dell'impiegato!!!!");
             cogn=tastiera.readLine();
             for(i=0;i<f.getElementi();i++){
             if(cogn.equals(a1.getCognome())){
                 System.out.println("Inserisci la nuova mansione!!!!");
                 a1.setMansione(tastiera.readLine());
                 f.inserisciInPosizione(a1, i);
             }
             }
             }else{
                 System.out.println("Non ci sono elementi nella lista!!!!");
             }
             break;
         case 3:
             if(f.getElementi()!=0){
             System.out.println("Inserisci la mansione da ricercare!!!!");
             mansione1=tastiera.readLine();
             for(i=0;i<f.getElementi();i++){
             if(mansione1.equals(a1.getMansione())){
                 System.out.println("Ecco l'impiegato avente questa mansione!!!!!");
                 a1.toString();
             }
             }
             }else{
                 System.out.println("Non ci sono elementi nella lista!!!!");
             }
             break;
         case 4:
             if(f.getElementi()!=0){
             System.out.println("Inserisci il nome dell'impiegato da licenziare!!!");
             nome1=tastiera.readLine();
             for(i=0;i<f.getElementi();i++){
             if(nome1.equals(a1.getNome())){
                 System.out.println("L'impiegato verrà ora licenziato!!!!!");
                 f.eliminaInPosizione(i);
             }
             }
             i--;
             }else{
                 System.out.println("Non ci sono elementi nella lista!!!!");
             }
             break;
         case 5:
             if(f.getElementi()!=0){
             System.out.println("Inserisci di quanto vuoi aumentare lo stipendio agli impiegati!!!!");
             aumento=Integer.parseInt(tastiera.readLine());
             for(i=0;i<f.getElementi();i++){
             a1.setStipendioMensile(a1.getStipendioMensile()+aumento);
             f.inserisciInPosizione(a1, i);
             }
             System.out.println("Lo stipentio di tutti gli impiegati è stato aumentato!!!");
             }else{
                 System.out.println("Non ci sono elementi nella lista!!!!");
             }
             break;
         case 6:
             if(f.getElementi()!=0){
             f.toString();
             }else{
                 System.out.println("Non ci sono elementi nella lista!!!!");
             }
             break;
     }
 }while(scelta!=7);
}
}