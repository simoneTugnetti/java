//Simone Tugnetti    4°AI

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author AleSimo76
 */
public class Gestione {
 private nodo head;
 private int elementi;
 
public Gestione() {
  head = null;
  elementi=0;
}
private nodo getLinkPosizione(int posizione) throws Exception {
  int n = 1;
  nodo p = head;
  if (head!=null){
  if ((posizione<elementi) || (posizione>1)){
  while ((p.getLink()!=null) && (n<posizione)) {
   p = p.getLink();
   n++;
  }
  }
  else{
      System.out.println("La posizione è errata!!!!");
  }
  }
  else{
      System.out.println("La lista è vuota!!!!");
  }
  return p;
}
private nodo creaNodo(impiegato persona, nodo link) {
   nodo nuovoNodo = new nodo(persona);
   nuovoNodo.setLink(link);
   return nuovoNodo;
}
public int getElementi() {
  return elementi;
}
public void inserisciInTesta(impiegato persona) {
   nodo p = creaNodo(persona, head);
   head = p;
   elementi++;
   return;
} 
 public void inserisciInCoda(impiegato persona) throws Exception{
  if (head==null)
   inserisciInTesta(persona);
  else {
      nodo p = getLinkPosizione(elementi);
      p.setLink(creaNodo(persona, null));
      elementi++;
   }
  return;
  }
 public void inserisciInPosizione(impiegato persona, int posizione) throws Exception {
     if (posizione<=1){
       inserisciInTesta(persona);
     }
     else {
       if (elementi<posizione){
         inserisciInCoda(persona);
       }
       else {
         nodo p = getLinkPosizione(posizione-1);
         p.setLink(creaNodo(persona, p.getLink()));
         elementi++;
    }
  }
  return;
} 
public void eliminaInTesta() {
 if (head!=null){
   head=head.getLink();
   elementi--;
   }
 else{
    System.out.println("La lista è vuota!!!");
 }
   return;
 }

 public void eliminaInCoda() throws Exception{
  if (head!=null){
  nodo p=getLinkPosizione(elementi-1);
  p.setLink(null);
  elementi--;
 }
  else{
      System.out.println("La lista è vuota!!!!");
  }
  return;
 }
 
 public void eliminaInPosizione(int posizione) throws Exception {
  if (posizione==1){
    eliminaInTesta();
  }
  else{
   if (posizione==elementi){
     eliminaInCoda();
  }
   else {
     nodo ps = getLinkPosizione(posizione);
     nodo pp = getLinkPosizione(posizione-1);
     pp.setLink(ps.getLink());
     elementi--;
  }
  return;
 }
 }
 private String visita(nodo p) {
  if (p==null)
   return "";
  return p.getInfo().toString()+"\n"+visita(p.getLink());
 }
 
 public String elenco() {
  return visita(head);
 }
 
 public String toString() {
  nodo p = head;
  String lista = new String("head->");
  if (p==null)
   return lista+"null";
  while (p!=null) {
   lista = lista+"["+p.getInfo().toString()+"|";
   if (p.getLink()==null)
    lista = lista+"null]";
   else
    lista = lista+"+]->";
    p = p.getLink();
  }
  return lista;
 }
}
