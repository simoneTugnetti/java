//Simone Tugnetti    4°AI

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author AleSimo76
 */
public class impiegato {
    private String nome;
    private int matricola;
    private String cognome;
    private String mansione;
    private int StipendioMensile;

 public impiegato(){//Costruttore con attributi come parametri
     nome="";
     cognome="";
     matricola=0;
     mansione="";
     StipendioMensile=0;
 }

 public impiegato(impiegato impiegato){//Costruttore con oggetto come parametro per avere l'informazione degli attributi
     this.nome=impiegato.getNome();
     this.cognome=impiegato.getCognome();
     this.matricola=impiegato.getMatricola();
     this.mansione=impiegato.getMansione();
     this.StipendioMensile=impiegato.getStipendioMensile();
 }
 
 public void setNome(String nome){
     this.nome=nome;
 }
 public String getNome(){
     return nome;
 }
  public void setCognome(String cognome){
     this.cognome=cognome;
 }
 public String getCognome(){
     return cognome;
 }
 public void setMatricola(int matricola){
      this.matricola=matricola;
 }
 public int getMatricola(){
     return matricola;
}
 public void setMansione(String mansione){
     this.mansione=mansione;
 }
 public String getMansione(){
     return mansione;
 }
 public void setStipendioMensile(int StipendioMensile){
     this.StipendioMensile=StipendioMensile;
 }
 public int getStipendioMensile(){
     return StipendioMensile;
 }
 public String toString(){
     return("Dati dell'impiegato: "
             + "nome --> "+nome+"; "
             + "cognome --> "+cognome+"; "
             + "numero matricola --> "+matricola+"; "
             + "mansione --> "+mansione+"; "
             + "stipendio mensile --> "+StipendioMensile+".");
 }
}
