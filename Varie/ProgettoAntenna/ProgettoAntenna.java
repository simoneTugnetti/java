//Simone Tugnetti      4°AI

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author AT
 */
public class ProgettoAntenna {
    public static void main(String args[]) throws Exception{
    int numero_antenne_totale, numero_antenne_trovate;
    Antenna a1=new Antenna(20.2, 23.1, "a1");
    Antenna a2=new Antenna(21.2, 24.1, "a2");
    Antenna a3=new Antenna(22.2, 25.1, "a3");
    Antenna a4=new Antenna(43.2, 46.1, "a4");
    Antenna a5=new Antenna(54.2, 57.1, "a5");
    Antenna a6=new Antenna(57.2, 51.1, "a6");
        Registro registro=new Registro();
        registro.aggiungiAntenna(a1);
        registro.aggiungiAntenna(a2);
        registro.aggiungiAntenna(a3);
        registro.aggiungiAntenna(a4);
        registro.aggiungiAntenna(a5);
        numero_antenne_totale = registro.getNumeroAntenne();
        System.out.println("Il numero di antenne: "+numero_antenne_totale);
        Ricerca ricerca1 = new Ricerca( registro, 0, numero_antenne_totale/2, 43.0, 60.6, 30.0, 60.4);
        Ricerca ricerca2 = new Ricerca( registro, numero_antenne_totale/2, numero_antenne_totale, 50.0, 65.6, 31.0, 58.4);
        ricerca1.start();
        ricerca2.start();
        try{
        ricerca1.join();
        ricerca2.join();
        }
        catch(InterruptedException e){
        }
        numero_antenne_trovate = ricerca1.getConteggioAntenne() + ricerca2.getConteggioAntenne();
        System.out.println("Il numero di antenne trovate: "+numero_antenne_trovate);
}
}