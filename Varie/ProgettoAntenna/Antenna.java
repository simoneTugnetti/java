//Simone Tugnetti     4°AI

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author AT
 */
public class Antenna {
    private double latitudine;
    private double longitudine;
    private String tipo;
    public Antenna( double latitudine, double longitudine,String tipo){
    this.latitudine = latitudine;
    this.longitudine = longitudine;
    this.tipo = tipo;
}
    public Antenna(Antenna antenna){
        this.latitudine=antenna.latitudine;
        this.longitudine=antenna.longitudine;
        this.tipo=antenna.tipo;
    }
    public double getLatitudine(){
        return latitudine;
    }
    public double getLongitudine(){
        return longitudine;
    }
    public String getTipo(){
        return tipo;
    }
    public void setLatitudine(double latitudine) {
        this.latitudine = latitudine;
    }
    public void setLongitudine(double longitudine) {
        this.longitudine = longitudine;
    }
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
}
