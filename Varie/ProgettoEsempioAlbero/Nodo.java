class Nodo
{
  // informazioni del nodo
  public int codice;
  public String descrizione;
  public boolean eliminato;

  // sottoalberi
  public Nodo sinistro;
  public Nodo destro;

  public Nodo(int codice, String descrizione)
  {
    this.codice = codice;
    this.descrizione = descrizione;

    eliminato = false;
    sinistro = null;
    destro = null;
  }

  public void stampa()
  {
    System.out.print("-"+codice+"- "+descrizione);
    if (eliminato)
    {
      System.out.print(" (eliminato)");
    }
    System.out.println();
  }
}
