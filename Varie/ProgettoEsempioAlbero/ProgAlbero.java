import java.io.*;

class ProgAlbero
{
  InputStreamReader input = new InputStreamReader(System.in);
  BufferedReader tastiera = new BufferedReader(input);
  Albero a;//Crea una variabile a di tipo Albero
  
  public ProgAlbero()
  {
    int scelta;

    // crea l'albero vuoto
    a = new Albero();

    do
    {
      System.out.println();
      System.out.println("***** MENU *****");
      System.out.println("1) Aggiunta di un dato");
      System.out.println("2) Eliminazione di un dato");
      System.out.println("3) Ricerca di un dato");
      System.out.println("4) Stampa ordinata");
      System.out.println("5) Stampa in preordine");
      System.out.println("6) Stampa in postordine");
      System.out.println("\n0) Fine\n");
      System.out.print("Scelta: ");

      try
      {
        scelta = Integer.valueOf(tastiera.readLine()).intValue();//Converte la variabile tastiera in un int per scelta
      }
      catch(Exception e)
      {
        scelta = 10;//Se il valore inserito dà un errore, scelta diventa uguale a 10
      }

      switch(scelta)//In base alla scelta effettuata, il programma viene reindirizzato in una procedura specifica
      {
        case 0:
          break;
        case 1:
          aggiunta();
          break;
        case 2:
          eliminazione();
          break;
        case 3:
          ricerca();
          break;
        case 4:
          a.stampaInordine();
          break;
        case 5:
          a.stampaPreordine();
          break;
        case 6:
          a.stampaPostordine();
          break;
        default://Se non è stato inserito nulla, verrà stampato a video "Scelta non corretta."
          System.out.println("Scelta non corretta.");
          break;
      }
    }
    while (scelta != 0);
  }

  public void aggiunta()//In questa procedura, si aggiunge un nuovo nodo dove verrà inserito il codice e la descrizione dell'elemento per poi infine aggiungerlo nell'albero.
  {
    Nodo n;
    int codice;
    String descrizione;

    try 
    {
      System.out.print("Codice: ");
      codice = Integer.valueOf(tastiera.readLine()).intValue();
      System.out.print("Descrizione: ");
      descrizione = tastiera.readLine();
      n = new Nodo(codice, descrizione);//Creazione di un nuovo nodo con l'inserimento del codice e della descrizione.
      a.inserisci(n);//Viene aggiunto all'albero il nodo precedentemente creato.
    }
    catch(Exception e) {}
  }

  public void eliminazione()//In questa procedura, si elimina dall'albero l'elemento avente lo stesso codice inserito dall'utente.
  {
    int codice;

    try
    {
      System.out.print("Codice: ");
      codice = Integer.valueOf(tastiera.readLine()).intValue();
      a.elimina(codice);
    }
    catch(Exception e) {}
  }
   
  public void ricerca()//In questa procedura, il programma cerca il nodo nell'albero avente lo stesso codice chiesto all'utente per poi stamparlo a video
  {
    Nodo n;
    int codice;

    try 
    {
      System.out.print("Codice: ");
      codice = Integer.valueOf(tastiera.readLine()).intValue();
      n = a.contiene(codice);
    }
    catch(Exception e)
    {
      n = null;
    }
    if (n != null)
    {
      System.out.println("Nodo trovato:");
      n.stampa();
    }
    else
    {
      System.out.println("Codice inesistente.");
    }
  }

  // Avvia il programma chiamando il costruttore
  public static void main(String argv[])
  {
    new ProgAlbero();
  }
}
