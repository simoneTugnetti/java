public class FestaException extends Exception{//Classe che gestisce gli errori
    private String error="";

    FestaException(String error){
        this.error = error;
    }

    String getError(){
     return error;
    }
}