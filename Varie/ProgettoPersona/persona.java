/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.progettopersona;


/**
 *
 * @author 4AI
 */
public class persona {

    private String nome,cognome,sesso,professione;  // Dichiaro le variabili private della classe Persona
    private int eta;  

    public String getNome(){  // Funzione che permette di prendere il contenuto della variabile "nome"
        return nome;
    }

    public String getCognome(){ // Funzione che permette di prendere il contenuto della variabile "cognome"
        return cognome;
    }


    public String getSesso(){// Funzione che permette di prendere il contenuto della variabile "sesso"
        return sesso;
    }


    public String getProfessione(){ // Funzione che permette di prendere il contenuto della variabile "professione"
        return professione;
    }

     public int getEta(){ // Funzione che permette di prendere il contenuto della variabile "eta"
     return eta;
    }

public void setNome(String nome){  // Funzione che permette di cambiare/inserire il valore della variabile "nome"

    this.nome=nome;

    }

public void setCognome(String cognome){ // Funzione che permette di cambiare/inserire il valore della variabile "cognome"

    this.cognome=cognome;

    }

public void setSesso(String sesso){ // Funzione che permette di cambiare/inserire il valore della variabile "sesso"

    this.sesso=sesso;

    }

public void setProfessione(String professione){ // Funzione che permette di cambiare/inserire il valore della variabile "professione"
    
    this.professione=professione;

    }

    public void setEta(int eta){ // Funzione che permette di cambiare/inserire il valore della variabile "eta"

    this.eta=eta;

    }

    public void ChiSei(){  //Funzione che stampa a video la frase sottostante.

        System.out.println("Sono una persona di nome: "+nome+", cognome:"+cognome+", sesso:"+sesso+", eta:"+eta+" e professione:"+professione+".");
    }


}
