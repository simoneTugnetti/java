//Simone Tugnetti     4°AI

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.awt.event.*;
import javax.swing.*;
import java.io.*;
import java.util.Collections;
/**
 *
 * @author 4AI
 */
public class Frame {
    public JButton ins;
    public JButton elim;
    public JButton ricerca;
    public JMenu m1;
    public JMenuItem salva;
    public JMenuItem chiudi;
    public JMenuItem apri;
    public String nome;
    public String cognome;
    public int telefono;
    public int i=0;
    public int j=0;
    public JFrame f;
    public Lista lista;
     public Frame(){}
    public void gestione(){//Creo il frame con gli algoritmi grafici grafici
    f = new JFrame("Rubrica");
    JPanel p = new JPanel();
    JMenuBar mb=new JMenuBar();
    lista=new Lista();
    f.setJMenuBar(mb);
    ins = new JButton("Inserisci");
    elim = new JButton("Elimina");
    ricerca = new JButton("Ricerca");
    p.setLayout(null);
    m1=new JMenu("File"); 
    mb.add(m1);
    salva=new JMenuItem("Salva");
    chiudi=new JMenuItem("Chiudi");
    apri=new JMenuItem("Apri");
    m1.add(salva);
    m1.add(apri);
    m1.add(chiudi);
    p.add(ins);
    p.add(elim);
    p.add(ricerca);
    ins.setBounds(10,10,100,50);
    elim.setBounds(10,70,100,50);
    ricerca.setBounds(10,130,100,50);
    f.addWindowListener(new GestioneFinestra());
    ins.addActionListener(new pulsanti());//Ogni volta che si preme un pulsante si crea la classe pulsanti per la gestione dei suddetti
    elim.addActionListener(new pulsanti());
    ricerca.addActionListener(new pulsanti());
    salva.addActionListener(new pulsanti());
    chiudi.addActionListener(new pulsanti());
    apri.addActionListener(new pulsanti());
    f.getContentPane().add(p);
    f.setSize(200,300);
    f.setVisible(true);
    }
    class pulsanti implements ActionListener{//Creo la classe per poter gestire i vari pulsanti
        private FileWriter f1;
        private PrintWriter fOUT;
        private String nome1;
        private FileReader f2;
        private BufferedReader fIN;
        private String s;
        private int scelta;
  public pulsanti(){}
  public void actionPerformed(ActionEvent e)//Metodo per gestire gli input ricevuti dai pulsanti
  {
    String pulsante = e.getActionCommand();
    
    if (pulsante.equals("Inserisci")){//Se si preme il pulsante Inserisci, si devono aggiungere i dati di un contatto nellla lista
        nome = JOptionPane.showInputDialog(null,"Inserisci il nome!!!!");
        while(nome.equals("")){
            JOptionPane.showMessageDialog(null, "Devi prima inserire il nome","info", JOptionPane.INFORMATION_MESSAGE);
            nome = JOptionPane.showInputDialog(null,"Inserisci il nome!!!!");
        }
        cognome = JOptionPane.showInputDialog(null,"Inserisci il cognome!!!!");
        while(cognome.equals("")){
            JOptionPane.showMessageDialog(null, "Devi prima inserire il cognome","info", JOptionPane.INFORMATION_MESSAGE);
            cognome = JOptionPane.showInputDialog(null,"Inserisci il cognome!!!!");
        }
        telefono = Integer.parseInt(JOptionPane.showInputDialog("Inserisci il numero di telefono!!!!",0));
        while(telefono==0){
            JOptionPane.showMessageDialog(null, "Devi prima inserire il numero di telefono","info", JOptionPane.INFORMATION_MESSAGE);
            telefono = Integer.parseInt(JOptionPane.showInputDialog(0,"Inserisci il numero di telefono!!!!"));
        }
        lista.inserisci(new Dati(nome,cognome,telefono));
    }
    if (pulsante.equals("Elimina")){//Se si preme il pulsante elimina, l'utente può decidere se eliminare tutta la lista o solo un determinato contatto
         scelta=Integer.parseInt(JOptionPane.showInputDialog(null,"Desideri eliminare tutti i contatti o uno solo???? (1-Tutti; 2-Uno)"));
         if(scelta==1){
             lista.elimina();
             try{
             f1=new FileWriter("Dati.txt");
             fOUT=new PrintWriter(f1);
             fOUT.println("");
             fOUT.flush();
             f1.close();
             }
             catch(IOException w){}
         }
         else{
         nome1=JOptionPane.showInputDialog(null,"Inserisci il nome della persona da cancellare!!!!");
         for(i=0;i<lista.size();i++){
         if(lista.rubrica.get(i).getNome().equals(nome1)){
             lista.rubrica.remove(i);
             JOptionPane.showMessageDialog(null, "Il contatto è stato eliminato","info", JOptionPane.INFORMATION_MESSAGE);
         }
         }
         }
    }
    if (pulsante.equals("Ricerca")){//Se si preme il pulsante ricerca, si ricerca il nome di quella persona modificandone poi i dati
      nome1=JOptionPane.showInputDialog(null,"Inserisci il nome della persona da ricercare!!!!");
      for(i=0;i<lista.size();i++){
         if(lista.rubrica.get(i).getNome().equals(nome1)){
             nome = JOptionPane.showInputDialog(null,"Inserisci il nome!!!!");
        while(nome.equals("")){
            JOptionPane.showMessageDialog(null, "Devi prima inserire il nome","info", JOptionPane.INFORMATION_MESSAGE);
            nome = JOptionPane.showInputDialog(null,"Inserisci il nome!!!!");
        }
        cognome = JOptionPane.showInputDialog(null,"Inserisci il cognome!!!!");
        while(cognome.equals("")){
            JOptionPane.showMessageDialog(null, "Devi prima inserire il cognome","info", JOptionPane.INFORMATION_MESSAGE);
            cognome = JOptionPane.showInputDialog(null,"Inserisci il cognome!!!!");
        }
        telefono = Integer.parseInt(JOptionPane.showInputDialog("Inserisci il numero di telefono!!!!",0));
        while(telefono==0){
            JOptionPane.showMessageDialog(null, "Devi prima inserire il numero di telefono","info", JOptionPane.INFORMATION_MESSAGE);
            telefono = Integer.parseInt(JOptionPane.showInputDialog(0,"Inserisci il numero di telefono!!!!"));
        }
             lista.rubrica.add(i, new Dati(nome,cognome,telefono));
             JOptionPane.showMessageDialog(null, "Il contatto è stato modificato","info", JOptionPane.INFORMATION_MESSAGE);
         }
      }
      }
    if(pulsante.equals("Salva")){//Se si preme il pulsante Salva, salverà tutti i dati dentro alla lista nel file di testo
        for(i=0;i<lista.size();i++){
            for(j=i+1;j<lista.size();j++){
                if(lista.rubrica.get(j).getCognome().compareTo(lista.rubrica.get(i).getCognome())){
                    Collections.sort(lista.rubrica);
                }
            }
        }
        try{
      f1=new FileWriter("Dati.txt");
      fOUT=new PrintWriter(f1);
      for(i=0;i<lista.size();i++){
      fOUT.println(lista.rubrica.get(i).getInfo());
      }
      fOUT.flush();
      f1.close();
        }
        catch(IOException n){}
    }
    if(pulsante.equals("Chiudi")){//Chiude il programma
    f.dispose();
    System.exit(0);
    }
    if(pulsante.equals("Apri")){//Apre il file visualizzandone i dati
        try{
         f2=new FileReader("Dati.txt");
         fIN=new BufferedReader(f2);
         s=fIN.readLine();
         while(s!=null){
             JOptionPane.showMessageDialog(null, s,"info", JOptionPane.INFORMATION_MESSAGE);
             s=fIN.readLine();
         }
         }
         catch(IOException y){}
    }
    }
  }
}