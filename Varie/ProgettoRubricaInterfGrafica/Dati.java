/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author 4AI
 */
public class Dati {//Classe per gestire i dati dell'utente
    private String nome;
    private String cognome;
    private int ntel;
            
    public Dati(Dati d){
    this.nome=d.getNome();
    this.cognome=d.getCognome();
    this.ntel=d.getNTel();
    }
    public Dati(String nome, String cognome, int ntel){
        this.nome=nome;
        this.cognome=cognome;
        this.ntel=ntel;
    }
    public String getNome(){
        return nome;
    }
    public String getCognome(){
        return cognome;
    }
    public int getNTel(){
        return ntel;
    }
    public String getInfo(){
        return getNome()+";"+getCognome()+";"+getNTel();
    }
}