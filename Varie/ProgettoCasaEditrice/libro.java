//Simone Tugnetti          4°AI
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.*;
/**
 *
 * @author 4AI
 */
class libro extends pubblicazione{//Questa è una sottoclasse di pubblicazione chiamata libro
public int ncopievendute;
public int scelta;
public libro(){//Qui istanzio a 0 i valori degli attributi
    ncopievendute=0;
    scelta=0;
}
public void aumento(){//In questa procedura chiedo all'utente se vuole comprare il libro
    InputStreamReader input=new InputStreamReader(System.in);
    BufferedReader tastiera=new BufferedReader(input);
    System.out.println("Vuoi acquistare questo libro??? (1-SI;2-NO)");
    try{
     scelta=Integer.parseInt(tastiera.readLine());   
    }
    catch(IOException g){
        System.out.println("Errore!!!");
    }
    do{
    if(scelta==1){
        ncopievendute++;
    }
    else{
        System.out.println("Peggio per te!!!");
    }
    System.out.println("Vuoi acquistare di nuovo lo stesso libro??? (1-SI;2-NO)");
    try{
     scelta=Integer.parseInt(tastiera.readLine());   
    }
    catch(IOException g){
        System.out.println("Errore!!!");
    }
    }while(scelta==1);
}
public void visualizza(){//Qui visualizzo il numero di copie vendute
    System.out.println("Questo è il numero di libri venduti: "+ncopievendute);
}
public int getNcopie(){//Qui ottengo il numero di copie vendute
    return ncopievendute;
}
}
