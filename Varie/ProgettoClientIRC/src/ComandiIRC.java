/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.net.*;
import java.io.*;
/**
 *
 * @author 5ai
 */
public class ComandiIRC {
    private DatiConnessione datiConn;
    private Socket socket;
    private OutputStream output;
    private InputStream input;
    private BufferedWriter writer;
    private BufferedReader reader;
    public ComandiIRC(){}
    public void disconnetti(){
        try{
            writer.write("QUIT \n");
            writer.flush();
            socket.close();
        }
        catch(IOException e){
        e.printStackTrace();
        }
    }
    public void join(){
        try{
            writer.write("JOIN #"+datiConn.get_canale()+"\n");
            writer.flush();
        }
        catch(IOException e){
            e.printStackTrace();
        }
    }
    public void part(){
        try{
            writer.write("PART #"+datiConn.get_canale()+"\n");
            writer.flush();
        }
        catch(IOException e){
        e.printStackTrace();
        }
    }
    public void pong(String line){
    int n=line.indexOf(":");
    String sottostringa=line.substring(n);
    try{
        writer.write("PONG "+sottostringa+"\n");
        writer.flush();
    }
    catch(IOException e){
        e.printStackTrace();
    }
}
    public void privmsg(String messaggio){
        try{
            writer.write("PRIVMSG #"+datiConn.get_canale()+": "+messaggio+"\n");
            writer.flush();
        }
        catch(IOException e){
            e.printStackTrace();
        }
    }
    public String ricevi(){
        try{
            String line=reader.readLine();
            return line;
        }
        catch(IOException e){
        return "esecuzione-catch-di-ComandiIRC.ricevi()";
        }
    }
    public void cambiaNick(String nickname){
        datiConn.set_nick(nickname);
        try{
            writer.write("NICK "+nickname+"\n");
            writer.flush();
        }
        catch(IOException e){
            e.printStackTrace();
        }
    }
    public void inviaComando(String comando){
        try{
            writer.write(comando+"\n");
            writer.flush();
        }
        catch(IOException e){
            e.printStackTrace();
        }
    }
    public boolean connetti(DatiConnessione datiConn,CredenzialiProxy credenziali){
        this.datiConn=datiConn;
        try{
            if(datiConn.get_diretta()){
                socket=new Socket(datiConn.get_server(),datiConn.get_porta());
            }
            else if(datiConn.get_tor()){
            SocketAddress addr=new InetSocketAddress("127.0.0.1",9050);
            Proxy proxy=new Proxy(Proxy.Type.SOCKS,addr);
            socket=new Socket(proxy);
            InetSocketAddress dest=new InetSocketAddress(datiConn.get_server(),datiConn.get_porta());
            socket.connect(dest,60000);
           }
            else{
                SocketAddress addr=new InetSocketAddress(credenziali.get_ipProxy(),credenziali.get_portaProxy());
                Proxy proxy=new Proxy(Proxy.Type.SOCKS,addr);
                socket=new Socket(proxy);
            }
            if(credenziali.get_proxyAuth()){
                final String nomeUtenteProxy=credenziali.get_nomeUtenteProxy();
                final char[] pwdProxy=credenziali.get_passwordProxy();
                Authenticator.setDefault(new Authenticator(){
                    protected PasswordAuthentication getPassword (){
                        return new PasswordAuthentication(nomeUtenteProxy,pwdProxy);
                    }
                });
            }
            InetSocketAddress dest = new InetSocketAddress(datiConn.get_server(),datiConn.get_porta());
            socket.connect(dest,60000);
            output=socket.getOutputStream();
            input=socket.getInputStream();
            writer=new BufferedWriter(new OutputStreamWriter(output));
            reader=new BufferedReader(new InputStreamReader(input));
            writer.write("NICK "+datiConn.get_nick()+"\n");
            writer.write("USER "+datiConn.get_nick()+" "+"hostname "+datiConn.get_server()+":Realname"+"\n");
            writer.flush();
        }
         catch(IOException e){
            e.printStackTrace();
        }
        return false;
    }
    
    
}
