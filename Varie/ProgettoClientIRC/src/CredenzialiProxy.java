/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author 5ai
 */
public class CredenzialiProxy {
    private String nomeUtenteProxy;
    private char[] passwordProxy;
    private String ipProxy;
    private int portaProxy;
    private boolean proxyAuth;
    public CredenzialiProxy(){}
    public String get_ipProxy(){
        return ipProxy;
    }
    public void set_ipProxy(String ipProxy){
        this.ipProxy = ipProxy;
    }
    public int get_portaProxy(){
        return portaProxy;
    }
    public void set_portaProxy(int portaProxy){
        this.portaProxy = portaProxy;
    }
    public boolean get_proxyAuth(){
        return proxyAuth;
    }
    public void set_proxyAuth(boolean proxyAuth){
        this.proxyAuth = proxyAuth;
    }
    public String get_nomeUtenteProxy(){
        return nomeUtenteProxy;
    }
    public void set_nomeUtenteProxy(String nomeUtenteProxy){
        this.nomeUtenteProxy = nomeUtenteProxy;
    }
    public char[] get_passwordProxy(){
        return passwordProxy;
    }
    public void set_passwordProxy(char[] passwordProxy){
        this.passwordProxy = passwordProxy;
    }
}
