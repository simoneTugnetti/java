/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import javax.swing.*;
/**
 *
 * @author AleSimo76
 */
public class ThreadLettura extends Thread{
    ComandiIRC irc;
    JTextArea txt_area_msg_ricevuti;
    JTextArea txt_area_msg_inviati;
    DatiConnessione datiConn;
    public ThreadLettura(String name, ComandiIRC irc, DatiConnessione datiConn, JTextArea txt_area_msg_ricevuti, JTextArea txt_area_msg_inviati){
    super(name);
    this.irc=irc;
    this.datiConn=datiConn;
    this.txt_area_msg_ricevuti=txt_area_msg_ricevuti;
    this.txt_area_msg_inviati=txt_area_msg_inviati;
    }
    public void run(){
        try{
            String line;
            for(;;){
                line=irc.ricevi();
                if(!line.equals("esecuzione-catch-di-ComandiIRC.ricevi()")){
                    txt_area_msg_ricevuti.append(line+"\n");
                    if(line.startsWith("PING")){
                        irc.pong(line);
                        txt_area_msg_inviati.append(datiConn.get_nick()+" > invio un PONG\n");
                    }
                }
                    if(line.equals("esecuzione-catch-di-ComandiIRC.ricevi()")){
                        txt_area_msg_ricevuti.append("> Client disconnesso.\n");
                        break;
                }
            }
        }
        catch(Exception ex){
         System.out.println("Op. non riuscita."
                 + "Sono in run di ThreadLettura");
        }
    }
}
