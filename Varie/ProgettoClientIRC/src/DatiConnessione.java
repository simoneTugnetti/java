/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author 5ai
 */
public class DatiConnessione {
    private boolean diretta;
    private boolean tor;
    private boolean proxy;
    private String server;
    private int porta;
    private String nick;
    private String canale;
    private ClientIRC clientirc;
    public DatiConnessione(){}
    public String get_canale(){
        return canale;
    }
    public void set_canale(String canale){
        this.canale = canale;
    }
    public String get_nick(){
        return nick;
    }
    public void set_nick(String nick){
        this.nick = nick;
    }
    public boolean get_diretta(){
        return diretta;
    }
    public void set_diretta(boolean diretta){
        this.diretta = diretta;
    }
    public String get_server(){
        return server;
    }
    public void set_server(String server){
        this.server = server;
    }
    public int get_porta(){
        return porta;
    }
    public void set_porta(int porta){
        this.porta = porta;
    }
    public boolean get_tor(){
        return tor;
    }
    public void set_tor(boolean tor){
        this.tor = tor;
    }
    
}
