/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.*;
import java.net.*;
/**
 *
 * @author simone.tugnetti
 */
public class AgendaMultiClient {
    public static void main(String[] args) {
        //Dichiarazione variabili
        String nome;
       Socket connessione;
       //Parametri di connessione
       String server="localhost";
       int porta=3333;
       //Stream per gestione input dati
       InputStream in;
       InputStreamReader input;
       BufferedReader sIn;
       BufferedReader tastiera;
       //Gestione flusso in uscita
        OutputStream out;
        PrintWriter sOut;
       try
       {
           connessione=new Socket(server,porta);//Costruttore connessione
           System.out.println("Connessione aperta");
           //Istanzia il flusso in ingresso (lettura)
           in=connessione.getInputStream();
           input=new InputStreamReader(in);
           sIn=new BufferedReader(input);
           //Istanzia flusso in uscita
           out=connessione.getOutputStream();
           sOut=new PrintWriter(out);
           sOut.flush();
           //Legge dato da tastiera
           tastiera=new BufferedReader(new InputStreamReader(System.in));
           do{
           System.out.println("\nInserisci nome da cercare (q=esci): ");
           nome=tastiera.readLine();
           //Invia richiesta al server
           sOut.println(nome);
           sOut.flush();
           if(!nome.equals("q")){
           //In attesa di risposta dal server
           String clock=sIn.readLine();
           System.out.println("Il numero di telefono di "+nome+": "+clock);
           }
           }while(!nome.equals("q"));
           sIn.close();
           connessione.close();
           System.out.println("Connessione chiusa");
       }
       catch(IOException e)
               {
                 System.err.println(e);
               }
    }
}
