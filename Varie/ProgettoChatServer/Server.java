//Simone Tugnetti       5°AI

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.*;
import java.net.*;
/**
 *
 * @author 5AI
 */
public class Server {
    public static void main(String args[]){
        ServerSocket sSocket;
        Socket connessione;
        int porta=3333;
        InputStream in;
        OutputStream out;
        PrintWriter sOut;
        int scelta=0;
        InputStreamReader input;
        BufferedReader info;
        try{
            sSocket=new ServerSocket(porta);
            while(true){
                System.out.println("In attesa di connessione...");
                do{
                connessione=sSocket.accept();
                System.out.println("Connessione stabilita...");
                input=new InputStreamReader(System.in);
                info=new BufferedReader(input);
                in=connessione.getInputStream();
                //creiamo il nostro lettore di buffer
                BufferedReader br= new BufferedReader(new InputStreamReader(in));
                //creiamo una variabile dove salvare di volta in volta le varie righe dello stream
                String line="";
                while((line=br.readLine())!=null){
                System.out.println(line);
                }
                out=connessione.getOutputStream();
                sOut=new PrintWriter(out);
                System.out.println(sOut.toString());
                sOut.print(info);
                System.out.println("Vuoi scrivere qualcos' altro????? (1-SI; 2-NO)");
                scelta=Integer.parseInt(info.readLine());
                }while(scelta==1);
                sOut.close();
                if(!connessione.isClosed()){
                connessione.close();
                System.out.println("Connessione chiusa.");
                }
            }
        
        }
        catch(IOException e){
            System.out.println("Errore!!!");
        }
        
}
}
