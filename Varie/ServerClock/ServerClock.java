/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package maida.server.soket;
import java.net.*;
import java.io.*;
import java.util.Date;
/**
 *
 * @author Vincenzo
 */
public class ServerClock {
    public static void main (String[] args){
        ServerSocket sSocket;
        Socket connessione;
        int porta=3333;
        
        //Gestione flusso input
        OutputStream out;
        PrintWriter sOut;
        try
        {
            sSocket=new ServerSocket(porta);
            while(true)
            {
                System.out.println("In attesa di connessione ...");
                connessione=sSocket.accept();
                System.out.println("Connessione stabilita...");
                out=connessione.getOutputStream();
                sOut=new PrintWriter(out);
                //Invia informazione al client
                Date oggi=new Date();
                String info=oggi.toString();
                sOut.print(info);
                sOut.close();
                connessione.close();
                System.out.println("Connessione chiusa.");
            }
        }
        catch (IOException e)
                {
                   System.err.println(e);
            }
        }
        
    }

