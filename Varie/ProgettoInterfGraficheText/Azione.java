/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import java.awt.*;
import java.awt.event.*;
/**
 *
 * @author 4AI
 */
public class Azione extends Frame{
    Button pulsante1=new Button("Pulsante 1");
    Button pulsante2=new Button("Esci");
    public Azione(){
        setLayout(null);
        this.add(pulsante1);
        this.add(pulsante2);
        pulsante1.setBounds(10,30,60,30);
        pulsante2.setBounds(150,30,60,30);
        pulsante1.addActionListener(new ActionListener());
        pulsante2.addActionListener(new ActionListener());
        this.addWindowListener(new ChiudiWin());

    }
public class AscoActionListener implements ActionListener{
public void actionPerformed(ActionEvent e) {
if (e.getSource()==pulsante2) {
System.out.println("ActionListener: Premuto Esci");
System.exit(9); //un numero qualsiasi si chiama exit code.
}
else{
System.out.println("ActionListener: premuto "+e.getActionCommand());
    }
}
}
public class ChiudiWin implements WindowListener{
    public void windowClosing(WindowEvent e) { System.exit(0); }
    public void windowActivated(WindowEvent e) {}
    public void windowClosed(WindowEvent e) {}
    public void windowDeactivated(WindowEvent e) {}
    public void windowDeiconified(WindowEvent e) {}
    public void windowIconified(WindowEvent e) {}
    public void windowOpened(WindowEvent e) {}
}
}
