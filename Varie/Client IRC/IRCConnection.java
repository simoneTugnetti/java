/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client.irc;

import java.io.*;
import java.net.*;

/**
 *
 * @author 5ai
 */
public class IRCConnection {
   private InputStreamReader isr;
   private BufferedReader bf;
    private String host;
    private int port;
    private Socket socket=null;
    public IRCConnection( String host )
    throws UnknownHostException, IOException
    {
        this( host, 6667 );
       
    }
    public IRCConnection( String host, int port )
    throws UnknownHostException, IOException
    {
        this.host = host;
        this.port = port;
        connect();
        register();
        bf=new BufferedReader(new InputStreamReader(System.in));
        new Lettore(socket).run();
    }
    public PrintStream out;
    public InputStream in;
    private void connect()
    throws UnknownHostException, IOException
    {
        socket = new Socket( host, port );
        out = new PrintStream( socket.getOutputStream() );
        in = socket.getInputStream();
    }
    private void register()
    {
        String nickname = "ErTugna";
        String localhost = "192.168.10.118";
        out.println( "USER" + " " + nickname + " " + localhost + " " + host + " " + nickname );
        out.println( "NICK" + " " + nickname);
    }
    public Channel getChannel( String name )
    {
        return( new Channel( name, out ) );
    }
}
