/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client.irc;

import java.io.*;
import java.net.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 5ai
 */
public class Lettore implements Runnable{
    Socket s;
    BufferedReader reader;
    Lettore(Socket s) throws IOException{
    this.s=s;
    reader=new BufferedReader(new InputStreamReader(s.getInputStream())); 
    }

    @Override
    public void run() {
     while(s.isConnected()){
         try {
             String msg=reader.readLine();
             if(msg!=null){System.out.println(msg);}
         } catch (IOException ex) {
             System.out.println("Non riesco a leggere");
         }
     }   
    }
}
