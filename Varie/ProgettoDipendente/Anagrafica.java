/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.*;
/**
 *
 * @author AleSimo76
 */
public class Anagrafica{
    private int stipendio;
    public int livello;
    private int scelta;
    private int aumento;
    private String nome;
    public Anagrafica(){
        stipendio=1200;
        livello=0;
        aumento=0;
        nome="Gianfranco";
    }
    public void incremento() throws Exception{
        InputStreamReader input=new InputStreamReader(System.in);
        BufferedReader tastiera=new BufferedReader(input);
        System.out.println("Questo dipendente sta facendo bene il suo lavoro??? (1-SI; 2-NO)");
            scelta=Integer.parseInt(tastiera.readLine());
        if(scelta==1){
            System.out.println("Allora gli aumenteremo il suo livello e gli daremo un aumento del 10% sul suo stipendio!!!");
            livello++;
            aumento=(stipendio*10)/100;
            stipendio=stipendio+aumento;
        }
        else{
            System.out.println("Con il tempo sicuramente migliorerà!!!");
        }
    }
    public void visualizza(){
        System.out.println("Il nome del dipendente è: "+nome);
        System.out.println("Il livello del dipendente è: "+livello);
        System.out.println("Il suo stipendio è di: "+stipendio+"€!!");
    }
}
