//Simone Tugnetti     4°AI

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import java.awt.*; //Libreria importata per le interfaccie grafiche
import java.awt.event.*;
/**
 *
 * @author 4AI
 */
public class main {
public static void main(String args[]) throws Exception{
    int i=0;
    final Frame f=new Frame("Robe");//Creo oggetto finestra f con nome Robe
    f.setSize(320,240);//Dimensioni
    f.setLocation(10,10);//Posizione
    WindowAdapter wa=new WindowAdapter(){
        public void windowClosing(WindowEvent we){
            f.dispose();//Rilascia le risorse
        }
    };//Ascoltatore
    f.addWindowListener(wa);//Registrazione ascoltatore
    f.setVisible(true);//Visibilità
    for(i=0;i<10;i++){
        System.out.println(f.getWidth()+","+f.getHeight());
        Thread.sleep(2000);
    }
    System.exit(0);
}
}
