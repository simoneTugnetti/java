//Simone Tugnetti        4°AI

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author AT
 */
public class main {//Creo la classe main
    public static void main(String args[]){//Creo il test per l'avvio del programma
        Runnable a1=new Campana("din",5);//Istanzio la classe campana di tipo Runnable con i valori scelti
        Thread a2=new Thread(a1);//Istanzio la classe Thread con i valori in Campana
        a2.start();//Avvio il thread
        Thread a3=new Thread(new Campana("don",5));//Istanzio in un'altra partizione di RAM la classe Campana con nuovi valori
        a3.start();//Avvio il thread
        new Thread(new Campana("dan",5)).start();//Avvio il thread con i nuovi valori inseriti
    }
}
