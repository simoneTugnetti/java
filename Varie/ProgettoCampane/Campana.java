//Simone Tugnetti      4°AI

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author AT
 */
public class Campana implements Runnable{//Creo la classe Campane con implementazione a Runnable
    String suono;//Creo un attributo suono di tipo String
    int volte;//Creo un attributo volte di tipo int
    public Campana(String suono, int volte){//Creo il costruttore Campana con passaggio di parametri
        this.suono=suono;//Inizializzo gli attributi con il valore scritto in seguito
        this.volte=volte;
    }
    public void run(){//Creo il metodo run per l'avvio del thread
        for(int i=0;i<volte;i++){//Creo un indice che arrivi fino al numero di volte inserite
            System.out.print((i+1)+"-"+suono+"   ");//Stampo a video il valore dell'indice con il suono scritto poi nel main
        }
    }
}
