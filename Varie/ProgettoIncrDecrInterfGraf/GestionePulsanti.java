/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import javax.swing.*;
import java.awt.event.*;
/**
 *
 * @author 4AI
 */
public class GestionePulsanti implements ActionListener{
  private JTextField a;
  private int risultato;
  public GestionePulsanti(JTextField a)
  {
    this.a = a;
    risultato=0;
  }

  public void actionPerformed(ActionEvent e)
  {
    String pulsante = e.getActionCommand();

    if (pulsante.equals("Incrementare"))
    {
      risultato=Integer.parseInt(a.getText())+10;
      a.setText(String.valueOf(risultato));
    }
    if (pulsante.equals("Decrementare"))
    {
      risultato=Integer.parseInt(a.getText())-10;
      a.setText(String.valueOf(risultato));
    }
  }
}
