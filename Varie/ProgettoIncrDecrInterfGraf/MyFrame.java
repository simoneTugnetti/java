
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import javax.swing.*;
import java.awt.*;
/**
 *
 * @author 4AI
 */
public class MyFrame {
    public MyFrame(){}
    public void gestione(){
    JFrame f = new JFrame("Incremento e decremento");
    JPanel p = new JPanel();
    JButton incr = new JButton("Incrementare");
    JButton decr = new JButton("Decrementare");
    JTextField a = new JTextField();
    p.setLayout(new BorderLayout());
    p.add(incr, "North");
    p.add(a, "Center");
    p.add(decr, "South");

    f.addWindowListener(new GestoreFinestra());
    incr.addActionListener(new GestionePulsanti(a));
    decr.addActionListener(new GestionePulsanti(a));

    a.setEditable(true);

    f.getContentPane().add(p);
    f.setSize(450,300);
    f.setVisible(true);
    }
}
