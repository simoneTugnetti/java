//Simone  Tugnetti               4°AI
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.*;
import java.util.*;
/**
 *
 * @author 4AI
 */
public class LetturaFile {
public LetturaFile(){
}
public void leggeToken(){
        FileReader f=null;
        BufferedReader fIN=null;
        citta c;
        String s;
        StringTokenizer st;

    try{
    f=new FileReader("città.txt");
    fIN= new BufferedReader(f);
    }
    catch(IOException q)
    {
        System.out.println("Il file non si apre!!!");
        System.exit(0);
    }
    c=new citta();
    try
    {
        s=fIN.readLine();
        while(s != null)
        {
            st=new StringTokenizer(s, ";");
            c.nomecitta=st.nextToken();
            c.provincia=st.nextToken();
            c.nabit=Integer.parseInt(st.nextToken());

            c.stampa();
            s=fIN.readLine();
        }

    }
    catch(IOException y){
         System.out.println("Non riesco a leggere il file!!!");
        System.exit(0);
    }
    try {
        f.close();
    }
    catch (IOException h){
         System.out.println("Non riesco a chiudere il file");
        System.exit(0);
    }
}
}
