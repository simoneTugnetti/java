/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.*;
/**
 *
 * @author AT
 */
public class Elenco implements Runnable{
    private String[] splits=new String[10];
    private String linea;
    private int i=0;
    private Carta[] carte;
    private int numero_carte;
//costruttore
public Elenco() {
carte=new Carta[1000];
numero_carte=0; 
        }
// ricerca sequenziale di una carta dato il numero
private int cercaCarta(String numero_carta) {
for (int indice=0; indice<numero_carte; indice++)
if (carte[indice].getNumero().equals(numero_carta))
return indice;
return -1; // carta non trovata
}
// costruttore: copia dell’elenco di carte fornito come parametro
public Elenco(Carta[] elenco) {
carte = new Carta[elenco.length];
numero_carte = elenco.length;
for (int indice=0; indice<numero_carte; indice++)
carte[indice] = new Carta(elenco[indice]);
}
//NuovaCarta
public int nuovaCarta(Carta carta) {
synchronized(carte[numero_carte]){
carte[numero_carte]= new Carta(carta);
numero_carte++;
}
return (numero_carte-1);
}
synchronized public int nuovaCarta(String numero, float saldo) {
Carta carta = new Carta(numero, saldo);
carte[numero_carte] = carta;
numero_carte++;
return (numero_carte-1);
}
// restituisce il numero di carte
public int getNumeroCarte() {
return numero_carte;
}
// ricarica di un importo su una carta
public boolean ricarica(String numero_carta, float importo) {
int indice = cercaCarta(numero_carta);
if (indice < 0)
return false;
else
{
synchronized(carte[indice]) {
carte[indice].setSaldo(carte[indice].getSaldo()+importo);
}
return true;
}
}
// pagamento di un importo da una carta
public boolean pagamento(String numero_carta, float importo) {
int indice = cercaCarta(numero_carta);
if (indice < 0)
return false;
else
{
synchronized(carte[indice]) {
carte[indice].setSaldo(carte[indice].getSaldo()-importo);
}
return true;
}
}
// richiesta del saldo di una carta
float saldo(String numero_carta) {
float saldo_carta;
int indice = cercaCarta(numero_carta);
if (indice < 0) return 0; // saldo fittizio per carta inesistente
else {synchronized(carte[indice]) {saldo_carta = carte[indice].getSaldo();}
return saldo_carta;}
}

    /**
     *
     */
    @Override
    public void run(){
        try{
        BufferedReader input=new BufferedReader(new FileReader(new File("transizioni.txt")));
        linea=input.readLine();
        while(linea!=null){
            splits=linea.split(";");
            linea=input.readLine();
        }
    }
        catch(IOException e){
        System.out.println("ERROREEEE!!!!");
        }
        if(splits[0].equals("new")&&Thread.currentThread().getName().equals("nuovo")){
            nuovaCarta(splits[1], Float.valueOf(splits[1]));
            numero_carte++;
        }
        if(splits[0].equals("ricarica")&&Thread.currentThread().getName().equals("prelievo")){
            nuovaCarta(splits[1], Float.valueOf(splits[2]));
            numero_carte++;
        }
        if(splits[0].equals("pagamento")&&Thread.currentThread().getName().equals("saldo")){
            nuovaCarta(splits[1], Float.valueOf(splits[3]));
            numero_carte++;
        }
        if(Thread.currentThread().getName().equals("pagamento")){
        for(i=0;i<this.getNumeroCarte();i++){
           System.out.println(carte[i].getNumero()+": "+carte[i].getSaldo());
        }
    }
}
}