/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author AT
 */
public class Carta {
    String numero;
    float saldo;
// costruttore
public Carta(String numero, float saldo) {
this.numero = numero;
this.saldo = saldo;
}
// costruttore di copia
public Carta(Carta carta) {
    this.numero = carta.numero;
    this.saldo = carta.saldo;
}
// metodi getter
public String getNumero() {
    return numero;
}
public float getSaldo() {
    return saldo;
}
// metodi setter
public void setSaldo(float saldo) {
    this.saldo = saldo;
}
}
