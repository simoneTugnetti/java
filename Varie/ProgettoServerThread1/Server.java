//pag.139 a 140

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.net.*;
import java.io.*;
import java.util.*;
/**
 *
 * @author 5ai
 */
public class Server extends Thread{
    private ServerSocket server;
    public Server(int port) throws IOException{
        server=new ServerSocket(port);
        server.setSoTimeout(1000);
    }
    public Server() throws IOException{
        server=new ServerSocket(13);
        server.setSoTimeout(1000);
    }
    public void run(){
        Socket connection=null;
        while(!Thread.interrupted()){
            try{
                connection=server.accept();
                System.out.println("Data/ora richiesta da: "+connection.getInetAddress().toString()+": "+connection.getPort());
                OutputStreamWriter out=new OutputStreamWriter(connection.getOutputStream(), "ISO-8859-1");
                Date now=new Date();
                out.write(now.toString()+"\r\n");
                out.flush();
                out.close();
                connection.shutdownOutput();
                connection.close();
            }
            catch(SocketTimeoutException exception){}
            catch(IOException exception){}
            finally{
                if(connection!=null){
                    try{
                        connection.shutdownOutput();
                        connection.close();
                    }
                    catch(IOException exception){}
                }
            }
        }
        try{
            server.close();
        }
        catch(IOException exception){}
    }
    public static void main(String args[]){
        
    }
}
