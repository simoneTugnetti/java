/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.*;
import java.net.*;
import java.util.*;
/**
 *
 * @author simone.tugnetti
 */
public class TugnettiClient {
    public static void main(String[] args) {
        //Dichiarazione variabili
        String infor;
       Socket connessione;
       //Parametri di connessione
       String server="localhost";
       int porta=3333;
       //Stream per gestione input dati
       InputStream in;
       InputStreamReader input;
       BufferedReader sIn;
       BufferedReader tastiera;
       //Gestione flusso in uscita
        OutputStream out;
        PrintWriter sOut;
        String roba;
       try
       {
           connessione=new Socket(server,porta);//Costruttore connessione
           System.out.println("Connessione aperta");
           //Istanzia il flusso in ingresso (lettura)
           in=connessione.getInputStream();
           input=new InputStreamReader(in);
           sIn=new BufferedReader(input);
           //Istanzia flusso in uscita
           out=connessione.getOutputStream();
           sOut=new PrintWriter(out);
           sOut.flush();
           //Legge dato da tastiera
           tastiera=new BufferedReader(new InputStreamReader(System.in));
           do{
           System.out.println("\nInserisci cosa vuoi fare (ON,OFF,INFO,q=esci): ");
           infor=tastiera.readLine();
           //Invia richiesta al server
           sOut.println(infor);
           sOut.flush();
           if(!infor.equals("q")){
               if(!infor.equals("ON") && !infor.equals("OFF") && !infor.equals("INFO")){
                   System.out.println("Comando non riconosciuto");
               }
               else{
               if(infor.equals("INFO")){
               String clock=sIn.readLine();
               Random num=new Random();
               int num1=num.nextInt(100)+1;
               System.out.println("Il sistema e' "+clock+" e la temperatura è di "+num1+"° C");
               }
               else{
           //In attesa di risposta dal server
                   String clock=sIn.readLine();
                   roba=clock;
                 System.out.println("Il sistema e' "+clock);
               }
               }
           }
           }while(!infor.equals("q"));
           sIn.close();
           connessione.close();
           System.out.println("Connessione chiusa");
       }
       catch(IOException e)
               {
                 System.err.println(e);
               }
    }
}
