/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.*;
import java.net.*;
/**
 *
 * @author 5ai
 */
public class server {
    public static void main(String[] args){
        ServerSocket sSocket;
        Socket connessione;
        int porta=3333;
        //Gestione flusso in uscita
        OutputStream out;
        PrintWriter sOut;
        //Stream per gestione input dati
       InputStream in;
       InputStreamReader input;
       BufferedReader sIn;
        try
        {
            sSocket=new ServerSocket(porta);
            AgendaTelefonica at=new AgendaTelefonica();
            at.inserimento();
            while(true)
            {
                System.out.println("In attesa di connessione ...");
                connessione=sSocket.accept();
                System.out.println("Connessione stabilita...");
                //Istanzio flusso in ingresso
                in=connessione.getInputStream();
                input=new InputStreamReader(in);
                sIn=new BufferedReader(input);
                String nome=sIn.readLine();
                out=connessione.getOutputStream();
                sOut=new PrintWriter(out);
                //Invia informazione al client
                String info=at.ricerca(nome);
                sOut.print(info);
                sOut.close();
                
                connessione.close();
                System.out.println("Connessione chiusa.");
            }
        }
        catch (Exception e)
                {
                   System.err.println(e);
            }
    }
}
