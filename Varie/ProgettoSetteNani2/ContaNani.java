//Simone Tugnetti      4°AI

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author AT
 */
public class ContaNani extends Thread{//Creo la classe conta nani con estensione a Thread
    public ContaNani(String nome){//Creo il costruttore del programma con passaggio di parametri
        super();//Richiamo gli attributi della classe superiore
        setName(nome);//Creo il metodo setName con attributo "nome"
    }
    public void run(){//Creo il metodo run per l'avvio del programma
        for(int i=0;i<7;i++){//Creo un indice che arrivi fino a 7
            System.out.println((i+1)+" "+getName());//Stampo a video il numero dell'indice e il nome settato nel main
        }
    }
}
