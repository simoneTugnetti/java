/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.java.treclassi;

/**
 *
 * @author 4AI
 */
public class triangolo {

    private double base;
    private double altezza;

    double getBase(){
        return base;
    }

    void setBase(double b){
        base=b;
    }

    double getAltezza(){
        return altezza;
    }

    void setAltezza(double h){
       altezza=h;
    }

    double area_triangolo(){
        return (base*altezza)/2;

    }

    double perimetro_triangolo(){
        return base*3;

    }
}
