/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.java.treclassi;

/**
 *
 * @author 4AI
 */
public class rettangolo {

private double ret_base;
private double ret_altezza;

public double getBase(){
    return ret_base;
    }
public void setBase(double b){
    ret_base=b;
    }
public double getAltezza(){
    return ret_altezza;
    }
public void setAltezza(double h){
    ret_altezza=h;
    }

public double area_rettangolo(){

    return ret_base*ret_altezza;
    }
public double perimetro_rettangolo(){
    return (ret_base+ret_altezza)*2;
    }
}

}
