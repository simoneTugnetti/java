//Simone Tugnetti      4°AI

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author AT
 */
public class main {
    public static void main(String args[]) throws Exception{
        int numero_antenne_totale, numero_antenne_trovate;
        Antenna a1=new Antenna(21.2, 23.5, "a1");
        Antenna a2=new Antenna(22.2, 24.5, "a2");
        Antenna a3=new Antenna(23.2, 25.5, "a3");
        Registro registro=new Registro();
        registro.aggiungiAntenna(a1);
        registro.aggiungiAntenna(a2);
        registro.aggiungiAntenna(a3);
        numero_antenne_totale = registro.getNumeroAntenne();
        System.out.println("Il numero di antenne totali: "+numero_antenne_totale);
        Ricerca ricerca1 = new Ricerca( registro, 0, numero_antenne_totale/2, 43.5, 43.6, 10.3, 10.4);
        Ricerca ricerca2 = new Ricerca( registro, numero_antenne_totale/2, numero_antenne_totale, 50.5, 56.6, 15.3, 19.4);
        Thread thread1 = new Thread(ricerca1);
        Thread thread2 = new Thread(ricerca2);
        thread1.start();
        thread2.start();
        try{
        thread1.join();
        thread2.join();
        }
        catch(InterruptedException e){}
        numero_antenne_trovate = ricerca1.getConteggioAntenne() + ricerca2.getConteggioAntenne();
        System.out.println("Il numero di antenne trovate: "+numero_antenne_trovate);
    }
}
