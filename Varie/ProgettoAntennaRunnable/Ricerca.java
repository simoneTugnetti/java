//Simone Tugnetti      4°AI

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author AT
 */
public class Ricerca implements java.lang.Runnable{
    private Registro registro;
    private int numero_antenna_minimo;
    private int numero_antenna_massimo;
    private double latitudine_minima;
    private double latitudine_massima;
    private double longitudine_minima;
    private double longitudine_massima;
    private int conta_antenne;
public Ricerca( Registro registro, int numero_antenna_minimo,int numero_antenna_massimo, double latitudine_minima, double latitudine_massima, double longitudine_minima,double longitudine_massima) {
        this.registro = registro;
        if ( numero_antenna_minimo < 0 || numero_antenna_minimo > Registro.NMA){
            this.numero_antenna_minimo = 0;
        }
        else{
            this.numero_antenna_minimo = numero_antenna_minimo;
        }
        if ( numero_antenna_massimo < 0 || numero_antenna_massimo > Registro.NMA){
            this.numero_antenna_massimo = Registro.NMA;
        }
        else{
            this.numero_antenna_massimo = numero_antenna_massimo;
            this.latitudine_minima = latitudine_minima;
            this.latitudine_massima = latitudine_massima;
            this.longitudine_minima = longitudine_minima;
            this.longitudine_massima = longitudine_massima;
        }
conta_antenne = 0;
}
public void run() {
        for (int n=numero_antenna_minimo; n<numero_antenna_massimo; n++){
              Antenna antenna = registro.getAntenna(n);
              if ( antenna.getLatitudine() < latitudine_massima && antenna.getLatitudine() > latitudine_minima && antenna.getLongitudine() < longitudine_massima && antenna.getLongitudine() > longitudine_minima)
                    conta_antenne++;
}
}
public int getConteggioAntenne() {
       return conta_antenne;
}
}
