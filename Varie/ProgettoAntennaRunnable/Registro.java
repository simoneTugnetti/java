/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author AT
 */
public class Registro {
    public static final int NMA=1000000;
    private Antenna[] antenne;
    private int nAntenne;
    public Registro(){
        antenne=new Antenna[NMA];
        nAntenne=0;
    }
    public int aggiungiAntenna( double latitudine, double longitudine,String tipo) {
        Antenna antenna = new Antenna( latitudine, longitudine, tipo);
        antenne[nAntenne] = antenna;
        nAntenne++;
        return nAntenne;
}
    public int aggiungiAntenna(Antenna a1) {
        antenne[nAntenne] = a1;
        nAntenne++;
        return nAntenne;
}
    public int getNumeroAntenne() {
        return nAntenne;
}
    public Antenna getAntenna(int numero_antenna) {
        if (numero_antenna >= nAntenne || numero_antenna < 0)
        return null;
        return new Antenna(antenne[numero_antenna]);
}
    public void modificaAntenna( int numero_antenna, String tipo, double latitudine, double longitudine) {
        if (numero_antenna >= nAntenne || numero_antenna < 0)
        return;
        antenne[numero_antenna].setLatitudine(latitudine);
        antenne[numero_antenna].setLongitudine(longitudine);
        antenne[numero_antenna].setTipo(tipo);
}
    public int contaAntenne( double latitudine_minima, double latitudine_massima, double longitudine_minima, double longitudine_massima) {
        int conta_antenne = 0;
        for (int n=0; n<nAntenne; n++)
            if ( antenne[n].getLatitudine() < latitudine_massima && antenne[n].getLatitudine() > latitudine_minima && antenne[n].getLongitudine() < longitudine_massima && antenne[n].getLongitudine() > longitudine_minima)
            conta_antenne++;
            return conta_antenne;
}
}
